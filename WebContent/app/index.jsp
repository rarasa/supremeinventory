<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%
	response.setHeader("Cache-Control","no-cache"); 
	response.setHeader("Pragma","no-cache"); 
	response.setDateHeader ("Expires", -1); 
%>
    
<%@page import="com.supreme.util.JSON"%>
<%@page import="com.supreme.dao.User"%>
<%@page import="com.supreme.dao.Inventory"%>
<%@page import="com.supreme.controller.UserController"%>
<%@page import="com.supreme.controller.MaterialController"%>
<%@page import="com.supreme.controller.SupplierController"%>
<%@page import="com.supreme.controller.InventoryController"%>
<%@page import="com.supreme.controller.LocationController"%>
<%@page import="com.supreme.controller.CustomerController"%>
<%@page import="com.supreme.controller.CuttingController"%>
<%@page import="com.supreme.controller.StatusController"%>
<%@page import="com.supreme.controller.BankController"%>
<%@page import="com.supreme.controller.CurrencyController"%>
<%@page import="com.supreme.controller.ContainerController"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Set"%>

<%@ page import="com.supreme.Config" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="inspinia">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Page title set in pageTitle directive -->
    <title page-title></title>
    
    <link rel="icon" type="image/png" href="../assets/images/icons/favicon-32x32.png"/>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Font awesome -->
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
	
	<!-- Toaster -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Main Inspinia CSS files -->
    
    <link href="css/animate.css" rel="stylesheet">
    <link id="loadBefore" href="css/style.css" rel="stylesheet">
    <!-- <link href="css/style-new.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="js/plugins/ng-tags/ng-tags-input.min.css" />
	<link rel="stylesheet" href="js/plugins/ng-tags/ng-tags-input.bootstrap.min.css" />
    
	<script>
		var me=<%
			Long userId=null;
			User user=null;
		
			if(session.getAttribute("id")!=null){
				if(session.getAttribute("as")==null){
					user=(User)session.getAttribute("user");
					userId=Long.parseLong(session.getAttribute("id").toString());				
					out.write(JSON.toJSONObject(user).toString());				
				}else{
					user=(User)session.getAttribute("as");
					userId=user.getId();
					out.write(JSON.toJSONObject(user).toString());								
				}
			}else{
				response.setStatus(response.SC_MOVED_TEMPORARILY);
				response.setHeader("Location", "../");			
				return;
			}
		%>;
		
		<%-- var allInventory=<%
			try{
				InventoryController inventoryCtrl = new InventoryController();
				List<Inventory> masterInventoryList = inventoryCtrl.getMasterInventoryList();
				out.print(JSON.toJSONArray(masterInventoryList));
			}catch(Exception e){
				out.print("[]");				
			}
		%>; --%>
		
		var allMaterialList=<%
			try{
				MaterialController mCtrl = new MaterialController();
				out.print(mCtrl.getAllMaterialList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allSupplierList=<%
			try{
				SupplierController sCtrl = new SupplierController();
				out.print(sCtrl.getAllSupplierList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allUserList=<%
			try{
				UserController uCtrl = new UserController();
				out.print(uCtrl.getAllUserList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allUserRolesList=<%
			try{
				UserController uCtrl = new UserController();
				out.print(uCtrl.getAllUserRoleList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allLocationsList=<%
			try{
				LocationController lCtrl = new LocationController();
				out.print(lCtrl.getAllLocationsList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allSmallCustomerList=<%
			try{
				CustomerController lCtrl = new CustomerController();
				out.print(lCtrl.getAllCustomerList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allCuttingTypeList=<%
			try{
				CuttingController cCtrl = new CuttingController();
				out.print(cCtrl.getAllCuttingTypeList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var machineCuttingTypeList=<%
			try{
				CuttingController cCtrl = new CuttingController();
				out.print(cCtrl.getMachineCuttingTypeList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var manualCuttingTypeList=<%
			try{
				CuttingController cCtrl = new CuttingController();
				out.print(cCtrl.getManualCuttingTypeList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var allStatusTypeList=<%
			try{
				StatusController statusCtrl = new StatusController();
				out.print(statusCtrl.getAllStatusList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var myBankDetails=<%
			try{
				BankController bCtrl = new BankController();
				out.print(bCtrl.listBankDetailsForCustomerId(1751L).get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var currencyDetails=<%
			try{
				CurrencyController bCtrl = new CurrencyController();
				out.print(bCtrl.getAllCurrencies().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var containerListInLastMonth=<%
			try{
				ContainerController contCtrl = new ContainerController();
				out.print(contCtrl.getNewContainerListInLastMonth().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var containerDetails=<%
			try{
				ContainerController contCtrl = new ContainerController();
				out.print(contCtrl.getNewContainerList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		var pendingContainerDetails=<%
			try{
				ContainerController contCtrl = new ContainerController();
				out.print(contCtrl.getNewContainerList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>;
		<%-- var getAllSmallCustomerList=<%
			try{
				CustomerController lCtrl = new CustomerController();
				out.print(lCtrl.getAllCustomerList().get("data").toString());
			}catch(Exception e){
				out.print("[]");				
			}
		%>; --%>
		
	</script>

</head>

<!-- ControllerAs syntax -->
<!-- Main controller with serveral data used in Inspinia theme on diferent view -->
<body ng-controller="MainCtrl as main" class="{{$state.current.data.specialClass}}" landing-scrollspy id="page-top">


<!-- Main view  -->
<div ui-view></div>

<!-- jQuery and Bootstrap -->
<script src="js/jquery/jquery-2.1.1.min.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>

<!-- MetsiMenu -->
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- SlimScroll -->
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Peace JS -->
<script src="js/plugins/pace/pace.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>


<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Main Angular scripts-->
<script src="js/angular/angular.min.js"></script>
<script src="js/angular/angular-sanitize.js"></script>
<script src="js/plugins/oclazyload/dist/ocLazyLoad.min.js"></script>
<script src="js/angular-translate/angular-translate.min.js"></script>
<script src="js/ui-router/angular-ui-router.min.js"></script>
<script src="js/bootstrap/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="js/plugins/angular-idle/angular-idle.js"></script>
<script src="js/angular/angular-animate.min.js"></script>


<!-- Anglar App Script -->

<script>
	var randomString = "";
</script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.2.0/jspdf.umd.min.js"></script> -->

<script src="js/jsonpack-master/main.js"></script>
<script src="js/plugins/moment/moment.min.js"></script>

<script src="js/plugins/ng-tags/ng-tags-input.min.js"></script>

<script src="js/app.js"></script>
<script src="js/config.js"></script>
<script src="js/translations.js"></script>
<script src="js/directives.js"></script>
<script src="js/controllers.js"></script>
<script src="js/plugins/highcharts/highcharts.js"></script>
<script src="js/controllers/MainCtrl.js"></script>

<!-- Toaster -->
<script src="js/plugins/toastr/toastr.min.js"></script>
<script>

	

</script>

</body>
</html>

