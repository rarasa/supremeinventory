inspinia.lazy.controller("LocationCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.locationBulkList = {};
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			$scope.uploadBulkLocationList = function() {
				console.log("accessed uploadBulkLocationList");
				var form = $("#upload-bulk-location-list-form")[0];
				var url = "../app/bulkLocationListUpload";
				console.log("form: ",form);
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							
							var obj = {
								type: 'success',
								title : "Success",
								body : data.message,
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
							
//							alert(data.message);
						} else {
//							alert(data.message);
							var obj = {
								type: 'error',
								title : "Failed",
								body : data.error,
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
					}
				});
			};
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			
			$scope.editMode = false;
			
			$scope.locationList = allLocationsList;
			console.log("$scope.locationList: ", $scope.locationList);
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.getLocationList = function(){
				var url = "../app/location?action=getAllLocationsList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.locationList = data;
							console.log($scope.locationList);
						});
					});
				});
			};
			
			$scope.clickedOnEditRow = function(location, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
						if($scope.editMode){
							var obj = {
								type: 'error',
								title : "Editing another row",
								body : "Please save all rows before editing another.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
						else{
							
							location.activeBool = (location.active) ? 1 : 0;
							
							var editModeLocationNameString = "<input class='form-control'' type='text' id=name"+index+" style='width:-webkit-fill-available;' name='name' value='"+location.name+"' placeholder='Name' ng-model='location.name'>";
	            			var editModeLocationCode = "<input class='form-control' type='text' id=code"+index+" style='width:-webkit-fill-available;' name='code' value='"+location.code+"' placeholder='Origin' ng-model='location.code'>";
	            			var editModeLocationAddress = "<input class='form-control' type='text' id=address"+index+" style='width:-webkit-fill-available;' name='address' value='"+location.address+"' placeholder='Origin' ng-model='location.address'>";
	            			var editModeLocationPhone = "<input class='form-control' type='text' id=phone"+index+" style='width:-webkit-fill-available;' name='phone' value='"+location.phone+"' placeholder='Origin' ng-model='location.phone'>";
	            			
							var editModeLocationActive = "<select id='active"+index+"' class='form-control' name='active' ng-model='location.activeBool'>"
											            	+"<option value=''>--Select Active--</option>"
											            	+"<option value='true'>Yes</option>"
											            	+"<option value='false'>No</option>"
											            +"</select>";
							

							console.log($("#sizeDiv"+index));
							$("#locationNameDiv"+index).empty();
							$("#locationNameDiv"+index).append(editModeLocationNameString);
							$("#locationCodeDiv"+index).empty();
							$("#locationCodeDiv"+index).append(editModeLocationCode);
							$("#locationContactDiv"+index).empty();
							$("#locationContactDiv"+index).append(editModeLocationPhone);
							$("#locationAddressDiv"+index).empty();
							$("#locationAddressDiv"+index).append(editModeLocationAddress);
							$("#locationActiveDiv"+index).empty();
							$("#locationActiveDiv"+index).append(editModeLocationActive);
							
							
							console.log(location);
							
							location.editMode = true;
							$scope.editMode = true;
							location.activeBool = location.active;
							
							$("#active"+ index).val(''+location.active);
							console.log(location);
						}
					});
				});
			};
			
			$scope.clickedOnSaveRow = function(location, index){
				$timeout(function() {
					$scope.$apply(function() {
						
						var obj = location;
						obj.name = $("#name"+index).val();
						obj.code = $("#code"+ index).val();
						obj.address = $("#address"+ index).val();
						obj.phone = $("#phone"+ index).val();
						obj.active = $("#active"+ index).val();
						
						console.log("location: ", location);
						console.log("Clicked on Save row");
						
						if($scope.editMode && location.editMode){
							$scope.saveEditedLocation(location, index);
							$scope.editMode = false;
							location.editMode = false;
						}
						else{
							
							var obj = {
								type: 'error',
								title : "Invalid Operation",
								body : "Location Is not being edited.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
					});
				});
			};
			
			$scope.saveEditedLocation = function(location, index){
				
				var obj = location;
				
				console.log("Location obj: ", location);
				if(typeof obj.name == "undefined" || obj.name == ""){
					var obj = {
						type: 'danger',
						title : "Location Name not given",
						body : '',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);

					return;
				}
					
				var url = "../app/location?action=editSingleLocation";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'Location saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										
										$scope.getLocationList();
										
										$scope.editMode = false;
									});
								});
							}
							else{
								
								var obj = {
									type: 'error',
									title : data.message,
									body : obj.name+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							
							}
						});
					});
				});
				
			};
			
	
			$scope.uploadSingleLocationList = function(){
				if ($scope.location_form.$valid) {
		            // Submit as normal
					
					var obj = $scope.location;
					obj.lowerPriceRange = 0;
					obj.higherPriceRange = 0;
					
										
					var exists = false;
					for(var i = 0; i < $scope.locationList.length; i++){
						if($scope.locationList[i].name == obj.name.toUpperCase()){
							exists = true;
							break
						}
					}
					
					if(exists){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'Location already exists',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					else{
						var url = "../app/location?action=updateSingleLocation";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									
									if(data.success){
										
										var obj = {
											type: 'success',
											title : 'Location saved.',
											body : $scope.location.name+' saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
										
										 $("#location_form_name").val("");
										 $("#location_form_code").val("");
										 $("#location_form_address").val("");
										 $("#phone").val("");
										
										$scope.getLocationList();
									}
									else{
										var obj = {
											type: 'error',
											title : 'Oops',
											body : data.message,
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
									}
								});
							});
						});
					}
				}
				else{
					$scope.location_form.submitted = true;
				}
				
			};
			
			
			$scope.deleteLocation = function(location, index){
				
				if($scope.editMode){
					
					var obj = {
						type: 'error',
						title : 'Error',
						body : 'Please save row before editing.',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					
					return;
				}
				
				if(!confirm("Do you really want to delete "+ location.name+" ?")){
					return;
				}
				else{
					console.log("call delete func");
					var url = "../app/location?action=deleteLocation";
					
					var obj = {
						name : location.name,
						id: location.id
					};
					
					console.log(url);
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									var obj = {
										type: 'success',
										title : 'Deleted!',
										body : 'Location deleted successfully.',
										showCloseButton: true,
        								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.getLocationList();
								}
								else{
									
									var obj = {
										type: 'error',
										title : "",
										body : data.message,
										showCloseButton: true,
        								timeout: 2000
									};
									
									$scope.toasterNotification(obj);
									return;
								
								}
							});
						});
					});
				}
				
			};
		});
	});
}]);
