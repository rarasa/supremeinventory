inspinia.lazy.controller("ContainerCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			$scope.containerToPrintList = $rootScope.containerListInLastMonth;
			$scope.showAllContainers = 0;
			$scope.showPendingPrintContainers = true;
			
			$scope.changeSelectionCriteria = function(flag){
				console.log(flag);
				if(flag == 0){
					$scope.showAllContainers = 0;
				}
				else{
					$scope.showAllContainers = 1;
				}
			};
			
			$scope.todaysDate = moment().format("DD-MM-YYYY");
			$scope.monthBackDate = moment().subtract(1, 'months').format("DD-MM-YYYY");
			
			$scope.dateLowerRange = moment().subtract(1, 'months').format("DD-MM-YYYY");
			$scope.dateHigherRange = moment().format("DD-MM-YYYY");
			
			$scope.getContainerWiseInventoryListByDateRange = function(dateLowerRange, dateHigherRange){
				var url = "../app/inventory?action=getContainerWiseInventoryList"
								+"&dateLowerRange="+moment(dateLowerRange, "DD-MM-YYYY").format("YYYY-MM-DD")
								+"&dateHigherRange="+moment(dateHigherRange, "DD-MM-YYYY").format("YYYY-MM-DD");
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					
					$timeout(function() {
						$scope.$apply(function() {
							
							$scope.containerToPrintList = data.data;
							console.log($scope.containerToPrintList);
						});
					});
				});
			};
			
			
			
			/*$scope.goToContainerDetails = function(container){
				console.log(container);
				$state.go("listView.containerInventoryDetailsToPrint", {
					container : container,
					id : container.containerId
				});
				
			};*/
			
			
				
			$scope.getContainerWiseInventoryList = function(containerId){

				if(typeof containerId == "undefined" || containerId == "" || containerId == 0){
					var webURLArray = window.location.hash.split("/");
					console.log(webURLArray);
					containerId = webURLArray[webURLArray.length - 1];
					console.log("if: ", containerId);
				}
				else{
					console.log("else: ", containerId);
						$state.go("listView.containerInventoryDetailsToPrint", {
							id : containerId
						});
					var url = "../app/inventory?action=getSingleContainerInventoryList&containerId="+containerId;
					console.log(url);
					$http.get(url).success(function(data, status, headers, config) {
						console.log("getSingleContainerInventoryList fetched data: ",data);
						
						$timeout(function() {
							$scope.$apply(function() {
								
								$scope.containerWiseMtSq3cm = 0;
								$scope.containerWiseMtSq2cm = 0;
								$scope.containerInventoryList = data.data;
								for(var i=0;i < $scope.containerInventoryList.inventoryList.length;i++){
									if($scope.containerInventoryList.inventoryList[i].thickness == 2){
										$scope.containerWiseMtSq2cm += parseFloat($scope.containerInventoryList.inventoryList[i].actualLength) * parseFloat($scope.containerInventoryList.inventoryList[i].actualHeight) / 10000;
									}
									else{
										$scope.containerWiseMtSq3cm += parseFloat($scope.containerInventoryList.inventoryList[i].actualLength) * parseFloat($scope.containerInventoryList.inventoryList[i].actualHeight) / 10000;
									}
								}
								console.log($scope.containerInventoryList);
							});
						});
					});
				}
			};
			$scope.getContainerWiseInventoryList();
			
			
			if($state.current.name == "listView.containerInventoryDetailsToPrint"){
				var webURLArray = window.location.hash.split("/");
				console.log(webURLArray);
				var containerId = webURLArray[webURLArray.length - 1];
				console.log("state name check function containerId: ", containerId);
				
				$scope.getContainerWiseInventoryList(containerId);
				
				$scope.dtOptions = DTOptionsBuilder.newOptions()
		        .withDOM('<"html5buttons"B>lTfgitp')
		        .withButtons([
		            {extend: 'copy'},
		            {extend: 'csv'},
		            {extend: 'excel', title: 'ContainerDetails-'+containerId},
		            {extend: 'pdf', title: 'ContainerDetails-'+containerId},
	
		            {extend: 'print',
		                customize: function (win){
		                    $(win.document.body).addClass('white-bg');
		                    $(win.document.body).css('font-size', '10px');
	
		                    $(win.document.body).find('table')
		                        .addClass('compact')
		                        .css('font-size', 'inherit');
		                }
		            }
		        ]);
			}
			
		});
	});
}]);

