inspinia.lazy.controller("CuttingCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify) {

	$timeout(function() {
		
			$scope.count = 0;
			$scope.totalSqMeters = 0;
		
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.cuttingOrderList = {
					customerName : "",
			};
			
			
			$scope.cuttingOrderList.cuttingOrder = [];
			
			$scope.cuttingOrderObject = {
					slabCode: '',
					name: '',
					actualLength: 0,
					actualHeight: 0,
					thickness: 0,
					slabId: 0,
					id: 0,
					actualSqMetre: 0,
					type: ""
			};

			$scope.cuttingOrderList.cuttingOrder.push($scope.cuttingOrderObject);
			
			$scope.addRow = function(){
				console.log("addRowFlag entered");
				$scope.cuttingOrderObject = {
						slabCode: '',
						name: '',
						actualLength: 0,
						actualHeight: 0,
						thickness: 0,
						slabId: 0,
						id: 0,
						actualSqMetre: 0,
						type: ""
				};

				var idString = "";
				var addRowFlag = true;
				for(var i=0; i < $scope.cuttingOrderList.cuttingOrder.length; i++){
					if(typeof $scope.cuttingOrderList.cuttingOrder[i].slabCode == "undefined" || $scope.cuttingOrderList.cuttingOrder[i].slabCode == ""){
						//one empty row exists so do nothing
						addRowFlag = false;
						idString = "slabCodeText"+indexOf($scope.cuttingOrderList.cuttingOrder[i]);
						break;
					}
					else{
						addRowFlag = true;
						idString = "slabCodeText"+$scope.cuttingOrderList.cuttingOrder.length;
					}
				}
				
				console.log("idString: "+ idString)
				if(addRowFlag){
					console.log("addrowFlag: ", addRowFlag);
					$scope.cuttingOrderList.cuttingOrder.push($scope.cuttingOrderObject);
					
					$timeout(function() {
						$scope.$apply(function() {
							console.log("idString 2: "+ idString)
					      // have to do this in a $timemout because
					      // we want it to happen after the view is updated
					      // with the newly added input
					      angular
					        .element(document.querySelectorAll('#'+idString))
					        .eq(-1)[0]
					        .focus()
					      ;
						});
				    }, 0);
				}
			};
			
			$scope.removeAt = function(index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log($scope.cuttingOrderList.cuttingOrder);
						console.log("$scope.cuttingOrderList.cuttingOrder.length: ", $scope.cuttingOrderList.cuttingOrder.length);
						if($scope.cuttingOrderList.cuttingOrder.length <= 0 
							|| (typeof $scope.cuttingOrderList.cuttingOrder[index].slabCode == "undefined" || $scope.cuttingOrderList.cuttingOrder[index].slabCode == "")){
							errorObj.message = 'No Purchase to remove.';
							notify(errorObj);
							return;
						}
						
						$scope.count--;
						$scope.totalSqMeters -= $scope.cuttingOrderList.cuttingOrder[index].actualSqMetre;
						$scope.cuttingOrderList.cuttingOrder.splice(index, 1);
						console.log($scope.cuttingOrderList.cuttingOrder);
					});
				});
			};
			
			$scope.viewSlabDetails = function(id, index, tag){
				htmltag = tag;
				console.log("this: ", tag);
				console.log(id);
				console.log("INDEX: ", index);
				
				var error = false;
				var duplicateMessage = "";
				for(var i = 0; i < $scope.cuttingOrderList.cuttingOrder.length; i++){
					if(i != index && $scope.cuttingOrderList.cuttingOrder[i].slabCode == id){
						duplicateMessage = "You have already scanned " + id;
						error = true;
						break;
					}
				}
				
				if(error){
					errorObj.message = duplicateMessage;
					notify(errorObj);
					return;
				}
				
				var url = "../app/inventory?action=readBarcode&barcodeString="+id;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);
								$scope.cuttingOrderList.cuttingOrder[index].slabCode = data.data.barcodeString;
								$scope.cuttingOrderList.cuttingOrder[index].name = data.data.material.name;
								$scope.cuttingOrderList.cuttingOrder[index].actualLength = data.data.actualLength;
								$scope.cuttingOrderList.cuttingOrder[index].actualHeight = data.data.actualHeight;
								$scope.cuttingOrderList.cuttingOrder[index].thickness = data.data.thickness;
								$scope.cuttingOrderList.cuttingOrder[index].slabId = data.data.slabNoSupreme;
								$scope.cuttingOrderList.cuttingOrder[index].id = data.data.id;
								$scope.cuttingOrderList.cuttingOrder[index].actualSqMetre = data.data.actualSqMetre;
								$scope.cuttingOrderList.cuttingOrder[index].type = data.data.type;
								
								if(typeof $scope.cuttingOrderList.cuttingOrder[index].type != "undefined"
										&& $scope.cuttingOrderList.cuttingOrder[index].type == "TILE"){
									
									$scope.cuttingOrderList.cuttingOrder[index].tileCount = data.data.tileCount;
									$scope.cuttingOrderList.cuttingOrder[index].originalTileCount = data.data.tileCount;
									$scope.count += $scope.cuttingOrderList.cuttingOrder[index].tileCount;
									$scope.totalSqMeters += data.data.actualSqMetre;
								}
								else{
									$scope.count += 1;
									$scope.totalSqMeters += data.data.actualSqMetre;
								}
								/*$scope.count += 1;
								$scope.totalSqMeters += data.data.actualSqMetre;*/
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								$scope.addRow();
							}
							else{
								errorObj.message = data.message;
								notify(errorObj);
							}
							
							
						});
					});
				});
			};
			
			$scope.validate = function(){
				$scope.error = false;
				if(typeof $scope.cuttingOrderList.customerName == "undefined" || $scope.cuttingOrderList.customerName == ""){
					errorObj.message = 'Add Customer Name';
					notify(errorObj);
					$scope.error = true;
					
					console.log("CUSTOMERNAME NOT ADDED");
					return;
				}
				if(!$scope.error){
					for(var i = 0; i < ($scope.cuttingOrderList.cuttingOrder.length); i++){
						if(typeof $scope.cuttingOrderList.cuttingOrder[i].slabCode == "undefined" || $scope.cuttingOrderList.cuttingOrder[i].slabCode == ""){
							errorObj.message = 'Slab Code does not exist.';
							notify(errorObj);
							$scope.error = true;
							break;
						}
						else if(typeof $scope.cuttingOrderList.cuttingOrder[i].id == "undefined" || $scope.cuttingOrderList.cuttingOrder[i].id == ""){
							errorObj.message = 'Stock Id does not exist.';
							notify(errorObj);
							$scope.error = true;
							break;
						}
						
						if((typeof $scope.cuttingOrderList.cuttingOrder[i].tile != "undefined" && typeof $scope.cuttingOrderList.cuttingOrder[i].tile)
							&& ($scope.cuttingOrderList.cuttingOrder[i].originalTileCount > 0)){
							
							if($scope.cuttingOrderList.cuttingOrder[i].tileCount > $scope.cuttingOrderList.cuttingOrder[i].originalTileCount){
								errorObj.message = 'Tile Count exceeds the limit of '+$scope.cuttingOrderList.cuttingOrder[i].originalTileCount;
								notify(errorObj);
								$scope.error = true;
								break;
							}
						}
						else if((typeof $scope.cuttingOrderList.cuttingOrder[i].tile != "undefined" && typeof $scope.cuttingOrderList.cuttingOrder[i].tile)
							&& ($scope.cuttingOrderList.cuttingOrder[i].originalTileCount <= 0)){
							
								errorObj.message = 'No tiles exist in '+$scope.cuttingOrderList.cuttingOrder[i].barcodeString;
								notify(errorObj);
								$scope.error = true;
								break;
						}
								
						
					}
				}
				if($scope.error){
					return;
				}
			};
			
			function checkForDuplicates(array) {
			  return new Set(array).size !== array.length;
			}
			
			
			$scope.changeTileCount = function(cuttingObj){
				if(cuttingObj.tileCount > cuttingObj.originalTileCount){
					errorObj.message = 'Count more than limit of '+ cuttingObj.originalTileCount;
					notify(errorObj);
					$scope.count = 0;
				}
				else if(cuttingObj.tileCount < 1){
					errorObj.message = 'Minimum 1 tile needs to be there in '+cuttingObj.barcodeString;
					notify(errorObj);
					$scope.count = 0;
				}
				
				else{
					console.log(cuttingObj);
					$scope.count = 0;
					console.log("$scope.cuttingOrderList.cuttingOrder.length: ", $scope.cuttingOrderList.cuttingOrder.length)
					console.log("$scope.cuttingOrderList.cuttingOrder: ", $scope.cuttingOrderList.cuttingOrder)
					for(var i = 0; i < $scope.cuttingOrderList.cuttingOrder.length; i++){
						console.log("$scope.count: ", $scope.count);
						console.log("$scope.count: ----------------------------");
						if(typeof $scope.cuttingOrderList.cuttingOrder[i].slabCode != "undefined" 
								&& $scope.cuttingOrderList.cuttingOrder[i].slabCode != ""
								&& typeof $scope.cuttingOrderList.cuttingOrder[i].type != "undefined"
								&& $scope.cuttingOrderList.cuttingOrder[i].type != ""
								&& $scope.cuttingOrderList.cuttingOrder[i].type == "TILE"){
							console.log("entered if");
							$scope.count += parseInt($scope.cuttingOrderList.cuttingOrder[i].tileCount);
						}
						else if(typeof $scope.cuttingOrderList.cuttingOrder[i].slabCode != "undefined" 
								&& $scope.cuttingOrderList.cuttingOrder[i].slabCode != ""
								&& typeof $scope.cuttingOrderList.cuttingOrder[i].type != "undefined"
								&& $scope.cuttingOrderList.cuttingOrder[i].type != ""
								&& $scope.cuttingOrderList.cuttingOrder[i].type != "TILE"){
							console.log("entered ielse");
							$scope.count += parseInt(1);
						}
						else{
						}
					}
				}
			};
			
			$scope.uploadCuttingOrder = function(){
				
				if(typeof $scope.cuttingOrderList.cuttingOrder[$scope.cuttingOrderList.cuttingOrder.length - 1].id == "undefined" 
					|| $scope.cuttingOrderList.cuttingOrder[$scope.cuttingOrderList.cuttingOrder.length - 1].id == ""
					|| $scope.cuttingOrderList.cuttingOrder[$scope.cuttingOrderList.cuttingOrder.length - 1].id == 0){
					
					$scope.cuttingOrderList.cuttingOrder.splice($scope.cuttingOrderList.cuttingOrder.length - 1, 1);
				}
				
				$scope.validate();
				
				if($scope.error){
					//alert("Error is still there");
					return;
				}
				
				console.log("Passed validation");
				
				console.log("$scope.cuttingOrderList: ", $scope.cuttingOrderList);
				
				var obj = {};
				obj.customerName = $scope.cuttingOrderList.customerName;
				obj.cuttingOrder = [];
				for(var i = 0; i < $scope.cuttingOrderList.cuttingOrder.length; i++){
					/*obj.cuttingOrder.push($scope.cuttingOrderList.cuttingOrder[i].id);*/
					var cuttingOrder = {};
					cuttingOrder.id = $scope.cuttingOrderList.cuttingOrder[i].id;
					cuttingOrder.type = $scope.cuttingOrderList.cuttingOrder[i].type;
					obj.cuttingOrder.push(cuttingOrder);
					
					if(typeof $scope.cuttingOrderList.cuttingOrder[i].type != "undefined"
						&&$scope.cuttingOrderList.cuttingOrder[i].type == "TILE"){
						cuttingOrder.tileCount = $scope.cuttingOrderList.cuttingOrder[i].tileCount;
					}
					
				}
				
				if(checkForDuplicates(obj.cuttingOrder)){
					console.log("Duplicate value in Cutting Order List");
					errorObj.message = 'Remove Duplicate Values from Cutting Order List';
					notify(errorObj);
					return;
				}
				
				
				obj.count = $scope.count;
				obj.totalMtSquare = $scope.totalSqMeters;
				console.log("OBJ: ", obj);
				
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
				$http.defaults.transformRequest = [ function(data) {
					return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
				}];
				var url = "../app/order?action=addCuttingOrder";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);

							if(data.success){
								successObj.message = " Cutting Order placed successfully";
								notify(successObj);
								
								$scope.count = 0;
								$scope.totalSqMeters = 0;
								
								$state.go("dashboards.start");
								
							}
							else{
								errorObj.message = "Cutting Order could not be placed";
								notify(errorObj);
							}
						});
					});
				});
			};
		
	});
}]);

