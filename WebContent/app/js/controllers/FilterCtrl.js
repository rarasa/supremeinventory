inspinia.lazy.controller("FilterCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);

			$scope.ok = function () {
		        $modalInstance.close();
		    };
			
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };

			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};

			$scope.editMode = false;
			
			$scope.filter = {};
			
			/*$scope.filter.sold = 1;*/
			
			$scope.totalSqMetersInCurrentList = 0;

			/*console.log($rootScope.masterInventoryList);*/
	        
			$scope.masterInventoryListLocal = [];

			/*for(var i = 0; i < $rootScope.masterInventoryList.length; i++){
				if($rootScope.masterInventoryList[i].sold){
					$scope.masterInventoryListLocal.push($rootScope.masterInventoryList[i]);
					$scope.totalSqMetersInCurrentList += ($rootScope.masterInventoryList[i].actualLength * $rootScope.masterInventoryList[i].actualHeight) / 10000;
				}
			}*/
			
			
			$scope.fetchStock = function(){
				$scope.masterInventoryListLocal = [];
				$scope.totalSqMetersInCurrentList = 0;
						
				var fileName = "";
				if(typeof $scope.filter.materialId != "undefined" && $scope.filter.materialId != ""){
					fileName += "-"+$scope.filter.materialId;
				}
				
				if(typeof $scope.filter.supplierId != "undefined" && $scope.filter.supplierId != ""){
					fileName += "-"+$scope.filter.supplierId;
				}
				
				if(typeof $scope.filter.type != "undefined" && $scope.filter.type != ""){
					fileName += "-"+$scope.filter.type;
				}
				
				if(typeof $scope.filter.thickness != "undefined" && $scope.filter.thickness != ""){
					fileName += "-"+$scope.filter.thickness;
				}
				
				if(typeof $scope.filter.sold != "undefined" && $scope.filter.sold != ""){
					fileName += "-"+$scope.filter.sold;
				}
				
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
				$http.defaults.transformRequest = [ function(data) {
					return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
				}];

				var url = "../app/inventory?action=searchStock";
				console.log(url);
				$http.post(url, $scope.filter, {}).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							$scope.masterInventoryListLocal = data.data;
							for(var i = 0; i < $scope.masterInventoryListLocal.length; i++){
								$scope.totalSqMetersInCurrentList += ($scope.masterInventoryListLocal[i].actualLength * $scope.masterInventoryListLocal[i].actualHeight) / 10000;
							}
							console.log("$scope.totalSqMetersInCurrentList", $scope.totalSqMetersInCurrentList);
						});
					});
				});
				
				console.log("$scope.masterInventoryListLocal: ", $scope.masterInventoryListLocal);
			};
			
			$scope.clickedOnEditRow = function(stock, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
						var editModeString = "<div class='col-sm-3'>"
	            			+"<input class='form-control' type='text' id=length"+index+" style='width:-webkit-fill-available; height:25px !important' name='status' value='"+stock.actualLength+"' placeholder='Length' ng-model='stock.actualLength'>"
	            		+"</div>"
	            		+"<div class='col-sm-1'>"
	            			+"<label style='padding-top:5px'> X </label>"
	            		+"</div>"
	            		+"<div class='col-sm-3'>"
	            			+"<input class='form-control' type='text' id=height"+index+" style='width:-webkit-fill-available; height:25px !important' name='status' value='"+stock.actualHeight+"' placeholder='Height' ng-model='stock.actualHeight'>"
	            		+"</div>"
	            		+"<div class='col-sm-1'>"
	            			+"<label style='padding-top:5px'> X </label>"
	            		+"</div>"
	            		+"<div class='col-sm-3'>"
	            			+"<input class='form-control' type='text' id=thickness"+index+" style='width:-webkit-fill-available; height:25px !important' name='status' value='"+stock.thickness+"' placeholder='Thickness' ng-model='stock.thickness'>"
	            		+"</div>"
	            		+"<div class='col-sm-1'>"
	            			+"<label style='padding-top:5px'> cm </label>"
	            		"</div>";
						console.log($("#sizeDiv"+index));
						$("#sizeDiv"+index).empty();
						$("#sizeDiv"+index).append(editModeString);
						console.log(stock);
						$scope.editMode = true;
					});
				});
			}
			
			
			
			$scope.clickedOnReStock = function(stock, index){
				$scope.open1 = function () {
			        var modalInstance = $modal.open({
			            templateUrl: 'views/restock-modal.html',
			            controller: FilterCtrl
			        });
			    };
			};
			
			
			$scope.updateStockRow = function(stock, index){
				/*$("#length"+index).prop('readonly', true);
				$("#height"+index).prop('readonly', true);
				$("#thickness"+index).prop('readonly', true);*/
				
				var obj = {};
				/*obj.stockId = stock.id;
				obj.actualLength = stock.actualLength;
				obj.actualHeight = stock.actualHeight;
				obj.actualThickness = stock.thickness;*/
				
				obj.stockId = stock.id;
				obj.actualLength = $("#length"+ index).val();
				obj.actualHeight = $("#height"+ index).val();
				obj.actualThickness = $("#thickness"+ index).val();
				
				var url = "../app/inventory?action=updateStockRow";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							if(data.success){
								successObj.message = "Stock Updated";
								notify(successObj);
								console.log(data);
								$scope.masterInventoryListLocal[index] = data.data;
								
								var afterSaveTableRow = "{{ stock.actualLength }} x {{ stock.actualHeight }} x {{stock.thickness}} cm"
								$("#sizeDiv"+index).empty();
								$("#sizeDiv"+index).append(afterSaveTableRow);
								
							}
							else{
								errorObj.message = "Stock could not be updated.";
								notify(errorObj);
							}
							
						});
					});
				});
			};
			
		});
	});
}]);

