inspinia.lazy.controller("UserCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.togglePasswordVisibility = function(elementId) {
				
				var x = document.getElementById(elementId);
				if(x.type === "password"){
				    x.type = "text";
				}
				else{
					x.type = "password";
				}
			}
			
			$rootScope.editUser = false;
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'Download'},
	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);

			for(var i = 0; i < allUserList.length; i++){
				if(allUserList[i].active){
					allUserList[i].active = 1;
				}
				else{
					allUserList[i].active = 0;
				}
			}

			$scope.userList = allUserList;
			$scope.userRolesList = allUserRolesList;
			
			$scope.newUser = {
				gender : 0,
				name : "",
				email : "",
				addressLine1 : "",
				addressLine2 : "",
				city : "",
				emiratesId : "",
				mobileNumber : "",
				password : "",
				role : "",
			};
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.userObj = {
				name : "",
				originCountry : "",
				lowerPriceRange: 0,
				higherPriceRange : 0
			};

			$scope.getUserList = function(){
				var url = "../app/user?action=getAllUserList";
				console.log(url);
				$http.get(url).success(function(data) {
					console.log("fetched data: ",data);
					
					$timeout(function() {
						$scope.$apply(function() {
							
							for(var i = 0; i < data.data.length; i++){
								if(data.data[i].active){
									data.data[i].active = 1;
								}
								else{
									data.data[i].active = 0;
								}
							}
							
							$scope.userList = data.data;
							console.log($scope.userList);
							
							
							
						});
					});
				});
			};
			
			
			$scope.uploadSingleUserList = function(){
				$timeout(function() {
							$scope.$apply(function() {
				if ($scope.user_form.$valid) {
		            // Submit as normal
					console.log("$scope.user_form: ", $scope.user_form);
					
					var obj = $scope.newUser;
					obj.editUser = false;
					
					console.log(obj);
					
					var exists = false;
					for(var i = 0; i < $scope.userList.length; i++){
						if($scope.userList[i].email == obj.email){
							exists = true;
							break
						}
					}
					
					
					if(exists){
						errorObj.message = 'User Email already exists';
						notify(errorObj);
						return;
					}
					else{
						
						console.log(obj);
						
						var url = "../app/user?action=addOrEditNewUser";
						console.log(url);
						$http.post(url, obj, {}).success(function(data) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									if(data.success){
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'User saved successfully.',
											showCloseButton: true,
            								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										
										$scope.getUserList();
										
										window.location.reload();
									}
									else{
										
										var obj = {
											type: 'error',
											title : data.message,
											body : $scope.userObj.userName+ 'could not be saved',
											showCloseButton: true,
            								timeout: 2000
										};
										
										$scope.toasterNotification(obj);

										$scope.getUserList();								
									}
								});
							});
						});
					}
				}
				else{
					$scope.user_form.submitted = true;
					console.log("$scope.user_form: ", $scope.user_form);
					console.log("not submitted correctly");
				}
					});
				});
			};
			
			
			$scope.editSingleUserFromModal = function(user){
				$timeout(function() {
							$scope.$apply(function() {
				if ($scope.edit_user_form.$valid) {
		            // Submit as normal
					console.log("$scope.edit_user_form: ", $scope.edit_user_form);
					
					var obj = user;
					obj.editUser = true;
					
					console.log(obj);
					
					var exists = false;
					if(exists){
						errorObj.message = 'User Email already exists';
						notify(errorObj);
						return;
					}
					else{
						
						console.log(obj);
						
						var url = "../app/user?action=addOrEditNewUser";
						console.log(url);
						$http.post(url, obj, {}).success(function(data) {
							$timeout(function() {
								$scope.$apply(function() {
									console.log("fetched data: ",data);
									console.log(data);
									if(data.success){
										$rootScope.editUser = false;
										user.editUser = false;
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'User saved successfully.',
											showCloseButton: true,
            								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
										
										console.log("$rootScope.editUser: "+ $rootScope.editUser);
										$scope.getUserList();
										$("#user-edit-modal").modal("hide");
										
										/*window.location.reload();*/
									}
									else{
										
										var obj = {
											type: 'error',
											title : data.error.toUpperCase(),
											body : user.name+ ' could not be saved',
											showCloseButton: true,
            								timeout: 2000
										};
										
										$scope.toasterNotification(obj);

										$scope.getUserList();								
									}
								});
							});
						});
					}
				}
				else{
					$scope.edit_user_form.submitted = true;
					console.log("$scope.edit_user_form: ", $scope.edit_user_form);
					console.log("not submitted correctly");
				}
					});
				});
			};
			
			
			$scope.deleteUser = function(newUser, index){
				
				console.log("new User: ", newUser);
				
				if(!confirm("Do you really want to delete "+ newUser.name+" ?")){
					return;
				}
				else{
					console.log("call delete func");
					var url = "../app/user?action=deleteUser";
					
					var obj = {
						name : newUser.name,
						id: newUser.id
					};
					
					console.log(url);
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									var obj = {
										type: 'success',
										title : 'Deleted!',
										body : 'User deleted successfully.',
										showCloseButton: true,
        								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.getUserList();
								}
								else{
									
									var obj = {
										type: 'error',
										title : "",
										body : data.message,
										showCloseButton: true,
        								timeout: 2000
									};
									
									$scope.toasterNotification(obj);
									return;
								
								}
							});
						});
					});
				}
				
			};
			
			$scope.clickedOnEditRow = function(user, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
							
							var userObj = user;
							
							if(userObj.active){
								userObj.active = 1;
							}
							else{
								userObj.active = 0;
							}
							
							
							$rootScope.editUserObject = userObj;
							console.log("$rootScope.editUserObject: ",$rootScope.editUserObject)
							user.editUser = true;
							$scope.editUser = true;
							$("#user-edit-modal").modal("show");
							
							/*var editModeUserNameString = "<input class='form-control' type='text' id=userName"+index+" style='height:25px !important' name='userName"+index+"' value='"+user.name+"' placeholder='Name' ng-model='user.name'>";
							var editModeUserPhoneString = "<input class='form-control' type='text' id=userPhone"+index+" style='height:25px !important' name='userPhone"+index+"' value='"+user.phone+"' placeholder='Phone' ng-model='user.phone'>";
							var editModeUserEmiratesIdString = "<input class='form-control' type='text' id=userEmiratesId"+index+" style='height:25px !important' name='userEmiratesId"+index+"' data-mask='999-9999-9999999-9' value='"+user.emiratesId+"' placeholder='Name' ng-model='user.emiratesId'>";
							var editModeUserEmailString = "<input class='form-control' type='text' id=userEmail"+index+" style='height:25px !important' name='userEmail"+index+"' value='"+user.email+"' placeholder='Email' ng-model='user.email'>";
							var editModeUserPasswordString = "<input class='form-control' type='text' id=userPassword"+index+" style='height:25px !important' name='userPassword"+index+"' value='"+user.password+"' placeholder='Password' ng-model='user.password'>";

							var editModeUserAddressLine1String = "<input class='form-control' type='text' id=userAddressLine1"+index+" style='height:25px !important' name='userAddressLine1"+index+"' value='"+user.addressLine1+"' placeholder='Address 2' ng-model='user.addressLine1'>";
							var editModeUserAddressLine2String = "<input class='form-control' type='text' id=userAddressLine2"+index+" style='height:25px !important' name='userAddressLine2"+index+"' value='"+user.addressLine2+"' placeholder='Address 1' ng-model='user.addressLine2'>";
							var editModeUserCityString = "<input class='form-control' type='text' id=userCity"+index+" style='height:25px !important' name='userCity"+index+"' value='"+user.city+"' placeholder='City' ng-model='user.city'>";

							var editModeUserRoleString = "<select class='form-control' id=userRole"+index+" name=userRole"+index+" placeholder='Role' ng-model='user.role' required style='height:34px' autocomplete='off'>"
											            	+"<option value=''>--Select Role--</option>"
											            	+"<option ng-repeat='role in userRolesList' value='{{role.role}}'>{{role.roleString}}</option>"
											            +"</select>";
							
							var editModeUserGenderString = "<div class='col-sm-6 col-xs-6 no-padding'>"
						                            			+"<label> <input iCheck type='radio' name='gender' value='0' ng-model='user.gender'> M</label>"
						                                	+"</div>"
						                                	+"<div class='col-sm-6 col-xs-6 no-padding'>"
					                                			+"<label> <input iCheck type='radio' name='gender' value='1' ng-model='user.gender'> F</label>"
					                               			+"</div>";

							console.log($("#sizeDiv"+index));
							
							$("#userNameDiv"+index).empty();
							$("#userNameDiv"+index).append(editModeUserNameString);
							$("#userPhoneDiv"+index).empty();
							$("#userPhoneDiv"+index).append(editModeUserPhoneString);
							$("#userEmiratesIdDiv"+index).empty();
							$("#userEmiratesIdDiv"+index).append(editModeUserEmiratesIdString);
							$("#userEmailDiv"+index).empty();
							$("#userEmailDiv"+index).append(editModeUserEmailString);
							$("#userPasswordDiv"+index).empty();
							$("#userPasswordDiv"+index).append(editModeUserPasswordString);
							$("#userAddressLine1Div"+index).empty();
							$("#userAddressLine1Div"+index).append(editModeUserAddressLine1String);
							$("#userAddressLine2Div"+index).empty();
							$("#userAddressLine2Div"+index).append(editModeUserAddressLine2String);
							$("#userCityDiv"+index).empty();
							$("#userCityDiv"+index).append(editModeUserCityString);
							$("#userRoleDiv"+index).empty();
							$("#userRoleDiv"+index).append(editModeUserRoleString);
							$("#userGenderDiv"+index).empty();
							$("#userGenderDiv"+index).append(editModeUserGenderString);*/
							
							
					});
				});
			};
			
			$scope.saveEditedUser = function(user, index){
				
				var obj = user;
				
					if(typeof obj.userName == "undefined" || obj.userName == ""){
						var obj = {
							type: 'error',
							title : "User Name not given",
							body : '',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);

						return;
					}
					
				var url = "../app/user?action=editSingleUser";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								var obj = {
									type: 'success',
									title : 'Saved!',
									body : 'User saved successfully.',
									showCloseButton: true,
    								timeout: 2000
									
								};
								
								$scope.toasterNotification(obj);
								
								$scope.getUserList();
							}
							else{
								
								var obj = {
									type: 'error',
									title : data.message,
									body : $scope.userObj.userName+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							
							}
						});
					});
				});
				
			};
			
			/*$scope.editUser = function(user, index){
				$timeout(function() {
		$scope.$apply(function() {
				$scope.editUser = supplier;
				var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/supplier-addition-modal.html',
		            windowClass: "animated fadeIn"
		        });
		        });
		        });
				
			};*/
			
			
			
			
			/*$scope.userObjForm = function() {
		        if ($scope.user_form.$valid) {
		            // Submit as normal
		        } else {
		            $scope.user_form.submitted = true;
		        }
		    }*/
		
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			$scope.uploadBulkUserList = function() {
				
				var form = $("#upload-bulk-user-list-form")[0];
				var url = "../app/bulkUserlistupload";
				
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							alert("Data submitted");
						} else {
							alert(data.error);
						}
					}
				});
			};
		});
	});
}]);

