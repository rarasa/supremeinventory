inspinia.lazy.controller("PrintCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'SweetAlert', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, SweetAlert, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
		
			if($state.current.name == "print.printCuttingOrder"){
				
				$scope.cuttingOrdersList = [];
				/**
				 * Fetch Cutting Order Block
				 */
				$scope.fetchCuttingOrder = function(index){
					var url = "../app/cutting?action=fetchCuttingOrderWithOrderId&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								$scope.cuttingOrder = data.data;
								console.log($scope.cuttingOrder);
								$scope.indexRow = 1;
								
								for(var i = 0; i < $scope.cuttingOrder.cuttingOrderList.length; i++){
								
									for(var j = 0; j < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.length; j++){
										var coListObj = {};
										
										coListObj.material = $scope.cuttingOrder.cuttingOrderList[i].material;
										coListObj.thickness = $scope.cuttingOrder.cuttingOrderList[i].thickness;
										coListObj.cuttingOrderCart = [];
										coListObj.coTaskTypeString = "";
										
										coListObj.coTaskTypeString = "";
										for(var k = 0; k < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType.length; k++){
											
											console.log($scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType[k]);
											coListObj.coTaskTypeString += $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType[k].cuttingType.name;
											if(k != $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType.length - 1){
												coListObj.coTaskTypeString += " | ";
											}
											console.log($scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType[k].cuttingType.name)
										}
										console.log("------------", coListObj.coTaskTypeString);
										coListObj.cuttingOrderCart = $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderCart;

										$scope.cuttingOrdersList.push(coListObj);
									}
								
								}
								
								console.log("$scope.cuttingOrdersList: ", $scope.cuttingOrdersList);
								console.log("$scope.cuttingOrdersList: ", JSON.stringify($scope.cuttingOrdersList));
								
								/*for(var i = 0; i < $scope.cuttingOrder.cuttingOrderList.length; i++){
									for(var j = 0; j < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.cuttingDetails.length; j++){
										$scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.cuttingDetails[j].indexRow = $scope.indexRow;
										$scope.indexRow += 1;
									}
								}*/
								
								/*for(var i = 0; i < $scope.cuttingOrder.cuttingOrderList.length; i++){
									for(var j = 0; j < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.cuttingDetails.length; j++){
										$scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.cuttingDetails[j].indexRow = $scope.indexRow;
										$scope.indexRow += 1;
									}
								}*/
								
								
							});
						});
					});
				};
				
				if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCuttingOrder(index);
				}
				else {
					var webUrlArray = window.location.hash.split("/");
					var index = webUrlArray[webUrlArray.length - 1];
					$scope.fetchCuttingOrder(index);
				}
			}
			
			if($state.current.name == "print.printDeliveryOrder"){
				
				$scope.sortedCartArray = [];
				$scope.fetchOrderForDOPrinting = function(index){
					var url = "../app/order?action=getOrderDetails&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								$scope.order = data.data;
								
								$scope.order.totalCount = 0;
								$scope.order.totalSqMtOfOrder = 0;
								console.log($scope.order);
								
								$scope.order.cart.sort(function(a, b) {
								  return a.material.id - b.material.id;
								});
								
								var unique = [...new Set($scope.order.cart.map(item => item.material.id))];
								
								console.log(unique);
								
								for(var j = 0; j < unique.length; j++){
									var sortedArrayObj = {
										materialId : unique[j],
										thickness : 2,
										details: [],
										totalMtSq : 0,
										count : 0
									}
									for(var i = 0; i < $scope.order.cart.length; i++){
										if($scope.order.cart[i].material.id == unique[j]){
											if($scope.order.cart[i].inventory.thickness == 2){
												sortedArrayObj.materialName = $scope.order.cart[i].material.name;
												
												if($scope.order.cart[i].inventory.type == "TILE"){
													sortedArrayObj.totalMtSq += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) * parseFloat($scope.order.cart[i].tileCount) / 10000);
													sortedArrayObj.count += $scope.order.cart[i].tileCount;
												}
												else{
													sortedArrayObj.totalMtSq += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) / 10000);
													sortedArrayObj.count += 1;
												}
												
												$scope.order.cart[i].inventory.count = $scope.order.cart[i].tileCount
												
												sortedArrayObj.details.push($scope.order.cart[i].inventory);
											}
										}
									}
									$scope.sortedCartArray.push(sortedArrayObj);
								}
								for(var j = 0; j < unique.length; j++){
									var sortedArrayObj = {
										materialId : unique[j],
										thickness : 3,
										details: [],
										totalMtSq : 0,
										count : 0
									}
									for(var i = 0; i < $scope.order.cart.length; i++){
										if($scope.order.cart[i].material.id == unique[j]){
											if($scope.order.cart[i].inventory.thickness == 3){
												sortedArrayObj.materialName = $scope.order.cart[i].material.name;
												
												if($scope.order.cart[i].inventory.type == "TILE"){
													sortedArrayObj.totalMtSq += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) * parseFloat($scope.order.cart[i].inventory.tileCount) / 10000);
													sortedArrayObj.count += $scope.order.cart[i].inventory.tileCount;
												}
												else{
													sortedArrayObj.totalMtSq += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) / 10000);
													sortedArrayObj.count += 1;
												}
												
												sortedArrayObj.details.push($scope.order.cart[i].inventory);
											}
										}
									}
									$scope.sortedCartArray.push(sortedArrayObj);
								}
								
								$scope.order.totalCount = 0;
								$scope.order.totalSqMtOfOrder = 0;
								for(var i = 0; i < $scope.order.cart.length; i++){
									if($scope.order.cart[i].inventory.type == "TILE"){
										$scope.order.totalCount += parseInt($scope.order.cart[i].count);
										$scope.order.totalSqMtOfOrder += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) * parseFloat($scope.order.cart[i].count) / 10000);
									}
									else{
										$scope.order.totalCount += 1;
										$scope.order.totalSqMtOfOrder += (parseFloat($scope.order.cart[i].inventory.actualLength) * parseFloat($scope.order.cart[i].inventory.actualHeight) / 10000);
									}
								}
								
								$scope.order.sortedCartArray = $scope.sortedCartArray;
								
								console.log($scope.order);
							});
						});
					});
				};
				
				if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchOrderForDOPrinting(index);
				}
				else {
					var webUrlArray = window.location.hash.split("/");
					var index = webUrlArray[webUrlArray.length - 1];
					$scope.fetchOrderForDOPrinting(index);
				}
			}
			
		});
	});
}]);

