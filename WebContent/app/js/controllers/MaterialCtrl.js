inspinia.lazy.controller("MaterialCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.materialBulkList = {};
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			$scope.uploadBulkMaterialList = function() {
				console.log("accessed uploadBulkMaterialList");
				var form = $("#upload-bulk-material-list-form")[0];
				var url = "../app/bulkMateriallistupload";
				console.log("form: ",form);
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							alert(data.message);
						} else {
							alert(data.message);
						}
					}
				});
			};
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			
			$scope.editMode = false;
			
			$scope.materialList = allMaterialList;
			console.log("$scope.materialList: ", $scope.materialList);
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.getMaterialList = function(){
				var url = "../app/material?action=getAllMaterialList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.materialList = data;
							console.log($scope.materialList);
						});
					});
				});
			};
			
			$scope.clickedOnEditRow = function(material, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
						if($scope.editMode){
							var obj = {
								type: 'error',
								title : "Editing another row",
								body : "Please save all rows before editing another.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
						else{
							
							var editModeMaterialNameString = "<input class='form-control' type='text' id=materialName"+index+" style='width:-webkit-fill-available; height:25px !important' name='materialName' value='"+material.materialName+"' placeholder='Name' ng-model='material.materialName'>";
	            			var editModeMaterialOriginString = "<input class='form-control' type='text' id=originCountry"+index+" style='width:-webkit-fill-available; height:25px !important' name='originCountry' value='"+material.originCountry+"' placeholder='Origin' ng-model='material.originCountry'>";

							console.log($("#sizeDiv"+index));
							$("#materialNameDiv"+index).empty();
							$("#materialNameDiv"+index).append(editModeMaterialNameString);
							$("#materialOriginDiv"+index).empty();
							$("#materialOriginDiv"+index).append(editModeMaterialOriginString);
							material.editMode = true;
							$scope.editMode = true;
						}
					});
				});
			};
			
			$scope.clickedOnSaveRow = function(material, index){
				$timeout(function() {
					$scope.$apply(function() {
						
						var obj = material;
						obj.materialName = $("#materialName"+index).val();
						obj.originCountry = $("#originCountry"+ index).val();
						
						console.log("material: ", material);
						console.log("Clicked on Save row");
						if($scope.editMode && material.editMode){
							$scope.saveEditedMaterial(material, index);
							
						}
						else{
							
							var obj = {
								type: 'error',
								title : "Invalid Operation",
								body : "Material Is not being edited.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
					});
				});
			};
			
			$scope.saveEditedMaterial = function(material, index){
				
				var obj = material;
				
				obj.lowerPriceRange = 0;
				obj.higherPriceRange = 0;
				
					if(typeof obj.materialName == "undefined" || obj.materialName == ""){
						var obj = {
							type: 'danger',
							title : "Material Name not given",
							body : '',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);

						return;
					}
					
				var url = "../app/material?action=editSingleMaterial";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'Material saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										$scope.editMode = false;
										
										$scope.getMaterialList();
										
									});
								});
							}
							else{
								
								var obj = {
									type: 'error',
									title : data.message,
									body : $scope.material.materialName+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							
							}
						});
					});
				});
				
			};
			
	
			$scope.uploadSingleMaterialList = function(){
				if ($scope.material_form.$valid) {
		            // Submit as normal
					
					var obj = $scope.material;
					obj.lowerPriceRange = 0;
					obj.higherPriceRange = 0;
					
					if(typeof obj.materialName == "undefined" || obj.materialName == ""){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'Material Name not given',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					else if(typeof obj.originCountry == "undefined" || obj.originCountry == ""){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'Origin Country not given',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					
					var exists = false;
					for(var i = 0; i < $scope.materialList.length; i++){
						if($scope.materialList[i].materialName == obj.materialName.toUpperCase()){
							exists = true;
							break
						}
					}
					
					if(exists){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'Material already exists',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					else{
						var url = "../app/material?action=updateSingleMaterial";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									
									if(data.success){
										
										var obj = {
											type: 'success',
											title : 'Material saved.',
											body : $scope.material.materialName+' saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
										
										$scope.getMaterialList();
									}
									else{
										var obj = {
											type: 'error',
											title : 'Oops',
											body : data.message,
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
									}
								});
							});
						});
					}
				}
				else{
					$scope.material_form.submitted = true;
				}
				
				
				
				
				
				
			};
		});
	});
}]);
