inspinia.lazy.controller("ContainerStatusCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster', 'SweetAlert',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster, SweetAlert) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ContainerStatusFile'},
	            {extend: 'pdf', title: 'ContainerStatusFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			
			$scope.message = "hello";
			console.log("$rootScope.materialList: ", $rootScope.materialList);
			console.log("$rootScope.statusList: ", $rootScope.statusList);
			$scope.containerStatusEditObject = {};
			$scope.materialList = $rootScope.materialList;
//			$scope.supplierList = allSupplierList;
			
//			console.log("$scope.supplierList: ", $scope.supplierList);
			
			$scope.materialListObj = {
				materialName : "",
				originCountry : "",
				materialId: 0,
				billRate: 0,
				realRate: 0,
				noOfPieces : 0,
				thickness : "",
				quantity : 0
			};
			
			$scope.containerStatus = {
				materialList : []
			};
			
			$scope.containerStatus.materialList.push($scope.materialListObj);
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			$scope.removeAt = function(array, index, type){
				
				$timeout(function() {
					$scope.$apply(function() {
						console.log("array: ", array);
						console.log("array length: ", array.length);
						console.log("index: ", index);
						
						if(typeof type != "undefined" && array.length <= 1){
							console.log("arraylength is <= 1");
							$scope.materialListObj = {
								materialName : "",
								originCountry : "",
								materialId: 0,
								noOfPieces : 0,
								billRate: 0,
								realRate: 0,
								thickness : "",
								quantity : 0
							};
							array.splice(index, 1);
							array.push($scope.materialListObj);
							$("#material-typeahead-"+index).val("");
						}
						else{
							array.splice(index, 1);
						}
						console.log("Array: ", array);
					});
				});
			};
			
			$scope.addToArray = function(){
				var materialListObj = {
					materialName : "",
					originCountry : "",
					materialId: 0,
					noOfPieces : 0,
					billRate: 0,
					realRate: 0,
					thickness : "",
					quantity : 0
				};
				$scope.containerStatus.materialList.push(materialListObj);
			};
			
			$scope.startMaterialSearch = function(term){
			    var results = [];
			    $scope.materialList.forEach(function(item){
			      if (typeof item.materialName != "undefined" && item.materialName != "" && item.materialName.toLowerCase().indexOf(term.toLowerCase()) > -1) {
			        results.push(item);
			      }
			    });
		      	return results;
			};
			
			$scope.addMaterialToContainer = function(item, array, index) {
				console.log("array: ", array);
				console.log("index: ", index);
				$timeout(function() {
					$scope.$apply(function() {
						
						var materialListObj = {
							materialName : "",
							originCountry : "",
							materialId: 0,
							noOfPieces : 0,
							billRate: 0,
							realRate: 0,
							thickness : "",
							quantity : 0
						};
						
						var addedToMaterialList = false;
						if(array.length > 0){
							console.log("array length > 0", item);
							
							var flag = false;
							for(var i =0; i < array.length; i++){
								if(array[i].materialId != item.materialId){
									flag = true;
								}
								else{
									
									flag = false;
									var obj = {
										type: 'error',
										title : array[i].materialName+" material.",
										body : "Please choose another material.",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									item = "";
									$("#material-typeahead-"+(index+1)).val("");
									break;
								}
							}
							
							if(flag){
								array[index] = item;
								array[index].noOfPieces = 0;
								array[index].quantity = 0;
								array.push(materialListObj);
								item = "";
								$("#material-typeahead-"+(index+1)).val("");
								addedToMaterialList = true;
							}
							
						}
						else{
							console.log("array length < 0", item);
							array[index] = item;
							array[index].noOfPieces = 0;
							array[index].quantity = 0;
							array.push(materialListObj);
							item = "";
							$("#material-typeahead-"+(index+1)).val("");
							addedToMaterialList = true;
						}
						
					});
				});
			};
			
			
			$scope.validate = function(validationObj){
				console.log("validate entered validationObj: ",validationObj);
				var toasterErrorObj = {
					type: 'error',
					title : "",
					body : "",
					showCloseButton: true,
					timeout: 2000
				};
				
				if(typeof validationObj.supplierId == "undefined" || validationObj.supplierId == "" || validationObj.supplierId == 0){
					toasterErrorObj.title = "Supplier missing.";
					toasterErrorObj.body = "Please enter Supplier.";
					$scope.toasterNotification(toasterErrorObj);
				}
				else if(typeof validationObj.containerNumber == "undefined" || validationObj.containerNumber == "" || validationObj.containerNumber == 0){
					toasterErrorObj.title = "Container Number missing.";
					toasterErrorObj.body = "Please enter Container Number.";
					$scope.toasterNotification(toasterErrorObj);
				}
				else if(typeof validationObj.containerStatus == "undefined" || validationObj.containerStatus == "" || validationObj.containerStatus == 0){
					toasterErrorObj.title = "Container Status missing.";
					toasterErrorObj.body = "Please enter Container Status.";
					$scope.toasterNotification(toasterErrorObj);
				}
				else if(typeof validationObj.materialList == "undefined" || validationObj.materialList.length <= 0){
					toasterErrorObj.title = "Material Missing.";
					toasterErrorObj.body = "Please enter atleast one Material.";
					$scope.toasterNotification(toasterErrorObj);
				}
				else if(typeof validationObj.materialList != "undefined" && validationObj.materialList.length > 0){
					
					for(var i = 0; i < validationObj.materialList.length; i++){
						
						if(typeof validationObj.materialList[i].materialId == "undefined" || validationObj.materialList[i].materialId == 0){
							
							if( validationObj.materialList.length == 1){
								toasterErrorObj.title = "Material Missing.";
								toasterErrorObj.body = "Please enter Material in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							if( validationObj.materialList.length > 1 && i != validationObj.materialList.length - 1){
								toasterErrorObj.title = "Material Missing.";
								toasterErrorObj.body = "Please enter Material in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							
						}
						else{
							if(typeof validationObj.materialList[i].thickness == "undefined" || validationObj.materialList[i].thickness == ""){
								toasterErrorObj.title = "Thickness Missing.";
								toasterErrorObj.body = "Please enter Thickness in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							else if(typeof validationObj.materialList[i].noOfPieces == "undefined" || validationObj.materialList[i].noOfPieces == ""){
								toasterErrorObj.title = "No Of Pieces Missing.";
								toasterErrorObj.body = "Please enter No Of Pieces in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							else if(typeof validationObj.materialList[i].quantity == "undefined" || validationObj.materialList[i].quantity == ""){
								toasterErrorObj.title = "Quantity Missing.";
								toasterErrorObj.body = "Please enter Quantity in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							else if(typeof validationObj.materialList[i].realRate == "undefined" || validationObj.materialList[i].realRate == ""){
								toasterErrorObj.title = "Real Rate Missing.";
								toasterErrorObj.body = "Please enter Real Rate in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
							else if(typeof validationObj.materialList[i].billRate == "undefined" || validationObj.materialList[i].billRate == ""){
								toasterErrorObj.title = "Bill Rate Missing.";
								toasterErrorObj.body = "Please enter Bill Rate in row "+ (i+1);
								$scope.toasterNotification(toasterErrorObj);
							}
						}
						
					}
				}
				else if(typeof validationObj.supplierId == "undefined" || validationObj.supplierId == "" || validationObj.supplierId == 0){
					toasterErrorObj.title = "Supplier missing.";
					toasterErrorObj.body = "Please enter Supplier.";
					$scope.toasterNotification(toasterErrorObj);
				}
				else if(typeof validationObj.supplierId == "undefined" || validationObj.supplierId == "" || validationObj.supplierId == 0){
					toasterErrorObj.title = "Supplier missing.";
					toasterErrorObj.body = "Please enter Supplier.";
					$scope.toasterNotification(toasterErrorObj);
				}
				
			};
			
			
			$scope.submitContainerStatus = function(){
				
				$scope.validate($scope.containerStatus);
				
				for(var i = 0; i < $scope.containerStatus.materialList.length; i++){
					if(typeof $scope.containerStatus.materialList[i].materialId == "undefined" || $scope.containerStatus.materialList[i].materialId == 0){
						$scope.containerStatus.materialList.splice(i, 1);
					}
				}
				
				var url = "../app/container?action=addContainer";
				console.log(url);
				$http.post(url, $scope.containerStatus, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										
										
										SweetAlert.swal({
						    				title: "Done!",
						    	            text: data.message,
						    	            type: "success",
						                    showCancelButton: false,
						                    confirmButtonColor: "#DD6B55",
						                    confirmButtonText: "OK!",
						                    cancelButtonText: "",
						                    closeOnConfirm: true,
					                    },
					                    function (isConfirm) {
						                    if (isConfirm) {
						                    	window.location.reload();
						                    }
					                    });
										
										
									});
								});
							}
							else{
								
								SweetAlert.swal({
					                title: "Oops!",
					                text: data.error,
					                type: "warning",
					                showCancelButton: false,
					                confirmButtonColor: "#DD6B55",
					                confirmButtonText: "OK! Let me check!",
					                closeOnConfirm: true,
					                closeOnCancel: true
					            });
								return;
							}
						});
					});
				});
				
			};
			
			$scope.editContainerDetails = function(container){
				$state.go("admin.editContainerStatus", {
					container : container,
					id : container.containerId
				});
			};
			
			if($state.current.name == "admin.editContainerStatus"){
				
				$timeout(function() {
					$scope.$apply(function() {
						$scope.containerStatusEditObject = {};
						
						$scope.mainContainerObject = {};
						
						if(typeof $stateParams.container != "undefined" && $stateParams.container != {}){
							$scope.containerStatusEditObject = $stateParams.container;
							$scope.mainContainerObject = $stateParams.container;
						}
						else{
							var urlArray = window.location.hash.split("/");
							var containerId = urlArray[urlArray.length - 1];
							
							for(var i = 0; i < $scope.containerDetails.length; i++){
								if(containerId == $scope.containerDetails[i].containerId){
									$scope.containerStatusEditObject = $scope.containerDetails[i];
									$scope.mainContainerObject = $scope.containerStatusEditObject;
								}
							}
						}
						
						console.log("mainContainerObject: ", JSON.stringify($scope.mainContainerObject));
							
						$scope.containerStatusEditObject.supplierId = $scope.mainContainerObject.supplier.supplierId;
								
						if($scope.mainContainerObject.statusDetails.bankId == 0){
							$scope.containerStatusEditObject.bankId = "";
						}
						else{
							$scope.containerStatusEditObject.bankId = $scope.mainContainerObject.statusDetails.bankId;
						}

						if($scope.mainContainerObject.statusDetails.BLStatus == 0){
							$scope.containerStatusEditObject.BLStatus = "";
						}
						else{
							$scope.containerStatusEditObject.BLStatus = $scope.mainContainerObject.statusDetails.BLStatus;
						}
						
						if($scope.mainContainerObject.statusDetails.paymentStatus == 0){
							$scope.containerStatusEditObject.paymentStatus = "";
						}
						else{
							$scope.containerStatusEditObject.paymentStatus = $scope.mainContainerObject.statusDetails.paymentStatus;
						}
						
						if($scope.mainContainerObject.statusDetails.containerStatus == 0){
							$scope.containerStatusEditObject.containerStatus = "";
						}
						else{
							$scope.containerStatusEditObject.containerStatus = $scope.mainContainerObject.statusDetails.containerStatus;
						}
						
						if($scope.mainContainerObject.statusDetails.DOStatus == 0){
							$scope.containerStatusEditObject.DOStatus = "";
						}
						else{
							$scope.containerStatusEditObject.DOStatus = $scope.mainContainerObject.statusDetails.DOStatus;
						}
						
						if(typeof $scope.mainContainerObject.statusDetails.invoiceNumber != "undefined" || $scope.mainContainerObject.statusDetails.invoiceNumber != ""){
							$scope.containerStatusEditObject.invoiceNumber = $scope.mainContainerObject.statusDetails.invoiceNumber;
						}
						else{
							$scope.containerStatusEditObject.invoiceNumber = "";
						}
						
						if(typeof $scope.mainContainerObject.statusDetails.description != "undefined" || $scope.mainContainerObject.statusDetails.description != ""){
							$scope.containerStatusEditObject.description = $scope.mainContainerObject.statusDetails.description;
						}
						else{
							$scope.containerStatusEditObject.invoiceNumber = "";
						}
						
						if(typeof $scope.mainContainerObject.statusDetails.paymentDate != "undefined" && $scope.mainContainerObject.statusDetails.paymentDate != ""){
							$scope.containerStatusEditObject.paymentDate = moment($scope.mainContainerObject.statusDetails.paymentDate, "YYYY-MM-DD").format("DD-MM-YYYY");
						}
						if(typeof $scope.mainContainerObject.statusDetails.invoiceDate != "undefined" && $scope.mainContainerObject.statusDetails.invoiceDate!=""){
							$scope.containerStatusEditObject.invoiceDate = moment($scope.mainContainerObject.statusDetails.invoiceDate, "YYYY-MM-DD").format("DD-MM-YYYY");
						}
						if(typeof $scope.mainContainerObject.createdOn != "undefined" && $scope.mainContainerObject.createdOn!=""){
							$scope.containerStatusEditObject.createdOn = moment($scope.mainContainerObject.createdOn, "YYYY-MM-DD").format("DD-MM-YYYY");
						}
						if(typeof $scope.mainContainerObject.updatedOn != "undefined" && $scope.mainContainerObject.updatedOn!=""){
							$scope.containerStatusEditObject.updatedOn = moment($scope.mainContainerObject.updatedOn, "YYYY-MM-DD").format("DD-MM-YYYY");
						}
						
						console.log("BEFORE FOR LOOP: ", $scope.mainContainerObject.materialList[x]);
						for(var x = 0; x < $scope.mainContainerObject.materialList.length; x++){
							$scope.containerStatusEditObject.materialList[x] = $scope.mainContainerObject.materialList[x];
							$scope.containerStatusEditObject.materialList[x].materialName = $scope.containerStatusEditObject.materialList[x].material.materialName;
							$scope.containerStatusEditObject.materialList[x].materialId = $scope.containerStatusEditObject.materialList[x].material.materialId;
						}
						
						console.log("before calling validate::::::::::::::::::::::::: ", JSON.stringify($scope.containerStatusEditObject));
						
						
						
						$scope.validate($scope.containerStatusEditObject);
						
						
						
						if(typeof $scope.containerStatusEditObject.materialList == "undefined" 
								|| $scope.containerStatusEditObject.materialList.length <= 0){
							
							var materialListObj = {
								materialName : "",
								originCountry : "",
								materialId: 0,
								noOfPieces : 0,
								billRate: 0,
								realRate: 0,
								thickness : "",
								quantity : 0
							};
							
							$scope.containerStatusEditObject.materialList = [];
							$scope.containerStatusEditObject.materialList.push(materialListObj);
							
						}
							
						
						console.log("$scope.containerStatusEditObject: ", $scope.containerStatusEditObject);
					
					});
				});
				
				
				$scope.deleteMaterialFormContainerWithId = function(containerId, materialId, index){
					var url = "../app/container?action=deleteMaterialFormContainerWithId";
					console.log(url);
					
					var obj = {
						materialId: materialId,
						containerId : containerId
					}
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									$timeout(function() {
										$scope.$apply(function() {
											
											var obj = {
												type: 'success',
												title : "Done.",
												body : data.message,
												showCloseButton: true,
												timeout: 6000
											};
											$scope.toasterNotification(obj);
											$scope.removeAt($scope.containerStatusEditObject.materialList, index, 'materialList');
											
										});
									});
								}
								else{
									var obj = {
										type: 'error',
										title : "Oops",
										body : data.error,
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
								}
							});
						});
					});
				};
				
				$scope.updateEditedContainerDetails = function(){
					
					$scope.validate($scope.containerStatusEditObject);
							
							if(typeof $scope.containerStatusEditObject.materialList == "undefined" 
									|| $scope.containerStatusEditObject.materialList == "" 
									|| $scope.containerStatusEditObject.materialList == [] 
									|| $scope.containerStatusEditObject.materialList.length <= 0){
								
								var materialListObj = {
									materialName : "",
									originCountry : "",
									materialId: 0,
									noOfPieces : 0,
									billRate: 0,
									realRate: 0,
									thickness : "",
									quantity : 0
								};
								
								$scope.containerStatusEditObject.materialList = [];
								$scope.containerStatusEditObject.materialList.push(materialListObj);
								
							}
							else{
								for(var i = 0; i < $scope.containerStatusEditObject.materialList.length; i++){
									if(typeof $scope.containerStatusEditObject.materialList[i].materialId == "undefined" || $scope.containerStatusEditObject.materialList[i].materialId == 0){
										$scope.containerStatusEditObject.materialList.splice(i, 1);
									}
								}
							}
							
						
						console.log("$scope.containerStatusEditObject: ", $scope.containerStatusEditObject);
					
					//editContainer
					var url = "../app/container?action=editContainer";
					console.log(url);
					$http.post(url, $scope.containerStatusEditObject, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									$timeout(function() {
										$scope.$apply(function() {
											SweetAlert.swal({
							    				title: "Done!",
							    	            text: data.message,
							    	            type: "success",
							                    showCancelButton: false,
							                    confirmButtonColor: "#DD6B55",
							                    confirmButtonText: "OK!",
							                    cancelButtonText: "",
							                    closeOnConfirm: true,
						                    },
						                    function (isConfirm) {
							                    if (isConfirm) {
							                    	window.location.reload();
							                    }
						                    });
										});
									});
								}
								else{
									SweetAlert.swal({
						                title: "Oops!",
						                text: data.error,
						                type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK! Let me check!",
						                closeOnConfirm: true,
						                closeOnCancel: true
						            });
									return;
								}
							});
						});
					});
				};
			}
			
			$scope.deleteContainerStatuswithId = function(container, index){
				
				console.log(container);
				
				var obj = {
					containerId: container.containerId
				};
				
				var url = "../app/container?action=deleteContainerWithId";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										
										$rootScope.containerDetails.splice(index, 1);
										SweetAlert.swal({
						    				title: "Done!",
						    	            text: data.message,
						    	            type: "success",
						                    showCancelButton: false,
						                    confirmButtonColor: "#DD6B55",
						                    confirmButtonText: "OK!",
						                    cancelButtonText: "",
						                    closeOnConfirm: true,
					                    },
					                    function (isConfirm) {
						                    
					                    });
										
										
									});
								});
							}
							else{
								
								SweetAlert.swal({
					                title: "Oops!",
					                text: data.error,
					                type: "warning",
					                showCancelButton: false,
					                confirmButtonColor: "#DD6B55",
					                confirmButtonText: "OK! Let me check!",
					                closeOnConfirm: true,
					                closeOnCancel: true
					            });
								return;
							}
						});
					});
				});
				
			};
			
		});
	});
}]);

