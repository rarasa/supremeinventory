inspinia.lazy.controller("CuttingOrderCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'SweetAlert', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, SweetAlert, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			/**
			 * Add Cutting Order Block
			 */
		
			$scope.count = 0;
			$scope.totalSqMeters = 0;
		
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called", obj);
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };


			$scope.tags = [];
			$scope.loadCuttingTypes = function($query) {
		      return allCuttingTypeList.filter(function(country) {
		        return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
		      });
		  };

			$scope.allSmallCustomerList = allSmallCustomerList;
			$scope.allCuttingTypeList = allCuttingTypeList;
			$scope.manualCuttingTypeList = manualCuttingTypeList;
			$scope.machineCuttingTypeList = machineCuttingTypeList;
			$scope.error = false;
			
			$scope.assignCustomerName = function(customerObj){
				
				if(customerObj.charAt(0) == "{"){
					customerObj = JSON.parse(customerObj);
					console.log(customerObj);
					$scope.cuttingOrder.customerId = customerObj.id;
					$scope.cuttingOrder.customerName = customerObj.name;
				}
				else{
					$scope.cuttingOrder.customerId = 0;
					$scope.cuttingOrder.customerName = customerObj;
				}
				
				console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
			};
			
			$scope.cuttingOrderCuttingListDetailsObject = {
				machineCut : '',
				manualCut : '',
				unit : '',
				cutToSize : 0,
				length : 0,
				height : 0,
				quantity : 0,
				nos : 0
			};
			
			$scope.cuttingOrderCuttingListObject = {
				materialId: '',
				details: [],
			};
			
			$scope.cuttingOrder = {
				cuttingOrderNumber: 0,
				customerName : '',
				customerId: '',
				customer: '',
				date: moment().format("DD-MM-YYYY"),
				cuttingList: [],
				materialCount : 0,
				totalOutputMtSquare : 0,
				totalOutputLinearMeter : 0
			};
			
			$scope.cuttingOrderCuttingListObject.details.push($scope.cuttingOrderCuttingListDetailsObject);
			
			$scope.cuttingOrder.cuttingList.push($scope.cuttingOrderCuttingListObject);
			
			$scope.removeAt = function(index, removeWhere, coList){
				
				if(typeof removeWhere != "undefined" && removeWhere != ""){
					if(removeWhere == "cuttingListArray"){
						//index is of coList
						$scope.cuttingOrder.cuttingList.splice(index, 1);
					}
					else if(removeWhere == 'cuttingListDetails'){
						//index is of coList.details
						coList.details.splice(index, 1);
					}
				}
				
				console.log($scope.cuttingOrder);
			};
			
			$scope.calculateTotalQty = function(){
				$scope.cuttingOrder.totalOutputMtSquare = 0;
				$scope.cuttingOrder.totalOutputLinearMeter = 0;
				for(var i = 0; i < $scope.cuttingOrder.cuttingList.length; i++){
					$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter = 0;
					$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare = 0;
					for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].details.length; j++){
						if($scope.cuttingOrder.cuttingList[i].details[j].unit == 1){
							$scope.cuttingOrder.totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
							$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
						}
						if($scope.cuttingOrder.cuttingList[i].details[j].unit == 2){
							$scope.cuttingOrder.totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
							$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
						}
					}
				}
			}
			
			$scope.calculateQty = function(cListObj){
				//pcs : quantity = nos 
				//LM = length*no of pcs / 100
				//Mt is l*h*pcs/10000

				if(cListObj.unit == 1){
					//LM
					cListObj.quantity = cListObj.length * cListObj.nos / 100;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 2){
					//Mt.Sq.
					cListObj.quantity = cListObj.length * cListObj.height * cListObj.nos / 10000;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 3){
					//Pcs
					cListObj.quantity = cListObj.nos;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				console.log("CalculateQty: ", $scope.cuttingOrder);
			}
			
			$scope.addRow = function(coList, addWhere){
				
				if(typeof addWhere != 'undefined' && addWhere != ''){
					$scope.cuttingOrderCuttingListDetailsObject = {
						machineCut : '',
						manualCut : '',
						unit : '',
						cutToSize : 0,
						length : 0,
						height : 0,
						quantity : 0,
						nos : 0,
					};
					if(addWhere == 'cuttingListArray'){
						$scope.cuttingOrderCuttingListObject = {
							materialId: '',
							date: moment().format("DD-MM-YYYY"),
							details: [],
						};
						$scope.cuttingOrderCuttingListObject.details.push($scope.cuttingOrderCuttingListDetailsObject);
						
						$scope.cuttingOrder.cuttingList.push($scope.cuttingOrderCuttingListObject);
					}
					else if(addWhere == 'cuttingListDetails'){
						coList.details.push($scope.cuttingOrderCuttingListDetailsObject);
					}
				}
				
				
				console.log($scope.cuttingOrder);
			};
			
			$scope.validate = function(){
				
				console.log("Validate $scope.cuttingOrder: ", $scope.cuttingOrder);
				
				if(typeof $scope.cuttingOrder.customerId == "undefined" || $scope.cuttingOrder.customerId == ""){
					
					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Customer.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					
					return true;
					
				}
				else if(typeof $scope.cuttingOrder.date == "undefined" || $scope.cuttingOrder.date == ""){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Date.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList == "undefined" || $scope.cuttingOrder.cuttingList.length <= 0){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please Material Details.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList != "undefined" && $scope.cuttingOrder.cuttingList.length > 0){
					
					for(var i =0; i < $scope.cuttingOrder.cuttingList.length; i++){
						
						if(typeof $scope.cuttingOrder.cuttingList[i].materialId == "undefined" || $scope.cuttingOrder.cuttingList[i].materialId == ""){
							
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Material Id in Material "+(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].materialThickness == "undefined" || $scope.cuttingOrder.cuttingList[i].materialThickness == ""){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Thickness in Material "+(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].details == "undefined" || $scope.cuttingOrder.cuttingList[i].details.length <= 0){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Material Details in Material "+(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						else if(typeof $scope.cuttingOrder.cuttingList[i].details != "undefined" && $scope.cuttingOrder.cuttingList[i].details.length > 0){
							
							for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].details.length; j++){
								
								if((typeof $scope.cuttingOrder.cuttingList[i].details[j].machineCut == "undefined" || $scope.cuttingOrder.cuttingList[i].details[j].machineCut == "")
									&& (typeof $scope.cuttingOrder.cuttingList[i].details[j].machineCut == "undefined" || $scope.cuttingOrder.cuttingList[i].details[j].manualCut == "")){
									
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Machine or Manual Tasks in Material Details.",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
									
								}
								else if(typeof $scope.cuttingOrder.cuttingList[i].details[j].unit == "undefined" || $scope.cuttingOrder.cuttingList[i].details[j].unit == ""){
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Unit in Material Details.",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
								}
								
							}
							
						}
						
					}
					
				}
				
			};
			
			$scope.submitCuttingOrder = function(){
				console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);

				
				var error = $scope.validate();
				
				console.log("error: ", error);
				
				console.log("$scope.error: ", $scope.error);
				
				
				console.log("BEFORE SUBMIT: ", $scope.cuttingOrder);
				
				if(error){
					return;
				}
				else{
					
					$scope.calculateTotalQty();
					
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
					$http.defaults.transformRequest = [ function(data) {
						return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
					}];
					var url = "../app/cutting?action=addCuttingOrders";
					console.log(url);
					$http.post(url, $scope.cuttingOrder, {}).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
	
								if(data.success){
									
									SweetAlert.swal({
										title: "Done!",
							            text: "Successfully updated Cutting Order. Taking you to Order Print Page.",
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	window.location.reload();
						                }
						            });
								}
								else{
									SweetAlert.swal({
									    title: "Oops!",
									    text: "Your Order was not saved!",
									    type: "warning",
									    showCancelButton: true,
									    confirmButtonColor: "#DD6B55",
									    confirmButtonText: "Yes, delete it!",
									    closeOnConfirm: true}
									);
								}
							});
						});
					});
				}
			};
			
			$scope.viewSlabDetails = function(id, index, tag, array){
				
				console.log("viewSlabDetails called");
				console.log(array);
				htmltag = tag;
				console.log("this: ", tag);
				console.log(id);
				console.log("INDEX: ", index);
				
				var error = false;
				var duplicateMessage = "";
				
				console.log(array);
				if(typeof array != "undefined" && array.length > 0){
					for(var i = 0; i < array.length; i++){
						if(i != index && array[i].slabCode == id){
							duplicateMessage = "You have already scanned " + id;
							error = true;
							break;
						}
					}
				}
				
				if(error){
					
					var obj = {
						type: 'error',
						title : "Duplicate slab error!",
						body : duplicateMessage,
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					
					return;
				}
				
				var url = "../app/inventory?action=readBarcode&barcodeString="+id;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);

								var arrayObj = {};
								
								arrayObj = data.data;
								
								arrayObj.name = data.data.material.name;
								
								if(typeof arrayObj.type != "undefined"
										&& arrayObj.type == "TILE"){
									
									arrayObj.tileCount = data.data.tileCount;
									arrayObj.originalTileCount = data.data.tileCount;
									
									arrayObj.actualSqMetre = parseFloat((arrayObj.actualLength * arrayObj.actualHeight) / 10000) * arrayObj.tileCount;
									$scope.count += arrayObj.tileCount;
									$scope.totalSqMeters += arrayObj.actualSqMetre;
									
									$scope.cuttingOrder.slabCount = $scope.count;
									$scope.cuttingOrder.totalMtSquare = $scope.totalSqMeters;
								}
								else{
									arrayObj.actualSqMetre = parseFloat((arrayObj.actualLength * arrayObj.actualHeight) / 10000);
									$scope.count += 1;
									$scope.totalSqMeters += arrayObj.actualSqMetre;
									
									$scope.cuttingOrder.slabCount = $scope.count;
									$scope.cuttingOrder.totalMtSquare = $scope.totalSqMeters;
								}
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								 
								$scope.cuttingOrder.cart[index] = arrayObj;
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								
								console.log($scope.cuttingOrder);
								
								$scope.addRow();
							}
							else{
								
								var obj = {
									type: 'error',
									title : "Error!",
									body : data.message,
									showCloseButton: true,
									timeout: 6000
								};
								$scope.toasterNotification(obj);
								
							}
							
							
						});
					});
				});
			};
			
			if($state.current.name == "cutting.scanCuttingOrder"){
				
				$scope.cart = [];
				$scope.fetchCuttingOrder = function(index){
					var url = "../app/cutting?action=fetchCuttingOrderWithOrderId&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								$scope.cuttingOrder = data.data;
								console.log($scope.cuttingOrder);
								$scope.indexRow = 1;
								
								for(var i = 0; i < $scope.cuttingOrder.cuttingOrderList.length; i++){
									for(var j = 0; j < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderCart.length; j++){
										$scope.cuttingOrder.cuttingOrderList[i].cuttingOrderCart[j].indexRow = $scope.indexRow;
										$scope.indexRow += 1;
									}
								}
								
								if(typeof $scope.cuttingOrder.cart != "undefined" && $scope.cuttingOrder.cart.length <= 0){
									$scope.addRow();
								}
								else{
									$scope.count = 0;
									$scope.totalSqMeters = 0;
									for(var i = 0; i < $scope.cuttingOrder.cart.length; i++){
										$scope.cuttingOrder.cart[i].name = $scope.cuttingOrder.cart[i].material.name;
										$scope.count = $scope.cuttingOrder.slabCount;
										$scope.totalSqMeters = $scope.cuttingOrder.totalMtSquare;
									}
									$scope.addRow();
								}
								
							});
						});
					});
				};
				
				if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCuttingOrder(index);
				}
				else {
					var webUrlArray = window.location.hash.split("/");
					var index = webUrlArray[webUrlArray.length - 1];
					$scope.fetchCuttingOrder(index);
				}
				
				
				
				$scope.addRow = function(){
					console.log("addRowFlag entered");

					var material = {
						name: ""
					}					
					$scope.cuttingOrderObject = {
							barcodeString: '',
							name: '',
							actualLength: 0,
							actualHeight: 0,
							thickness: 0,
							slabId: 0,
							id: 0,
							actualSqMetre: 0,
							type: ""
					};
	
					var idString = "";
					var addRowFlag = true;
					/*for(var i=0; i < $scope.cuttingOrder.cart.length; i++){
						if(typeof $scope.cuttingOrder.cart[i].slabCode == "undefined" || $scope.cuttingOrder.cart[i].slabCode == ""){
							//one empty row exists so do nothing
							addRowFlag = false;
							idString = "slabCodeText"+indexOf($scope.cuttingOrder.cart[i]);
							break;
						}
						else{
							addRowFlag = true;
							idString = "slabCodeText"+$scope.cuttingOrder.cart.length;
						}
					}*/
					
					console.log("idString: "+ idString)
					if(addRowFlag){
						console.log("addrowFlag: ", addRowFlag);
						$scope.cuttingOrder.cart.push($scope.cuttingOrderObject);
						
						$timeout(function() {
							$scope.$apply(function() {
								console.log("idString 2: "+ idString)
						      // have to do this in a $timemout because
						      // we want it to happen after the view is updated
						      // with the newly added input
						      angular
						        .element(document.querySelectorAll('#'+idString))
						        .eq(-1)[0]
						        .focus()
						      ;
							});
					    }, 0);
					}
				};
				
				function checkForDuplicates(array) {
				  return new Set(array).size !== array.length;
				}
				
				$scope.validateTileCount = function(cartObj){
					
					if(cartObj.tileCount > cartObj.originalTileCount){
						var obj = {
							type: 'error',
							title : "Oops!",
							body : 'Tile Count cannot be more than '+ cartObj.originalTileCount,
							showCloseButton: true,
							timeout: 6000
						};
						$scope.toasterNotification(obj);
						
						cartObj.tileCount = cartObj.originalTileCount;
						
						return;
					}
					
				};
				
				$scope.updateEditedCuttingOrder = function(){
					
					console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
					if(typeof $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == "undefined" 
						|| $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == ""
						|| $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == 0){
						
						$scope.cuttingOrder.cart.splice($scope.cuttingOrder.cart.length - 1, 1);
					}
					
					var obj = {};
					
					obj.orderId = $scope.cuttingOrder.id;
					obj.slabCount = $scope.cuttingOrder.slabCount;
					obj.totalMtSquare = $scope.cuttingOrder.totalMtSquare;
					obj.cart = [];
					
					for(var i =0; i < $scope.cuttingOrder.cart.length; i++){
						var cartObj = {
							id: $scope.cuttingOrder.cart[i].id,
							type: $scope.cuttingOrder.cart[i].type
						}
						
						if(cartObj.type == "TILE"){
							cartObj.tileCount = $scope.cuttingOrder.cart[i].tileCount;
						}
						obj.cart.push(cartObj);
					}
					
					if(checkForDuplicates(obj.cart)){
						console.log("Duplicate value in Scan list");
						
						var obj = {
							type: 'error',
							title : "Error!",
							body : 'Remove Duplicate Values from Scan List',
							showCloseButton: true,
							timeout: 6000
						};
						$scope.toasterNotification(obj);
						
						return;
					}
					
					console.log("BEFORE SUBMIT: ", obj);
					
					
					var url = "../app/cutting?action=addScannedSlabsToCuttingOrder";
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
					$http.defaults.transformRequest = [ function(data) {
						return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
					}];
					$http.post(url, obj, {}).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								
								if(data.success){
									SweetAlert.swal({
										title: "Done!",
							            text: data.message,
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	$state.go("sales.salesInvoice");
						                }
						            });
								}
								else{
									SweetAlert.swal({
										title: "Error!",
							            text: data.message,
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	window.location.reload();
						                }
						            });
								}
							});
						});
					});
				};
			}
			
			/*SweetAlert.swal({
				title: "Done!",
	            text: data.message,
	            type: "success",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK!",
                cancelButtonText: "",
                closeOnConfirm: true,
            },
            function (isConfirm) {
                if (isConfirm) {
                	window.location.reload();
                }
            });*/
			
		});
	});
}]);

