inspinia.lazy.controller("CustomerDetailsCtrl", ['$scope', '$location', '$modalInstance', '$http', '$timeout', '$state', '$rootScope', '$stateParams',
    function ($scope, $location, $modalInstance, $http, $timeout, $state, $rootScope, $stateParams) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.message = "EditedCusttttt";
			
			$scope.customerList = allSmallCustomerList;
			console.log("$scope.customerList: ", $scope.customerList);
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.getCustomerList = function(){
				var url = "../app/customer?action=getAllCustomersList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.customerList = data;
							console.log($scope.customerList);
						});
					});
				});
			};
			
			$scope.editedCustomer = function(){
				console.log("editedCustomer");
			};
			
			$scope.clickedOnCustomerEditRow = function(customer, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
						if($scope.editMode){
							var obj = {
								type: 'error',
								title : "Editing another row",
								body : "Please save all rows before editing another.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
						else{
							
							customer.activeBool = (customer.active) ? 1 : 0;
							
							var editModeCustomerNameString = "<input class='form-control'' type='text' id=name"+index+" style='width:-webkit-fill-available;' name='name' value='"+customer.name+"' placeholder='Name' ng-model='customer.name'>";
	            			var editModeCustomerCode = "<input class='form-control' type='text' id=code"+index+" style='width:-webkit-fill-available;' name='code' value='"+customer.code+"' placeholder='Origin' ng-model='customer.code'>";
	            			var editModeCustomerAddress = "<input class='form-control' type='text' id=address"+index+" style='width:-webkit-fill-available;' name='address' value='"+customer.address+"' placeholder='Origin' ng-model='customer.address'>";
	            			var editModeCustomerPhone = "<input class='form-control' type='text' id=phone"+index+" style='width:-webkit-fill-available;' name='phone' value='"+customer.phone+"' placeholder='Origin' ng-model='customer.phone'>";
	            			
							var editModeCustomerActive = "<select id='active"+index+"' class='form-control' name='active' ng-model='customer.activeBool'>"
											            	+"<option value=''>--Select Active--</option>"
											            	+"<option value='true'>Yes</option>"
											            	+"<option value='false'>No</option>"
											            +"</select>";
							

							console.log($("#sizeDiv"+index));
							$("#customerNameDiv"+index).empty();
							$("#customerNameDiv"+index).append(editModeCustomerNameString);
							$("#customerCodeDiv"+index).empty();
							$("#customerCodeDiv"+index).append(editModeCustomerCode);
							$("#customerContactDiv"+index).empty();
							$("#customerContactDiv"+index).append(editModeCustomerPhone);
							$("#customerAddressDiv"+index).empty();
							$("#customerAddressDiv"+index).append(editModeCustomerAddress);
							$("#customerActiveDiv"+index).empty();
							$("#customerActiveDiv"+index).append(editModeCustomerActive);
							
							
							console.log(customer);
							
							customer.editMode = true;
							$scope.editMode = true;
							customer.activeBool = customer.active;
							
							$("#active"+ index).val(''+customer.active);
							console.log(customer);
						}
					});
				});
			};
			
			$scope.clickedOnSaveRow = function(customer, index){
				$timeout(function() {
					$scope.$apply(function() {
						
						var obj = customer;
						obj.name = $("#name"+index).val();
						obj.code = $("#code"+ index).val();
						obj.address = $("#address"+ index).val();
						obj.phone = $("#phone"+ index).val();
						obj.active = $("#active"+ index).val();
						
						console.log("customer: ", customer);
						console.log("Clicked on Save row");
						
						if($scope.editMode && customer.editMode){
							$scope.saveEditedCustomer(customer, index);
							$scope.editMode = false;
							customer.editMode = false;
						}
						else{
							
							var obj = {
								type: 'error',
								title : "Invalid Operation",
								body : "Customer Is not being edited.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
					});
				});
			};
			
			$scope.saveEditedCustomer = function(customer, index){
				
				var obj = customer;
				
				console.log("Customer obj: ", customer);
				if(typeof obj.name == "undefined" || obj.name == ""){
					var obj = {
						type: 'danger',
						title : "Customer Name not given",
						body : '',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);

					return;
				}
					
				var url = "../app/customer?action=editSingleCustomer";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'Customer saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										
										$scope.getCustomerList();
										
										$scope.editMode = false;
									});
								});
							}
							else{
								
								var obj = {
									type: 'error',
									title : data.message,
									body : obj.name+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							}
						});
					});
				});
			};
			
	
			$scope.uploadSingleCustomerList = function(){
				if ($scope.customer_form.$valid) {
		            // Submit as normal
					
					var obj = $scope.customer;
					obj.lowerPriceRange = 0;
					obj.higherPriceRange = 0;
					
										
					var exists = false;
					for(var i = 0; i < $scope.customerList.length; i++){
						if($scope.customerList[i].name == obj.name.toUpperCase()){
							exists = true;
							break
						}
					}
					
					if(exists){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'Customer already exists',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					else{
						var url = "../app/customer?action=updateSingleCustomer";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									
									if(data.success){
										
										var obj = {
											type: 'success',
											title : 'Customer saved.',
											body : $scope.customer.name+' saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
										
										 $("#customer_form_name").val("");
										 $("#customer_form_code").val("");
										 $("#customer_form_address").val("");
										 $("#phone").val("");
										
										$scope.getCustomerList();
									}
									else{
										var obj = {
											type: 'error',
											title : 'Oops',
											body : data.message,
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
									}
								});
							});
						});
					}
				}
				else{
					$scope.customer_form.submitted = true;
				}
				
			};
			
			
			$scope.deleteCustomer = function(customer, index){
				
				if($scope.editMode){
					
					var obj = {
						type: 'error',
						title : 'Error',
						body : 'Please save row before editing.',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					
					return;
				}
				
				if(!confirm("Do you really want to delete "+ customer.name+" ?")){
					return;
				}
				else{
					console.log("call delete func");
					var url = "../app/customer?action=deleteCustomer";
					
					var obj = {
						name : customer.name,
						id: customer.id
					};
					
					console.log(url);
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									var obj = {
										type: 'success',
										title : 'Deleted!',
										body : 'Customer deleted successfully.',
										showCloseButton: true,
        								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.getCustomerList();
								}
								else{
									
									var obj = {
										type: 'error',
										title : "",
										body : data.message,
										showCloseButton: true,
        								timeout: 2000
									};
									
									$scope.toasterNotification(obj);
									return;
								
								}
							});
						});
					});
				}
				
			};
		});
	});
}]);
