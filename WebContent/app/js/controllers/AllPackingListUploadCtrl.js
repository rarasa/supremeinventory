inspinia.lazy.controller("AllPackingListUploadCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify) {

	$timeout(function() {
		$scope.$apply(function() {
			
		    $scope.containerDetailsEntered = false;
			
			$scope.fileName != "";
			$scope.cleanExistingExcel = false ;
			
			$scope.singleStock = {};
			$scope.singleStock.oldStock = "1";
			
			$scope.packingList = {};
			$scope.packingList.oldStock = "1";
			$scope.packingList.tile = "0";
			
			$scope.packingList.materialListString = "";
			
			console.log($rootScope.supplierList);
			console.log($rootScope.materialList);
			
			console.log("PackingList ctrl entered");
			
			
			$scope.packingList.arrivalDate = moment().format("DD-MM-YYYY");
			
			
			$scope.validate = function(){
				var error = false;
				var errorObj = {
						templateUrl : 'views/common/notify.html',
						classes : 'alert-danger'
				};
				if(typeof $scope.packingList.containerId == "undefined" || $scope.packingList.containerId == ""){
		            errorObj.message = 'Select Container';
		            error = true;
				}
				else if(typeof $scope.packingList.oldStock == "undefined" || $scope.packingList.oldStock == ""){
		            errorObj.message = 'Enter Old Stock Value';
		            error = true;
				}
				else if(typeof $scope.packingList.tile == "undefined" || $scope.packingList.tile == ""){
		            errorObj.message = 'Enter Tile details Value';
		            error = true;
				}
				else if(typeof $scope.packingList.arrivalDate == "undefined" || $scope.packingList.arrivalDate == ""){
					console.log("ARRIVAL DATEEEE: ", $scope.packingList.arrivalDate);
		            errorObj.message = 'Enter Arrival Date';
		            error = true;
				}
				else if(typeof $scope.packingList.locationId == "undefined" || $scope.packingList.locationId == ""){
					console.log("YARD: ", $scope.packingList.locationId);
		            errorObj.message = 'Enter Yard Details';
		            error = true;
				}
				else if(document.getElementById("file-input").value == "") {
					errorObj.message = 'No File Chosen';
		            error = true;
				}
				
				if(error){
					notify(errorObj);
					return;
				}
			};
			
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			$scope.uploadPackingList = function() {
				
				/**
				 * Validate Form submit
				 */
				$scope.validate();
				
				/**
				 * Now make the ajax call 
				 */
				var form = $("#upload-packing-list-form")[0];
				var url = "../app/packinglistupload?"
											 +"containerId="+$scope.packingList.containerId
											 +"&oldStock="+$scope.packingList.oldStock
											 +"&tile="+$scope.packingList.tile
											 +"&locationId="+$scope.packingList.locationId
											 +"&containerCode="+$scope.packingList.containerCode
											 +"&arrivalDate="+moment($scope.packingList.arrivalDate, "DD-MM-YYYY").format("YYYY-MM-DD");
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							alert(data.message);
						} else {
							alert(data.message);
						}
					}
				});
			};
			
			
			$scope.validateSingleStockEntry = function(){
				var error = false;
				var errorObj = {
						templateUrl : 'views/common/notify.html',
						classes : 'alert-danger'
				};
				if(typeof $scope.singleStock.supplierId == "undefined" || $scope.singleStock.supplierId == ""){
		            errorObj.message = 'Enter Supplier Name';
		            error = true;
				}
				else if(typeof $scope.singleStock.materialId == "undefined" || $scope.singleStock.materialId == ""){
		            errorObj.message = 'Enter Material Name';
		            error = true;
				}
				else if(typeof $scope.singleStock.type == "undefined" || $scope.singleStock.type == ""){
		            errorObj.message = 'Enter Material Type';
		            error = true;
				}
				else if(typeof $scope.singleStock.thickness == "undefined" || $scope.singleStock.thickness == ""){
		            errorObj.message = 'Enter Material Thickness';
		            error = true;
				}
				else if(typeof $scope.singleStock.length == "undefined" || $scope.singleStock.length == ""){
		            errorObj.message = 'Enter Material Length';
		            error = true;
				}
				else if(typeof $scope.singleStock.height == "undefined" || $scope.singleStock.height == ""){
		            errorObj.message = 'Enter Material Height';
		            error = true;
				}
				else if(typeof $scope.singleStock.polished == "undefined" || $scope.singleStock.polished == ""){
		            errorObj.message = 'Enter Polished Value';
		            error = true;
				}
				else if(typeof $scope.singleStock.flamed == "undefined" || $scope.singleStock.flamed == ""){
		            errorObj.message = 'Enter Flamed Value';
		            error = true;
				}
				else if(typeof $scope.singleStock.oldStock == "undefined" || $scope.singleStock.oldStock == ""){
		            errorObj.message = 'Enter Old Stock Value';
		            error = true;
				}
				else if(typeof $scope.singleStock.arrivalDate == "undefined" || $scope.singleStock.arrivalDate == ""){
					console.log("ARRIVAL DATEEEE: ", $scope.singleStock.arrivalDate);
		            errorObj.message = 'Enter Arrival Date';
		            error = true;
				}
				else if(typeof $scope.singleStock.locationId == "undefined" || $scope.singleStock.locationId == ""){
					console.log("YARD: ", $scope.singleStock.yard);
		            errorObj.message = 'Enter Yard Details';
		            error = true;
				}
				
				if(error){
					notify(errorObj);
					return;
				}
			};
			
			$scope.uploadSinglePackingList = function() {
				
				/**
				 * Validate Form submit
				 */
				$scope.validateSingleStockEntry();
				
				/**
				 * Now make the ajax call 
				 */
				
				var obj = $scope.singleStock;
				
				console.log("singleStock: ", obj);
				
				return;
				
				
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
				$http.defaults.transformRequest = [ function(data) {
					return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
				}];

				var url = "../app/packinglistupload?action=singleStockUpload";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
						});
					});
				});
			};
			
			$scope.supplierChanged = function(containerId){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("$scope.supplierChanged accessed");
						console.log(containerId);
						console.log("$scope.containerDetails: ", $scope.containerDetails);
						for(var i = 0; i < $scope.containerDetails.length; i++){
							if($scope.containerDetails[i].containerId == containerId){
								
								console.log("$scope.containerDetails[i]: ", $scope.containerDetails[i]);
								$scope.packingList.supplier = $scope.containerDetails[i].supplier;
								$scope.packingList.supplierId = $scope.containerDetails[i].supplier.supplierId;
								$scope.packingList.materialList = $scope.containerDetails[i].materialList;
							}
						}
					});
				});
			};
		});
	});
}]);

