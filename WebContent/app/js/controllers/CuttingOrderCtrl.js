inspinia.lazy.controller("CuttingOrderCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'SweetAlert', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, SweetAlert, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			/**
			 * Add Cutting Order Block
			 */
		
			$scope.count = 0;
			$scope.totalSqMeters = 0;
		
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called", obj);
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };

			$scope.reInitializeSelectInputs = function(){
				
			};

			$scope.materialList = allMaterialList;
			$scope.allSmallCustomerList = allSmallCustomerList;
			$scope.allCuttingTypeList = allCuttingTypeList;
			$scope.manualCuttingTypeList = manualCuttingTypeList;
			$scope.machineCuttingTypeList = machineCuttingTypeList;
			$scope.error = false;
			
			$scope.tags = [];
			$scope.loadCuttingTypes = function($query) {
		      	return allCuttingTypeList.filter(function(country) {
		        	return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
		      	});
		  	};
		
			$scope.task = "";

			$scope.assignCustomerName = function(customerObj){
				
				if(customerObj.charAt(0) == "{"){
					customerObj = JSON.parse(customerObj);
					console.log(customerObj);
					$scope.cuttingOrder.customerId = customerObj.id;
					$scope.cuttingOrder.customerName = customerObj.name;
				}
				else{
					$scope.cuttingOrder.customerId = 0;
					$scope.cuttingOrder.customerName = customerObj;
				}
				
				console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
			};
			
			$scope.startSearch = function(term){
			    var results = [];
			    $scope.allSmallCustomerList.forEach(function(item){
			      if (typeof item.name != "undefined" && item.name != "" && item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
			        results.push(item);
			      }
			    });
		      	return results;
			};
			$scope.startMaterialSearch = function(term){
			    var results = [];
			    $scope.materialList.forEach(function(item){
			      if (typeof item.materialName != "undefined" && item.materialName != "" && item.materialName.toLowerCase().indexOf(term.toLowerCase()) > -1) {
			        results.push(item);
			      }
			    });
		      	return results;
			};
			$scope.startTaskSearch = function(term){
			    var results = [];
			    $scope.allCuttingTypeList.forEach(function(item){
			      if (typeof item.name != "undefined" && item.name != "" && item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
			        results.push(item);
			      }
			    });
		      	return results;
			};
			$scope.addTaskToMaterial = function(item, array, index){
				console.log("array: ", array);
				$timeout(function() {
					$scope.$apply(function() {
						
						if(array.length > 0){
							console.log("array length > 0", item);
							
							var flag = false;
							for(var i =0; i < array.length; i++){
								if(array[i].id != item.id){
									flag = true;
								}
								else{
									
									flag = false;
									var obj = {
										type: 'error',
										title : array[i].name+" already added.",
										body : "Please choose another cutting type.",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									item = "";
									$("#task-typeahead-"+index).val("");
									break;
								}
							}
							
							if(flag){
								array.push(item);
								item = "";
								$("#task-typeahead-"+index).val("");
							}
							
						}
						else{
							console.log("array length < 0", item);
							array.push(item);
							item = "";
							$("#task-typeahead-"+index).val("");
						}
						
					});
				});
			};
			$scope.addCustomerToOrder = function(item, element) {
				console.log("customerItem: ", item);
				$timeout(function() {
					$scope.$apply(function() {
						$scope.cuttingOrder.customerId = item.id;
						$scope.cuttingOrder.customerName = item.name;
					});
				});
			};
			$scope.addMaterialToOrder = function(item, coList) {
				console.log("materialItem: ",item);
				$timeout(function() {
					$scope.$apply(function() {
						coList.materialId = item.materialId;
						coList.materialName = item.materialName;
					});
				});
			};
			
			$scope.addNewCustomer = function () {
				console.log("addCustomer chosen")
				$scope.addCustomer = true;
				$scope.editCustomer = false;
				
		        var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/Customer-Modal.html',
		            controller: AddNewCustomerCtrl,
		            windowClass: "animated fadeInDown",
					scope: $scope,
					size: 'lg',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						loadPlugin: function ($ocLazyLoad) {
							return $ocLazyLoad.load([
		                         {
		                            insertBefore: '#loadBefore',
		                            name: 'toaster',
		                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
		                         },
		                        {
		                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
		                        }
		                     ]);
						}
					}
		        });
		    };

			$scope.addNewMaterial = function () {
				console.log("addMaterial chosen")
				$scope.addMaterial = true;
				$scope.editMaterial = false;
				
		        var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/Material-Modal.html',
		            controller: AddNewMaterialCtrl,
		            windowClass: "animated fadeInDown",
					scope: $scope,
					size: 'md',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						loadPlugin: function ($ocLazyLoad) {
							return $ocLazyLoad.load([
		                         {
		                            insertBefore: '#loadBefore',
		                            name: 'toaster',
		                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
		                         },
		                        {
		                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
		                        }
		                     ]);
						}
					}
		        });
		    };
			
			$scope.taskDetailsObject = {
				unit : '',
				cutToSize : 0,
				length : 0,
				height : 0,
				quantity : 0,
				nos : 0
			};
			
			$scope.taskObject = {
				taskList : [],
				taskDetails : [],
			}
			
			$scope.cuttingOrderCuttingListObject = {
				materialId: '',
				thickness: '',
				tasks: [],
			};
			
			$scope.cuttingOrder = {
				cuttingOrderNumber: 0,
				customerName : '',
				customerId: '',
				customer: '',
				date: moment().format("DD-MM-YYYY"),
				deliveryDate: moment().add('days', 1).format("DD-MM-YYYY"),
				cuttingList: [],
				materialCount : 0,
				totalOutputMtSquare : 0,
				totalOutputLinearMeter : 0
			};
			
			$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
			$scope.cuttingOrderCuttingListObject.tasks.push($scope.taskObject);
//			$scope.cuttingOrderCuttingListObject.tasks.push($scope.taskObject);

			
			$scope.cuttingOrder.cuttingList.push($scope.cuttingOrderCuttingListObject);
			
			/*setTimeout(function () {
						$("#Material-1").addClass("active");
						$("#MaterialTabPanel-1").parent().addClass("active");
						$("#Task-1").addClass("active");
						$("#TaskTabPanel-1").parent().addClass("active");
			}, 1000)*/
			$scope.removeAt = function(type, array, index){
				
				console.log("type: ", type);
				console.log("array: ", array);
				console.log("index: ", index);
				
				array.splice(index, 1);
				
				console.log("Array: ", array);
			};
			
			$scope.addMaterial = function(coList){
				
				console.log(coList);
				
				$scope.taskDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0
				};
				
				$scope.taskObject = {
					taskList : [],
					taskDetails : []
				}
				
				$scope.cuttingOrderCuttingListObject = {
					materialId: '',
					thickness: '',
					tasks: [],
				};
				
				$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
				$scope.cuttingOrderCuttingListObject.tasks.push($scope.taskObject);
				
				coList.push($scope.cuttingOrderCuttingListObject);
				/*var lastIndex = coList.length - 1;
				
				for(var i = 0; i <= lastIndex; i++){
					if(i == lastIndex){
						coList[i].class = 'active';
					}
					else{
						coList[i].class = '';
					}
				}*/
			}
			
			$scope.addTaskInMaterial = function(taskArray){
				$scope.taskDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0
				};
				
				$scope.taskObject = {
					taskList : [],
					taskDetails : []
				}
				
				$scope.cuttingOrderCuttingListObject = {
					materialId: '',
					thickness: '',
					tasks: [],
				};
				
				$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
				taskArray.push($scope.taskObject);
				
			};
			
			$scope.addTaskDetailsRow = function(taskDetailsArray){
				
				$scope.cuttingOrderCuttingListDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0,
				};
				taskDetailsArray.push($scope.cuttingOrderCuttingListDetailsObject);
				console.log($scope.cuttingOrder);
			};
			
			$scope.calculateTotalQty = function(){
				$scope.cuttingOrder.totalOutputMtSquare = 0;
				$scope.cuttingOrder.totalOutputLinearMeter = 0;
				for(var i = 0; i < $scope.cuttingOrder.cuttingList.length; i++){
					$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter = 0;
					$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare = 0;
					
					for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].tasks.length; j++){
						for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length; k++){
							
							if($scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == 1){
								$scope.cuttingOrder.totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
								$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
							}
							
							if($scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == 2){
								$scope.cuttingOrder.totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
								$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
							}
						}
					}
				}
			}
			
			$scope.getCuttingTypeList = function(){
				var url = "../app/cutting?action=getAllCuttingTypeList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.allCuttingTypeList = data.data;
							console.log("laaa: ",$scope.allCuttingTypeList);
							allCuttingTypeList = $scope.allCuttingTypeList;
							allSmallCuttingTypeList = $scope.allCuttingTypeList;
						});
					});
				});
			};
			
			$scope.addNewCuttingTyps = function () {
				console.log("addNewCuttingTyps chosen")
				$scope.addCuttingMode = true;
				
		        var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/Cutting-Type-Modal.html',
		            controller: AddNewCuttingTypeCtrl,
		            windowClass: "animated fadeInDown",
					scope: $scope,
					size: 'sm',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						loadPlugin: function ($ocLazyLoad) {
							return $ocLazyLoad.load([
		                         {
		                            insertBefore: '#loadBefore',
		                            name: 'toaster',
		                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
		                         },
		                        {
		                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
		                        }
		                     ]);
						}
					}
		        });
		    };

			$scope.calculateQty = function(cListObj){
				//pcs : quantity = nos 
				//LM = length*no of pcs / 100
				//Mt is l*h*pcs/10000
				
				if(cListObj.unit == 1){
					//LM
					cListObj.quantity = cListObj.length * cListObj.nos / 100;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 2){
					//Mt.Sq.
					cListObj.quantity = cListObj.length * cListObj.height * cListObj.nos / 10000;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 3){
					//Pcs
					cListObj.quantity = cListObj.nos;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
			}
			
			
			
			$scope.validate = function(){
				
				console.log("Validate $scope.cuttingOrder: ", $scope.cuttingOrder);
				
				console.log("$scope.cuttingOrder.customerId: ", $scope.cuttingOrder.customerId);
				if(typeof $scope.cuttingOrder.customerId == "undefined"){
					
					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Customer.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					
					return true;
					
				}
				else if(typeof $scope.cuttingOrder.date == "undefined" || $scope.cuttingOrder.date == ""){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Date.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.deliveryDate == "undefined" || $scope.cuttingOrder.deliveryDate == ""){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Delivery Date.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList == "undefined" || $scope.cuttingOrder.cuttingList.length <= 0){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please Material Details.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList != "undefined" && $scope.cuttingOrder.cuttingList.length > 0){
					
					for(var i =0; i < $scope.cuttingOrder.cuttingList.length; i++){
						
						if(typeof $scope.cuttingOrder.cuttingList[i].materialId == "undefined" || $scope.cuttingOrder.cuttingList[i].materialId == ""){
							
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Material in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].materialThickness == "undefined" || $scope.cuttingOrder.cuttingList[i].materialThickness == ""){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Thickness in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].tasks == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks.length <= 0){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Task List in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						else if(typeof $scope.cuttingOrder.cuttingList[i].tasks != "undefined" && $scope.cuttingOrder.cuttingList[i].tasks.length > 0){
							
							for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].tasks.length; j++){
								
								if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskList == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskList.length <= 0){
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Cutting Tasks in Material Details in Task "+parseInt(j+1)+".",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
								}
								else{
									for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskList.length; k++){
										
										if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskList[k].id == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskList[k].id == ""){
											var obj = {
												type: 'error',
												title : "Missing Data",
												body : "Please enter Cutting Tasks in Material "+parseInt(i+1)+" in Task "+parseInt(j+1)+".",
												showCloseButton: true,
												timeout: 6000
											};
											$scope.toasterNotification(obj);
											return true;
										}
										
									}
								}
								
								if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length <= 0){
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Slab Details in Material "+parseInt(i+1)+", Task "+parseInt(j+1)+".",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
								}
								else{
									for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length; k++){
										
										if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == ""){
											var obj = {
												type: 'error',
												title : "Missing Data",
												body : "Please enter Unit in slab details in Material "+parseInt(i+1)+" Task "+parseInt(j+1)+".",
												showCloseButton: true,
												timeout: 6000
											};
											$scope.toasterNotification(obj);
											return true;
										}
										
									}
								}
							}
						}
					}
				}
			};
			
			$scope.submitCuttingOrder = function(){
				console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);

				
				var error = $scope.validate();
				
				console.log("error: ", error);
				
				console.log("$scope.error: ", $scope.error);
				
				
				$scope.finalObjectBeforeSubmit = {};
				
				$scope.finalObjectBeforeSubmit.customerId = $scope.cuttingOrder.customerId;
				$scope.finalObjectBeforeSubmit.customerName = $scope.cuttingOrder.customerName;
				$scope.finalObjectBeforeSubmit.cuttingOrderNumber = $scope.cuttingOrder.cuttingOrderNumber;
				$scope.finalObjectBeforeSubmit.date = $scope.cuttingOrder.date;
				$scope.finalObjectBeforeSubmit.deliveryDate = $scope.cuttingOrder.deliveryDate;
				$scope.finalObjectBeforeSubmit.materialCount = $scope.cuttingOrder.materialCount;
				$scope.finalObjectBeforeSubmit.totalOutputLinearMeter = $scope.cuttingOrder.totalOutputLinearMeter;
				$scope.finalObjectBeforeSubmit.totalOutputMtSquare = $scope.cuttingOrder.totalOutputMtSquare;
				
				$scope.finalObjectBeforeSubmit.cuttingList = [];
				
				for(var i =0; i < $scope.cuttingOrder.cuttingList.length; i++){
					
					var cuttingListObj = {};
					
					cuttingListObj.materialId = $scope.cuttingOrder.cuttingList[i].materialId;
					cuttingListObj.materialThickness = $scope.cuttingOrder.cuttingList[i].materialThickness;
					cuttingListObj.thickness = $scope.cuttingOrder.cuttingList[i].thickness;
					cuttingListObj.totalOutputLinearMeter = $scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter;
					cuttingListObj.totalOutputMtSquare = $scope.cuttingOrder.cuttingList[i].totalOutputMtSquare;
					
					cuttingListObj.tasks = [];
							
					for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].tasks.length; j++){
						
						
						var taskDetailsObj = {};
						taskDetailsObj.number = (j+1);
						taskDetailsObj.taskList = [];
						taskDetailsObj.taskDetails = [];
						
						for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskList.length; k++){
							taskDetailsObj.taskList.push($scope.cuttingOrder.cuttingList[i].tasks[j].taskList[k].id);
						}
						
						for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length; k++){
							
							var taskDetSmallObj = {};
							taskDetSmallObj.unit = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit;
							taskDetSmallObj.length = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].length;
							taskDetSmallObj.height = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].height;
							taskDetSmallObj.nos = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].nos;
							taskDetSmallObj.quantity = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
							taskDetSmallObj.cutToSize = $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].cutToSize;
							
							taskDetailsObj.taskDetails.push(taskDetSmallObj);
						}
						
						cuttingListObj.tasks.push(taskDetailsObj);
						
					}
					
					$scope.finalObjectBeforeSubmit.cuttingList.push(cuttingListObj);
				}
				
				console.log("ORIGINAL OBJ: ", $scope.cuttingOrder);
				console.log("BEFORE SUBMIT: ", $scope.finalObjectBeforeSubmit);
				
				console.log("BEFORE SUBMIT String: ", JSON.stringify($scope.finalObjectBeforeSubmit));
				
				if(error){
					return;
				}
				else{
					
					$scope.calculateTotalQty();
					
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
					$http.defaults.transformRequest = [ function(data) {
						return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
					}];
					var url = "../app/cutting?action=addCuttingOrders";
					console.log(url);
					$http.post(url, $scope.finalObjectBeforeSubmit, {}).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
	
								if(data.success){
									
									SweetAlert.swal({
										title: "Done!",
							            text: "Successfully updated Cutting Order. Taking you to Order Print Page.",
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
//						                	window.location.reload();
						                }
						            });
								}
								else{
									SweetAlert.swal({
									    title: "Oops!",
									    text: "Your Order was not saved!",
									    type: "warning",
									    showCancelButton: true,
									    confirmButtonColor: "#DD6B55",
									    confirmButtonText: "Yes, delete it!",
									    closeOnConfirm: true}
									);
								}
							});
						});
					});
				}
			};
			
			$scope.getCustomerList = function(){
				var url = "../app/customer?action=getAllCustomerList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.customerList = data;
							console.log("data: ", data);
							allSmallCustomerList = data;
							console.log($scope.customerList);
						});
					});
				});
			};
			
			$scope.viewSlabDetails = function(id, index, tag, array){
				
				console.log("viewSlabDetails called");
				console.log(array);
				htmltag = tag;
				console.log("this: ", tag);
				console.log(id);
				console.log("INDEX: ", index);
				
				var error = false;
				var duplicateMessage = "";
				
				console.log(array);
				if(typeof array != "undefined" && array.length > 0){
					for(var i = 0; i < array.length; i++){
						if(i != index && array[i].inventory.barcodeString == id){
							duplicateMessage = "You have already scanned " + id;
							error = true;
							break;
						}
					}
				}
				
				if(error){
					
					var obj = {
						type: 'error',
						title : "Duplicate slab error!",
						body : duplicateMessage,
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					
					return;
				}
				
				var url = "../app/inventory?action=readBarcode&barcodeString="+id;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);

								var arrayObj = {};
								
								arrayObj.inventory = {};
								
								arrayObj.inventory = data.data;
								
								arrayObj.name = data.data.material.name;
								
								if(typeof data.data.type != "undefined"
										&& data.data.type == "TILE"){
									
									data.data.originalTileCount = data.data.tileCount;
									data.data.actualSqMetre = parseFloat((data.data.actualLength * data.data.actualHeight) / 10000) * data.data.tileCount;
									$scope.count += data.data.tileCount;
									$scope.totalSqMeters += data.data.actualSqMetre;
									
									$scope.cuttingOrder.slabCount = $scope.count;
									$scope.cuttingOrder.totalMtSquare = $scope.totalSqMeters;
								}
								else{
									data.data.actualSqMetre = parseFloat((data.data.actualLength * data.data.actualHeight) / 10000);
									$scope.count += 1;
									$scope.totalSqMeters += data.data.actualSqMetre;
									
									$scope.cuttingOrder.slabCount = $scope.count;
									$scope.cuttingOrder.totalMtSquare = $scope.totalSqMeters;
								}
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								arrayObj.inventory = data.data;
								
								arrayObj.id = 0;
								
								$scope.cuttingOrder.cart[index] = arrayObj;
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								
								console.log($scope.cuttingOrder);
								
								$scope.addRow();
							}
							else{
								
								var obj = {
									type: 'error',
									title : "Error!",
									body : data.message,
									showCloseButton: true,
									timeout: 6000
								};
								$scope.toasterNotification(obj);
								
							}
							
							
						});
					});
				});
			};
			
			if($state.current.name == "cutting.scanCuttingOrder"){
				
				$scope.cart = [];
				$scope.materialInCuttingOrderList = [];
				$scope.fetchCuttingOrder = function(index){
					var url = "../app/cutting?action=fetchCuttingOrderWithOrderId&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								$scope.cuttingOrder = data.data;
								console.log($scope.cuttingOrder);
								$scope.indexRow = 1;
								
								/*for(var i = 0; i < $scope.cuttingOrder.cuttingOrderList.length; i++){
									
									$scope.materialInCuttingOrderList.push($scope.cuttingOrder.cuttingOrderList[i].material);
									for(var j = 0; j < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks.length; j++){
										for(var k = 0; k < $scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType.length; k++){
											$scope.cuttingOrder.cuttingOrderList[i].cuttingOrderTasks[j].cuttingOrderTaskType[k].indexRow = $scope.indexRow;
											$scope.indexRow += 1;
										}
									}
								}*/
								
								console.log("$scope.materialInCuttingOrderList: ", $scope.materialInCuttingOrderList);
								
								if(typeof $scope.cuttingOrder.cart != "undefined" && $scope.cuttingOrder.cart.length <= 0){
									$scope.addRow();
								}
								else{
									$scope.count = 0;
									$scope.totalSqMeters = 0;
									for(var i = 0; i < $scope.cuttingOrder.cart.length; i++){
										$scope.cuttingOrder.cart[i].name = $scope.cuttingOrder.cart[i].material.name;
										$scope.count = $scope.cuttingOrder.slabCount;
										$scope.totalSqMeters = $scope.cuttingOrder.totalMtSquare;
										
										if($scope.cuttingOrder.cart[i].inventory.tileCount != $scope.cuttingOrder.cart[i].tileCount){
											$scope.cuttingOrder.cart[i].inventory.tileCount = $scope.cuttingOrder.cart[i].tileCount;
										}
										
									}
									$scope.addRow();
								}
								
							});
						});
					});
				};
				
				if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCuttingOrder(index);
				}
				else {
					var webUrlArray = window.location.hash.split("/");
					var index = webUrlArray[webUrlArray.length - 1];
					$scope.fetchCuttingOrder(index);
				}
				
				
				
				$scope.addRow = function(){
					console.log("addRowFlag entered");

					var material = {
						name: ""
					}
					var inventory = {
						barcodeString: '',
						name: '',
						actualLength: 0,
						actualHeight: 0,
						thickness: 0,
						slabId: 0,
						id: 0,
						actualSqMetre: 0,
						type: ""
					};		
					$scope.cuttingOrderObject = {
						id: 0,
						inventory: inventory,
						material : material
					};
	
					var idString = "";
					var addRowFlag = true;
					for(var i=0; i < $scope.cuttingOrder.cart.length; i++){
						console.log("$scope.cuttingOrder.cart[i].barcodeString: ", $scope.cuttingOrder.cart[i].barcodeString);
						console.log("$scope.cuttingOrder.cart[i]: ", $scope.cuttingOrder.cart[i]);
						if(typeof $scope.cuttingOrder.cart[i].inventory.barcodeString == "undefined" || $scope.cuttingOrder.cart[i].inventory.barcodeString == ""){
							//one empty row exists so do nothing
							addRowFlag = false;
							idString = "barcodeStringText"+i;
							break;
						}
						else{
							addRowFlag = true;
							idString = "barcodeStringText"+$scope.cuttingOrder.cart.length;
						}
					}
					
					console.log("addrowFlag: ", addRowFlag)
					
					console.log("idString: "+ idString)
					if(addRowFlag){
						console.log("addrowFlag: ", addRowFlag);
						$scope.cuttingOrder.cart.push($scope.cuttingOrderObject);
						
						$timeout(function() {
							$scope.$apply(function() {
								console.log("idString 2: "+ idString)
								console.log($scope.cuttingOrder);
						      // have to do this in a $timemout because
						      // we want it to happen after the view is updated
						      // with the newly added input
						      angular
						        .element(document.querySelectorAll('#'+idString))
						        .eq(-1)[0]
						        .focus()
						      ;
							});
					    }, 0);
					}
				};
				
				function checkForDuplicates(array) {
				  return new Set(array).size !== array.length;
				}
				
				$scope.validateTileCount = function(cartObj){
					
					if(cartObj.tileCount > cartObj.originalTileCount){
						var obj = {
							type: 'error',
							title : "Oops!",
							body : 'Tile Count cannot be more than '+ cartObj.originalTileCount,
							showCloseButton: true,
							timeout: 6000
						};
						$scope.toasterNotification(obj);
						
						cartObj.tileCount = cartObj.originalTileCount;
						
						return;
					}
					
					console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
					
					$scope.count=0;
					for(var i = 0; i < $scope.cuttingOrder.cart.length; i++){
						console.log("$scope.cuttingOrder.cart[i]: ", $scope.cuttingOrder.cart[i]);
						
						if(typeof $scope.cuttingOrder.cart[i].inventory.barcodeString != "undefined" && $scope.cuttingOrder.cart[i].inventory.barcodeString != ""){
							
							if($scope.cuttingOrder.cart[i].inventory.type== "TILE"){
								$scope.count += parseInt($scope.cuttingOrder.cart[i].inventory.tileCount);
							}
							else{
								$scope.count += 1;
							}
							
							
						}
						
					}
					
				};
				
				$scope.updateEditedCuttingOrder = function(){
					
					console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
					if(typeof $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == "undefined" 
						|| $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == ""
						|| $scope.cuttingOrder.cart[$scope.cuttingOrder.cart.length - 1].id == 0){
						
						$scope.cuttingOrder.cart.splice($scope.cuttingOrder.cart.length - 1, 1);
					}
					
					var obj = {};
					
					obj.orderId = $scope.cuttingOrder.id;
					obj.slabCount = $scope.cuttingOrder.slabCount;
					obj.totalMtSquare = $scope.cuttingOrder.totalMtSquare;
					obj.cart = [];
					
					console.log("$scope.cuttingOrder.cart: ", $scope.cuttingOrder.cart);
					
					for(var i =0; i < $scope.cuttingOrder.cart.length; i++){
						
						var inventoryObj = {
							id: $scope.cuttingOrder.cart[i].inventory.id,
							type: $scope.cuttingOrder.cart[i].inventory.type
						};
						
						var cartObj = {
							id: $scope.cuttingOrder.cart[i].id,
							inventory : inventoryObj
						};
						
						if(cartObj.inventory.type == "TILE"){
							cartObj.inventory.tileCount = $scope.cuttingOrder.cart[i].inventory.tileCount;
						}
						obj.cart.push(cartObj);
					}
					
					if(checkForDuplicates(obj.cart)){
						console.log("Duplicate value in Scan list");
						
						var obj = {
							type: 'error',
							title : "Error!",
							body : 'Remove Duplicate Values from Scan List',
							showCloseButton: true,
							timeout: 6000
						};
						$scope.toasterNotification(obj);
						
						return;
					}
					
					console.log("BEFORE SUBMIT: ", obj);
					
					var url = "../app/cutting?action=addScannedSlabsToCuttingOrder";
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
					$http.defaults.transformRequest = [ function(data) {
						return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
					}];
					$http.post(url, obj, {}).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								
								if(data.success){
									SweetAlert.swal({
										title: "Done!",
							            text: data.message,
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	$state.go("sales.salesInvoice");
						                }
						            });
								}
								else{
									SweetAlert.swal({
										title: "Error!",
							            text: data.message,
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	window.location.reload();
						                }
						            });
								}
							});
						});
					});
				};
			}
			
			if($state.current.name == "cutting.editCuttingOrder"){
				
				$scope.addToCuttingOrder = function(slab, addToCuttingOrder, index){
					
					if(addToCuttingOrder == 1 && !$scope.editOrderList.addToCuttingOrderArray.includes(slab.id)){
						$scope.editOrderList.addToCuttingOrderArray.push(slab.id);
					}
					else if(addToCuttingOrder == 0 && $scope.editOrderList.addToCuttingOrderArray.includes(slab.id)){
						var indexOfSlabId = $scope.editOrderList.addToCuttingOrderArray.indexOf(slab.id);
						$scope.editOrderList.addToCuttingOrderArray.splice(indexOfSlabId, 1);
					}
					
					$scope.inputTotalMeterSquare += slab.actualLength * slab.actualHeight / 10000;
					
					console.log($scope.editOrderList);
				};
				
				$scope.fetchCurrentOrder = function(index){
					var url = "../app/cutting?action=fetchCuttingOrderWithOrderId&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log("First: ", data);
								$scope.editOrderList = data.data;
								if(typeof $scope.editOrderList.companyPickup == "undefined" || $scope.editOrderList.companyPickup == 0 || $scope.editOrderList.companyPickup == ""){
									$scope.editOrderList.companyPickup = 0;
								}
								
								if($scope.editOrderList.customer.VATRegistration == "1"){
									console.log("VAT REG is 1 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(1);
								}
								else{
									console.log("VAT REG is 0 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(0);
								}
								if(typeof $scope.editOrderList.customer.VATRegistrationDate != "undefined" && $scope.editOrderList.customer.VATRegistrationDate != ""){
									$scope.editOrderList.customer.VATRegistrationDate = moment($scope.editOrderList.customer.VATRegistrationDate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
									$scope.editOrderList.LPODate = moment($scope.editOrderList.LPODate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								for(var i = 0; i < $scope.editOrderList.cart.length; i++){
									if($scope.editOrderList.cart[i].addToCuttingOrder){
										$scope.editOrderList.cart[i].addToCuttingOrder = 1;
									}
									else{
										$scope.editOrderList.cart[i].addToCuttingOrder = 0;
									}
								}
								
								$scope.editOrderList.addToCuttingOrderArray = [];
								console.log("$scope.editOrderList: ", $scope.editOrderList);
								
								
							});
						});
					});
				};
				
				if($stateParams.order != null &&  typeof $stateParams.order != "undefined" && $stateParams.order != ""){
					$scope.editOrderList = order;
					console.log("$state.order: ", $scope.editOrderList);
				}
				else if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCurrentOrder(index);
				}
				
				$scope.deleteCartRow = function(order, cart, index){
					console.log(cart);
					var url = "../app/order?action=deleteCartRow&cartId=" + cart.id;
					console.log(url);
					$http.post(url).success(function(data, status, headers, config) {
						console.log("deleteCartRow result: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);

								if(data.success){
									
									$scope.editOrderList.cart.splice(index, 1);
									
									
									$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
									$scope.editOrderList.totalMtSquare = 0;
									for(var j = 0; j < $scope.editOrderList.cart.length; j++){
										$scope.editOrderList.totalMtSquare += $scope.editOrderList.cart[j].actualSqMetre;
									}
									
									if($scope.editOrderList.cart.length <= 0){
										/**
										 * All cart rows deleted
										 * delete order as well
										 */
										
										successObj.message = "Cart has been cleared";
										notify(successObj);
										console.log("All orders deleted");
										
										
										var url = "../app/order?action=deleteOrder&orderId=" + order.id;
										$http.post(url).success(function(data, status, headers, config) {
											console.log("fetched data: ",data);
											$timeout(function() {
												$scope.$apply(function() {
													
													if(data.success){
														console.log(data);
														$scope.salesOrderList.splice(index, 1);
														successObj.message = "Order has been deleted";
														notify(successObj);
														
														$state.go("sales.salesInvoice");
														
														for(var i = 0; i < $scope.salesOrderList.length; i++){
															$scope.salesOrderList[i].slabCount = $scope.salesOrderList[i].cart.length;
															$scope.salesOrderList[i].totalMtSquare = 0;
															for(var j = 0; j < $scope.salesOrderList[i].cart.length; j++){
																$scope.salesOrderList[i].totalMtSquare += $scope.salesOrderList[i].cart[j].actualSqMetre;
															}
														}
														
													}
													else{
														errorObj.message = data.message;
														notify(errorObj);
													}
													
												});
											});
										});
									}
									else{
										successObj.message = "Cart row deleted successfully.";
										notify(successObj);
									}
								}
								else{
									errorObj.message = data.message;
									notify(errorObj);
								}
							});
						});
					});
				};
				
				$scope.validateEditedOrder = function(){
					
					console.log("$scope.editOrderList: ", $scope.editOrderList);
					$scope.error = false;
					if(typeof $scope.editOrderList.customer.customerName == "undefined" || $scope.editOrderList.customer.customerName == ""){
						errorObj.message = 'Add Customer Name';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					
					if(typeof $scope.editOrderList.customer.emirate == "undefined" || $scope.editOrderList.customer.emirate == ""){
						errorObj.message = 'Please fill Address.';
						notify(errorObj);
						return;
					}
					else{
						$scope.editOrderList.emirate = $scope.editOrderList.emirate;
					}
					
					if(typeof $scope.editOrderList.companyPickup != "undefined" 
						&& $scope.editOrderList.companyPickup == "0" 
						&& (typeof $scope.editOrderList.vehicleNumber == "undefined" || $scope.editOrderList.vehicleNumber == "")){
						errorObj.message = 'Please fill Vehicle Number.';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					else{
						$scope.editOrderList.vehicleNumber = $scope.editOrderList.vehicleNumber;
					}
					
					if(typeof $scope.editOrderList.customer.VATRegistration != "undefined" 
						&& $scope.editOrderList.customer.VATRegistration == "1"){
							
						if(typeof $scope.editOrderList.customer.VATRegistrationDate == "undefined" || $scope.editOrderList.customer.VATRegistrationDate == ""){
							errorObj.message = 'Please Add VAT Registration Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						if(typeof $scope.editOrderList.customer.trnNumber == "undefined" || $scope.editOrderList.customer.trnNumber == ""){
							errorObj.message = 'Please add TRN NUmber.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						
					}
					else{
						$scope.editOrderList.customer.trnNumber = $scope.editOrderList.customer.trnNumber;
						$scope.editOrderList.customer.VATRegistrationDate = $scope.editOrderList.customer.VATRegistrationDate;
						$scope.editOrderList.customer.VATRegistration = $scope.editOrderList.customer.VATRegistration;
					}
					
					if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
						if(typeof $scope.editOrderList.LPODate == "undefined" || $scope.editOrderList.LPODate == ""){
							errorObj.message = 'Please fill LPO Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
						if(typeof $scope.editOrderList.LPONumber == "undefined" || $scope.editOrderList.LPONumber == ""){
							errorObj.message = 'Please fill LPO Number.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					
					if(!$scope.error){
						for(var i = 0; i < ($scope.editOrderList.cart.length); i++){
							if(typeof $scope.editOrderList.cart[i].barcodeString == "undefined" || $scope.editOrderList.cart[i].barcodeString == ""){
								errorObj.message = 'Code does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							else if(typeof $scope.editOrderList.cart[i].id == "undefined" || $scope.editOrderList.cart[i].id == ""){
								errorObj.message = 'Slab Id does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							
							if($scope.editOrderList.cart[i].tile && $scope.editOrderList.cart[i].type == "TILE"){
								if($scope.editOrderList.cart[i].tileCount > $scope.editOrderList.cart[i].originalTileCount){
									errorObj.message = 'Count more than limit of '+ $scope.editOrderList.cart[i].originalTileCount;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
								else if($scope.editOrderList.cart[i].tileCount < 1){
									errorObj.message = 'Minimum 1 tile needs to be there in '+$scope.editOrderList.cart[i].barcodeString;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
							}
						}
					}
					if($scope.error){
						return;
					}
				};
				
				/*$scope.changeAddressDetailsOfCustomer = function(custDetId){
					
					console.log($scope.editOrderList);
					
					for(var i = 0; i < $scope.editOrderList.customerDetails.length; i++){
						if($scope.editOrderList.customerDetails[i].id == custDetId){
							if(typeof $scope.editOrderList.customerDetails[i].emirate != "undefined" && $scope.editOrderList.customerDetails[i].emirate != ""){
								$scope.editOrderList.emirate = $scope.editOrderList.customerDetails[i].emirate;
							}
							if(typeof $scope.editOrderList.customerDetails[i].phone != "undefined" && $scope.editOrderList.customerDetails[i].phone != ""){
								$scope.editOrderList.phone = $scope.editOrderList.customerDetails[i].phone;
							}
							if(typeof $scope.editOrderList.customerDetails[i].vehicleNumber != "undefined" && $scope.editOrderList.customerDetails[i].vehicleNumber != ""){
								$scope.editOrderList.vehicleNumber = $scope.editOrderList.customerDetails[i].vehicleNumber;
							}
						}
					}
					
					console.log($scope.editOrderList);
				};*/
				
				

				$scope.saveEditedOrder = function(approveOrder, order){
					
					console.log("$scope.editOrder_form: ", $scope.editOrder_form);
					
					if ($scope.editOrder_form.$valid) {
						
						console.log("approveOrder: ", approveOrder);
						console.log("Order: ", order);
						
						$scope.validateEditedOrder();
						
						if($scope.error){
							//alert("Error is still there");
							return;
						}
						
						console.log("Passed validation");
						
						console.log("$scope.editOrderList: ", $scope.editOrderList);
						
						var obj = {};
						obj.id = order.id;
						obj.customerName = order.customer.customerName;
						obj.approveOrder = approveOrder;
						obj.userId = me.id;
						obj.slabCount = order.slabCount;
						obj.totalMtSquare = order.totalMtSquare;
						obj.emirate = order.customer.emirate;
						obj.vehicleNumber = order.vehicleNumber;
						obj.companyPickup = order.companyPickup;
						obj.narration = order.narration;

						obj.VATRegistration = order.customer.VATRegistration;
						obj.VATRegistrationDate = order.customer.VATRegistrationDate;
						obj.trnNumber = order.customer.trnNumber;

						obj.phone = order.customer.phone;
						obj.userId = $scope.user.id;
						
						
						
						if($scope.editOrderList.addToCuttingOrderArray.length > 0){
							obj.inputTotalMeterSquare = $scope.inputTotalMeterSquare;
							obj.addToCuttingOrderArray = order.addToCuttingOrderArray
							obj.hasCuttingOrders = 1;
						}
						else{
							obj.hasCuttingOrders = 0;
						}
						
						if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						
						obj.cart = [];
	
						for(var i = 0; i < $scope.editOrderList.cart.length; i++){
							var cartObj = {};
							cartObj.id = $scope.editOrderList.cart[i].id;
							cartObj.inventoryId = $scope.editOrderList.cart[i].inventoryId;
							cartObj.actualLength = $scope.editOrderList.cart[i].actualLength;
							cartObj.actualHeight = $scope.editOrderList.cart[i].actualHeight;
							cartObj.thickness = $scope.editOrderList.cart[i].thickness;
							cartObj.type = $scope.editOrderList.cart[i].type;
							
							if(cartObj.type == 'TILE'){
								cartObj.tileCount = $scope.editOrderList.cart[i].tileCount;
								cartObj.actualSqMetre = $scope.editOrderList.cart[i].actualLength * $scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].tileCount / 10000;
							}
							else{
								cartObj.actualSqMetre = $scope.editOrderList.cart[i].actualLength * $scope.editOrderList.cart[i].actualHeight / 10000;
								
							}
							
							obj.cart.push(cartObj);
						}
						
						
						console.log("OBJ: ", obj);
						
//						return;
	
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
						$http.defaults.transformRequest = [ function(data) {
							return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
						}];
	
						var url = "../app/order?action=editExistingPurchaseOrder";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
	
									if(data.success){
										successObj.message = "Order saved successfully";
										notify(successObj);
										
										/**
										 * Call Download PDF function
										 */
										
										if(obj.hasCuttingOrders == 1 && data.cuttingOrderId != 0){
											$state.go("cutting.editExistingHalfCuttingOrder", {
												id : data.cuttingOrderId,
											});
										}
										else{
											$state.go("sales.salesInvoice");
										}
										
									}
									else{
										errorObj.message = "Order could not be saved";
										notify(errorObj);
									}
								});
							});
						});
					}
					else{
						$scope.editOrder_form.submitted = true;
					}
				};
				
				
				$scope.calculateTotalSqMetersOnEdit = function(slab){
					$scope.editOrderList.totalMtSquare = 0;
					$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
					console.log(slab);
					
					if(slab.tile){
						console.log("slab.tile is true: ", slab);
						if(slab.tileCount > slab.originalTileCount){
							errorObj.message = 'Count more than limit of '+ slab.originalTileCount;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else if(slab.tileCount < 1){
							errorObj.message = 'Minimum 1 tile needs to be there in '+slab.barcodeString;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else{
							console.log(slab);
							$scope.count = 0;
							$scope.totalSqMeters = 0;
							
							$scope.totalSqMeters += parseFloat(slab.actualLength * slab.actualHeight / 10000) * parseInt(slab.tileCount);
							slab.actualSqMetre = ((slab.actualLength * slab.actualHeight) / 10000) * slab.tileCount;
							
							
							$scope.editOrderList.totalMtSquare = 0;
							$scope.editOrderList.slabCount = 0;
							for(var i=0; i < $scope.editOrderList.cart.length; i++){
								
								
								if($scope.editOrderList.cart[i].tile){
									$scope.editOrderList.cart[i].actualSqMetre = (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000)) * $scope.editOrderList.cart[i].tileCount;
									console.log("$scope.editOrderList.cart[i].tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000)) * $scope.editOrderList.cart[i].tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].tile is false");
									$scope.editOrderList.cart[i].actualSqMetre = (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].actualSqMetre);
								}
							}
						}
					}
					else{
						console.log("slab.tile is false");
						console.log("entered ielse");
						$scope.editOrderList.totalMtSquare = 0;
						$scope.editOrderList.slabCount = 0;
						for(var i=0; i < $scope.editOrderList.cart.length; i++){
							
							if($scope.editOrderList.cart[i].tile){
									$scope.editOrderList.cart[i].actualSqMetre = (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000)) * $scope.editOrderList.cart[i].tileCount;
									console.log("$scope.editOrderList.cart[i].tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000)) * $scope.editOrderList.cart[i].tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].tile is false");
									$scope.editOrderList.cart[i].actualSqMetre = (parseFloat($scope.editOrderList.cart[i].actualHeight * $scope.editOrderList.cart[i].actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].actualSqMetre);
								}
						}
					}
					
				};
				
				
				$scope.markAllAsCuttingSlab = function(value){
					$timeout(function() {
						$scope.$apply(function() {
							for(var i = 0; i < $scope.editOrderList.cart.length; i++){
								if(value == 1){
									$scope.editOrderList.cart[i].addToCuttingOrder = 1;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i], 1, i)
								}
								else{
									$scope.editOrderList.cart[i].addToCuttingOrder = 0;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i], 0, i)
								}
							}
						});
					});
				};
			}
			
			$scope.getMaterialList = function(){
				var url = "../app/material?action=getAllMaterialList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.materialList = data;
							console.log($scope.materialList);
							allMaterialList = data;
						});
					});
				});
			};
			
		});
	});
}]);

