inspinia.lazy.controller("BulkMaterialListUploadCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify) {

	$timeout(function() {
		$scope.$apply(function() {
			

			$scope.materialList = {};
			
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			$scope.uploadBulkMaterialList = function() {
				
				var form = $("#upload-bulk-material-list-form")[0];
				var url = "../app/bulkMateriallistupload";
				
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							alert(data.message);
						} else {
							alert(data.message);
						}
					}
				});
			};
		});
	});
}]);

