inspinia.lazy.controller("RestockCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster', 'SweetAlert', 
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster, SweetAlert) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.message = "hello";
			
			$scope.restock = {};
			$scope.restockInventoryResult = {};

			$scope.restockError = false;

			$scope.markAsSold = {};
			$scope.markAsSoldInventoryResult = {};
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			$scope.clear = function(variable){
				
				console.log("clear called");
				$timeout(function() {
					$scope.$apply(function() {
						
						if(variable == "restock"){
							$scope.restock = {};
							$scope.restockInventoryResult = {};
						}
						else if(variable == "markAsSold"){
							$scope.markAsSold = {};
							$scope.markAsSoldInventoryResult = {};
						}
					});
				});
			};
			
			$scope.findBarcodeInOrder = function(){
				
				$scope.restockInventoryResult = {};
				/**
					inventory with id
					cart with this inventoryId
					order containing that cart id
				 */
			
				
				var url = "../app/restock?action=getOrderDetailsForBarcodeString&barcodeString="+$scope.restock.barcodeString;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								$scope.restockInventoryResult = data.data;
								
								for(var i = 0; i < $scope.restockInventoryResult.length; i++){
									
									if($scope.restockInventoryResult[i].tile){
										$scope.restockInventoryResult[i].existingTileCount = $scope.restockInventoryResult[i].tileCount;
									} 
								}
								
							}
							else{
								//error alert
								SweetAlert.swal({
										title: "Error!",
							            text: data.error,
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            });
							}
						});
					});
				});
				
			};
			
			$scope.validateRestockBeforeSubmit = function(obj){
				
				$scope.restockError = false;
				console.log("validate obj: ", obj);
				var submitObj = {};
				if(typeof obj != "undefined" && obj != {}){
					
					if(typeof obj.cartId == "undefined" || obj.cartId == "" || obj.cartId == 0){
						var toasterObj = {
							type: 'error',
							title : "Cart Id missing.",
							body : "Please re check data and submit.",
							showCloseButton: true,
							timeout: 2000
						};
						
						$scope.restockError = true;
					}
					else{
						submitObj.cartId = obj.cartId;
					}
					
					if(!obj.hasOwnProperty('statusTypeId') || typeof obj.statusTypeId == "undefined" || obj.statusTypeId == "" || obj.statusTypeId == 0){
						var toasterObj = {
							type: 'error',
							title : "Status Type Id missing.",
							body : "Please re check data and submit.",
							showCloseButton: true,
							timeout: 2000
						};
						
						$scope.restockError = true;
						
						console.log("validate $scope.restockError: ", $scope.restockError);
					}
					else{
						submitObj.statusTypeId = obj.statusTypeId;
					}
					
					if(typeof obj.inventoryId == "undefined" || obj.inventoryId == "" || obj.inventoryId == 0){
						var toasterObj = {
							type: 'error',
							title : "Inventory Id missing.",
							body : "Please re check data and submit.",
							showCloseButton: true,
							timeout: 2000
						};
						
						$scope.restockError = true;
					}
					else{
						submitObj.inventoryId = obj.inventoryId;
					}
					
					if(typeof obj.orderId == "undefined" || obj.orderId == "" || obj.orderId == 0){
						var toasterObj = {
							type: 'error',
							title : "Order Id missing.",
							body : "Please re check data and submit.",
							showCloseButton: true,
							timeout: 2000
						};
						
						$scope.restockError = true;
					}
					else{
						submitObj.orderId = obj.orderId;
					}
					
					if(typeof obj.tile != "undefined" && obj.tile != "" && obj.tile == true){
						
						if(typeof obj.tileCount == "undefined" || obj.tileCount == "" || obj.tileCount == 0){
							var toasterObj = {
								type: 'error',
								title : "Tile Count missing.",
								body : "Tile Count needs to be specified.",
								showCloseButton: true,
								timeout: 2000
							};
							$scope.restockError = true;
						}
						else{
							submitObj.tile = obj.tile;
							submitObj.tileCount = obj.tileCount;
						}
					}
					
					if(typeof obj.remarks != "undefined" && obj.remarks != ""){
						submitObj.remarks = obj.remarks;
					}
					else{
						submitObj.remarks = "";
					}
					
				}
				else{
					var toasterObj = {
						type: 'error',
						title : "Data undefined",
						body : "Please re check data and submit.",
						showCloseButton: true,
						timeout: 2000
					};
					
					$scope.restockError = true;
				}
				
				console.log("$scope.restockError: ", $scope.restockError);
				if($scope.restockError){
					$scope.toasterNotification(toasterObj);
					return submitObj;
				}
				else{
					return submitObj;
				}
			};
			
			$scope.restockSlabs = function(obj){
				
				console.log("Object: ",obj);
				
				var submitObj = $scope.validateRestockBeforeSubmit(obj);
				
				if($scope.restockError){
					return;
				}
				
				console.log("submitObj: ", submitObj);
				
//				return;
				var url = "../app/restock?action=restockInventoryId";
				console.log(url);
				$http.post(url, submitObj, {}).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								SweetAlert.swal({
										title: "Done!",
							            text: "Slab restocked!",
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	$state.go("sales.salesInvoice");
						                }
						            });
							}
							else{
								//error alert
								SweetAlert.swal({
										title: "Error!",
							            text: "Could not restock slab.",
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            });
							}
						});
					});
				});
				
			};
			
			$scope.findBarcodeToBeAddedInOrder = function(){
				
				
				$scope.markAsSoldInventoryResult = {};
				/**
					inventory with id
					cart with this inventoryId
					order containing that cart id
				 */
			
				
				var url = "../app/restock?action=getOrderDetailsForBarcodeStringToAddToOrder&barcodeString="+$scope.markAsSold.barcodeString;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								$scope.markAsSoldInventoryResult = data.data;
								for(var i = 0; i < $scope.markAsSoldInventoryResult.length; i++){
									if($scope.markAsSoldInventoryResult[i].tile){
										$scope.markAsSoldInventoryResult[i].existingTileCount = $scope.markAsSoldInventoryResult[i].tileCount;
									} 
								}
							}
							else{
								//error alert
								SweetAlert.swal({
										title: "Error!",
							            text: data.error,
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            });
							}
						});
					});
				});
				
			};
			
			$scope.addSlabToOrder = function(obj){
				console.log("$scope.markAsSold: ", obj);
				
				var url = "../app/restock?action=addSlabToOrder";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								SweetAlert.swal({
										title: "Done!",
							            text: "Slab restocked!",
							            type: "success",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            },
						            function (isConfirm) {
						                if (isConfirm) {
						                	$state.go("sales.salesInvoice");
						                }
						            });
							}
							else{
								//error alert
								SweetAlert.swal({
										title: "Error!",
							            text: "Could not restock slab.",
							            type: "warning",
						                showCancelButton: false,
						                confirmButtonColor: "#DD6B55",
						                confirmButtonText: "OK!",
						                cancelButtonText: "",
						                closeOnConfirm: true,
						            });
							}
						});
					});
				});
			};
			
		});
	});
}]);

