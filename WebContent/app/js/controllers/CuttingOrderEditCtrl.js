inspinia.lazy.controller("CuttingOrderEditCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'SweetAlert', 'toaster', 'notify',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, SweetAlert, toaster, notify) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			/**
			 * Add Cutting Order Block
			 */
		
			$scope.count = 0;
			$scope.totalSqMeters = 0;
		
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called", obj);
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };

			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};

			$scope.tags = [];
			$scope.loadCuttingTypes = function($query) {
		      	return allCuttingTypeList.filter(function(country) {
		        	return country.name.toLowerCase().indexOf($query.toLowerCase()) != -1;
		      	});
		  	};

			$scope.materialList = allMaterialList;
			$scope.allSmallCustomerList = allSmallCustomerList;
			$scope.allCuttingTypeList = allCuttingTypeList;
			$scope.manualCuttingTypeList = manualCuttingTypeList;
			$scope.machineCuttingTypeList = machineCuttingTypeList;
			$scope.error = false;
			
			$scope.assignCustomerName = function(customerObj){
				
				if(customerObj.charAt(0) == "{"){
					customerObj = JSON.parse(customerObj);
					console.log(customerObj);
					$scope.cuttingOrder.customerId = customerObj.id;
					$scope.cuttingOrder.customerName = customerObj.name;
				}
				else{
					$scope.cuttingOrder.customerId = 0;
					$scope.cuttingOrder.customerName = customerObj;
				}
				
				console.log("$scope.cuttingOrder: ", $scope.cuttingOrder);
			};
			
			$scope.taskDetailsObject = {
				unit : '',
				cutToSize : 0,
				length : 0,
				height : 0,
				quantity : 0,
				nos : 0
			};
			
			$scope.taskObject = {
				taskList : [],
				taskDetails : [],
			}
			
			$scope.cuttingOrderCuttingListObject = {
				materialId: '',
				thickness: '',
				tasks: [],
			};
			
			$scope.cuttingOrder = {
				cuttingOrderNumber: 0,
				customerName : '',
				customerId: '',
				customer: '',
				date: moment().format("DD-MM-YYYY"),
				cuttingList: [],
				materialCount : 0,
				totalOutputMtSquare : 0,
				totalOutputLinearMeter : 0
			};
			
			$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
			$scope.cuttingOrderCuttingListObject.tasks.push($scope.taskObject);
			
			$scope.cuttingOrder.cuttingList.push($scope.cuttingOrderCuttingListObject);
			
			$scope.removeAt = function(type, array, index){
				console.log("type: ", type);
				console.log("array: ", array);
				console.log("index: ", index);
				
				array.splice(index, 1);
				console.log($scope.cuttingOrder);
			};
			
			$scope.addMaterial = function(coList){
				
				console.log(coList);
				
				$scope.taskDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0
				};
				
				$scope.taskObject = {
					taskList : [],
					taskDetails : []
				}
				
				$scope.cuttingOrderCuttingListObject = {
					materialId: '',
					thickness: '',
					tasks: [],
				};
				
				$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
				$scope.cuttingOrderCuttingListObject.tasks.push($scope.taskObject);
				
				coList.push($scope.cuttingOrderCuttingListObject);
				/*var lastIndex = coList.length - 1;
				
				for(var i = 0; i <= lastIndex; i++){
					if(i == lastIndex){
						coList[i].class = 'active';
					}
					else{
						coList[i].class = '';
					}
				}*/
			}
			
			$scope.addTaskInMaterial = function(taskArray){
				$scope.taskDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0
				};
				
				$scope.taskObject = {
					taskList : [],
					taskDetails : []
				}
				
				$scope.cuttingOrderCuttingListObject = {
					materialId: '',
					thickness: '',
					tasks: [],
				};
				
				$scope.taskObject.taskDetails.push($scope.taskDetailsObject);
				taskArray.push($scope.taskObject);
				
			};
			
			$scope.addTaskDetailsRow = function(taskDetailsArray){
				
				$scope.cuttingOrderCuttingListDetailsObject = {
					unit : '',
					cutToSize : 0,
					length : 0,
					height : 0,
					quantity : 0,
					nos : 0,
				};
				taskDetailsArray.push($scope.cuttingOrderCuttingListDetailsObject);
				console.log($scope.cuttingOrder);
			};
			
			$scope.calculateTotalQty = function(){
				$scope.cuttingOrder.totalOutputMtSquare = 0;
				$scope.cuttingOrder.totalOutputLinearMeter = 0;
				for(var i = 0; i < $scope.cuttingOrder.cuttingList.length; i++){
					$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter = 0;
					$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare = 0;
					
					for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].tasks.length; j++){
						for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length; k++){
							
							if($scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == 1){
								$scope.cuttingOrder.totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
								$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
							}
							
							if($scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == 2){
								$scope.cuttingOrder.totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
								$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].quantity;
							}
							
						}
					}
					
					/*for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].details.length; j++){
						if($scope.cuttingOrder.cuttingList[i].details[j].unit == 1){
							$scope.cuttingOrder.totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
							$scope.cuttingOrder.cuttingList[i].totalOutputLinearMeter += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
						}
						if($scope.cuttingOrder.cuttingList[i].details[j].unit == 2){
							$scope.cuttingOrder.totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
							$scope.cuttingOrder.cuttingList[i].totalOutputMtSquare += $scope.cuttingOrder.cuttingList[i].details[j].quantity;
						}
					}*/
				}
			}
			
			$scope.calculateQty = function(cListObj){
				//pcs : quantity = nos 
				//LM = length*no of pcs / 100
				//Mt is l*h*pcs/10000
				
				if(cListObj.unit == 1){
					//LM
					cListObj.quantity = cListObj.length * cListObj.nos / 100;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 2){
					//Mt.Sq.
					cListObj.quantity = cListObj.length * cListObj.height * cListObj.nos / 10000;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
				else if(cListObj.unit == 3){
					//Pcs
					cListObj.quantity = cListObj.nos;
					if(cListObj.quantity > 0){
						$scope.calculateTotalQty();
					}
				}
			}
			
			
			
			$scope.validate = function(){
				
				console.log("Validate $scope.cuttingOrder: ", $scope.cuttingOrder);
				
				if(typeof $scope.cuttingOrder.customerId == "undefined" || $scope.cuttingOrder.customerId == ""){
					
					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Customer.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					
					return true;
					
				}
				else if(typeof $scope.cuttingOrder.date == "undefined" || $scope.cuttingOrder.date == ""){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please enter Date.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList == "undefined" || $scope.cuttingOrder.cuttingList.length <= 0){

					var obj = {
						type: 'error',
						title : "Missing Data",
						body : "Please Material Details.",
						showCloseButton: true,
						timeout: 6000
					};
					$scope.toasterNotification(obj);
					return true;
				}
				else if(typeof $scope.cuttingOrder.cuttingList != "undefined" && $scope.cuttingOrder.cuttingList.length > 0){
					
					for(var i =0; i < $scope.cuttingOrder.cuttingList.length; i++){
						
						if(typeof $scope.cuttingOrder.cuttingList[i].materialId == "undefined" || $scope.cuttingOrder.cuttingList[i].materialId == ""){
							
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Material in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].materialThickness == "undefined" || $scope.cuttingOrder.cuttingList[i].materialThickness == ""){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Thickness in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						
						else if(typeof $scope.cuttingOrder.cuttingList[i].tasks == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks.length <= 0){
							var obj = {
								type: 'error',
								title : "Missing Data",
								body : "Please enter Task List in Material "+parseInt(i+1)+".",
								showCloseButton: true,
								timeout: 6000
							};
							$scope.toasterNotification(obj);
							return true;
						}
						else if(typeof $scope.cuttingOrder.cuttingList[i].tasks != "undefined" && $scope.cuttingOrder.cuttingList[i].tasks.length > 0){
							
							for(var j = 0; j < $scope.cuttingOrder.cuttingList[i].tasks.length; j++){
								
								if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskList == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskList.length <= 0){
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Cutting Tasks in Material Details in Task "+parseInt(j+1)+".",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
								}
								else{
									for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskList.length; k++){
										
										if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskList[k].id == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskList[k].id == ""){
											var obj = {
												type: 'error',
												title : "Missing Data",
												body : "Please enter Cutting Tasks in Material "+parseInt(i+1)+" in Task "+parseInt(j+1)+".",
												showCloseButton: true,
												timeout: 6000
											};
											$scope.toasterNotification(obj);
											return true;
										}
										
									}
								}
								
								if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length <= 0){
									var obj = {
										type: 'error',
										title : "Missing Data",
										body : "Please enter Slab Details in Material "+parseInt(i+1)+", Task "+parseInt(j+1)+".",
										showCloseButton: true,
										timeout: 6000
									};
									$scope.toasterNotification(obj);
									return true;
								}
								else{
									for(var k = 0; k < $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails.length; k++){
										
										if(typeof $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == "undefined" || $scope.cuttingOrder.cuttingList[i].tasks[j].taskDetails[k].unit == ""){
											var obj = {
												type: 'error',
												title : "Missing Data",
												body : "Please enter Unit in slab details in Material "+parseInt(i+1)+" Task "+parseInt(j+1)+".",
												showCloseButton: true,
												timeout: 6000
											};
											$scope.toasterNotification(obj);
											return true;
										}
										
									}
								}
							}
						}
					}
				}
			};
			

			
				$scope.addToCuttingOrder = function(slab, addToCuttingOrder, index){
					
					if(addToCuttingOrder == 1 && !$scope.editOrderList.addToCuttingOrderArray.includes(slab.id)){
						$scope.editOrderList.addToCuttingOrderArray.push(slab.id);
					}
					else if(addToCuttingOrder == 0 && $scope.editOrderList.addToCuttingOrderArray.includes(slab.id)){
						var indexOfSlabId = $scope.editOrderList.addToCuttingOrderArray.indexOf(slab.id);
						$scope.editOrderList.addToCuttingOrderArray.splice(indexOfSlabId, 1);
					}
					
					$scope.inputTotalMeterSquare += slab.actualLength * slab.actualHeight / 10000;
					
					console.log($scope.editOrderList);
				};
				
				$scope.fetchCurrentOrder = function(index){
					var url = "../app/cutting?action=fetchCuttingOrderWithOrderId&orderId="+index;
					console.log("fetchCuttingOrderWithOrderId called ");
					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log("First: ", data);
								$scope.editOrderList = data.data;
								if(typeof $scope.editOrderList.companyPickup == "undefined" || $scope.editOrderList.companyPickup == 0 || $scope.editOrderList.companyPickup == ""){
									$scope.editOrderList.companyPickup = 0;
								}
								
								if($scope.editOrderList.customer.VATRegistration == "1"){
									console.log("VAT REG is 1 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(1);
								}
								else{
									console.log("VAT REG is 0 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(0);
								}
								if(typeof $scope.editOrderList.customer.VATRegistrationDate != "undefined" && $scope.editOrderList.customer.VATRegistrationDate != ""){
									$scope.editOrderList.customer.VATRegistrationDate = moment($scope.editOrderList.customer.VATRegistrationDate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
									$scope.editOrderList.LPODate = moment($scope.editOrderList.LPODate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								for(var i = 0; i < $scope.editOrderList.cart.length; i++){
									if($scope.editOrderList.cart[i].inventory.addToCuttingOrder){
										$scope.editOrderList.cart[i].inventory.addToCuttingOrder = 1;
									}
									else{
										$scope.editOrderList.cart[i].inventory.addToCuttingOrder = 0;
									}
								}
								
								$scope.editOrderList.addToCuttingOrderArray = [];
								console.log("$scope.editOrderList: ", $scope.editOrderList);
								
								
							});
						});
					});
				};
				
				if($stateParams.order != null &&  typeof $stateParams.order != "undefined" && $stateParams.order != ""){
					$scope.editOrderList = order;
					console.log("$state.order: ", $scope.editOrderList);
				}
				else if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCurrentOrder(index);
				}
				
				$scope.deleteCartRow = function(order, cart, index){
					console.log(cart);
					var url = "../app/order?action=deleteCartRow&cartId=" + cart.id;
					console.log(url);
					$http.post(url).success(function(data, status, headers, config) {
						console.log("deleteCartRow result: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);

								if(data.success){
									
									$scope.editOrderList.cart.splice(index, 1);
									
									
									$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
									$scope.editOrderList.totalMtSquare = 0;
									for(var j = 0; j < $scope.editOrderList.cart.length; j++){
										$scope.editOrderList.totalMtSquare += $scope.editOrderList.cart[j].actualSqMetre;
									}
									
									if($scope.editOrderList.cart.length <= 0){
										/**
										 * All cart rows deleted
										 * delete order as well
										 */
										
										successObj.message = "Cart has been cleared";
										notify(successObj);
										console.log("All orders deleted");
										
										
										var url = "../app/order?action=deleteOrder&orderId=" + order.id;
										$http.post(url).success(function(data, status, headers, config) {
											console.log("fetched data: ",data);
											$timeout(function() {
												$scope.$apply(function() {
													
													if(data.success){
														console.log(data);
														$scope.salesOrderList.splice(index, 1);
														successObj.message = "Order has been deleted";
														notify(successObj);
														
														$state.go("sales.salesInvoice");
														
														for(var i = 0; i < $scope.salesOrderList.length; i++){
															$scope.salesOrderList[i].slabCount = $scope.salesOrderList[i].cart.length;
															$scope.salesOrderList[i].totalMtSquare = 0;
															for(var j = 0; j < $scope.salesOrderList[i].cart.length; j++){
																$scope.salesOrderList[i].totalMtSquare += $scope.salesOrderList[i].cart[j].actualSqMetre;
															}
														}
														
													}
													else{
														errorObj.message = data.message;
														notify(errorObj);
													}
													
												});
											});
										});
									}
									else{
										successObj.message = "Cart row deleted successfully.";
										notify(successObj);
									}
								}
								else{
									errorObj.message = data.message;
									notify(errorObj);
								}
							});
						});
					});
				};
				
				$scope.validateEditedOrder = function(){
					
					console.log("$scope.editOrderList: ", $scope.editOrderList);
					$scope.error = false;
					if(typeof $scope.editOrderList.customer.customerName == "undefined" || $scope.editOrderList.customer.customerName == ""){
						errorObj.message = 'Add Customer Name';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					
					if(typeof $scope.editOrderList.customer.emirate == "undefined" || $scope.editOrderList.customer.emirate == ""){
						errorObj.message = 'Please fill Address.';
						notify(errorObj);
						return;
					}
					else{
						$scope.editOrderList.emirate = $scope.editOrderList.emirate;
					}
					
					if(typeof $scope.editOrderList.companyPickup != "undefined" 
						&& $scope.editOrderList.companyPickup == "0" 
						&& (typeof $scope.editOrderList.vehicleNumber == "undefined" || $scope.editOrderList.vehicleNumber == "")){
						errorObj.message = 'Please fill Vehicle Number.';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					else{
						$scope.editOrderList.vehicleNumber = $scope.editOrderList.vehicleNumber;
					}
					
					if(typeof $scope.editOrderList.customer.VATRegistration != "undefined" 
						&& $scope.editOrderList.customer.VATRegistration == "1"){
							
						if(typeof $scope.editOrderList.customer.VATRegistrationDate == "undefined" || $scope.editOrderList.customer.VATRegistrationDate == ""){
							errorObj.message = 'Please Add VAT Registration Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						if(typeof $scope.editOrderList.customer.trnNumber == "undefined" || $scope.editOrderList.customer.trnNumber == ""){
							errorObj.message = 'Please add TRN NUmber.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						
					}
					else{
						$scope.editOrderList.customer.trnNumber = $scope.editOrderList.customer.trnNumber;
						$scope.editOrderList.customer.VATRegistrationDate = $scope.editOrderList.customer.VATRegistrationDate;
						$scope.editOrderList.customer.VATRegistration = $scope.editOrderList.customer.VATRegistration;
					}
					
					if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
						if(typeof $scope.editOrderList.LPODate == "undefined" || $scope.editOrderList.LPODate == ""){
							errorObj.message = 'Please fill LPO Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
						if(typeof $scope.editOrderList.LPONumber == "undefined" || $scope.editOrderList.LPONumber == ""){
							errorObj.message = 'Please fill LPO Number.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					
					if(!$scope.error){
						for(var i = 0; i < ($scope.editOrderList.cart.length); i++){
							if(typeof $scope.editOrderList.cart[i].inventory.barcodeString == "undefined" || $scope.editOrderList.cart[i].inventory.barcodeString == ""){
								errorObj.message = 'Code does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							else if(typeof $scope.editOrderList.cart[i].inventory.id == "undefined" || $scope.editOrderList.cart[i].inventory.id == ""){
								errorObj.message = 'Slab Id does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							
							if($scope.editOrderList.cart[i].inventory.tile && $scope.editOrderList.cart[i].inventory.type == "TILE"){
								if($scope.editOrderList.cart[i].inventory.tileCount > $scope.editOrderList.cart[i].inventory.originalTileCount){
									errorObj.message = 'Count more than limit of '+ $scope.editOrderList.cart[i].inventory.originalTileCount;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
								else if($scope.editOrderList.cart[i].inventory.tileCount < 1){
									errorObj.message = 'Minimum 1 tile needs to be there in '+$scope.editOrderList.cart[i].inventory.barcodeString;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
							}
						}
					}
					if($scope.error){
						return;
					}
				};
				
				/*$scope.changeAddressDetailsOfCustomer = function(custDetId){
					
					console.log($scope.editOrderList);
					
					for(var i = 0; i < $scope.editOrderList.customerDetails.length; i++){
						if($scope.editOrderList.customerDetails[i].id == custDetId){
							if(typeof $scope.editOrderList.customerDetails[i].emirate != "undefined" && $scope.editOrderList.customerDetails[i].emirate != ""){
								$scope.editOrderList.emirate = $scope.editOrderList.customerDetails[i].emirate;
							}
							if(typeof $scope.editOrderList.customerDetails[i].phone != "undefined" && $scope.editOrderList.customerDetails[i].phone != ""){
								$scope.editOrderList.phone = $scope.editOrderList.customerDetails[i].phone;
							}
							if(typeof $scope.editOrderList.customerDetails[i].vehicleNumber != "undefined" && $scope.editOrderList.customerDetails[i].vehicleNumber != ""){
								$scope.editOrderList.vehicleNumber = $scope.editOrderList.customerDetails[i].vehicleNumber;
							}
						}
					}
					
					console.log($scope.editOrderList);
				};*/
				
				$scope.addNewCustomer = function (addCustomerObject) {
					$scope.addCustomer = true;
					$scope.editCustomer = false;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Customer-Modal.html',
			            controller: AddCustomerCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						size: 'lg',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         }
			                     ]);
							}
						}
			        });
	
			    };


				$scope.addNewCustomerAddress = function (editOrderList) {
					$scope.addCustomerAddress = true;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Add-Customer-Address-Modal.html',
			            controller: AddCustomerAddressDetailsCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						size: 'lg',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         },
									{
			                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
			                        }
			                     ]);
							}
						}
			        });

			    };

				$scope.editExistingCustomer = function (addCustomerObject) {
					$scope.addCustomer = false;
					$scope.editCustomer = true;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Edit-Customer-Modal.html',
			            controller: EditCustomerDetailsCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         }
			                     ]);
							}
						}
			        });

					$scope.fetchCurrentOrder($scope.editOrderList.id);
					
			    };
				
				$scope.saveEditedCuttingOrder = function(approveOrder, order){
					
					console.log("$scope.editOrder_form: ", $scope.editOrder_form);
					
					if ($scope.editOrder_form.$valid) {
						
						console.log("approveOrder: ", approveOrder);
						console.log("Order: ", order);
						
						$scope.validateEditedOrder();
						
						if($scope.error){
							//alert("Error is still there");
							return;
						}
						
						console.log("Passed validation");
						
						console.log("$scope.editOrderList: ", $scope.editOrderList);
						
						var obj = {};
						obj.id = order.id;
						obj.customerName = order.customer.customerName;
						obj.approveOrder = approveOrder;
						obj.userId = me.id;
						obj.slabCount = order.slabCount;
						obj.totalMtSquare = order.totalMtSquare;
						obj.emirate = order.customer.emirate;
						obj.vehicleNumber = order.vehicleNumber;
						obj.companyPickup = order.companyPickup;
						obj.narration = order.narration;

						obj.VATRegistration = order.customer.VATRegistration;
						obj.VATRegistrationDate = order.customer.VATRegistrationDate;
						obj.trnNumber = order.customer.trnNumber;

						obj.phone = order.customer.phone;
						obj.userId = $scope.user.id;
						
						obj.hasCuttingSlabs = order.hasCuttingSlabs;
						obj.skipScan = order.skipScan;
						obj.outsideMaterial = order.outsideMaterial;
						
						if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						
						if((typeof $scope.editOrderList.skipScan != "undefined" && typeof $scope.editOrderList.skipScan == 1)
							|| (typeof $scope.editOrderList.outsideMaterial != "undefined" || typeof $scope.editOrderList.outsideMaterial == 1)){
							
							obj.cart = [];
	
							for(var i = 0; i < $scope.editOrderList.cart.length; i++){
								var cartObj = {};
								cartObj.inventory = {};
								cartObj.id = $scope.editOrderList.cart[i].id;
								cartObj.inventory.id = $scope.editOrderList.cart[i].inventory.id;
								cartObj.inventory.actualLength = $scope.editOrderList.cart[i].inventory.actualLength;
								cartObj.inventory.actualHeight = $scope.editOrderList.cart[i].inventory.actualHeight;
								cartObj.inventory.thickness = $scope.editOrderList.cart[i].inventory.thickness;
								cartObj.inventory.type = $scope.editOrderList.cart[i].inventory.type;
								
								if(cartObj.type == 'TILE'){
									cartObj.inventory.tileCount = $scope.editOrderList.cart[i].inventory.tileCount;
									cartObj.inventory.actualSqMetre = $scope.editOrderList.cart[i].inventory.actualLength * $scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.tileCount / 10000;
								}
								else{
									cartObj.inventory.actualSqMetre = $scope.editOrderList.cart[i].inventory.actualLength * $scope.editOrderList.cart[i].inventory.actualHeight / 10000;
									
								}
								
								obj.cart.push(cartObj);
							}
							
						}
						
						
						
						
						console.log("OBJ: ", obj);
						
	
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
						$http.defaults.transformRequest = [ function(data) {
							return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
						}];
	
						
						var url = "../app/cutting?action=editExistingCuttingOrder";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
	
									if(data.success){
										
										SweetAlert.swal({
											title: "Done!",
								            text: data.message,
								            type: "success",
							                showCancelButton: false,
							                confirmButtonColor: "#DD6B55",
							                confirmButtonText: "OK!",
							                cancelButtonText: "",
							                closeOnConfirm: true,
							            },
							            function (isConfirm) {
							                if (isConfirm) {
							                	$state.go("sales.approvedSalesOrder");
							                }
							            });
										
										
										/*successObj.message = "Order saved successfully";
										notify(successObj);*/
										
										/**
										 * Call Download PDF function
										 */
									}
									else{
										SweetAlert.swal({
											title: "Error!",
								            text: data.error,
								            type: "warning",
							                showCancelButton: false,
							                confirmButtonColor: "#DD6B55",
							                confirmButtonText: "OK!",
							                cancelButtonText: "",
							                closeOnConfirm: true,
							            },
							            function (isConfirm) {
							                
							            });
									}
								});
							});
						});
					}
					else{
						$scope.editOrder_form.submitted = true;
					}
				};
				
				
				$scope.calculateTotalSqMetersOnEdit = function(slab){
					$scope.editOrderList.totalMtSquare = 0;
					$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
					console.log(slab);
					
					if(slab.tile){
						console.log("slab.tile is true: ", slab);
						if(slab.tileCount > slab.originalTileCount){
							errorObj.message = 'Count more than limit of '+ slab.originalTileCount;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else if(slab.tileCount < 1){
							errorObj.message = 'Minimum 1 tile needs to be there in '+slab.barcodeString;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else{
							console.log(slab);
							$scope.count = 0;
							$scope.totalSqMeters = 0;
							
							$scope.totalSqMeters += parseFloat(slab.actualLength * slab.actualHeight / 10000) * parseInt(slab.tileCount);
							slab.actualSqMetre = ((slab.actualLength * slab.actualHeight) / 10000) * slab.tileCount;
							
							
							$scope.editOrderList.totalMtSquare = 0;
							$scope.editOrderList.slabCount = 0;
							for(var i=0; i < $scope.editOrderList.cart.length; i++){
								
								
								if($scope.editOrderList.cart[i].inventory.tile){
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									console.log("$scope.editOrderList.cart[i].inventory.tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].inventory.tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].inventory.tile is false");
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].inventory.actualSqMetre);
								}
							}
						}
					}
					else{
						console.log("slab.tile is false");
						console.log("entered ielse");
						$scope.editOrderList.totalMtSquare = 0;
						$scope.editOrderList.slabCount = 0;
						for(var i=0; i < $scope.editOrderList.cart.length; i++){
							
							if($scope.editOrderList.cart[i].inventory.tile){
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									console.log("$scope.editOrderList.cart[i].inventory.tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].inventory.tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].inventory.tile is false");
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].inventory.actualSqMetre);
								}
						}
					}
					
				};
				
				
				$scope.markAllAsCuttingSlab = function(value){
					$timeout(function() {
						$scope.$apply(function() {
							for(var i = 0; i < $scope.editOrderList.cart.length; i++){
								if(value == 1){
									$scope.editOrderList.cart[i].inventory.addToCuttingOrder = 1;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i].inventory, 1, i)
								}
								else{
									$scope.editOrderList.cart[i].inventory.addToCuttingOrder = 0;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i].inventory, 0, i)
								}
							}
						});
					});
				};
			
			
		});
	});
}]);

