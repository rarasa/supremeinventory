inspinia.lazy.controller("CuttingTypeCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster', 'SweetAlert',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster, SweetAlert) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			$scope.cuttingType = {};
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			$scope.cuttingTypeBulkList = {};
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			
			$scope.editMode = false;
			
			$scope.cuttingTypeList = allCuttingTypeList;
			console.log("$scope.cuttingTypeList: ", $scope.cuttingTypeList);
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.getCuttingTypeList = function(){
				var url = "../app/cutting?action=getAllCuttingTypeList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.cuttingTypeList = data.data;
							console.log("laaa: ",$scope.cuttingTypeList);
							allSmallCuttingTypeList = $scope.cuttingTypeList;
						});
					});
				});
			};
			
			$scope.to = ["John", "Marie", "Ghita"];
        
	        $scope.info = {
	          select: []
	        }
	        
	        $scope.typeaheadOpts = {
	          minLength: 1,
	        };
	        
	        $scope.$on("decipher.tags.added", tagAdded);
	
	        function tagAdded(info, obj) {
	          for (var i = 0; i < $scope.to.length; i++) {
	            console.log($scope.info.select);
	          if ($scope.to[i] === obj.tag.name) {
	            $scope.to.splice(i, 1);
	            //console.log("splice " +$scope.to)
	            } 
	          }
	        }
	        
	        $scope.delete = function () {
	          if ($scope.info.select !== "") {
	           $scope.info = null;
	           console.log($scope.info);
	          }
	        }
			
			$scope.clickedOnEditRow = function(customer, index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log("Clicked on edit row");
						if($scope.editMode){
							var obj = {
								type: 'error',
								title : "Editing another row",
								body : "Please save all rows before editing another.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
						else{
							
							customer.activeBool = (customer.active) ? 1 : 0;
							
							var editModeCuttingTypeNameString = "<input class='form-control'' type='text' id=name"+index+" style='width:-webkit-fill-available;' name='name' value='"+customer.name+"' placeholder='Name' ng-model='customer.name'>";
	            			var editModeCuttingTypeCode = "<input class='form-control' type='text' id=code"+index+" style='width:-webkit-fill-available;' name='code' value='"+customer.code+"' placeholder='Origin' ng-model='customer.code'>";
	            			var editModeCuttingTypeEmirate = "<input class='form-control' type='text' id=emirate"+index+" style='width:-webkit-fill-available;' name='emirate' value='"+customer.emirate+"' placeholder='Origin' ng-model='customer.emirate'>";
	            			var editModeCuttingTypePhone = "<input class='form-control' type='text' id=phone"+index+" style='width:-webkit-fill-available;' name='phone' value='"+customer.phone+"' placeholder='Origin' ng-model='customer.phone'>";
	            			
							var editModeCuttingTypeActive = "<select id='active"+index+"' class='form-control' name='active' ng-model='customer.activeBool'>"
											            	+"<option value=''>--Select Active--</option>"
											            	+"<option value='true'>Yes</option>"
											            	+"<option value='false'>No</option>"
											            +"</select>";
							

							console.log($("#sizeDiv"+index));
							$("#customerNameDiv"+index).empty();
							$("#customerNameDiv"+index).append(editModeCuttingTypeNameString);
							$("#customerCodeDiv"+index).empty();
							$("#customerCodeDiv"+index).append(editModeCuttingTypeCode);
							$("#customerContactDiv"+index).empty();
							$("#customerContactDiv"+index).append(editModeCuttingTypePhone);
							$("#customerEmirateDiv"+index).empty();
							$("#customerEmirateDiv"+index).append(editModeCuttingTypeEmirate);
							$("#customerActiveDiv"+index).empty();
							$("#customerActiveDiv"+index).append(editModeCuttingTypeActive);
							
							
							console.log(customer);
							
							customer.editMode = true;
							$scope.editMode = true;
							customer.activeBool = customer.active;
							
							$("#active"+ index).val(''+customer.active);
							console.log(customer);
						}
					});
				});
			};
			
			$scope.clickedOnSaveRow = function(customer, index){
				$timeout(function() {
					$scope.$apply(function() {
						
						var obj = customer;
						obj.name = $("#name"+index).val();
						obj.code = $("#code"+ index).val();
						obj.emirate = $("#emirate"+ index).val();
						obj.phone = $("#phone"+ index).val();
						obj.active = $("#active"+ index).val();
						
						console.log("customer: ", customer);
						console.log("Clicked on Save row");
						
						if($scope.editMode && customer.editMode){
							$scope.saveEditedCuttingType(customer, index);
							$scope.editMode = false;
							customer.editMode = false;
						}
						else{
							
							var obj = {
								type: 'error',
								title : "Invalid Operation",
								body : "CuttingType Is not being edited.",
								showCloseButton: true,
								timeout: 2000
							};
							
							$scope.toasterNotification(obj);
						}
					});
				});
			};
			
			$scope.saveEditedCuttingType = function(customer, index){
				
				var obj = customer;
				
				console.log("CuttingType obj: ", customer);
				if(typeof obj.name == "undefined" || obj.name == ""){
					var obj = {
						type: 'danger',
						title : "CuttingType Name not given",
						body : '',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);

					return;
				}
					
				var url = "../app/customer?action=editSingleCuttingType";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								$timeout(function() {
									$scope.$apply(function() {
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'CuttingType saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										
										$scope.getCuttingTypeList();
										
										$scope.editMode = false;
									});
								});
							}
							else{
								
								var obj = {
									type: 'error',
									title : data.message,
									body : obj.name+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							}
						});
					});
				});
			};
			
	
			$scope.uploadSingleCuttingTypeList = function(){
				if ($scope.cutting_type_form.$valid) {
		            // Submit as normal
					
					var obj = $scope.cuttingType;
					
					console.log(obj)
										
					var exists = false;
					for(var i = 0; i < $scope.cuttingTypeList.length; i++){
						if($scope.cuttingTypeList[i].name == obj.name.toUpperCase()){
							exists = true;
							break
						}
					}
					
					if(exists){
						var obj = {
							type: 'error',
							title : 'Oops',
							body : 'CuttingType already exists',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						return;
					}
					else{
						var url = "../app/cutting?action=updateSingleCuttingType";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									
									if(data.success){
										
										var obj = {
											type: 'success',
											title : 'CuttingType saved.',
											body : 'Cutting Type saved successfully.',
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
										
										$scope.getCuttingTypeList();
									}
									else{
										var obj = {
											type: 'error',
											title : 'Oops',
											body : data.message,
											showCloseButton: true,
		    								timeout: 2000
											
										};
										$scope.toasterNotification(obj);
									}
								});
							});
						});
					}
				}
				else{
					$scope.cutting_type_form.submitted = true;
				}
				
			};
			
			
			$scope.deleteCuttingType = function(customer, index){
				
				if($scope.editMode){
					
					var obj = {
						type: 'error',
						title : 'Error',
						body : 'Please save row before editing.',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					return;
				}
				
				if(!confirm("Do you really want to delete "+ customer.name+" ?")){
					return;
				}
				else{
					console.log("call delete func");
					var url = "../app/customer?action=deleteCuttingType";
					
					var obj = {
						name : customer.name,
						id: customer.id
					};
					
					console.log(url);
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									var obj = {
										type: 'success',
										title : 'Deleted!',
										body : 'CuttingType deleted successfully.',
										showCloseButton: true,
        								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.cuttingTypeList.splice(index, 1);
									
//									$scope.getCuttingTypeList();
								}
								else{
									
									var obj = {
										type: 'error',
										title : "",
										body : data.message,
										showCloseButton: true,
        								timeout: 2000
									};
									
									$scope.toasterNotification(obj);
									return;
								
								}
							});
						});
					});
				}
				
			};

			$scope.uploadBulkCuttingTypeList = function() {
				
				console.log("accessed uploadBulkCuttingTypeList");
				var form = $("#upload-bulk-cuttingType-list-form")[0];
				var url = "../app/bulkCuttingTypeListUpload";
				console.log("form: ",form);
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					success : function(data) {
						console.log("DATAAAA: ", data);
						if (data.success) {
							// Request 100% completed
							console.log("data: ", data);
							
							SweetAlert.swal({
				    				title: "Done!",
				    	            text: data.message,
				    	            type: "success",
				                    showCancelButton: false,
				                    confirmButtonColor: "#DD6B55",
				                    confirmButtonText: "OK!",
				                    cancelButtonText: "",
				                    closeOnConfirm: true,
			                    },
			                    function (isConfirm) {
				                    if (isConfirm) {
				                    	$scope.getCuttingTypeList();
				                    }
			                    });
							
							
						} else {
							SweetAlert.swal({
				                title: "Oops!",
				                text: data.error,
				                type: "warning",
				                showCancelButton: false,
				                confirmButtonColor: "#DD6B55",
				                confirmButtonText: "OK! Let me check!",
				                closeOnConfirm: true,
				                closeOnCancel: true
				            });
						}
					}
				});
			};
			
			$scope.addNewCuttingTyps = function () {
				console.log("addNewCuttingTyps chosen")
				$scope.addCuttingMode = true;
				
		        var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/Cutting-Type-Modal.html',
		            controller: AddNewCuttingTypeCtrl,
		            windowClass: "animated fadeInDown",
					scope: $scope,
					size: 'sm',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						loadPlugin: function ($ocLazyLoad) {
							return $ocLazyLoad.load([
		                         {
		                            insertBefore: '#loadBefore',
		                            name: 'toaster',
		                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
		                         },
		                        {
		                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
		                        }
		                     ]);
						}
					}
		        });
		    };	
			$scope.getOrdersOfCuttingType = function (cuttingType) {
				console.log("fetchOrdersOfCuttingType chosen")
				
				$state.go("admin.getCuttingTypeOrders", {
					id : cuttingType.id,
					cuttingType : cuttingType
				});
					
		    };	
		});
	});
}]);
