inspinia.lazy.controller("ReadQRCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify) {

	function decodeOnce(codeReader, selectedDeviceId) {
      codeReader.decodeFromInputVideoDevice(selectedDeviceId, 'video').then(function(result){
        console.log(result);
        document.getElementById('result').textContent = result.text;
      })
      /*.catch((err) => {
        console.error(err)
        document.getElementById('result').textContent = err
      })*/
    }
	
	function decodeContinuously(codeReader, selectedDeviceId) {
		codeReader.decodeFromInputVideoDeviceContinuously(selectedDeviceId, 'video', function(result, err){
		    if (result) {
	          // properly decoded qr code
	          console.log('Found QR code!', result)
	          document.getElementById('result').textContent = result.text
	        }
	
	        if (err) {
	          // As long as this error belongs into one of the following categories
	          // the code reader is going to continue as excepted. Any other error
	          // will stop the decoding loop.
	          //
	          // Excepted Exceptions:
	          //
	          //  - NotFoundException
	          //  - ChecksumException
	          //  - FormatException
	
	        	if (err instanceof ZXing.NotFoundException) {
	        		console.log('No QR code found.')
	        	}
	
	        	if (err instanceof ZXing.ChecksumException) {
	        		console.log('A code was found, but it\'s read value was not valid.')
	        	}
	
	        	if (err instanceof ZXing.FormatException) {
	        		console.log('A code was found, but it was in a invalid format.')
	        	}
	        }
		})
    };
    
    window.addEventListener('load', checkJSLoaded);
    checkJSLoaded = function () {
        let selectedDeviceId;
        const codeReader = new ZXing.BrowserQRCodeReader()
        console.log('ZXing code reader initialized')

        codeReader.getVideoInputDevices()
          .then(function(videoInputDevices){
            const sourceSelect = document.getElementById('sourceSelect');
            selectedDeviceId = videoInputDevices[0].deviceId;
            if (videoInputDevices.length >= 1) {
              videoInputDevices.forEach(function(element) {
                const sourceOption = document.createElement('option');
                sourceOption.text = element.label
                sourceOption.value = element.deviceId
                sourceSelect.appendChild(sourceOption)
              })

              sourceSelect.onchange = function() {
                selectedDeviceId = sourceSelect.value;
              };

              const sourceSelectPanel = document.getElementById('sourceSelectPanel')
              sourceSelectPanel.style.display = 'block'
            }

            document.getElementById('startButton').addEventListener('click', function(){

              const decodingStyle = document.getElementById('decoding-style').value;

              if (decodingStyle == "once") {
                decodeOnce(codeReader, selectedDeviceId);
              } else {
                decodeContinuously(codeReader, selectedDeviceId);
              }

            })

            document.getElementById('resetButton').addEventListener('click', function(){
              codeReader.reset()
              document.getElementById('result').textContent = '';
              console.log('Reset.')
            })

          });
          /*.catch((err) => {
            console.error(err)
          })*/
      }

}]);
