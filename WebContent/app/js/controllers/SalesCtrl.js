
inspinia.lazy.controller("SalesCtrl", ['$scope', '$location', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', '$modal',
    function ($scope, $location, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, $modal) {

	$timeout(function() {
		$scope.$apply(function() {
			
			htmltag = "";
			
			$scope.inputTotalMeterSquare = 0;
			
			$scope.mergeAction = false;
			$scope.mergeOrderList = [];
			$scope.mergeOrderObject = {
				secondaryMergeOrderArray : []
			};
			
			$scope.customerList = allSmallCustomerList;
			$scope.materialList = allMaterialList;
			
			$scope.salesOrderList2CM = [];
			$scope.salesOrderList3CM = [];
			$scope.salesOrderList2CMSqMeters = 0;
			$scope.salesOrderList3CMSqMeters = 0;
			
			$scope.pendingOrderObj = {};
			
			console.log("$scope.materialList: ", $scope.materialList);
			
			$scope.getCustomerList = function(){
				var url = "../app/customer?action=getAllCustomerList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.customerList = data;
							console.log("data: ", data);
							allSmallCustomerList = data;
							console.log($scope.customerList);
						});
					});
				});
			};
			
			/*$scope.startSearch = function(term){
			    var results = [];
			    $scope.customerList.forEach(function(item){
			      if (typeof item.name != "undefined" && item.name != "" && item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
			        results.push(item);
			      }
			    });
		      	return results;
			};
			$scope.addCustomerToOrder = function(item, element) {
				console.log("customerItem: ", item);
				$timeout(function() {
					$scope.$apply(function() {
						$scope.pendingOrderObj.customerId = item.id;
						$scope.pendingOrderObj.customerName = item.name;
						
						console.log("$scope.pendingOrderObj: ", $scope.pendingOrderObj);
					});
				});
			};*/
			
			$scope.count = 0;
			$scope.totalSqMeters = 0;
			$scope.approvedOrderObj = {};
			$scope.approvedOrderObj.duration = 10;
			
			$scope.approvedOrdersList = [];
			
			console.log("$scope.count: ", $scope.count);
			console.log("$scope.totalSqMeters: ", $scope.totalSqMeters);
			
			$scope.stopPropagationFunction = function(e){
				e.stopPropagation();
			};
			
			$scope.editOrderList = {};
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.purchaseList = {
					customerName : "",
			};
			
			
			$scope.purchaseList.purchaseOrder = [];
			
			$scope.purchaseOrderObject = {
					slabCode: '',
					name: '',
					actualLength: 0,
					actualHeight: 0,
					thickness: 0,
					slabId: 0,
					id: 0,
					actualSqMetre: 0,
					type: ""
			};

			$scope.purchaseList.purchaseOrder.push($scope.purchaseOrderObject);
			
			$scope.addRow = function(){
				console.log("addRowFlag entered");
				$scope.purchaseOrderObject = {
						slabCode: '',
						name: '',
						actualLength: 0,
						actualHeight: 0,
						thickness: 0,
						slabId: 0,
						id: 0,
						actualSqMetre: 0,
						type: ""
				};

				var idString = "";
				var addRowFlag = true;
				for(var i=0; i < $scope.purchaseList.purchaseOrder.length; i++){
					if(typeof $scope.purchaseList.purchaseOrder[i].slabCode == "undefined" || $scope.purchaseList.purchaseOrder[i].slabCode == ""){
						//one empty row exists so do nothing
						addRowFlag = false;
						idString = "slabCodeText"+indexOf($scope.purchaseList.purchaseOrder[i]);
						break;
					}
					else{
						addRowFlag = true;
						idString = "slabCodeText"+$scope.purchaseList.purchaseOrder.length;
					}
				}
				
				console.log("idString: "+ idString)
				if(addRowFlag){
					console.log("addrowFlag: ", addRowFlag);
					$scope.purchaseList.purchaseOrder.push($scope.purchaseOrderObject);
					
					$timeout(function() {
						$scope.$apply(function() {
							console.log("idString 2: "+ idString)
					      // have to do this in a $timemout because
					      // we want it to happen after the view is updated
					      // with the newly added input
					      angular
					        .element(document.querySelectorAll('#'+idString))
					        .eq(-1)[0]
					        .focus()
					      ;
						});
					    }, 0);
	
					
					
				}
			};
			
			
			
			$scope.removeAt = function(index){
				$timeout(function() {
					$scope.$apply(function() {
						console.log($scope.purchaseList.purchaseOrder);
						console.log("$scope.purchaseList.purchaseOrder.length: ", $scope.purchaseList.purchaseOrder.length);
						if($scope.purchaseList.purchaseOrder.length <= 0 
							|| (typeof $scope.purchaseList.purchaseOrder[index].slabCode == "undefined" || $scope.purchaseList.purchaseOrder[index].slabCode == "")){
							errorObj.message = 'No Purchase to remove.';
							notify(errorObj);
							return;
						}
						
						if(typeof $scope.purchaseList.purchaseOrder[index].type != "undefined" && $scope.purchaseList.purchaseOrder[index].type == "TILE"){
							$scope.purchaseList.purchaseOrder[index].actualSqMetre = parseFloat(($scope.purchaseList.purchaseOrder[index].actualLength * $scope.purchaseList.purchaseOrder[index].actualHeight) / 10000) * $scope.purchaseList.purchaseOrder[index].tileCount;
							$scope.count -= $scope.purchaseList.purchaseOrder[index].tileCount;
							$scope.totalSqMeters -= $scope.purchaseList.purchaseOrder[index].actualSqMetre;
							$scope.purchaseList.purchaseOrder.splice(index, 1);
						}
						else{
							$scope.count--;
							$scope.totalSqMeters -= $scope.purchaseList.purchaseOrder[index].actualSqMetre;
							$scope.purchaseList.purchaseOrder.splice(index, 1);
						}
						console.log($scope.purchaseList.purchaseOrder);
					});
				});
			};
			
			$scope.viewSlabDetails = function(id, index, tag){
				htmltag = tag;
				console.log("this: ", tag);
				console.log(id);
				console.log("INDEX: ", index);
				
				var error = false;
				var duplicateMessage = "";
				for(var i = 0; i < $scope.purchaseList.purchaseOrder.length; i++){
					if(i != index && $scope.purchaseList.purchaseOrder[i].slabCode == id){
						duplicateMessage = "You have already scanned " + id;
						error = true;
						break;
					}
				}
				
				if(error){
					errorObj.message = duplicateMessage;
					notify(errorObj);
					return;
				}
				
				var url = "../app/inventory?action=readBarcode&barcodeString="+id;
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);
								$scope.purchaseList.purchaseOrder[index].slabCode = data.data.barcodeString;
								$scope.purchaseList.purchaseOrder[index].name = data.data.material.name;
								$scope.purchaseList.purchaseOrder[index].actualLength = data.data.actualLength;
								$scope.purchaseList.purchaseOrder[index].actualHeight = data.data.actualHeight;
								$scope.purchaseList.purchaseOrder[index].thickness = data.data.thickness;
								$scope.purchaseList.purchaseOrder[index].slabId = data.data.slabNoSupreme;
								$scope.purchaseList.purchaseOrder[index].id = data.data.id;
								$scope.purchaseList.purchaseOrder[index].type = data.data.type;
								
								if(typeof $scope.purchaseList.purchaseOrder[index].type != "undefined"
										&& $scope.purchaseList.purchaseOrder[index].type == "TILE"){
									
									$scope.purchaseList.purchaseOrder[index].tileCount = data.data.tileCount;
									$scope.purchaseList.purchaseOrder[index].originalTileCount = data.data.tileCount;
									
									$scope.purchaseList.purchaseOrder[index].actualSqMetre = parseFloat((data.data.actualLength * data.data.actualHeight) / 10000) * data.data.tileCount;
									$scope.count += $scope.purchaseList.purchaseOrder[index].tileCount;
									$scope.totalSqMeters += $scope.purchaseList.purchaseOrder[index].actualSqMetre;
								}
								else{
									$scope.count += 1;
									$scope.totalSqMeters += data.data.actualSqMetre;
									$scope.purchaseList.purchaseOrder[index].actualSqMetre = parseFloat((data.data.actualLength * data.data.actualHeight) / 10000);
								}
								/*$scope.count += 1;
								$scope.totalSqMeters += data.data.actualSqMetre;*/
								
								console.log("$scope.count: ",$scope.count);
								console.log("$scope.totalSqMeters: ",$scope.totalSqMeters);
								$scope.addRow();
							}
							else{
								errorObj.message = data.message;
								notify(errorObj);
							}
							
							
						});
					});
				});
			};
			
			$scope.validate = function(){
				$scope.error = false;
				if(typeof $scope.purchaseList.customerName == "undefined" || $scope.purchaseList.customerName == ""){
					errorObj.message = 'Add Customer Name';
					notify(errorObj);
					$scope.error = true;
					
					console.log("CUSTOMERNAME NOT ADDED");
					return;
				}
				if(!$scope.error){
					for(var i = 0; i < ($scope.purchaseList.purchaseOrder.length); i++){
						if(typeof $scope.purchaseList.purchaseOrder[i].slabCode == "undefined" || $scope.purchaseList.purchaseOrder[i].slabCode == ""){
							errorObj.message = 'Slab Code does not exist.';
							notify(errorObj);
							$scope.error = true;
							break;
						}
						else if(typeof $scope.purchaseList.purchaseOrder[i].id == "undefined" || $scope.purchaseList.purchaseOrder[i].id == ""){
							errorObj.message = 'Stock Id does not exist.';
							notify(errorObj);
							$scope.error = true;
							break;
						}
						
						if((typeof $scope.purchaseList.purchaseOrder[i].tile != "undefined" && typeof $scope.purchaseList.purchaseOrder[i].tile)
							&& ($scope.purchaseList.purchaseOrder[i].originalTileCount > 0)){
							
							if($scope.purchaseList.purchaseOrder[i].tileCount > $scope.purchaseList.purchaseOrder[i].originalTileCount){
								errorObj.message = 'Tile Count exceeds the limit of '+$scope.purchaseList.purchaseOrder[i].originalTileCount;
								notify(errorObj);
								$scope.error = true;
								break;
							}
						}
						else if((typeof $scope.purchaseList.purchaseOrder[i].tile != "undefined" && typeof $scope.purchaseList.purchaseOrder[i].tile)
							&& ($scope.purchaseList.purchaseOrder[i].originalTileCount <= 0)){
							
								errorObj.message = 'No tiles exist in '+$scope.purchaseList.purchaseOrder[i].barcodeString;
								notify(errorObj);
								$scope.error = true;
								break;
						}
						
					}
				}
				if($scope.error){
					return;
				}
			};
			
			function checkForDuplicates(array) {
			  return new Set(array).size !== array.length;
			}
			
			
			$scope.changeTileCount = function(purchaseObj){
				if(purchaseObj.tileCount > purchaseObj.originalTileCount){
					errorObj.message = 'Count more than limit of '+ purchaseObj.originalTileCount;
					notify(errorObj);
					$scope.count = 0;
				}
				else if(purchaseObj.tileCount < 1){
					errorObj.message = 'Minimum 1 tile needs to be there in '+purchaseObj.barcodeString;
					notify(errorObj);
					$scope.count = 0;
				}
				
				else{
					console.log(purchaseObj);
					$scope.count = 0;
					$scope.totalSqMeters = 0;
					console.log("$scope.purchaseList.purchaseOrder.length: ", $scope.purchaseList.purchaseOrder.length)
					console.log("$scope.purchaseList.purchaseOrder: ", $scope.purchaseList.purchaseOrder)
					for(var i = 0; i < $scope.purchaseList.purchaseOrder.length; i++){
						console.log("$scope.count: ", $scope.count);
						console.log("$scope.count: ----------------------------");
						if(typeof $scope.purchaseList.purchaseOrder[i].slabCode != "undefined" 
								&& $scope.purchaseList.purchaseOrder[i].slabCode != ""
								&& typeof $scope.purchaseList.purchaseOrder[i].type != "undefined"
								&& $scope.purchaseList.purchaseOrder[i].type != ""
								&& $scope.purchaseList.purchaseOrder[i].type == "TILE"){
							console.log("entered if");
							$scope.count += parseInt($scope.purchaseList.purchaseOrder[i].tileCount);
							console.log("$scope.purchaseList.purchaseOrder[i].actualSqMetre: ", $scope.purchaseList.purchaseOrder[i].actualSqMetre);
							$scope.totalSqMeters += parseFloat($scope.purchaseList.purchaseOrder[i].actualLength * $scope.purchaseList.purchaseOrder[i].actualHeight / 10000) * parseInt($scope.purchaseList.purchaseOrder[i].tileCount);
						}
						else if(typeof $scope.purchaseList.purchaseOrder[i].slabCode != "undefined" 
								&& $scope.purchaseList.purchaseOrder[i].slabCode != ""
								&& typeof $scope.purchaseList.purchaseOrder[i].type != "undefined"
								&& $scope.purchaseList.purchaseOrder[i].type != ""
								&& $scope.purchaseList.purchaseOrder[i].type != "TILE"){
							console.log("entered ielse");
							$scope.count += parseInt(1);
							$scope.totalSqMeters += parseFloat($scope.purchaseList.purchaseOrder[i].actualSqMetre);
						}
						else{
						}
					}
				}
			};
			
			$scope.assignCustomerName = function(customerObj){
				customerObj = JSON.parse(customerObj);
				console.log(customerObj);
				$scope.purchaseList.customerId = customerObj.id;
				$scope.purchaseList.customerName = customerObj.name;
				
			};
			
			
			
			
			$scope.assignCustomerAddress = function(customerObj){
				
				$timeout(function() {
					$scope.$apply(function() {
						console.log(customerObj);
		//				customerObj = JSON.parse(customerObj);
		//				console.log(customerObj);
		
						if(typeof $scope.editOrderList.potentialCustomerDetailsList != "undefined" && $scope.editOrderList.potentialCustomerDetailsList.length > 0){
							for(var i = 0; i < $scope.editOrderList.potentialCustomerDetailsList.length; i++){
								if($scope.editOrderList.potentialCustomerDetailsList[i].emirate == $scope.editOrderList.emirate){
									$scope.editOrderList.emirateDetails = $scope.editOrderList.potentialCustomerDetailsList[i];
									$scope.editOrderList.emirate = $scope.editOrderList.potentialCustomerDetailsList[i].emirate;
									$scope.editOrderList.phone = $scope.editOrderList.potentialCustomerDetailsList[i].phone;
									break;
								}
							}
						}
						
						console.log("$scope.editOrderList: ", $scope.editOrderList);
					});
				});
			};
			
			
			
			$scope.uploadPurchaseOrder = function(){
				
				console.log("$scope.purchaseList: ", $scope.purchaseList);
				if(typeof $scope.purchaseList.purchaseOrder[$scope.purchaseList.purchaseOrder.length - 1].id == "undefined" 
					|| $scope.purchaseList.purchaseOrder[$scope.purchaseList.purchaseOrder.length - 1].id == ""
					|| $scope.purchaseList.purchaseOrder[$scope.purchaseList.purchaseOrder.length - 1].id == 0){
					
					$scope.purchaseList.purchaseOrder.splice($scope.purchaseList.purchaseOrder.length - 1, 1);
				}
				
				
				if(typeof $scope.purchaseList.customerName != "undefined" && $scope.purchaseList.customerName!= ""){
					$scope.purchaseList.customerName = $scope.purchaseList.customerName;
					if(typeof $scope.purchaseList.customerId != "undefined" && $scope.purchaseList.customerId!= ""){
						$scope.purchaseList.customerId = $scope.purchaseList.customerId;
					}
				}
				else{
					$scope.purchaseList.customerName = $scope.purchaseList.customer;
					$scope.purchaseList.customerId = 0;
				}
				
				$scope.validate();
				
				if($scope.error){
					//alert("Error is still there");
					return;
				}
				
				console.log("Passed validation");
				
				console.log("$scope.purchaseList: ", $scope.purchaseList);
				
				var obj = {};
				
				obj.customerName = $scope.purchaseList.customerName;
				obj.customerId = $scope.purchaseList.customerId;
				if(typeof $scope.purchaseList.cuttingOrderNumber != "undefined" && $scope.purchaseList.cuttingOrderNumber != ""){
					obj.cuttingOrderNumber = $scope.purchaseList.cuttingOrderNumber;
				}
				else{
					obj.cuttingOrderNumber = 0;
				}
				obj.purchaseOrder = [];
				for(var i = 0; i < $scope.purchaseList.purchaseOrder.length; i++){
					/*obj.purchaseOrder.push($scope.purchaseList.purchaseOrder[i].id);*/
					var purchaseOrder = {};
					purchaseOrder.id = $scope.purchaseList.purchaseOrder[i].id;
					purchaseOrder.type = $scope.purchaseList.purchaseOrder[i].type;
					obj.purchaseOrder.push(purchaseOrder);
					
					if(typeof $scope.purchaseList.purchaseOrder[i].type != "undefined"
						&&$scope.purchaseList.purchaseOrder[i].type == "TILE"){
						purchaseOrder.tileCount = $scope.purchaseList.purchaseOrder[i].tileCount;
					}
					
				}
				
				if(checkForDuplicates(obj.purchaseOrder)){
					console.log("Duplicate value in purchase list");
					errorObj.message = 'Remove Duplicate Values from Purchase List';
					notify(errorObj);
					return;
				}
				
				
				obj.count = $scope.count;
				obj.totalMtSquare = $scope.totalSqMeters;
				obj.orderScannedBy = $scope.user.id;
				
				console.log("OBJ: ", obj);

				
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
				$http.defaults.transformRequest = [ function(data) {
					return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
				}];
				var url = "../app/order?action=addPurchaseOrder";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);

							if(data.success){
								successObj.message = "Order placed successfully";
								notify(successObj);
								
								$scope.count = 0;
								$scope.totalSqMeters = 0;
								window.location.reload();
//								$state.go("dashboards.start");
								
							}
							else{
								errorObj.message = "Order could not be placed";
								notify(errorObj);
							}
						});
					});
				});
			};
			
			
			
			
			/**
			 * View and approve sales order block
			 */
			
			$scope.fetchSalesOrders = function(){
				var url = "../app/order?action=fetchAllOrders";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								
								$scope.salesOrderList = data.data;
								
								$scope.salesOrderList2CM = [];
								$scope.salesOrderList3CM = [];
								
								
								for(var i = 0; i < $scope.salesOrderList.length; i++){
									$scope.salesOrderList2CMSqMeters = 0;
									$scope.salesOrderList3CMSqMeters = 0;
									$scope.salesOrderList[i].customerName = $scope.salesOrderList[i].customer.customerName;
									$scope.salesOrderList[i].slabCount = $scope.salesOrderList[i].cart.length;
									$scope.salesOrderList[i].totalMtSquare = 0;
									for(var j = 0; j < $scope.salesOrderList[i].cart.length; j++){
										$scope.salesOrderList[i].totalMtSquare += $scope.salesOrderList[i].cart[j].inventory.actualSqMetre;
										if($scope.salesOrderList[i].cart[j].inventory.thickness == 2){
											
											$scope.salesOrderList2CMSqMeters += $scope.salesOrderList[i].cart[j].inventory.actualSqMetre;
											$scope.salesOrderList[i].cart[j].thickness = 2;
											
											$scope.salesOrderList[i].sqMeters2Cm = $scope.salesOrderList2CMSqMeters;
										}
										else{
											$scope.salesOrderList[i].cart[j].thickness = 3;
											$scope.salesOrderList3CMSqMeters += $scope.salesOrderList[i].cart[j].inventory.actualSqMetre;
											$scope.salesOrderList[i].sqMeters3Cm = $scope.salesOrderList3CMSqMeters;
										}
									}
								}
								
								console.log("salesOrderList: ", $scope.salesOrderList);
								
								
							}
							else{
								console.log("data success is false");
								console.log(data.message);
								if(data.message == "No New Orders"){
									$scope.salesOrderList = [];
								}
								/*errorObj.message = data.message;
								notify(errorObj);*/
							}
							
						});
					});
				});
			};
			
			$scope.fetchSalesOrders();
			
			if($state.current.name == "sales.approvedSalesOrder"){
				
				$scope.approvedOrderObj = {};
				$scope.approvedOrderObj.duration = 10;
				
				$scope.addToMergeList = function(order, mergeValue){
					$timeout(function() {
						$scope.$apply(function() {
							
							console.log(order);
							console.log(mergeValue);
							if(mergeValue == 2){
								/**
								 *	Merge Action Button Clicked
								 *  Add this as main merged order
								 */
							
								console.log("order.mergechecked: ", mergeValue);
								$scope.mergeAction = true;
								
								order.mergeChecked = 1;
								$scope.mergeOrderList.push(order.id);
								
								$scope.mergeOrderObject.mainOrder = order.id;
								$scope.mergeOrderObject.mergeOrderList = $scope.mergeOrderList;
								console.log($scope.mergeOrderList);
								console.log($scope.mergeOrderObject);

							}
							else if(mergeValue == 1){
								/**
								 *	Checkbox checked
								 */
								$scope.mergeAction = true;
								
								$scope.mergeOrderList.push(order.id);
								$scope.mergeOrderObject.secondaryMergeOrderArray.push(order.id);
								$scope.mergeOrderObject.mergeOrderList = $scope.mergeOrderList;

								console.log($scope.mergeOrderList);
								console.log($scope.mergeOrderObject);
							}
							else if(mergeValue == 0){
								/**
								 *	Checkbox unchecked
								 */
								
								var mergeOrderListIndex =$scope.mergeOrderList.findIndex(x => x ===order.id);
								console.log(mergeOrderListIndex);
								$scope.mergeOrderList.splice(mergeOrderListIndex, 1);
								$scope.clearSelection();
								console.log($scope.mergeOrderList);
							}
							
							
							
						});
					});
				};
				
				$scope.clearSelection = function(){
					$timeout(function() {
						$scope.$apply(function() {
							$scope.mergeAction = false;
						});
					});
					
				};
				
				$scope.cancelMergeOrder = function(){
				
				};
				
				$scope.mergeOrder = function(){
					
					if(typeof $scope.mergeOrderList == "undefined" || $scope.mergeOrderList.length <= 1){
						errorObj.message = "You need to select more than one order.";
						notify(errorObj);
						return;
					}
					var obj={
						mergeOrderList : $scope.mergeOrderList
					};
					var url = "../app/order?action=mergeOrders";
					
					$http.post(url, obj, {}).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								
								if(data.success){
									console.log(data);
									successObj.message = "Orders successfully Merged. Taking you to Order Edit Page.";
									notify(successObj);
									$state.go("sales.editOrder", {
										id : data.id,
									});
								}
								else{
									errorObj.message = data.message;
									notify(errorObj);
								}
								
							});
						});
					});
				};
				
				$scope.assignCustomerNameInApprovedOrders = function(customerObj){
					customerObj = JSON.parse(customerObj);
					console.log(customerObj);
					$scope.approvedOrderObj.customer = customerObj;
					$scope.approvedOrderObj.customerId = customerObj.id;
					$scope.approvedOrderObj.customerName = customerObj.name;
					console.log("$scope.approvedOrderObj: ", $scope.approvedOrderObj);
				};
				
				$scope.startSearch = function(term){
				    var results = [];
				    $scope.customerList.forEach(function(item){
				      if (typeof item.name != "undefined" && item.name != "" && item.name.toLowerCase().indexOf(term.toLowerCase()) > -1) {
				        results.push(item);
				      }
				    });
			      	return results;
				};
				$scope.startMaterialSearch = function(term){
				    var results = [];
				    $scope.materialList.forEach(function(item){
				      if (typeof item.materialName != "undefined" && item.materialName != "" && item.materialName.toLowerCase().indexOf(term.toLowerCase()) > -1) {
				        results.push(item);
				      }
				    });
			      	return results;
				};
				
				$scope.addMaterialToOrder = function(item, coList) {
					console.log("materialItem: ",item);
					$timeout(function() {
						$scope.$apply(function() {
							$scope.approvedOrderObj.materialId = item.materialId;
							$scope.approvedOrderObj.materialName = item.materialName;
						});
					});
				};
				$scope.addCustomerToOrder = function(item, element) {
					console.log("customerItem: ", item);
					$timeout(function() {
						$scope.$apply(function() {
							$scope.approvedOrderObj.customerId = item.id;
							$scope.approvedOrderObj.customerName = item.name;
							
							console.log("$scope.approvedOrderObj: ", $scope.approvedOrderObj);
						});
					});
				};
				
				$scope.fetchApprovedOrders = function(){
					console.log("HELOOOOOOOOOOOOOOOOOO");
					var date;
					
					console.log("$scope.approvedOrderObj.duration: ", $scope.approvedOrderObj.duration);
					if($scope.approvedOrderObj.duration == 6){
						date = moment().subtract($scope.approvedOrderObj.duration, 'months').format("DD-MM-YYYY");
					}
					else if($scope.approvedOrderObj.duration == "All"){
						date = "All";
					}
					else{
						date = moment().subtract($scope.approvedOrderObj.duration, 'days').format("DD-MM-YYYY");
					}
					
					console.log("date: ", date);

					var url = "";
					if(typeof $scope.approvedOrderObj.customerId != "undefined" && $scope.approvedOrderObj.customerId != ""){
						url = "../app/order?action=fetchApprovedOrdersFilterByDate&createdOn="+date+"&customerId="+$scope.approvedOrderObj.customerId;
					}
					else{
						url = "../app/order?action=fetchApprovedOrdersFilterByDate&createdOn="+date;
					}
					
					/*if(typeof $scope.approvedOrderObj.materialId != "undefined" && $scope.approvedOrderObj.materialId != ""){
						url += "&materialId="+$scope.approvedOrderObj.materialId;
					}*/
					
					console.log(url);
					$http.get(url).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								
								if(data.success){
									
									$scope.approvedOrdersList = data.data;
									
									for(var i = 0; i < $scope.approvedOrdersList.length; i++){
										$scope.approvedOrdersList[i].slabCount = $scope.approvedOrdersList[i].cart.length;
										$scope.approvedOrdersList[i].totalMtSquare = 0;
										for(var j = 0; j < $scope.approvedOrdersList[i].cart.length; j++){
											$scope.approvedOrdersList[i].totalMtSquare += $scope.approvedOrdersList[i].cart[j].inventory.actualSqMetre;
										}
									}
									console.log("fetchApprovedOrders: ", $scope.approvedOrdersList);
								}
								else{
									console.log("data success is false");
									console.log(data.error);
									$scope.approvedOrdersList = [];
									$scope.errorObjForApprovedOrder = data;
								}
								
							});
						});
					});
				};
				$scope.fetchApprovedOrders();
				
				
				
				/*$scope.printDO = function(order){
					
					console.log(order);
					
					var url = "../app/deliveryOrder?action=printDO&orderId="+order.id;
					
					$http.get(url, obj, {}).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								if(data.success){
									console.log(data);
									
									*//**
									 * Download DO
									 *//*
								}
								else{
									console.log(data);
								}
								
							});
						});
					});
					
				};*/
			}
			
			
			$scope.moveOrderToInvoice = function(orderId, index){
				
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
				$http.defaults.transformRequest = [ function(data) {
					return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
				}];
				var obj = {};
				obj.orderId = orderId;
				console.log(obj.orderId);
				var url = "../app/order?action=moveOrderToInvoice";
				
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);
								$scope.salesOrderList.splice(index, 1);
								successObj.message = "Order successfully placed. Receipt can be printed now.";
								notify(successObj);
								$state.go("sales.approvedSalesOrder");
							}
							else{
								errorObj.message = data.message;
								notify(errorObj);
							}
							
						});
					});
				});
				
			};
			
			$scope.deleteOrder = function(orderId, index){
				
				console.log(orderId);
				
				var obj = {};
				obj.orderId = orderId;
				
				var url = "../app/order?action=deleteOrder";
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							
							if(data.success){
								console.log(data);
								$scope.salesOrderList.splice(index, 1);
								successObj.message = "Order has been deleted";
								notify(successObj);
								
							}
							else{
								errorObj.message = data.message;
								notify(errorObj);
							}
							
						});
					});
				});
				
			};
			
			$scope.editOrderPage = function(order){
				
				if(order.hasCuttingSlabs){
					$state.go("cutting.editCuttingOrder", {
						id : order.id,
						editOrderList : order
					});
				}
				else{
					$state.go("sales.editOrder", {
						id : order.id,
						editOrderList : order
					});
				}

			};
			
			if($state.current.name == "sales.editOrder"){
				
				$scope.addToCuttingOrder = function(slab, addToCuttingOrder, index){
					
					if(addToCuttingOrder == 1 && !$scope.editOrderList.addToCuttingOrderArray.includes(slab.inventory.id)){
						$scope.editOrderList.addToCuttingOrderArray.push(slab.inventory.id);
					}
					else if(addToCuttingOrder == 0 && $scope.editOrderList.addToCuttingOrderArray.includes(slab.inventory.id)){
						var indexOfSlabId = $scope.editOrderList.addToCuttingOrderArray.indexOf(slab.inventory.id);
						$scope.editOrderList.addToCuttingOrderArray.splice(indexOfSlabId, 1);
					}
					
					$scope.inputTotalMeterSquare += slab.inventory.actualLength * slab.inventory.actualHeight / 10000;
					
					console.log($scope.editOrderList);
				};
				
				$scope.fetchCurrentOrder = function(index){
					var url = "../app/order?action=getOrderDetails&orderId="+index;

					$http.get(url).success(function(data, status, headers, config) {
						$timeout(function() {
							$scope.$apply(function() {
								console.log("First: ", data);
								$scope.editOrderList = data.data;
								if(typeof $scope.editOrderList.companyPickup == "undefined" || $scope.editOrderList.companyPickup == 0 || $scope.editOrderList.companyPickup == ""){
									$scope.editOrderList.companyPickup = 0;
								}
								else if(typeof $scope.editOrderList.companyPickup != "undefined" && $scope.editOrderList.companyPickup){
									$scope.editOrderList.companyPickup = 1;
								}
								
								
								if($scope.editOrderList.customer.VATRegistration == "1"){
									console.log("VAT REG is 1 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(1);
								}
								else{
									console.log("VAT REG is 0 string")
									$scope.editOrderList.customer.VATRegistration = parseInt(0);
								}
								if(typeof $scope.editOrderList.customer.VATRegistrationDate != "undefined" && $scope.editOrderList.customer.VATRegistrationDate != ""){
									$scope.editOrderList.customer.VATRegistrationDate = moment($scope.editOrderList.customer.VATRegistrationDate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
									$scope.editOrderList.LPODate = moment($scope.editOrderList.LPODate, "YYYY-MM-DD").format("DD-MM-YYYY");
								}
								
								for(var i = 0; i < $scope.editOrderList.cart.length; i++){
									if($scope.editOrderList.cart[i].addToCuttingOrder){
										$scope.editOrderList.cart[i].addToCuttingOrder = 1;
									}
									else{
										$scope.editOrderList.cart[i].addToCuttingOrder = 0;
									}
								}
								
								$scope.editOrderList.addToCuttingOrderArray = [];
								console.log("$scope.editOrderList: ", $scope.editOrderList);
								
								
							});
						});
					});
				};
				
				if($stateParams.order != null &&  typeof $stateParams.order != "undefined" && $stateParams.order != ""){
					$scope.editOrderList = order;
					console.log("$state.order: ", $scope.editOrderList);
				}
				else if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCurrentOrder(index);
				}
				
				$scope.deleteCartRow = function(order, cart, index){
					console.log(cart);
					var url = "../app/order?action=deleteCartRow&cartId=" + cart.id;
					console.log(url);
					$http.post(url).success(function(data, status, headers, config) {
						console.log("deleteCartRow result: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);

								if(data.success){
									
									$scope.editOrderList.cart.splice(index, 1);
									
									
									$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
									$scope.editOrderList.totalMtSquare = 0;
									for(var j = 0; j < $scope.editOrderList.cart.length; j++){
										$scope.editOrderList.totalMtSquare += $scope.editOrderList.cart[j].actualSqMetre;
									}
									
									if($scope.editOrderList.cart.length <= 0){
										/**
										 * All cart rows deleted
										 * delete order as well
										 */
										
										successObj.message = "Cart has been cleared";
										notify(successObj);
										console.log("All orders deleted");
										
										
										var url = "../app/order?action=deleteOrder&orderId=" + order.id;
										$http.post(url).success(function(data, status, headers, config) {
											console.log("fetched data: ",data);
											$timeout(function() {
												$scope.$apply(function() {
													
													if(data.success){
														console.log(data);
														$scope.salesOrderList.splice(index, 1);
														successObj.message = "Order has been deleted";
														notify(successObj);
														
														$state.go("sales.salesInvoice");
														
														for(var i = 0; i < $scope.salesOrderList.length; i++){
															$scope.salesOrderList[i].slabCount = $scope.salesOrderList[i].cart.length;
															$scope.salesOrderList[i].totalMtSquare = 0;
															for(var j = 0; j < $scope.salesOrderList[i].cart.length; j++){
																$scope.salesOrderList[i].totalMtSquare += $scope.salesOrderList[i].cart[j].actualSqMetre;
															}
														}
														
													}
													else{
														errorObj.message = data.message;
														notify(errorObj);
													}
													
												});
											});
										});
									}
									else{
										successObj.message = "Cart row deleted successfully.";
										notify(successObj);
									}
								}
								else{
									errorObj.message = data.message;
									notify(errorObj);
								}
							});
						});
					});
				};
				
				$scope.validateEditedOrder = function(){
					
					console.log("$scope.editOrderList: ", $scope.editOrderList);
					$scope.error = false;
					if(typeof $scope.editOrderList.customer.customerName == "undefined" || $scope.editOrderList.customer.customerName == ""){
						errorObj.message = 'Add Customer Name';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					
					if(typeof $scope.editOrderList.customer.emirate == "undefined" || $scope.editOrderList.customer.emirate == ""){
						errorObj.message = 'Please fill Address.';
						notify(errorObj);
						return;
					}
					else{
						$scope.editOrderList.emirate = $scope.editOrderList.emirate;
					}
					
					if(typeof $scope.editOrderList.companyPickup != "undefined" 
						&& $scope.editOrderList.companyPickup == "0" 
						&& (typeof $scope.editOrderList.vehicleNumber == "undefined" || $scope.editOrderList.vehicleNumber == "")){
						errorObj.message = 'Please fill Vehicle Number.';
						notify(errorObj);
						$scope.error = true;
						return;
					}
					else{
						$scope.editOrderList.vehicleNumber = $scope.editOrderList.vehicleNumber;
					}
					
					if(typeof $scope.editOrderList.customer.VATRegistration != "undefined" 
						&& $scope.editOrderList.customer.VATRegistration == "1"){
							
						if(typeof $scope.editOrderList.customer.VATRegistrationDate == "undefined" || $scope.editOrderList.customer.VATRegistrationDate == ""){
							errorObj.message = 'Please Add VAT Registration Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						if(typeof $scope.editOrderList.customer.trnNumber == "undefined" || $scope.editOrderList.customer.trnNumber == ""){
							errorObj.message = 'Please add TRN NUmber.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						
					}
					else{
						$scope.editOrderList.customer.trnNumber = $scope.editOrderList.customer.trnNumber;
						$scope.editOrderList.customer.VATRegistrationDate = $scope.editOrderList.customer.VATRegistrationDate;
						$scope.editOrderList.customer.VATRegistration = $scope.editOrderList.customer.VATRegistration;
					}
					
					if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
						if(typeof $scope.editOrderList.LPODate == "undefined" || $scope.editOrderList.LPODate == ""){
							errorObj.message = 'Please fill LPO Date.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
						if(typeof $scope.editOrderList.LPONumber == "undefined" || $scope.editOrderList.LPONumber == ""){
							errorObj.message = 'Please fill LPO Number.';
							notify(errorObj);
							$scope.error = true;
							return;
						}
						else{
							$scope.editOrderList.LPODate = $scope.editOrderList.LPODate;
							$scope.editOrderList.LPONumber = $scope.editOrderList.LPONumber;
						}
					}
					
					if(!$scope.error){
						for(var i = 0; i < ($scope.editOrderList.cart.length); i++){
							if(typeof $scope.editOrderList.cart[i].inventory.barcodeString == "undefined" || $scope.editOrderList.cart[i].inventory.barcodeString == ""){
								errorObj.message = 'Code does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							else if(typeof $scope.editOrderList.cart[i].inventory.id == "undefined" || $scope.editOrderList.cart[i].inventory.id == ""){
								errorObj.message = 'Slab Id does not exist.';
								notify(errorObj);
								$scope.error = true;
								break;
							}
							
							if($scope.editOrderList.cart[i].inventory.tile && $scope.editOrderList.cart[i].inventory.type == "TILE"){
								if($scope.editOrderList.cart[i].inventory.tileCount > $scope.editOrderList.cart[i].inventory.originalTileCount){
									errorObj.message = 'Count more than limit of '+ $scope.editOrderList.cart[i].inventory.originalTileCount;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
								else if($scope.editOrderList.cart[i].tileCount < 1){
									errorObj.message = 'Minimum 1 tile needs to be there in '+$scope.editOrderList.cart[i].inventory.barcodeString;
									notify(errorObj);
									$scope.count = 0;
									$scope.error = true;
									return;
								}
							}
						}
					}
					if($scope.error){
						return;
					}
				};
				
				/*$scope.changeAddressDetailsOfCustomer = function(custDetId){
					
					console.log($scope.editOrderList);
					
					for(var i = 0; i < $scope.editOrderList.customerDetails.length; i++){
						if($scope.editOrderList.customerDetails[i].id == custDetId){
							if(typeof $scope.editOrderList.customerDetails[i].emirate != "undefined" && $scope.editOrderList.customerDetails[i].emirate != ""){
								$scope.editOrderList.emirate = $scope.editOrderList.customerDetails[i].emirate;
							}
							if(typeof $scope.editOrderList.customerDetails[i].phone != "undefined" && $scope.editOrderList.customerDetails[i].phone != ""){
								$scope.editOrderList.phone = $scope.editOrderList.customerDetails[i].phone;
							}
							if(typeof $scope.editOrderList.customerDetails[i].vehicleNumber != "undefined" && $scope.editOrderList.customerDetails[i].vehicleNumber != ""){
								$scope.editOrderList.vehicleNumber = $scope.editOrderList.customerDetails[i].vehicleNumber;
							}
						}
					}
					
					console.log($scope.editOrderList);
				};*/
				
				$scope.addNewCustomer = function () {
					console.log("addCustomer chosen")
					$scope.addCustomer = true;
					$scope.editCustomer = false;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Customer-Modal.html',
			            controller: AddNewCustomerCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						size: 'lg',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         },
			                        {
			                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
			                        }
			                     ]);
							}
						}
			        });
			    };


				$scope.addNewCustomerAddress = function (editOrderList) {
					$scope.addCustomerAddress = true;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Add-Customer-Address-Modal.html',
			            controller: AddCustomerAddressDetailsCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						size: 'lg',
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         },
									{
			                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
			                        }
			                     ]);
							}
						}
			        });

			    };

				$scope.editExistingCustomer = function (addCustomerObject) {
					$scope.addCustomer = false;
					$scope.editCustomer = true;
					
			        var modalInstance = $modal.open({
			            templateUrl: 'views/common/modal/Edit-Customer-Modal.html',
			            controller: EditCustomerDetailsCtrl,
			            windowClass: "animated fadeInDown",
						scope: $scope,
						backdrop: 'static',
						keyboard: false,
						resolve: {
							loadPlugin: function ($ocLazyLoad) {
								return $ocLazyLoad.load([
			                         {
			                            insertBefore: '#loadBefore',
			                            name: 'toaster',
			                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
			                         }
			                     ]);
							}
						}
			        });

					$scope.fetchCurrentOrder($scope.editOrderList.id);
					
			    };
				
				$scope.saveEditedOrder = function(approveOrder, order){
					
					console.log("$scope.editOrder_form: ", $scope.editOrder_form);
					
					if ($scope.editOrder_form.$valid) {
						
						console.log("approveOrder: ", approveOrder);
						console.log("Order: ", order);
						
						$scope.validateEditedOrder();
						
						if($scope.error){
							//alert("Error is still there");
							return;
						}
						
						console.log("Passed validation");
						
						console.log("$scope.editOrderList: ", $scope.editOrderList);
						
						var obj = {};
						obj.id = order.id;
						obj.customerName = order.customer.customerName;
						obj.approveOrder = approveOrder;
						obj.userId = me.id;
						obj.slabCount = order.slabCount;
						obj.totalMtSquare = order.totalMtSquare;
						obj.emirate = order.customer.emirate;
						obj.vehicleNumber = order.vehicleNumber;
						obj.companyPickup = order.companyPickup;
						obj.narration = order.narration;

						obj.VATRegistration = order.customer.VATRegistration;
						obj.VATRegistrationDate = order.customer.VATRegistrationDate;
						obj.trnNumber = order.customer.trnNumber;

						obj.phone = order.customer.phone;
						obj.userId = $scope.user.id;
						
						
						
						if($scope.editOrderList.addToCuttingOrderArray.length > 0){
							obj.inputTotalMeterSquare = $scope.inputTotalMeterSquare;
							obj.addToCuttingOrderArray = order.addToCuttingOrderArray
							obj.hasCuttingOrders = 1;
						}
						else{
							obj.hasCuttingOrders = 0;
						}
						
						if(typeof $scope.editOrderList.LPONumber != "undefined" && $scope.editOrderList.LPONumber != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						if(typeof $scope.editOrderList.LPODate != "undefined" && $scope.editOrderList.LPODate != ""){
							obj.LPODate = $scope.editOrderList.LPODate;
							obj.LPONumber = $scope.editOrderList.LPONumber;
						}
						
						obj.cart = [];
	
						for(var i = 0; i < $scope.editOrderList.cart.length; i++){
							var cartObj = {};
							cartObj.id = $scope.editOrderList.cart[i].inventory.id;
							cartObj.inventoryId = $scope.editOrderList.cart[i].inventory.id;
							cartObj.actualLength = $scope.editOrderList.cart[i].inventory.actualLength;
							cartObj.actualHeight = $scope.editOrderList.cart[i].inventory.actualHeight;
							cartObj.thickness = $scope.editOrderList.cart[i].inventory.thickness;
							cartObj.type = $scope.editOrderList.cart[i].inventory.type;
							
							if(cartObj.type == 'TILE'){
								cartObj.tileCount = $scope.editOrderList.cart[i].inventory.tileCount;
								cartObj.actualSqMetre = $scope.editOrderList.cart[i].inventory.actualLength * $scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.tileCount / 10000;
							}
							else{
								cartObj.actualSqMetre = $scope.editOrderList.cart[i].inventory.actualLength * $scope.editOrderList.cart[i].inventory.actualHeight / 10000;
								
							}
							
							obj.cart.push(cartObj);
						}
						
						
						console.log("OBJ: ", obj);
						
//						return;
	
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
						$http.defaults.transformRequest = [ function(data) {
							return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
						}];
	
						var url = "../app/order?action=editExistingPurchaseOrder";
						console.log(url);
						$http.post(url, obj, {}).success(function(data, status, headers, config) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
	
									if(data.success){
										successObj.message = "Order saved successfully";
										notify(successObj);
										
										/**
										 * Call Download PDF function
										 */
										
										if(obj.hasCuttingOrders == 1 && data.cuttingOrderId != 0){
											$state.go("cutting.editExistingHalfCuttingOrder", {
												id : data.cuttingOrderId,
											});
										}
										else{
											$state.go("sales.salesInvoice");
										}
										
									}
									else{
										errorObj.message = "Order could not be saved";
										notify(errorObj);
									}
								});
							});
						});
					}
					else{
						$scope.editOrder_form.submitted = true;
					}
				};
				
				
				$scope.calculateTotalSqMetersOnEdit = function(slab){
					$scope.editOrderList.totalMtSquare = 0;
					$scope.editOrderList.slabCount = $scope.editOrderList.cart.length;
					console.log(slab);
					
					if(slab.inventory.tile){
						console.log("slab.inventory.tile is true: ", slab);
						if(slab.inventory.tileCount > slab.inventory.originalTileCount){
							errorObj.message = 'Count more than limit of '+ slab.inventory.originalTileCount;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else if(slab.inventory.tileCount < 1){
							errorObj.message = 'Minimum 1 tile needs to be there in '+slab.inventory.barcodeString;
							notify(errorObj);
							$scope.count = 0;
							return;
						}
						else{
							console.log(slab);
							$scope.count = 0;
							$scope.totalSqMeters = 0;
							
							$scope.totalSqMeters += parseFloat(slab.inventory.actualLength * slab.inventory.actualHeight / 10000) * parseInt(slab.inventory.tileCount);
							slab.inventory.actualSqMetre = ((slab.inventory.actualLength * slab.inventory.actualHeight) / 10000) * slab.inventory.tileCount;
							
							
							$scope.editOrderList.totalMtSquare = 0;
							$scope.editOrderList.slabCount = 0;
							for(var i=0; i < $scope.editOrderList.cart.length; i++){
								
								
								if($scope.editOrderList.cart[i].inventory.tile){
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									console.log("$scope.editOrderList.cart[i].inventory.tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].inventory.tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].inventory.tile is false");
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].inventory.actualSqMetre);
								}
							}
						}
					}
					else{
						console.log("slab.inventory.tile is false");
						console.log("entered ielse");
						$scope.editOrderList.totalMtSquare = 0;
						$scope.editOrderList.slabCount = 0;
						for(var i=0; i < $scope.editOrderList.cart.length; i++){
							
							if($scope.editOrderList.cart[i].inventory.tile){
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									console.log("$scope.editOrderList.cart[i].inventory.tile is true");
									$scope.editOrderList.totalMtSquare += (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000)) * $scope.editOrderList.cart[i].inventory.tileCount;
									$scope.editOrderList.slabCount += parseInt($scope.editOrderList.cart[i].inventory.tileCount)
								}
								else{
									console.log("$scope.editOrderList.cart[i].inventory.tile is false");
									$scope.editOrderList.cart[i].inventory.actualSqMetre = (parseFloat($scope.editOrderList.cart[i].inventory.actualHeight * $scope.editOrderList.cart[i].inventory.actualLength / 10000));
									$scope.editOrderList.slabCount += parseInt(1);
									$scope.editOrderList.totalMtSquare += parseFloat($scope.editOrderList.cart[i].inventory.actualSqMetre);
								}
						}
					}
					
				};
				
				
				$scope.markAllAsCuttingSlab = function(value){
					$timeout(function() {
						$scope.$apply(function() {
							for(var i = 0; i < $scope.editOrderList.cart.length; i++){
								if(value == 1){
									$scope.editOrderList.cart[i].addToCuttingOrder = 1;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i], 1, i)
								}
								else{
									$scope.editOrderList.cart[i].addToCuttingOrder = 0;
									$scope.addToCuttingOrder($scope.editOrderList.cart[i], 0, i)
								}
							}
						});
					});
				};
				
				
				
			}
			
			$scope.goToPrintCuttingOrder = function(id){
				$state.go("print.printCuttingOrder", {
					id : id,
				});
			};
			$scope.goToPrintDeliveryOrder = function(id){
				$state.go("print.printDeliveryOrder", {
					id : id,
				});
			};
			$scope.goToScanCuttingOrder = function(id){
				$state.go("cutting.scanCuttingOrder", {
					id : id,
				});
			};
			
		});
	});
}]);

