inspinia.lazy.controller("SupplierCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.editSupplier = {};
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'Download'},
	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			$scope.supplierList = allSupplierList;
			
			console.log("$scope.materialList: ", $scope.materialList);
			
			$scope.ok = function () {
		        $modalInstance.close();
		    };
		
		    $scope.cancel = function () {
		        $modalInstance.dismiss('cancel');
		    };
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.supplier = {
				name : "",
				originCountry : "",
				lowerPriceRange: 0,
				higherPriceRange : 0
			};

			$scope.getSupplierList = function(){
				var url = "../app/supplier?action=getAllSupplierList";
				console.log(url);
				$http.get(url).success(function(data) {
					console.log("fetched data: ",data);
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.supplierList = data;
							console.log($scope.supplierList);
						});
					});
				});
			};
			
			
			$scope.uploadSingleSupplierList = function(){
				if ($scope.supplier_form.$valid) {
		            // Submit as normal
					
					var obj = $scope.supplier;
				
					if(typeof obj.supplierName == "undefined" || obj.supplierName == ""){
						var obj = {
							type: 'danger',
							title : "Supplier Name not given",
							body : 'Supplier could not be saved',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);

						return;
					}
					
					var exists = false;
					for(var i = 0; i < $scope.supplierList.length; i++){
						if($scope.supplierList[i].supplierName == obj.supplierName.toUpperCase()){
							exists = true;
							break
						}
					}
					
					
					if(exists){
						errorObj.message = 'Supplier already exists';
						notify(errorObj);
						return;
					}
					else{
						var url = "../app/supplier?action=updateSingleSupplier";
						console.log(url);
						$http.post(url, obj, {}).success(function(data) {
							console.log("fetched data: ",data);
							$timeout(function() {
								$scope.$apply(function() {
									console.log(data);
									if(data.success){
										var obj = {
											type: 'success',
											title : 'Saved!',
											body : 'Supplier saved successfully.',
											showCloseButton: true,
            								timeout: 2000
											
										};
										
										$scope.toasterNotification(obj);
										
										$scope.getSupplierList();
									}
									else{
										
										var obj = {
											type: 'danger',
											title : data.message,
											body : $scope.supplier.supplierName+ 'could not be saved',
											showCloseButton: true,
            								timeout: 2000
										};
										
										$scope.toasterNotification(obj);
										return;
									
									}
								});
							});
						});
					}

		        } else {
		            $scope.supplier_form.submitted = true;
		        }
				
			};
			
			$scope.deleteSupplier = function(supplier, index){
				
				if(!confirm("Do you really want to delete "+ supplier.supplierName+" ?")){
					return;
				}
				else{
					console.log("call delete func");
					var url = "../app/supplier?action=deleteSupplier";
					
					var obj = {
						supplierName : supplier.supplierName,
						id: supplier.supplierId
					};
					
					console.log(url);
					$http.post(url, obj, {}).success(function(data) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								if(data.success){
									var obj = {
										type: 'success',
										title : 'Deleted!',
										body : 'Supplier deleted successfully.',
										showCloseButton: true,
        								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.getSupplierList();
								}
								else{
									
									var obj = {
										type: 'error',
										title : "",
										body : data.message,
										showCloseButton: true,
        								timeout: 2000
									};
									
									$scope.toasterNotification(obj);
									return;
								
								}
							});
						});
					});
				}
				
			};
			
			$scope.editSupplier = function(supplier, index){
				
				$timeout(function() {
					$scope.$apply(function() {
					
						supplier.editSupplier = true;
						console.log("supplier for edit: ", supplier);
						console.log(index);
						
					});
				});
				
			};
			
			$scope.saveEditedSupplier = function(supplier, index){
				
				var obj = supplier;
				
					if(typeof obj.supplierName == "undefined" || obj.supplierName == ""){
						var obj = {
							type: 'danger',
							title : "Supplier Name not given",
							body : '',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);

						return;
					}
					
				var url = "../app/supplier?action=editSingleSupplier";
				console.log(url);
				$http.post(url, obj, {}).success(function(data) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							if(data.success){
								var obj = {
									type: 'success',
									title : 'Saved!',
									body : 'Supplier saved successfully.',
									showCloseButton: true,
    								timeout: 2000
									
								};
								
								$scope.toasterNotification(obj);
								
								$scope.getSupplierList();
							}
							else{
								
								var obj = {
									type: 'danger',
									title : data.message,
									body : $scope.supplier.supplierName+ 'could not be saved',
									showCloseButton: true,
    								timeout: 2000
								};
								
								$scope.toasterNotification(obj);
								return;
							
							}
						});
					});
				});
				
			};
			
			/*$scope.editSupplier = function(supplier, index){
				$timeout(function() {
		$scope.$apply(function() {
				$scope.editSupplier = supplier;
				var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/supplier-addition-modal.html',
		            windowClass: "animated fadeIn"
		        });
		        });
		        });
				
			};*/
			
			
			
			
			/*$scope.supplierForm = function() {
		        if ($scope.supplier_form.$valid) {
		            // Submit as normal
		        } else {
		            $scope.supplier_form.submitted = true;
		        }
		    }*/
		
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			$scope.uploadBulkSupplierList = function() {
				
				var form = $("#upload-bulk-supplier-list-form")[0];
				var url = "../app/bulkSupplierlistupload";
				
				console.log(new FormData(form));
				$.ajax({
					url : url,
					data : new FormData(form),
					enctype: 'multipart/form-data',
			        processData: false,  // Important!
			        contentType: false,
			        cache: false,
					type : "post",
					dataType : "json",
					xhr : function() {
						var myXhr = $.ajaxSettings.xhr();
						if (myXhr.upload) {
							// For handling the progress of the upload

							myXhr.upload.addEventListener('progress', function(e) {
								if (e.lengthComputable) {
									var loaded = (e.loaded / e.total) * 90;
									console.log("Loaded : " + loaded);
								}
							}, false);
						}
						return myXhr;
					},
					success : function(data) {
						if (data.success) {
							// Request 100% completed
							alert("Data submitted");
						} else {
							alert(data.error);
						}
					}
				});
			};
		});
	});
}]);

