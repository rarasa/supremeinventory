var param = function(obj) {
	var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

	for (name in obj) {
		value = obj[name];

		if (value instanceof Array) {
			for (i = 0; i < value.length; ++i) {
				subValue = value[i];
				fullSubName = name + '[' + i + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += param(innerObj) + '&';
			}
		} else if (value instanceof Object) {
			for (subName in value) {
				subValue = value[subName];
				fullSubName = name + '[' + subName + ']';
				innerObj = {};
				innerObj[fullSubName] = subValue;
				query += param(innerObj) + '&';
			}
		} else if (value !== undefined && value !== null)
			query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
	}

	return query.length ? query.substr(0, query.length - 1) : query;
};

showDatePicker = function(elem){
	console.log(elem);
    $(elem).datepicker({ dateFormat: 'dd-mm-yy' }).datepicker("show");
}

inspinia.controller("MainCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams) {


	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
	$http.defaults.transformRequest = [ function(data) {
		return angular.isObject(data) && String(data) !== '[object File]' ? param(data): data;
	}];
	
	
	$rootScope.user = {};
	$rootScope.materialList = {};
	$rootScope.statusList = {};
	$rootScope.containerDetails = containerDetails;
	$rootScope.containerListInLastMonth = containerListInLastMonth;
	$rootScope.allLocationsList = allLocationsList;
	$rootScope.myBankList = myBankDetails;
	
	$rootScope.completedContainerDetails = [];
	
	
	$scope.menuItemsMainPage = [
		
		{
			id: 1,
			heading: 'Open Orders',
			imageURL: 'img/icon2/open-orders.png',
			description : 'Edit, Approve, or Delete Pending Orders',
			link: 'sales.salesInvoice',
			show: true,
			roleGreaterThan: 0
		},
		{
			id: 2,
			heading: 'Print DO',
			imageURL: 'img/icon2/delivery.png',
			description : 'Merge or Print DO of Approved Orders',
			link: 'sales.approvedSalesOrder',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 3,
			heading: 'Container Logistics',
			imageURL: 'img/icon2/delivery.png',
			description : 'Add New, List Existing, Or Edit existing Container Status',
			link: 'admin.viewContainerStatus',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 4,
			heading: 'All Stock Upload',
			imageURL: 'img/icon2/upload-stock.png',
			description : 'Add New Container Slab Details',
			link: 'uploads.bulkInventoryUploadAll',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 5,
			heading: 'Print Labels',
			imageURL: 'img/icon2/print-labels.png',
			description : 'Print Labels for Uploaded Containers',
			link: 'listView.containerToPrint',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 6,
			heading: 'Admin',
			imageURL: 'img/icon2/admin.png',
			description : 'Add, Edit, or ApproveAdmin Details',
			link: 'admin.dashboard',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 7,
			heading: 'New Sales',
			imageURL: 'img/icon2/new-sales.png',
			description : 'Add New Sales',
			link: 'sales.salesOrder',
			show: true,
			roleGreaterThan: 0
		},
		{
			id: 8,
			heading: 'Cutting Order',
			imageURL: 'img/icon2/new-sales.png',
			description : 'Add New Cutting Orders',
			link: 'cutting.addCuttingOrder',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 9,
			heading: 'Stock Details',
			imageURL: 'img/icon2/stock-details.png',
			description : 'View Existing Stock Details',
			link: 'listView.fetchFilteredInventoryList',
			show: true,
			roleGreaterThan: 4
		},
		
	];
	
	
	$scope.menuItemsAdminPage = [
		
		{
			id: 1,
			heading: 'Re-Stock',
			imageURL: 'img/icon2/old-stock.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.restock',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 2,
			heading: 'Container Status',
			imageURL: 'img/icon2/delivery.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.viewContainerStatus',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 3,
			heading: 'Material',
			imageURL: 'img/icon2/add-material.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.materialUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 4,
			heading: 'Suppliers',
			imageURL: 'img/icon2/add-supplier.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.supplierUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 5,
			heading: 'Users',
			imageURL: 'img/icon2/add-roles.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.userUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 6,
			heading: 'Customers',
			imageURL: 'img/icon2/add-roles.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.customerUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 7,
			heading: 'Add Slabs',
			imageURL: 'img/icon2/add-roles.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.customerUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 8,
			heading: 'Locations',
			imageURL: 'img/icon2/new-location.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.locationUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 9,
			heading: 'Machines',
			imageURL: 'img/icon2/add-material.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.machineUpload',
			show: true,
			roleGreaterThan: 4
		},
		{
			id: 10,
			heading: 'Cutting Types',
			imageURL: 'img/icon2/add-material.png',
			/*description : 'Edit, Approve, or Delete Pending Orders',*/
			link: 'admin.cuttingTypeUpload',
			show: true,
			roleGreaterThan: 4
		},
		
	];
	
	for(var i = 0; i < $rootScope.containerDetails.length; i++){
		
		if($rootScope.containerDetails[i].statusDetails.containerStatusObj.finalStatus == true
				&& $rootScope.containerDetails[i].statusDetails.DOStatusObj.finalStatus == true){
			
			console.log("finalStatus is false");
			$rootScope.completedContainerDetails.push($rootScope.containerDetails[i]);
			$rootScope.containerDetails.splice(i, 1);
			i-=1;
		}
		
	}
	
	console.log("$rootScope.completedContainerDetails: ", $rootScope.completedContainerDetails);
	console.log("$rootScope.containerDetails: ", $rootScope.containerDetails);
	
	$scope.getMaterialList = function(){
		var url = "../app/material?action=getAllMaterialList";
		console.log(url);
		$http.get(url).success(function(data, status, headers, config) {
			
			$timeout(function() {
				$scope.$apply(function() {
					$rootScope.materialList = data;
					console.log($scope.materialList);
					allMaterialList = data;
				});
			});
		});
	};
	
	$scope.getStatuses = function(){
		var url = "../app/status?action=getAllStatusesAndTypes";
		$http.get(url).success(function(data, status, headers, config) {
			$timeout(function() {
				$scope.$apply(function() {
					$rootScope.statusList = data;
					console.log(data);
				});
			});
		});
	};
	$scope.getStatuses();
	
	$rootScope.addNewMaterial = function () {
		console.log("addMaterial chosen")
		$scope.addMaterial = true;
		$scope.editMaterial = false;
		
        var modalInstance = $modal.open({
            templateUrl: 'views/common/modal/Material-Modal.html',
            controller: AddNewMaterialCtrl,
            windowClass: "animated fadeInDown",
			scope: $scope,
			size: 'md',
			backdrop: 'static',
			keyboard: false,
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                         },
                        {
                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
                        }
                     ]);
				}
			}
        });
    };
	
	
	/*function getRandomString() {
    	var text = "";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    	for (var i = 0; i < 6; i++)
    		text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
    };
    
	randomString = $scope.randomString;*/
    $scope.randomString = randomString;
    
    console.log("RANDOM STRING: ", $scope.randomString);
	
	$timeout(function() {
		$scope.$apply(function() {

			$rootScope.user = me;
			$rootScope.masterInventoryList = [];
			
			$scope.NumberOfCuttingOrders = 0;
			$scope.ExtraProfitGiven = 0;
			$scope.ExtraProfitAmount = 0;
			$scope.CuttingOrderAmount = 0;
			
			$scope.Machine1Amount = 0;
			$scope.Machine2Amount = 0;
			$scope.Machine3Amount = 0;
			$scope.Machine4Amount = 0;
			$scope.Machine5Amount = 0;
			$scope.Machine6Amount = 0;
			
			$scope.Machine1DataArray10Days = [];
			$scope.Machine2DataArray10Days = [];
			$scope.Machine3DataArray10Days = [];
			$scope.Machine4DataArray10Days = [];
			$scope.Machine5DataArray10Days = [];
			$scope.Machine6DataArray10Days = [];
			
			$scope.Machine1DataArrayMonthly = [];
			$scope.Machine2DataArrayMonthly = [];
			$scope.Machine3DataArrayMonthly = [];
			$scope.Machine4DataArrayMonthly = [];
			$scope.Machine5DataArrayMonthly = [];
			$scope.Machine6DataArrayMonthly = [];
			
			$scope.Machine1DataArrayQuarterly = [];
			$scope.Machine2DataArrayQuarterly = [];
			$scope.Machine3DataArrayQuarterly = [];
			$scope.Machine4DataArrayQuarterly = [];
			$scope.Machine5DataArrayQuarterly = [];
			$scope.Machine6DataArrayQuarterly = [];

			$scope.Machine1DataArrayHalfYearly = [];
			$scope.Machine2DataArrayHalfYearly = [];
			$scope.Machine3DataArrayHalfYearly = [];
			$scope.Machine4DataArrayHalfYearly = [];
			$scope.Machine5DataArrayHalfYearly = [];
			$scope.Machine6DataArrayHalfYearly = [];

			$scope.Machine1DataArrayYearly = [];
			$scope.Machine2DataArrayYearly = [];
			$scope.Machine3DataArrayYearly = [];
			$scope.Machine4DataArrayYearly = [];
			$scope.Machine5DataArrayYearly = [];
			$scope.Machine6DataArrayYearly = [];
			
			getSupplierList = function(){
				var url = "../app/supplier?action=getAllSupplierList";
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$rootScope.supplierList = data;
						});
					});
				});
			};
			getMaterialList = function(){
				var url = "../app/material?action=getAllMaterialList";
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$rootScope.materialList = data;
						});
					});
				});
			};
			
			getAllCuttingOrdersList = function(){
				var url = "../app/cutting?action=fetchAllCuttingOrders";
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$rootScope.cuttingOrdersList = data.data;
							console.log("$rootScope.cuttingOrdersList: ",$rootScope.cuttingOrdersList);

							for(var i = 0 ; i < $rootScope.cuttingOrdersList.length; i++){
								if(typeof $rootScope.cuttingOrdersList[i].particulars != "undefined" 
									&& $rootScope.cuttingOrdersList[i].particulars != "Remarks"){
									
									$scope.NumberOfCuttingOrders += 1;
									$scope.CuttingOrderAmount += $rootScope.cuttingOrdersList[i].amount;
									
									if($rootScope.cuttingOrdersList[i].particulars == "1"){
										$scope.Machine1Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){
											
											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine1DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine1DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine1DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine1DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																console.log(momentGivenDate);
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine1DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									else if($rootScope.cuttingOrdersList[i].particulars == "2"){
										$scope.Machine2Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){

											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine2DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine2DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine2DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine2DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine2DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									else if($rootScope.cuttingOrdersList[i].particulars == "3"){
										$scope.Machine3Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){

											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine3DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine3DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine3DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine3DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine3DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									else if($rootScope.cuttingOrdersList[i].particulars == "4"){
										$scope.Machine4Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){

											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine4DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine4DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine4DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine4DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine4DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									else if($rootScope.cuttingOrdersList[i].particulars == "5"){
										$scope.Machine5Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){

											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine5DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine5DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine5DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine5DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine5DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									else if($rootScope.cuttingOrdersList[i].particulars == "6"){
										$scope.Machine6Amount += $rootScope.cuttingOrdersList[i].amount;
										
										if(typeof $rootScope.cuttingOrdersList[i].cuttingOrderDate != "undefined" 
											&& $rootScope.cuttingOrdersList[i].cuttingOrderDate != ""){

											var intermediateArray = [];
											var momentDate = moment();
											var momentGivenDate = moment($rootScope.cuttingOrdersList[i].cuttingOrderDate, "YYYY-MM-DD");
											var GivenDateUTC = Date.UTC(momentGivenDate.format("YYYY"),
													momentGivenDate.format("M") - 1,
													momentGivenDate.format("D")
											);
											if(momentDate.diff(momentGivenDate, 'days') <= 365){
												//add to yearly date
												intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
												$scope.Machine6DataArrayYearly.push(intermediateArray);
												if(momentDate.diff(momentGivenDate, 'days') <= 183){
													//add to half yearly date
													intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
													$scope.Machine6DataArrayHalfYearly.push(intermediateArray);
													if(momentDate.diff(momentGivenDate, 'days') <= 93){
														//add to quarterly date
														intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
														$scope.Machine6DataArrayQuarterly.push(intermediateArray);
														if(momentDate.diff(momentGivenDate, 'days') <= 31){
															//add to yearly date
															intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
															$scope.Machine6DataArrayMonthly.push(intermediateArray);
															if(momentDate.diff(momentGivenDate, 'days') <= 10){
																//add to yearly date
																intermediateArray = [GivenDateUTC, $rootScope.cuttingOrdersList[i].amount];
																$scope.Machine6DataArray10Days.push(intermediateArray);
															}
														}
													}
												}
											}
										}
									}
									
									
									
								}
								else{
									$scope.ExtraProfitAmount += $rootScope.cuttingOrdersList[i].amount;
								}
							}
							
								/*Highcharts.chart('cuttingOrderChart', {
									chart: {
								        type: 'spline'
								    },
								    title: {
								        text: 'Snow depth at Vikjafjellet, Norway'
								    },
								    subtitle: {
								        text: 'Irregular time data in Highcharts JS'
								    },
								    xAxis: {
								        type: 'datetime',
								        dateTimeLabelFormats: { // don't display the dummy year
								            month: '%e. %b',
								            year: '%b'
								        },
								        title: {
								            text: 'Date'
								        }
								    },
								    yAxis: {
								        title: {
								            text: 'Snow depth (m)'
								        },
								        min: 0
								    },
								    tooltip: {
								        headerFormat: '<b>{series.name}</b><br>',
								        pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
								    },

								    plotOptions: {
								        series: {
								            marker: {
								                enabled: true
								            }
								        }
								    },

								    colors: ['#6CF', '#39F', '#06C', '#036', '#000'],

								    // Define the data points. All series have a dummy year
								    // of 1970/71 in order to be compared on the same x axis. Note
								    // that in JavaScript, months start at 0 for January, 1 for February etc.
								    series: [{
								        name: "Machine 1 - 10 Days",
								        data: $scope.Machine1DataArray10Days
								    }],

								    responsive: {
								        rules: [{
								            condition: {
								                maxWidth: 500
								            },
								            chartOptions: {
								                plotOptions: {
								                    series: {
								                        marker: {
								                            radius: 2.5
								                        }
								                    }
								                }
								            }
								        }]
								    }
								});*/
						});
					});
				});
			};

			getSupplierList();
			getMaterialList();
			/*getAllCuttingOrdersList();*/
			
			
			
		});
	});
	/**
     * daterange - Used as initial model for data range picker in Advanced form view
     */
    this.daterange = {startDate: null, endDate: null}

    /**
     * slideInterval - Interval for bootstrap Carousel, in milliseconds:
     */
    this.slideInterval = 5000;


    /**
     * states - Data used in Advanced Form view for Chosen plugin
     */
    this.states = [
        'Alabama',
        'Alaska',
        'Arizona',
        'Arkansas',
        'California',
        'Colorado',
        'Connecticut',
        'Delaware',
        'Florida',
        'Georgia',
        'Hawaii',
        'Idaho',
        'Illinois',
        'Indiana',
        'Iowa',
        'Kansas',
        'Kentucky',
        'Louisiana',
        'Maine',
        'Maryland',
        'Massachusetts',
        'Michigan',
        'Minnesota',
        'Mississippi',
        'Missouri',
        'Montana',
        'Nebraska',
        'Nevada',
        'New Hampshire',
        'New Jersey',
        'New Mexico',
        'New York',
        'North Carolina',
        'North Dakota',
        'Ohio',
        'Oklahoma',
        'Oregon',
        'Pennsylvania',
        'Rhode Island',
        'South Carolina',
        'South Dakota',
        'Tennessee',
        'Texas',
        'Utah',
        'Vermont',
        'Virginia',
        'Washington',
        'West Virginia',
        'Wisconsin',
        'Wyoming'
    ];

    /**
     * check's - Few variables for checkbox input used in iCheck plugin. Only for demo purpose
     */
    this.checkOne = true;
    this.checkTwo = true;
    this.checkThree = true;
    this.checkFour = true;

    /**
     * knobs - Few variables for knob plugin used in Advanced Plugins view
     */
    this.knobOne = 75;
    this.knobTwo = 25;
    this.knobThree = 50;

    /**
     * Variables used for Ui Elements view
     */
    this.bigTotalItems = 175;
    this.bigCurrentPage = 1;
    this.maxSize = 5;
    this.singleModel = 1;
    this.radioModel = 'Middle';
    this.checkModel = {
        left: false,
        middle: true,
        right: false
    };

    /**
     * groups - used for Collapse panels in Tabs and Panels view
     */
    this.groups = [
        {
            title: 'Dynamic Group Header - 1',
            content: 'Dynamic Group Body - 1'
        },
        {
            title: 'Dynamic Group Header - 2',
            content: 'Dynamic Group Body - 2'
        }
    ];

    /**
     * alerts - used for dynamic alerts in Notifications and Tooltips view
     */
    this.alerts = [
        { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
        { type: 'success', msg: 'Well done! You successfully read this important alert message.' },
        { type: 'info', msg: 'OK, You are done a great job man.' }
    ];

    /**
     * addAlert, closeAlert  - used to manage alerts in Notifications and Tooltips view
     */
    this.addAlert = function() {
        this.alerts.push({msg: 'Another alert!'});
    };

    this.closeAlert = function(index) {
        this.alerts.splice(index, 1);
    };

    /**
     * randomStacked - used for progress bar (stacked type) in Badges adn Labels view
     */
    this.randomStacked = function() {
        this.stacked = [];
        var types = ['success', 'info', 'warning', 'danger'];

        for (var i = 0, n = Math.floor((Math.random() * 4) + 1); i < n; i++) {
            var index = Math.floor((Math.random() * 4));
            this.stacked.push({
                value: Math.floor((Math.random() * 30) + 1),
                type: types[index]
            });
        }
    };
    /**
     * initial run for random stacked value
     */
    this.randomStacked();

    /**
     * summernoteText - used for Summernote plugin
     */
    this.summernoteText = ['<h3>Hello Jonathan! </h3>',
    '<p>dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the dustrys</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more',
        'recently with</p>'].join('');

    /**
     * General variables for Peity Charts
     * used in many view so this is in Main controller
     */
    this.BarChart = {
        data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2, 4, 7, 3, 2, 7, 9, 6, 4, 5, 7, 3, 2, 1, 0, 9, 5, 6, 8, 3, 2, 1],
        options: {
            fill: ["#1ab394", "#d7d7d7"],
            width: 100
        }
    };

    this.BarChart2 = {
        data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };

    this.BarChart3 = {
        data: [5, 3, 2, -1, -3, -2, 2, 3, 5, 2],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };

    this.LineChart = {
        data: [5, 9, 7, 3, 5, 2, 5, 3, 9, 6, 5, 9, 4, 7, 3, 2, 9, 8, 7, 4, 5, 1, 2, 9, 5, 4, 7],
        options: {
            fill: '#1ab394',
            stroke: '#169c81',
            width: 64
        }
    };

    this.LineChart2 = {
        data: [3, 2, 9, 8, 47, 4, 5, 1, 2, 9, 5, 4, 7],
        options: {
            fill: '#1ab394',
            stroke: '#169c81',
            width: 64
        }
    };

    this.LineChart3 = {
        data: [5, 3, 2, -1, -3, -2, 2, 3, 5, 2],
        options: {
            fill: '#1ab394',
            stroke: '#169c81',
            width: 64
        }
    };

    this.LineChart4 = {
        data: [5, 3, 9, 6, 5, 9, 7, 3, 5, 2],
        options: {
            fill: '#1ab394',
            stroke: '#169c81',
            width: 64
        }
    };

    this.PieChart = {
        data: [1, 5],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };

    this.PieChart2 = {
        data: [226, 360],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };
    this.PieChart3 = {
        data: [0.52, 1.561],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };
    this.PieChart4 = {
        data: [1, 4],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };
    this.PieChart5 = {
        data: [226, 134],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };
    this.PieChart6 = {
        data: [0.52, 1.041],
        options: {
            fill: ["#1ab394", "#d7d7d7"]
        }
    };
}])
.run(['$state','$rootScope', function($state, $rootScope) {
	$rootScope.$on('$stateChangeStart',function(e, toState, toParams, fromState, fromParams) {
		$('html, body').stop().animate({scrollTop : 0}, 500);
	});
}]);
