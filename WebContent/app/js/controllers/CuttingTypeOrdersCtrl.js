inspinia.lazy.controller("CuttingTypeOrdersCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder', 'toaster', 'SweetAlert',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder, toaster, SweetAlert) {

	$timeout(function() {
		$scope.$apply(function() {
			
			
			$scope.cuttingType = {};
			
			$scope.toasterNotification = function(obj){
				console.log("toastr pop called");
		        toaster.pop({
		            type: obj.type,
		            title: obj.title,
		            body: obj.body,
		            showCloseButton: obj.showCloseButton,
					timeout : obj.timeout
		        });
		    };
			
			$scope.fileNameChanged = function() {
				string = $("#file-input").val().toString().split('\\')[2];
				$scope.fileName = string;
				$("#fileName").text("");
				$("#fileName").text("Choose file:   "+$scope.fileName);
			};
			
			
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
			
			
			var errorObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-danger'
			};
			
			var successObj = {
					templateUrl : 'views/common/notify.html',
					classes : 'alert-info'
			};
			
			$scope.getCuttingTypeList = function(){
				var url = "../app/cutting?action=getAllCuttingTypeList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					
					$timeout(function() {
						$scope.$apply(function() {
							$scope.cuttingTypeList = data.data;
							console.log("laaa: ",$scope.cuttingTypeList);
							allSmallCuttingTypeList = $scope.cuttingTypeList;
						});
					});
				});
			};
			
			$scope.to = ["John", "Marie", "Ghita"];
        
	        $scope.info = {
	          select: []
	        }
	        
	        $scope.typeaheadOpts = {
	          minLength: 1,
	        };
	        
	        $scope.$on("decipher.tags.added", tagAdded);
	
	        function tagAdded(info, obj) {
	          for (var i = 0; i < $scope.to.length; i++) {
	            console.log($scope.info.select);
	          if ($scope.to[i] === obj.tag.name) {
	            $scope.to.splice(i, 1);
	            //console.log("splice " +$scope.to)
	            } 
	          }
	        }
	        
	        $scope.delete = function () {
	          if ($scope.info.select !== "") {
	           $scope.info = null;
	           console.log($scope.info);
	          }
	        }
			
			$scope.fetchOrdersOfCuttingType = function (cuttingType) {
				console.log("fetchOrdersOfCuttingType chosen")
				
		        var modalInstance = $modal.open({
		            templateUrl: 'views/common/modal/Cutting-Type-Modal.html',
		            controller: AddNewCuttingTypeCtrl,
		            windowClass: "animated fadeInDown",
					scope: $scope,
					size: 'sm',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						loadPlugin: function ($ocLazyLoad) {
							return $ocLazyLoad.load([
		                         {
		                            insertBefore: '#loadBefore',
		                            name: 'toaster',
		                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
		                         },
		                        {
		                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
		                        }
		                     ]);
						}
					}
		        });
		    };	

			if($state.current.name == "admin.getCuttingTypeOrders"){
				if($stateParams.id != null &&  typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					$scope.editOrderList = order;
					console.log("$state.order: ", $scope.editOrderList);
				}
				else if($stateParams.id != null && typeof $stateParams.id != "undefined" && $stateParams.id != ""){
					index = $stateParams.id;
					$scope.fetchCurrentOrder(index);
				}
				
			}
			
		});
	});
}]);
