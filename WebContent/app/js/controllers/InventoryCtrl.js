inspinia.lazy.controller("InventoryCtrl", ['$scope', '$location', '$modal', '$http', '$timeout', '$state', '$rootScope', '$stateParams', 'notify', 'DTOptionsBuilder',
    function ($scope, $location, $modal, $http, $timeout, $state, $rootScope, $stateParams, notify, DTOptionsBuilder) {

	$timeout(function() {
		$scope.$apply(function() {
			
			$scope.dtOptions = DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {extend: 'copy'},
	            {extend: 'csv'},
	            {extend: 'excel', title: 'ExampleFile'},
	            {extend: 'pdf', title: 'ExampleFile'},

	            {extend: 'print',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ]);
	        
	        $scope.filterObject = {
	        	supplier : "",
	        	material : "",
	        	sold : true
	        };
			

			$scope.inventoryList = [];
			
			getInventoryList = function(){
				var url = "../app/inventory?action=getFirstTwentyInventoryList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					//console.log("fetched data: ",data);
					
					$timeout(function() {
						$scope.$apply(function() {
							
							$scope.inventoryList = data.data;
							console.log($scope.inventoryList);
						});
					});
				});
			};
	
			getInventoryList();
			
			
			getFilteredInventoryList = function(){
				var url = "../app/inventory?action=getFilteredInventoryList";
				console.log(url);
				$http.get(url).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					
					$timeout(function() {
						$scope.$apply(function() {
							
							$scope.inventoryList = data;
							//console.log($scope.inventoryList);
						});
					});
				});
			};
			
			if($state.current.name == "listView.containerToPrint" || $state.current.name == "listView.containerInventoryDetailsToPrint"){
				
				$scope.getContainerWiseInventoryList = function(){
					var url = "../app/inventory?action=getContainerWiseInventoryList";
					console.log(url);
					$http.get(url).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						
						$timeout(function() {
							$scope.$apply(function() {
								
								$scope.containerToPrintList = data.data;
								console.log($scope.containerToPrintList);
							});
						});
					});
				};
				
				$scope.getContainerWiseInventoryList();
				
				$scope.goToContainerDetails = function(container){
					console.log(container);
					$state.go("listView.containerInventoryDetailsToPrint", {
						container : container,
						id : container.containerId
					});
					
				};
				
				
				
			}
			
			$scope.fetchFilteredInventoryList = function(object){
				console.log(object);
			};
			
		});
	});
}]);

