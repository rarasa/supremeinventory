/**
 * INSPINIA - Responsive Admin Theme
 *
 */
/*(function () {*/
    var inspinia = angular.module('inspinia', [
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                    // ngSanitize
        'ngAnimate',
		'ngTagsInput',
        'ui.bootstrap',                 // Ui Bootstrap
    ]);
    
    inspinia.filter('moment', function() {
		return function(input) {
			return moment(input).format("DD-MM-YYYY");
		};
	});

    inspinia.filter('momentYMD', function() {
		return function(input) {
			return moment(input, "YYYY-MM-DD").format("DD-MM-YYYY");
		};
	});
	
	var EditSupplierDialogModel = function () {
			  this.visible = false;
			};
			EditSupplierDialogModel.prototype.open = function(supplier) {
			  this.supplier = supplier;
			  this.visible = true;
			};
			EditSupplierDialogModel.prototype.close = function() {
			  this.visible = false;
			};
/*})();*/

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad