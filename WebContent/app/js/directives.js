/**
 * INSPINIA - Responsive Admin Theme
 *
 * Main directives.js file
 * Define directives for used plugin
 *
 *
 * Functions (directives)
 *  - sideNavigation
 *  - iboxTools
 *  - minimalizaSidebar
 *  - vectorMap
 *  - sparkline
 *  - icheck
 *  - ionRangeSlider
 *  - dropZone
 *  - responsiveVideo
 *  - chatSlimScroll
 *  - customValid
 *  - fullScroll
 *  - closeOffCanvas
 *  - clockPicker
 *  - landingScrollspy
 *  - fitHeight
 *  - iboxToolsFullScreen
 *  - slimScroll
 *  - truncate
 *  - touchSpin
 *  - markdownEditor
 *  - resizeable
 *
 */




/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function(scope, element) {
            var listener = function(event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'Supreme | Responsive Admin Theme';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'InStone | ' + toState.data.pageTitle;
                $timeout(function() {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
};

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();

            });
        }
    };
};

/**
 * responsibleVideo - Directive for responsive video
 */
function responsiveVideo() {
    return {
        restrict: 'A',
        link:  function(scope, element) {
            var figure = element;
            var video = element.children();
            video
                .attr('data-aspectRatio', video.height() / video.width())
                .removeAttr('height')
                .removeAttr('width')

            //We can use $watch on $window.innerWidth also.
            $(window).resize(function() {
                var newWidth = figure.width();
                video
                    .width(newWidth)
                    .height(newWidth * video.attr('data-aspectRatio'));
            }).resize();
        }
    }
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function() {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
};


function closeOffCanvas() {
    return {
        restrict: 'A',
        template: '<a class="close-canvas-menu" ng-click="closeOffCanvas()"><i class="fa fa-times"></i></a>',
        controller: function ($scope, $element) {
            $scope.closeOffCanvas = function () {
                $("body").toggleClass("mini-navbar");
            }
        }
    };
}

/**
 * vectorMap - Directive for Vector map plugin
 */
function vectorMap() {
    return {
        restrict: 'A',
        scope: {
            myMapData: '=',
        },
        link: function (scope, element, attrs) {
            element.vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },
                series: {
                    regions: [
                        {
                            values: scope.myMapData,
                            scale: ["#1ab394", "#22d6b1"],
                            normalizeFunction: 'polynomial'
                        }
                    ]
                },
            });
        }
    }
}


/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function(){
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    }
};

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
            });
        }
    };
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
function ionRangeSlider() {
    return {
        restrict: 'A',
        scope: {
            rangeOptions: '='
        },
        link: function (scope, elem, attrs) {
            elem.ionRangeSlider(scope.rangeOptions);
        }
    }
}

/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */
function dropZone() {
    return function(scope, element, attrs) {
        element.dropzone({
            url: "/upload",
            maxFilesize: 100,
            paramName: "uploadfile",
            maxThumbnailFilesize: 5,
            init: function() {
                scope.files.push({file: 'added'});
                this.on('success', function(file, json) {
                });
                this.on('addedfile', function(file) {
                    scope.$apply(function(){
                        alert(file);
                        scope.files.push({file: 'added'});
                    });
                });
                this.on('drop', function(file) {
                    alert('file');
                });
            }
        });
    }
}

/**
 * chatSlimScroll - Directive for slim scroll for small chat
 */
function chatSlimScroll($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: '234px',
                    railOpacity: 0.4
                });

            });
        }
    };
}

/**
 * customValid - Directive for custom validation example
 */
function customValid(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function() {

                // You can call a $http method here
                // Or create custom validation

                var validText = "Inspinia";

                if(scope.extras == validText) {
                    c.$setValidity('cvalid', true);
                } else {
                    c.$setValidity('cvalid', false);
                }

            });
        }
    }
}


/**
 * fullScroll - Directive for slimScroll with 100%
 */
function fullScroll($timeout){
    return {
        restrict: 'A',
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: '100%',
                    railOpacity: 0.9
                });

            });
        }
    };
}

/**
 * slimScroll - Directive for slimScroll with custom height
 */
function slimScroll($timeout){
    return {
        restrict: 'A',
        scope: {
            boxHeight: '@'
        },
        link: function(scope, element) {
            $timeout(function(){
                element.slimscroll({
                    height: scope.boxHeight,
                    railOpacity: 0.9
                });

            });
        }
    };
}

/**
 * clockPicker - Directive for clock picker plugin
 */
function clockPicker() {
    return {
        restrict: 'A',
        link: function(scope, element) {
                element.clockpicker();
        }
    };
};


/**
 * landingScrollspy - Directive for scrollspy in landing page
 */
function landingScrollspy(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    }
}

/**
 * fitHeight - Directive for set height fit to window height
 */
function fitHeight(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.css("height", $(window).height() + "px");
            element.css("min-height", $(window).height() + "px");
        }
    };
}

/**
 * truncate - Directive for truncate string
 */
function truncate($timeout){
    return {
        restrict: 'A',
        scope: {
            truncateOptions: '='
        },
        link: function(scope, element) {
            $timeout(function(){
                element.dotdotdot(scope.truncateOptions);

            });
        }
    };
}


/**
 * touchSpin - Directive for Bootstrap TouchSpin
 */
function touchSpin() {
    return {
        restrict: 'A',
        scope: {
            spinOptions: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.spinOptions, function(){
                render();
            });
            var render = function () {
                $(element).TouchSpin(scope.spinOptions);
            };
        }
    }
};

/**
 * markdownEditor - Directive for Bootstrap Markdown
 */
function markdownEditor() {
    return {
        restrict: "A",
        require:  'ngModel',
        link:     function (scope, element, attrs, ngModel) {
            $(element).markdown({
                savable:false,
                onChange: function(e){
                    ngModel.$setViewValue(e.getContent());
                }
            });
        }
    }
};

function datepicker(){
	return {
    require: 'ngModel',
    link: function(scope, el, attr, ngModel) {
      $(el).datepicker({
        onSelect: function(dateText) {
          scope.$apply(function() {
            ngModel.$setViewValue(dateText);
          });
        }
      });
    }
};
};

function EditCustomerCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "EditCustomerCtrl";
	
	$scope.ok = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.close();

		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

    $scope.cancel = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.dismiss('cancel');
		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.saveCustomerName = function(editOrderList){
		console.log("saveCustomerName in directive called");
		console.log($scope.editOrderList);
	};
	
	$scope.assignCustomerNameOnEditOrder = function(customerObj){
		console.log(customerObj);
		if(typeof JSON.parse(customerObj) === 'object' && JSON.parse(customerObj) !== null){
			console.log("customerObj is json")
			customerObj = JSON.parse(customerObj);
			console.log(customerObj);
			$scope.editOrderList.customerId = customerObj.id;
			$scope.editOrderList.customerName = customerObj.name;
		}
		else{
			console.log("creating new object");
			$scope.editOrderList.customerName = customerObj;
			$scope.editOrderList.customerId = 0;
			console.log("$scope.editOrderList: ", $scope.editOrderList);
		}
		
	};
	
	$scope.editSingleCustomer = function(editOrderList){
					
		var obj = {
			customerName: editOrderList.customerName,
			customerId : editOrderList.customerId,
			name: editOrderList.customerName,
			attachToOrder : true,
			orderId : editOrderList.id
		}
		
		var url = "";
		if(obj.customerId == 0){
			url = "../app/customer?action=updateSingleCustomer";
		}
		else{
			url = "../app/customer?action=editSingleCustomer";
		}
		console.log(url);
		$http.post(url, obj, {}).success(function(data, status, headers, config) {
			console.log("fetched data: ",data);
			$timeout(function() {
				$scope.$apply(function() {
					console.log(data);
					
					if(data.success){
						
						var obj = {
							type: 'success',
							title : 'Customer saved.',
							body : editOrderList.customerName+' updated successfully.',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						
						$scope.fetchCurrentOrder($scope.editOrderList.id);
						
						$scope.getCustomerList();
						
						$scope.ok();
					}
					else{
						var obj = {
							type: 'error',
							title : 'Oops',
							body : data.message,
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
					}
				});
			});
		});
	};
};
function AddNewCuttingTypeCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "AddNewCustomerCtrl";
	
	
	
	$scope.cuttingType = {
		name: "",
		cuttingMode : 0,
	};
	
	$scope.ok = function () {
		$scope.addCuttingMode = false;
		$scope.getCuttingTypeList();
        $modalInstance.close();
    };

    $scope.cancel = function () {
		$scope.addCuttingMode = false;
		$scope.getCuttingTypeList();
        $modalInstance.dismiss('cancel');
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.addNewCuttingType = function(cutObj){
		
		if ($scope.cutting_type_form.$valid) {
			
					
		var obj = {
			name: cutObj.name,
			cuttingMode : cutObj.cuttingMode,
		};
		
		var url = "../app/cutting?action=updateSingleCuttingType";
			console.log(url);
			$http.post(url, obj, {}).success(function(data, status, headers, config) {
				console.log("fetched data: ",data);
				$timeout(function() {
					$scope.$apply(function() {
						console.log(data);
						
						if(data.success){
							
							var obj = {
								type: 'success',
								title : 'CuttingType saved.',
								body : 'Cutting Type saved successfully.',
								showCloseButton: true,
								timeout: 2000
								
							};
							$scope.toasterNotification(obj);
							
							$scope.getCuttingTypeList();
							$scope.ok();
						}
						else{
							var obj = {
								type: 'error',
								title : 'Oops',
								body : data.message,
								showCloseButton: true,
								timeout: 2000
								
							};
							$scope.toasterNotification(obj);
						}
					});
				});
			});
		
		}
		else{
			$scope.cutting_type_form.submitted = true;
		}
	};
};
function AddNewCustomerCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "AddNewCustomerCtrl";
	
	$scope.customer = {
		name: "",
		emirate : "",
		phone : "",
		country: "",
		VATRegistration : 0,
		email : "",
		VATRegistrationDate : "01-01-2018"
	};
	
	
	$scope.customer.VATRegistrationDate = "01-01-2018";
	
	$scope.ok = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.close();
    };

    $scope.cancel = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.dismiss('cancel');
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.addNewCustomer = function(customerObj){
		
		if ($scope.customer_form.$valid) {
			
					
		var obj = {
			name: customerObj.name,
			emirate : customerObj.emirate,
			phone : customerObj.phone,
			country: customerObj.country,
			VATRegistration : customerObj.VATRegistration,
			email : customerObj.email,
		};
		
		if(typeof $scope.editOrderList != "udefined" && $scope.editOrderList != {}){
			
			if(typeof $scope.editOrderList.id != "undefined" && $scope.editOrderList.id != 0){
				obj.orderId = $scope.editOrderList.id;
			}
			
		}
		
		if(customerObj.VATRegistration == "1"){
			
			if(typeof customerObj.VATRegistrationDate == "undefined" || customerObj.VATRegistrationDate == ""){
				var obj = {
					type: 'error',
					title : 'Error',
					body : "Please neter VAT Registration Date",
					showCloseButton: true,
					timeout: 2000
					
				};
				$scope.toasterNotification(obj);
				return;
			}
			else{
				obj.VATRegistrationDate = customerObj.VATRegistrationDate;
			}
			
			if(typeof customerObj.trnNumber == "undefined" || customerObj.trnNumber == ""){
				var obj = {
					type: 'error',
					title : 'Error',
					body : "Please enter TRN.",
					showCloseButton: true,
					timeout: 2000
					
				};
				$scope.toasterNotification(obj);
				return;
			}
			else{
				obj.trnNumber = customerObj.trnNumber;
			}
		}
		
		var url = "../app/customer?action=addSingleNewCustomer";
		console.log(obj);
		console.log(url);
		$http.post(url, obj, {}).success(function(data, status, headers, config) {
			console.log("fetched data: ",data);
			$timeout(function() {
				$scope.$apply(function() {
					console.log(data);
					if(data.success){
						var obj = {
							type: 'success',
							title : 'Customer saved.',
							body : customerObj.name+' updated successfully.',
							showCloseButton: true,
							timeout: 2000
							
						};
						console.log("customer data.data: ", data.data);
						if(typeof $scope.cuttingOrder != "undefined" && $scope.cuttingOrder != {}){
							$scope.cuttingOrder.customer = data.data;
							$scope.cuttingOrder.customerName = data.data.customerName;
							$scope.cuttingOrder.customerId = data.data.customerId;
						}
						if(typeof $scope.editOrderList != "undefined" && $scope.editOrderList != {}){
							$scope.editOrderList.customer = data.data;
							$scope.editOrderList.customerName = data.data.customerName;
							$scope.editOrderList.customerId = data.data.customerId;
							$scope.editOrderList.customer.VATRegistrationDate = moment(data.data.VATRegistrationDate).format("DD-MM-YYYY");
						}
						
						
						
						$scope.toasterNotification(obj);
						$scope.getCustomerList();
						$scope.ok();
					}
					else{
						var obj = {
							type: 'error',
							title : 'Oops',
							body : data.message,
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
					}
				});
			});
		});
		
		}
		else{
			$scope.customer_form.submitted = true;
		}
	};
};


function AddNewMaterialCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "AddNewCustomerCtrl";
	
	$scope.customer = {
		name: "",
		emirate : "",
		phone : "",
		country: "",
		VATRegistration : 0,
		email : "",
		VATRegistrationDate : "01-01-2018"
	};
	
	
	$scope.customer.VATRegistrationDate = "01-01-2018";
	
	$scope.ok = function () {
		$scope.addMaterial = false;
		$scope.editMaterial = false;
		$scope.getMaterialList();
        $modalInstance.close();
    };

    $scope.cancel = function () {
		$scope.addMaterial = false;
		$scope.editMaterial = false;
		$scope.getMaterialList();
        $modalInstance.dismiss('cancel');
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.material = {
		materialName: "",
		originCountry : ""
	};
	
	
	

	$scope.uploadSingleMaterialList = function(){
		
		if ($scope.material_form.$valid) {
	        // Submit as normal
			
			var obj = $scope.material;
			obj.lowerPriceRange = 0;
			obj.higherPriceRange = 0;
				
				if(typeof obj.materialName == "undefined" || obj.materialName == ""){
					var obj = {
						type: 'error',
						title : 'Oops',
						body : 'Material Name not given',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					return;
				}
				else if(typeof obj.originCountry == "undefined" || obj.originCountry == ""){
					var obj = {
						type: 'error',
						title : 'Oops',
						body : 'Origin Country not given',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					return;
				}
				
				var exists = false;
				for(var i = 0; i < $scope.materialList.length; i++){
					if($scope.materialList[i].materialName == obj.materialName.toUpperCase()){
						exists = true;
						break
					}
				}
				
				if(exists){
					var obj = {
						type: 'error',
						title : 'Oops',
						body : 'Material already exists',
						showCloseButton: true,
						timeout: 2000
						
					};
					$scope.toasterNotification(obj);
					return;
				}
				else{
					var url = "../app/material?action=updateSingleMaterial";
					console.log(url);
					$http.post(url, obj, {}).success(function(data, status, headers, config) {
						console.log("fetched data: ",data);
						$timeout(function() {
							$scope.$apply(function() {
								console.log(data);
								
								if(data.success){
									
									var obj = {
										type: 'success',
										title : 'Material saved.',
										body : $scope.material.materialName+' saved successfully.',
										showCloseButton: true,
	    								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									$scope.ok();
								}
								else{
									var obj = {
										type: 'error',
										title : 'Oops',
										body : data.message,
										showCloseButton: true,
	    								timeout: 2000
										
									};
									$scope.toasterNotification(obj);
									
									$scope.getMaterialList();
								}
							});
						});
					});
				}
			}
			else{
				$scope.material_form.submitted = true;
			}
	};
				
};


function EditCustomerDetailsCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "EditedCusttttt";
	
	$scope.ok = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.close();

		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

    $scope.cancel = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.dismiss('cancel');
		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.saveCustomerName = function(editOrderList){
		console.log("saveCustomerName in directive called");
		console.log($scope.editOrderList);
	};
	
	$scope.assignCustomerNameOnEditOrder = function(customerObj){
		console.log(customerObj);
		if(typeof JSON.parse(customerObj) === 'object' && JSON.parse(customerObj) !== null){
			console.log("customerObj is json")
			customerObj = JSON.parse(customerObj);
			console.log(customerObj);
			$scope.editOrderList.customerId = customerObj.id;
			$scope.editOrderList.customerName = customerObj.name;
		}
		else{
			console.log("creating new object");
			$scope.editOrderList.customerName = customerObj;
			$scope.editOrderList.customerId = 0;
			console.log("$scope.editOrderList: ", $scope.editOrderList);
		}
		
	};
	
	$scope.editSingleCustomer = function(editOrderList){
					
		var obj = {
			customerName: editOrderList.customerName,
			customerId : editOrderList.customerId,
			name: editOrderList.customerName,
			attachToOrder : true,
			orderId : editOrderList.id
		}
		
		var url = "";
		if(obj.customerId == 0){
			url = "../app/customer?action=updateSingleCustomer";
		}
		else{
			url = "../app/customer?action=editSingleCustomer";
		}
		console.log(url);
		$http.post(url, obj, {}).success(function(data, status, headers, config) {
			console.log("fetched data: ",data);
			$timeout(function() {
				$scope.$apply(function() {
					console.log(data);
					
					if(data.success){
						
						var obj = {
							type: 'success',
							title : 'Customer saved.',
							body : editOrderList.customerName+' updated successfully.',
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
						
						$scope.fetchCurrentOrder($scope.editOrderList.id);
						
						$scope.getCustomerList();
						
						$scope.ok();
					}
					else{
						var obj = {
							type: 'error',
							title : 'Oops',
							body : data.message,
							showCloseButton: true,
							timeout: 2000
							
						};
						$scope.toasterNotification(obj);
					}
				});
			});
		});
	};
};
function AddCustomerDetailsCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	$scope.message = "addCusttttt";
	
	console.log("on enter: ");
	console.log("$scope.addCustomer: ", $scope.addCustomer);
	console.log("$scope.editCustomer: ", $scope.editCustomer);
	
	$scope.ok = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.close();

		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

    $scope.cancel = function () {
		$scope.addCustomer = false;
		$scope.editCustomer = false;
        $modalInstance.dismiss('cancel');

		$("#addressEditOrder").select2({
			  tags: true,
			});
		$(".select2-selection--single").css({
			'border': '1px solid #e5e6e7',
			'border-radius' : '1px',
			'height' : '33px'
		});
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.saveCustomerName = function(editOrderList){
		console.log("saveCustomerName in directive called");
		console.log($scope.editOrderList);
	}
	
	$scope.uploadSingleCustomerList = function(){
					
			var obj = {
				name: $scope.newCustomerName,
				attachToOrder : $scope.editOrderList.id
			}
								
			var exists = false;
			for(var i = 0; i < $scope.customerList.length; i++){
				if($scope.customerList[i].name == obj.name.toUpperCase()){
					exists = true;
					break
				}
			}
			
			if(exists){
				var obj = {
					type: 'error',
					title : 'Oops',
					body : 'Customer already exists',
					showCloseButton: true,
					timeout: 2000
					
				};
				$scope.toasterNotification(obj);
				return;
			}
			else{
				var url = "../app/customer?action=updateSingleCustomer";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								
								var obj = {
									type: 'success',
									title : 'Customer saved.',
									body : $scope.newCustomerName+' saved successfully.',
									showCloseButton: true,
    								timeout: 2000
									
								};
								$scope.toasterNotification(obj);
								
								$scope.fetchCurrentOrder($scope.editOrderList.id);
								
								$scope.getCustomerList();
								
								$scope.ok();
							}
							else{
								var obj = {
									type: 'error',
									title : 'Oops',
									body : data.message,
									showCloseButton: true,
    								timeout: 2000
									
								};
								$scope.toasterNotification(obj);
							}
						});
					});
				});
			}
		
	};
};

 function number(){
    return {
        require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$parsers.push(function (input) {
                    if (input == undefined) return ''
                    var inputNumber = input.toString().replace(/[^0-9]/g, '');
                    if (inputNumber != input) {
                        ctrl.$setViewValue(inputNumber);
                        ctrl.$render();
                    }
                    return inputNumber;
                });
            }
    }
};

function AddCustomerAddressDetailsCtrl($scope, $modalInstance, $location, $http, $timeout, $state, $rootScope, $stateParams, toaster){
	console.log("$scope.addCustomerAddress: ", $scope.addCustomerAddress);
	
	$scope.ok = function () {
		$scope.addCustomerAddress = false;
        $modalInstance.close();
    };

    $scope.cancel = function () {
		$scope.addCustomerAddress = false;
        $modalInstance.dismiss('cancel');
    };

	$scope.toasterNotification = function(obj){
		console.log("toastr pop called");
        toaster.pop({
            type: obj.type,
            title: obj.title,
            body: obj.body,
            showCloseButton: obj.showCloseButton,
			timeout : obj.timeout
        });
    };

	$scope.uploadNewAddress = function(){
			var obj = $scope.newCustomerDetails;
				obj.customerId = $scope.editOrderList.customerId;
				obj.attachToOrder = $scope.editOrderList.id;
								
			var exists = false;
			if(typeof $scope.editOrderList.potentialCustomerDetailsList != "undefined" && $scope.editOrderList.potentialCustomerDetailsList.length > 0){
				
				for(var i = 0; i < $scope.editOrderList.potentialCustomerDetailsList.length; i++){
					if($scope.editOrderList.potentialCustomerDetailsList[i].address.toUpperCase() == obj.address.toUpperCase()){
						exists = true;
						break
					}
				}
			}
			
			if(exists){
				var obj = {
					type: 'error',
					title : 'Oops',
					body : 'Address already exists',
					showCloseButton: true,
					timeout: 2000
					
				};
				$scope.toasterNotification(obj);
				return;
			}
			else{
				var url = "../app/customerDetails?action=updateNewAddress";
				console.log(url);
				$http.post(url, obj, {}).success(function(data, status, headers, config) {
					console.log("fetched data: ",data);
					$timeout(function() {
						$scope.$apply(function() {
							console.log(data);
							
							if(data.success){
								
								var obj = {
									type: 'success',
									title : 'Customer saved.',
									body : $scope.newCustomerName+' saved successfully.',
									showCloseButton: true,
    								timeout: 2000
									
								};
								$scope.toasterNotification(obj);
								
								$scope.fetchCurrentOrder($scope.editOrderList.id);
								
//								$scope.getCustomerAddressList();
								
								$scope.ok();
							}
							else{
								var obj = {
									type: 'error',
									title : 'Oops',
									body : data.message,
									showCloseButton: true,
    								timeout: 2000
									
								};
								$scope.toasterNotification(obj);
							}
						});
					});
				});
			}
		
	};
};

/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('vectorMap', vectorMap)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('ionRangeSlider', ionRangeSlider)
    .directive('dropZone', dropZone)
    .directive('responsiveVideo', responsiveVideo)
    .directive('chatSlimScroll', chatSlimScroll)
    .directive('customValid', customValid)
    .directive('fullScroll', fullScroll)
    .directive('closeOffCanvas', closeOffCanvas)
    .directive('clockPicker', clockPicker)
    .directive('landingScrollspy', landingScrollspy)
    .directive('fitHeight', fitHeight)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    .directive('slimScroll', slimScroll)
    .directive('truncate', truncate)
    .directive('touchSpin', touchSpin)
    .directive('markdownEditor', markdownEditor)
    .directive('datepicker', datepicker)
	.directive('number', number)

	.controller('EditCustomerCtrl', EditCustomerCtrl)
	.controller('AddNewCustomerCtrl', AddNewCustomerCtrl)

	.controller('AddNewCuttingTypeCtrl', AddNewCuttingTypeCtrl)

	.controller('EditCustomerDetailsCtrl', EditCustomerDetailsCtrl)
	.controller('AddCustomerDetailsCtrl', AddCustomerDetailsCtrl)
	.controller('AddCustomerAddressDetailsCtrl', AddCustomerAddressDetailsCtrl)

	.controller('AddNewMaterialCtrl', AddNewMaterialCtrl)
	
    
    .directive('selectOnClick', ['$window', function ($window) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attrs) {
	            element.on('click', function () {
	                if (!$window.getSelection().toString()) {
	                    // Required for mobile Safari
	                    this.setSelectionRange(0, this.value.length)
	                }
	            });
	        }
	    };
    }])
    .directive('selectOnFocus', ['$window', function ($window) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attrs) {
	            element.on('focus', function () {
	                if (!$window.getSelection().toString()) {
	                    // Required for mobile Safari
	                    this.setSelectionRange(0, this.value.length)
	                }
	            });
	        }
	    };
    }])
	.directive('editPersonDialog', [function() {
	  return {
	    restrict: 'E',
	    scope: {
	      model: '=',
	    },
	    link: function(scope, element, attributes) {
	      scope.$watch('model.visible', function(newValue) {
	        var modalElement = element.find('.modal');
	        modalElement.modal(newValue ? 'show' : 'hide');
	      });
	      
	      element.on('shown.bs.modal', function() {
	        scope.$apply(function() {
	          scope.model.visible = true;
	        });
	      });
	
	      element.on('hidden.bs.modal', function() {
	        scope.$apply(function() {
	          scope.model.visible = false;
	        });
	      });
	      
	    },
	    templateUrl: 'views/common/modal/supplier-addition-modal.html',
	  };
	}])
    .directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                    $(element).blur();
                });
                event.preventDefault();
            }
        });
    };
    
})
.directive('capitalize', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            // see where the cursor is before the update so that we can set it back
            var selection = element[0].selectionStart;
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
            // set back the cursor after rendering
            element[0].selectionStart = selection;
            element[0].selectionEnd = selection;
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
      }
    }
})

;
;
