function config($stateProvider, $urlRouterProvider, $controllerProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

	inspinia.lazy = {
    		controller: $controllerProvider.register
    };
    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    if(me.role <= 2){
    	$urlRouterProvider.otherwise("/cutting/add");
    }
    else{
    	$urlRouterProvider.otherwise("/dashboards/start");
    }
    
    function getRandomString() {
    	var text = "";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    	for (var i = 0; i < 6; i++)
    		text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
    };
    
    randomString = getRandomString();
    
    console.log("RANDOM STRING: ", randomString);

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

	    .state('dashboards', {
	        abstract: true,
	        url: "/dashboards",
	        templateUrl: "views/common/content.html?t="+randomString,
	    })
	    .state('dashboards.start', {
	        url: "/start",
	        templateUrl: "views/start.html?t="+randomString,
	        data: { pageTitle: 'Action' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        }
                        
                    ]);
                }
    			/*loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListUploadCtrl.js?t='+randomString]);
    			}]*/
    			
    		}
	    })
	    .state('dashboards.dashboard', {
	        url: "/dashboard",
	        templateUrl: "views/dashboard/dashboard.html?t="+randomString,
	        data: { pageTitle: 'Dashboard' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
	    })
		.state('admin', {
	        abstract: true,
	        url: "/admin",
	        templateUrl: "views/admin/content.html?t="+randomString,
			resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/AdminCtrl.js?t='+randomString]);
    			}]
    			
    		}

	    })
		.state('admin.dashboard', {
            url: "/dash",
            templateUrl: "views/admin/dashboard.html?t="+randomString,
            data: { pageTitle: 'Admin Dashboard' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				/*return $ocLazyLoad.load(['js/controllers/AdminCtrl.js?t='+randomString]);*/
    			}]
    			
    		}
        })
		.state('admin.supplierUpload', {
            url: "/add-supplier",
            templateUrl: "views/admin/supplier/SupplierUpload.html?t="+randomString,
            data: { pageTitle: 'Add Supplier' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/SupplierCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.restock', {
            url: "/restock",
            templateUrl: "views/admin/restock/Restock.html?t="+randomString,
            data: { pageTitle: 'Restock' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/RestockCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.customerUpload', {
            url: "/add-customer",
            templateUrl: "views/admin/customer/AddCustomer.html?t="+randomString,
            data: { pageTitle: 'Add Customer' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CustomerCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.locationUpload', {
            url: "/add-location",
            templateUrl: "views/admin/location/AddLocation.html?t="+randomString,
            data: { pageTitle: 'Add Location' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/LocationCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		
		.state('admin.materialUpload', {
            url: "/add-material",
            templateUrl: "views/admin/material/MaterialUpload.html?t="+randomString,
            data: { pageTitle: 'Add Material' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/MaterialCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.userUpload', {
            url: "/add-user",
            templateUrl: "views/admin/user/AddUser.html?t="+randomString,
            data: { pageTitle: 'Add User' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css','css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css','js/plugins/ionRangeSlider/ion.rangeSlider.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css','js/plugins/nouslider/jquery.nouislider.min.js','js/plugins/nouslider/angular-nouislider.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css','js/plugins/colorpicker/bootstrap-colorpicker-module.js']
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js','css/plugins/ngImgCrop/ng-img-crop.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/UserCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.cuttingTypeUpload', {
            url: "/add-cutting-type",
            templateUrl: "views/admin/cuttingType/AddCuttingTypes.html?t="+randomString,
            data: { pageTitle: 'Add Cutting Types' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css','css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css','js/plugins/ionRangeSlider/ion.rangeSlider.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css','js/plugins/nouslider/jquery.nouislider.min.js','js/plugins/nouslider/angular-nouislider.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css','js/plugins/colorpicker/bootstrap-colorpicker-module.js']
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js','css/plugins/ngImgCrop/ng-img-crop.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingTypeCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.getCuttingTypeOrders', {
            url: "/cutting-type-orders/:id",
            templateUrl: "views/admin/cuttingType/GetCuttingTypeOrders.html?t="+randomString,
            data: { pageTitle: 'Get Cutting Type Orders' },
			params: {id:null},
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css','css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css','js/plugins/ionRangeSlider/ion.rangeSlider.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css','js/plugins/nouslider/jquery.nouislider.min.js','js/plugins/nouslider/angular-nouislider.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css','js/plugins/colorpicker/bootstrap-colorpicker-module.js']
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js','css/plugins/ngImgCrop/ng-img-crop.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingTypeOrdersCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.machineUpload', {
            url: "/add-machine",
            templateUrl: "views/admin/machine/AddMachine.html?t="+randomString,
            data: { pageTitle: 'Add User' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
						{
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js','js/plugins/jsKnob/angular-knob.js']
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css','css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css','js/plugins/ionRangeSlider/ion.rangeSlider.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/chosen.css','js/plugins/chosen/chosen.jquery.js','js/plugins/chosen/chosen.js']
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css','js/plugins/nouslider/jquery.nouislider.min.js','js/plugins/nouslider/angular-nouislider.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css','js/plugins/switchery/switchery.js','js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css','js/plugins/colorpicker/bootstrap-colorpicker-module.js']
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js','css/plugins/ngImgCrop/ng-img-crop.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/UserCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })

        
		.state('admin.viewContainerStatus', {
            url: "/view-container",
            templateUrl: "views/admin/container/ContainerStatusView.html?t="+randomString,
            data: { pageTitle: 'View Container Details' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/ContainerStatusCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.editContainerStatus', {
            url: "/edit-container/:id",
			params: {container:{}, id:null},
            templateUrl: "views/admin/container/ContainerStatusEdit.html?t="+randomString,
            data: { pageTitle: 'Edit Container Details' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/ContainerStatusCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('admin.addContainer', {
            url: "/add-container",
            templateUrl: "views/admin/container/ContainerStatusAdd.html?t="+randomString,
            data: { pageTitle: 'Upload Container Details' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/ContainerStatusCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        .state('uploads', {
            abstract: true,
            url: "/upload",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        
        .state('uploads.bulkInventoryUpload', {
            url: "/inventory",
            templateUrl: "views/container/BulkInventoryUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Packing List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        .state('uploads.bulkInventoryUploadAll', {
            url: "/stock",
            templateUrl: "views/stock/BulkInventoryUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Packing List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/AllPackingListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('uploads.bulkTileInventoryUpload', {
            url: "/inventory-tile",
            templateUrl: "views/container/BulkTileUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Tile List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListTileUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        
        .state('uploads.singleInventoryUpload', {
            url: "/single-inventory",
            templateUrl: "views/inventory/SingleInventoryUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Single Inventory' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        
        .state('uploads.bulkSupplierUpload', {
            url: "/supplier",
            templateUrl: "views/supplier/BulkSupplierUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Supplier List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/BulkSupplierListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        .state('uploads.singleSupplierUpload', {
            url: "/single-supplier",
            templateUrl: "views/supplier/SingleSupplierUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Single Supplier' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/SupplierCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        
        
        .state('uploads.bulkMaterialUpload', {
            url: "/material",
            templateUrl: "views/material/BulkMaterialUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Material List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/BulkMaterialListUploadCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        
        .state('uploads.singleMaterialUpload', {
            url: "/single-material",
            templateUrl: "views/material/SingleMaterialUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Single Material' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
						{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/MaterialCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        
        
        
		.state('listView', {
			abstract: true,
			url: "/view",
			templateUrl: "views/common/content.html?t="+randomString,
		})
		.state('listView.fetchFilteredInventoryList', {
            url: "/filtered-list",
            templateUrl: "views/filter/filter-inventory.html?t="+randomString,
            data: { pageTitle: 'Filter' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: [
									'css/plugins/angular-notify/angular-notify.min.css',
                                    'js/plugins/angular-notify/angular-notify.min.js'
                                   ]
                        },
{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/FilterCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('listView.supplier', {
			url: "/supplier",
			templateUrl: "views/supplier/ViewSupplierList.html?t="+randomString,
			data: { pageTitle: 'Supplier List' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/SupplierCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('listView.material', {
			url: "/material",
			templateUrl: "views/material/ViewMaterialList.html?t="+randomString,
			data: { pageTitle: 'Material List' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/MaterialCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('listView.stock', {
			url: "/stock",
			templateUrl: "views/inventory/ViewInventoryList.html?t="+randomString,
			data: { pageTitle: 'Material List' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/InventoryCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('listView.containerToPrint', {
			url: "/printStock",
			templateUrl: "views/inventory/ViewContainerList.html?t="+randomString,
			data: { pageTitle: 'Material List' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/ContainerCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('listView.containerInventoryDetailsToPrint', {
			url: "/containerDetails/:id",
			params: {container:{}, id:null},
			templateUrl: "views/inventory/ViewContainerInventoryList.html?t="+randomString,
			data: { pageTitle: 'Material List' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/ContainerCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('sales', {
			abstract: true,
			url: "/sale",
			templateUrl: "views/common/content.html?t="+randomString,
		})
		.state('sales.salesOrder', {
			url: "/add",
			templateUrl: "views/inventory/SalesOrder.html?t="+randomString,
			data: { pageTitle: 'Cart' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
						{
                        	 serie: true,
                        	 files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/SalesCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('sales.salesInvoice', {
			url: "/view",
			templateUrl: "views/inventory/SalesInvoice.html?t="+randomString,
			data: { pageTitle: 'Sales Invoice' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
                         {
                             files: ['js/plugins/footable/footable.all.min.js?t='+randomString, 'css/plugins/footable/footable.core.css?t='+randomString]
                         },
                         {
                             name: 'ui.footable',
                             files: ['js/plugins/footable/angular-footable.js?t='+randomString]
                         },
                         {
                             reconfig: true,
                             serie: true,
                             files: ['js/plugins/rickshaw/vendor/d3.v3.js?t='+randomString,'js/plugins/rickshaw/rickshaw.min.js?t='+randomString]
                         },
                         {
                             reconfig: true,
                             name: 'angular-rickshaw',
                             files: ['js/plugins/rickshaw/angular-rickshaw.js?t='+randomString]
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/SalesCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('sales.editOrder',{
    		url:'/edit/:id',
    		templateUrl: "views/inventory/SalesEdit.html?t="+randomString,
    		data:{pageTitle:"Edit Sales Order"},
    		params:{id:null},
    		resolve:{
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
						 {
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                         },
						 {
                        	 serie: true,
                        	 files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
                         {
                             files: ['js/plugins/footable/footable.all.min.js?t='+randomString, 'css/plugins/footable/footable.core.css?t='+randomString]
                         },
                         {
                             name: 'ui.footable',
                             files: ['js/plugins/footable/angular-footable.js?t='+randomString]
                         },
                         {
                             reconfig: true,
                             serie: true,
                             files: ['js/plugins/rickshaw/vendor/d3.v3.js?t='+randomString,'js/plugins/rickshaw/rickshaw.min.js?t='+randomString]
                         },
                         {
                             reconfig: true,
                             name: 'angular-rickshaw',
                             files: ['js/plugins/rickshaw/angular-rickshaw.js?t='+randomString]
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/SalesCtrl.js?t='+randomString]);
				}]
			}
    	})
    	.state('sales.approvedSalesOrder', {
			url: "/approvedOrders",
			templateUrl: "views/inventory/ApprovedSalesOrder.html?t="+randomString,
			data: { pageTitle: 'Approved Sales Order' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
						 {
                        	 serie: true,
                        	 files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
                         {
                             name: 'datePicker',
                             files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                         },
                         {
                             serie: true,
                             files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                         },
						 {
                             files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                         },
                         {
                        	 serie: true,
                        	 files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/SalesCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('listView.cart', {
			url: "/stock",
			templateUrl: "views/cart/cart.html?t="+randomString,
			data: { pageTitle: 'Cart' },
			resolve: {
				loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         }
                     ]);
				},
				loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
					return $ocLazyLoad.load(['js/controllers/CartCtrl.js?t='+randomString]);
				}]
			}
		})
		.state('cutting', {
			abstract: true,
			url: "/cutting",
			templateUrl: "views/common/content.html?t="+randomString,
		})
		
		.state('cutting.addCuttingOrder', {
            url: "/new",
            templateUrl: "views/cutting/AddCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Add Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
						{
                        	serie: true,
                        	files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                        },
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                        {
                            files: ['js/plugins/slim-select/slimselect.min.css','js/plugins/slim-select/slimselect.min.js']
                        },
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('cutting.scanCuttingOrder', {
            url: "/scan/:id",
            templateUrl: "views/cutting/ScanSlabsInCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Scan Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
						{
                        	serie: true,
                        	files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                        },
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('cutting.editCuttingOrder', {
            url: "/edit/:id",
			params: {id:null},
            templateUrl: "views/cutting/EditCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Edit Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
						{
                        	serie: true,
                        	files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                        },
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderEditCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })

		
		.state('cutting.viewCuttingOrder', {
            url: "/view",
            templateUrl: "views/inventory/CuttingInvoice.html?t="+randomString,
            data: { pageTitle: 'View Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                    	{
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                        },
                        {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })

		.state('print', {
			abstract: true,
			url: "/print",
			templateUrl: "views/common/content-bare.html?t="+randomString,
		})
		.state('print.printCuttingOrder', {
            url: "/cuttingOrder/:id",
            templateUrl: "views/print/PrintCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Print Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
						{
                        	serie: true,
                        	files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                        },
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PrintCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		.state('print.printDeliveryOrder', {
            url: "/deliveryOrder/:id",
            templateUrl: "views/print/PrintDeliveryOrder.html?t="+randomString,
            data: { pageTitle: 'Print Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
					return $ocLazyLoad.load([
                         {
                        	 serie: true,
                        	 files: ['js/plugins/dataTables/datatables.min.js','css/plugins/dataTables/datatables.min.css']
                         },
                         {
                        	 name: 'cgNotify',
                        	 files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables',
                        	 files: ['js/plugins/dataTables/angular-datatables.min.js']
                         },
                         {
                        	 serie: true,
                        	 name: 'datatables.buttons',
                        	 files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                         },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
						{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
						{
                        	serie: true,
                        	files: ['js/plugins/select2/select2.min.js','js/plugins/select2/select2.min.css']
                        },
						{
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js', 'css/plugins/toastr/toastr.min.css']
                        },
                     ]);
				},
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PrintCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		/*.state('cutting.bulkCuttingOrderInputForm', {
            url: "/add",
            templateUrl: "views/cutting/InputCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Upload Cutting Order List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })*/
		.state('cutting.editExistingHalfCuttingOrder', {
            url: "/add/:id",
			params: {id:null},
            templateUrl: "views/cutting/EditHalfCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Upload Cutting Order List' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
						{
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
{
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
        .state('cutting.bulkCuttingOrderEditForm', {
            url: "/edit",
            templateUrl: "views/cutting/EditCuttingOrder.html?t="+randomString,
            data: { pageTitle: 'Edit Cutting Order' },
            resolve:{
            	loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css',
						            'js/plugins/chosen/chosen.jquery.js',
						            'js/plugins/chosen/chosen.js']
						},
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css',
                                    'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        }
                        
                    ]);
                },
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/CuttingOrderCtrl.js?t='+randomString]);
    			}]
    			
    		}
        })
		;

        
        /*.state('inventory', {
            abstract: true,
            url: "/inventory",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('inventory.bulkInventoryUpload', {
            url: "/bulk-upload",
            templateUrl: "views/inventory/bulkInventoryUpload.html?t="+randomString,
            data: { pageTitle: 'Upload Packing List' },
            resolve:{
    			loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
    				return $ocLazyLoad.load(['js/controllers/PackingListUploadCtrl.js?t='+randomString]);
    			}],
    			loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
 						{
						    insertBefore: '#loadBefore',
						    name: 'localytics.directives',
						    files: ['css/plugins/chosen/chosen.css?t='+randomString,'js/plugins/chosen/chosen.jquery.js?t='+randomString,'js/plugins/chosen/chosen.js?t='+randomString]
						},
                        {
                            files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                        }
                    ]);
                }
    		}
        });*/
        /*.state('layouts', {
            url: "/layouts",
            templateUrl: "views/layouts.html?t="+randomString,
            data: { pageTitle: 'Layouts' },
        })
        .state('charts', {
            abstract: true,
            url: "/charts",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('charts.flot_chart', {
            url: "/flot_chart",
            templateUrl: "views/graph_flot.html?t="+randomString,
            data: { pageTitle: 'Flot chart' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name: 'angular-flot',
                            files: [ 'js/plugins/flot/jquery.flot.js?t='+randomString, 'js/plugins/flot/jquery.flot.time.js?t='+randomString, 'js/plugins/flot/jquery.flot.tooltip.min.js?t='+randomString, 'js/plugins/flot/jquery.flot.spline.js?t='+randomString, 'js/plugins/flot/jquery.flot.resize.js?t='+randomString, 'js/plugins/flot/jquery.flot.pie.js?t='+randomString, 'js/plugins/flot/curvedLines.js?t='+randomString, 'js/plugins/flot/angular-flot.js?t='+randomString, ]
                        }
                    ]);
                }
            }
        })
        .state('charts.rickshaw_chart', {
            url: "/rickshaw_chart",
            templateUrl: "views/graph_rickshaw.html?t="+randomString,
            data: { pageTitle: 'Rickshaw chart' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            reconfig: true,
                            serie: true,
                            files: ['js/plugins/rickshaw/vendor/d3.v3.js?t='+randomString,'js/plugins/rickshaw/rickshaw.min.js?t='+randomString]
                        },
                        {
                            reconfig: true,
                            name: 'angular-rickshaw',
                            files: ['js/plugins/rickshaw/angular-rickshaw.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('charts.peity_chart', {
            url: "/peity_chart",
            templateUrl: "views/graph_peity.html?t="+randomString,
            data: { pageTitle: 'Peity graphs' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'angular-peity',
                            files: ['js/plugins/peity/jquery.peity.min.js?t='+randomString, 'js/plugins/peity/angular-peity.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('charts.sparkline_chart', {
            url: "/sparkline_chart",
            templateUrl: "views/graph_sparkline.html?t="+randomString,
            data: { pageTitle: 'Sparkline chart' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sparkline/jquery.sparkline.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('charts.chartjs_chart', {
            url: "/chartjs_chart",
            templateUrl: "views/chartjs.html?t="+randomString,
            data: { pageTitle: 'Chart.js?t='+randomString },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/chartJs/Chart.min.js?t='+randomString]
                        },
                        {
                            name: 'angles',
                            files: ['js/plugins/chartJs/angles.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('charts.chartist_chart', {
            url: "/chartist_chart",
            templateUrl: "views/chartist.html?t="+randomString,
            data: { pageTitle: 'Chartist' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name: 'angular-chartist',
                            files: ['js/plugins/chartist/chartist.min.js?t='+randomString, 'css/plugins/chartist/chartist.min.css?t='+randomString, 'js/plugins/chartist/angular-chartist.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('charts.c3charts', {
            url: "/c3charts",
            templateUrl: "views/c3charts.html?t="+randomString,
            data: { pageTitle: 'c3charts' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: ['css/plugins/c3/c3.min.css?t='+randomString, 'js/plugins/d3/d3.min.js?t='+randomString, 'js/plugins/c3/c3.min.js?t='+randomString]
                        },
                        {
                            serie: true,
                            name: 'gridshore.c3js.chart',
                            files: ['js/plugins/c3/c3-angular.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('mailbox', {
            abstract: true,
            url: "/mailbox",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('mailbox.inbox', {
            url: "/inbox",
            templateUrl: "views/mailbox.html?t="+randomString,
            data: { pageTitle: 'Mail Inbox' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('mailbox.email_view', {
            url: "/email_view",
            templateUrl: "views/mail_detail.html?t="+randomString,
            data: { pageTitle: 'Mail detail' }
        })
        .state('mailbox.email_compose', {
            url: "/email_compose",
            templateUrl: "views/mail_compose.html?t="+randomString,
            data: { pageTitle: 'Mail compose' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/summernote/summernote.css?t='+randomString,'css/plugins/summernote/summernote-bs3.css?t='+randomString,'js/plugins/summernote/summernote.min.js?t='+randomString]
                        },
                        {
                            name: 'summernote',
                            files: ['css/plugins/summernote/summernote.css?t='+randomString,'css/plugins/summernote/summernote-bs3.css?t='+randomString,'js/plugins/summernote/summernote.min.js?t='+randomString,'js/plugins/summernote/angular-summernote.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('mailbox.email_template', {
            url: "/email_template",
            templateUrl: "views/email_template.html?t="+randomString,
            data: { pageTitle: 'Mail compose' }
        })
        .state('widgets', {
            url: "/widgets",
            templateUrl: "views/widgets.html?t="+randomString,
            data: { pageTitle: 'Widhets' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name: 'angular-flot',
                            files: [ 'js/plugins/flot/jquery.flot.js?t='+randomString, 'js/plugins/flot/jquery.flot.time.js?t='+randomString, 'js/plugins/flot/jquery.flot.tooltip.min.js?t='+randomString, 'js/plugins/flot/jquery.flot.spline.js?t='+randomString, 'js/plugins/flot/jquery.flot.resize.js?t='+randomString, 'js/plugins/flot/jquery.flot.pie.js?t='+randomString, 'js/plugins/flot/curvedLines.js?t='+randomString, 'js/plugins/flot/angular-flot.js?t='+randomString, ]
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                        },
                        {
                            files: ['js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js?t='+randomString, 'js/plugins/jvectormap/jquery-jvectormap-2.0.2.css?t='+randomString]
                        },
                        {
                            files: ['js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js?t='+randomString]
                        },
                        {
                            name: 'ui.checkbox',
                            files: ['js/bootstrap/angular-bootstrap-checkbox.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('metrics', {
            url: "/metrics",
            templateUrl: "views/metrics.html?t="+randomString,
            data: { pageTitle: 'Metrics' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sparkline/jquery.sparkline.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('forms', {
            abstract: true,
            url: "/forms",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('forms.basic_form', {
            url: "/basic_form",
            templateUrl: "views/form_basic.html?t="+randomString,
            data: { pageTitle: 'Basic form' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('forms.advanced_plugins', {
            url: "/advanced_plugins",
            templateUrl: "views/form_advanced.html?t="+randomString,
            data: { pageTitle: 'Advanced form' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js?t='+randomString,'js/plugins/jsKnob/angular-knob.js?t='+randomString]
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css?t='+randomString,'css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css?t='+randomString,'js/plugins/ionRangeSlider/ion.rangeSlider.min.js?t='+randomString]
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/chosen.css?t='+randomString,'js/plugins/chosen/chosen.jquery.js?t='+randomString,'js/plugins/chosen/chosen.js?t='+randomString]
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css?t='+randomString,'js/plugins/nouslider/jquery.nouislider.min.js?t='+randomString,'js/plugins/nouslider/angular-nouislider.js?t='+randomString]
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css','js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js?t='+randomString]
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css?t='+randomString, 'js/plugins/clockpicker/clockpicker.js?t='+randomString]
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css?t='+randomString,'js/plugins/switchery/switchery.js?t='+randomString,'js/plugins/switchery/ng-switchery.js?t='+randomString]
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css?t='+randomString,'js/plugins/colorpicker/bootstrap-colorpicker-module.js?t='+randomString]
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js?t='+randomString,'css/plugins/ngImgCrop/ng-img-crop.css?t='+randomString]
                        },
                        {
                            serie: true,
                            files: ['js/plugins/moment/moment.min.js?t='+randomString, 'js/plugins/daterangepicker/daterangepicker.js?t='+randomString, 'css/plugins/daterangepicker/daterangepicker-bs3.css?t='+randomString]
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js?t='+randomString]
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css?t='+randomString]
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js?t='+randomString, 'css/plugins/ui-select/select.min.css?t='+randomString]
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css?t='+randomString, 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js?t='+randomString]
                        }

                    ]);
                }
            }
        })
        .state('forms.wizard', {
            url: "/wizard",
            templateUrl: "views/form_wizard.html?t="+randomString,
            controller: wizardCtrl,
            data: { pageTitle: 'Wizard form' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/steps/jquery.steps.css?t='+randomString]
                        }
                    ]);
                },
                loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
  			      // you can lazy load files for an existing module
  			      return $ocLazyLoad.load(['js/controllers/WizardCtrl.js?t='+randomString]);
    			}],
            }
        })
        .state('forms.wizard.step_one', {
            url: '/step_one',
            templateUrl: 'views/wizard/step_one.html',
            data: { pageTitle: 'Wizard form' }
        })
        .state('forms.wizard.step_two', {
            url: '/step_two',
            templateUrl: 'views/wizard/step_two.html',
            data: { pageTitle: 'Wizard form' }
        })
        .state('forms.wizard.step_three', {
            url: '/step_three',
            templateUrl: 'views/wizard/step_three.html',
            data: { pageTitle: 'Wizard form' }
        })
        .state('forms.file_upload', {
            url: "/file_upload",
            templateUrl: "views/form_file_upload.html?t="+randomString,
            data: { pageTitle: 'File upload' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/dropzone/basic.css','css/plugins/dropzone/dropzone.css','js/plugins/dropzone/dropzone.js']
                        }
                    ]);
                }
            }
        })
        .state('forms.text_editor', {
            url: "/text_editor",
            templateUrl: "views/form_editors.html?t="+randomString,
            data: { pageTitle: 'Text editor' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'summernote',
                            files: ['css/plugins/summernote/summernote.css?t='+randomString,'css/plugins/summernote/summernote-bs3.css?t='+randomString,'js/plugins/summernote/summernote.min.js?t='+randomString,'js/plugins/summernote/angular-summernote.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('forms.markdown', {
            url: "/markdown",
            templateUrl: "views/markdown.html?t="+randomString,
            data: { pageTitle: 'Markdown' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: ['js/plugins/bootstrap-markdown/bootstrap-markdown.js?t='+randomString,'js/plugins/bootstrap-markdown/markdown.js?t='+randomString,'css/plugins/bootstrap-markdown/bootstrap-markdown.min.css?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('app', {
            abstract: true,
            url: "/app",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('app.contacts', {
            url: "/contacts",
            templateUrl: "views/contacts.html?t="+randomString,
            data: { pageTitle: 'Contacts' }
        })
        .state('app.contacts_2', {
            url: "/contacts_2",
            templateUrl: "views/contacts_2.html?t="+randomString,
            data: { pageTitle: 'Contacts 2' }
        })
        .state('app.profile', {
            url: "/profile",
            templateUrl: "views/profile.html?t="+randomString,
            data: { pageTitle: 'Profile' }
        })
        .state('app.profile_2', {
            url: "/profile_2",
            templateUrl: "views/profile_2.html?t="+randomString,
            data: { pageTitle: 'Profile_2'},
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sparkline/jquery.sparkline.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('app.projects', {
            url: "/projects",
            templateUrl: "views/projects.html?t="+randomString,
            data: { pageTitle: 'Projects' }
        })
        .state('app.project_detail', {
            url: "/project_detail",
            templateUrl: "views/project_detail.html?t="+randomString,
            data: { pageTitle: 'Project detail' }
        })
        .state('app.file_manager', {
            url: "/file_manager",
            templateUrl: "views/file_manager.html?t="+randomString,
            data: { pageTitle: 'File manager' }
        })
        .state('app.calendar', {
            url: "/calendar",
            templateUrl: "views/calendar.html?t="+randomString,
            data: { pageTitle: 'Calendar' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            insertBefore: '#loadBefore',
                            files: ['css/plugins/fullcalendar/fullcalendar.css?t='+randomString,'js/plugins/fullcalendar/fullcalendar.min.js?t='+randomString,'js/plugins/fullcalendar/gcal.js?t='+randomString]
                        },
                        {
                            name: 'ui.calendar',
                            files: ['js/plugins/fullcalendar/calendar.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('app.faq', {
            url: "/faq",
            templateUrl: "views/faq.html?t="+randomString,
            data: { pageTitle: 'FAQ' }
        })
        .state('app.timeline', {
            url: "/timeline",
            templateUrl: "views/timeline.html?t="+randomString,
            data: { pageTitle: 'Timeline' }
        })
        .state('app.pin_board', {
            url: "/pin_board",
            templateUrl: "views/pin_board.html?t="+randomString,
            data: { pageTitle: 'Pin board' }
        })
        .state('app.invoice', {
            url: "/invoice",
            templateUrl: "views/invoice.html?t="+randomString,
            data: { pageTitle: 'Invoice' }
        })
        .state('app.blog', {
            url: "/blog",
            templateUrl: "views/blog.html?t="+randomString,
            data: { pageTitle: 'Blog' }
        })
        .state('app.article', {
            url: "/article",
            templateUrl: "views/article.html?t="+randomString,
            data: { pageTitle: 'Article' }
        })
        .state('app.issue_tracker', {
            url: "/issue_tracker",
            templateUrl: "views/issue_tracker.html?t="+randomString,
            data: { pageTitle: 'Issue Tracker' }
        })
        .state('app.clients', {
            url: "/clients",
            templateUrl: "views/clients.html?t="+randomString,
            data: { pageTitle: 'Clients' }
        })
        .state('app.teams_board', {
            url: "/teams_board",
            templateUrl: "views/teams_board.html?t="+randomString,
            data: { pageTitle: 'Teams board' }
        })
        .state('app.social_feed', {
            url: "/social_feed",
            templateUrl: "views/social_feed.html?t="+randomString,
            data: { pageTitle: 'Social feed' }
        })
        .state('app.vote_list', {
            url: "/vote_list",
            templateUrl: "views/vote_list.html?t="+randomString,
            data: { pageTitle: 'Vote list' }
        })
        .state('pages', {
            abstract: true,
            url: "/pages",
            templateUrl: "views/common/content.html?t="+randomString
        })
        .state('pages.search_results', {
            url: "/search_results",
            templateUrl: "views/search_results.html?t="+randomString,
            data: { pageTitle: 'Search results' }
        })
        .state('pages.empy_page', {
            url: "/empy_page",
            templateUrl: "views/empty_page.html?t="+randomString,
            data: { pageTitle: 'Empty page' }
        })
        .state('login', {
            url: "/login",
            templateUrl: "views/login.html?t="+randomString,
            data: { pageTitle: 'Login', specialClass: 'gray-bg' }
        })
        .state('login_two_columns', {
            url: "/login_two_columns",
            templateUrl: "views/login_two_columns.html?t="+randomString,
            data: { pageTitle: 'Login two columns', specialClass: 'gray-bg' }
        })
        .state('register', {
            url: "/register",
            templateUrl: "views/register.html?t="+randomString,
            data: { pageTitle: 'Register', specialClass: 'gray-bg' }
        })
        .state('lockscreen', {
            url: "/lockscreen",
            templateUrl: "views/lockscreen.html?t="+randomString,
            data: { pageTitle: 'Lockscreen', specialClass: 'gray-bg' }
        })
        .state('forgot_password', {
            url: "/forgot_password",
            templateUrl: "views/forgot_password.html?t="+randomString,
            data: { pageTitle: 'Forgot password', specialClass: 'gray-bg' }
        })
        .state('errorOne', {
            url: "/errorOne",
            templateUrl: "views/errorOne.html?t="+randomString,
            data: { pageTitle: '404', specialClass: 'gray-bg' }
        })
        .state('errorTwo', {
            url: "/errorTwo",
            templateUrl: "views/errorTwo.html?t="+randomString,
            data: { pageTitle: '500', specialClass: 'gray-bg' }
        })
        .state('ui', {
            abstract: true,
            url: "/ui",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('ui.typography', {
            url: "/typography",
            templateUrl: "views/typography.html?t="+randomString,
            data: { pageTitle: 'Typography' }
        })
        .state('ui.icons', {
            url: "/icons",
            templateUrl: "views/icons.html?t="+randomString,
            data: { pageTitle: 'Icons' }
        })
        .state('ui.buttons', {
            url: "/buttons",
            templateUrl: "views/buttons.html?t="+randomString,
            data: { pageTitle: 'Buttons' }
        })
        .state('ui.tabs_panels', {
            url: "/tabs_panels",
            templateUrl: "views/tabs_panels.html?t="+randomString,
            data: { pageTitle: 'Panels' }
        })
        .state('ui.tabs', {
            url: "/tabs",
            templateUrl: "views/tabs.html?t="+randomString,
            data: { pageTitle: 'Tabs' }
        })
        .state('ui.notifications_tooltips', {
            url: "/notifications_tooltips",
            templateUrl: "views/notifications.html?t="+randomString,
            data: { pageTitle: 'Notifications and tooltips' }
        })
        .state('ui.badges_labels', {
            url: "/badges_labels",
            templateUrl: "views/badges_labels.html?t="+randomString,
            data: { pageTitle: 'Badges and labels and progress' }
        })
        .state('ui.video', {
            url: "/video",
            templateUrl: "views/video.html?t="+randomString,
            data: { pageTitle: 'Responsible Video' }
        })
        .state('ui.draggable', {
            url: "/draggable",
            templateUrl: "views/draggable.html?t="+randomString,
            data: { pageTitle: 'Draggable panels' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.sortable',
                            files: ['js/plugins/ui-sortable/sortable.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('grid_options', {
            url: "/grid_options",
            templateUrl: "views/grid_options.html?t="+randomString,
            data: { pageTitle: 'Grid options' }
        })
        .state('miscellaneous', {
            abstract: true,
            url: "/miscellaneous",
            templateUrl: "views/common/content.html?t="+randomString,
        })
        .state('miscellaneous.google_maps', {
            url: "/google_maps",
            templateUrl: "views/google_maps.html?t="+randomString,
            data: { pageTitle: 'Google maps' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.event',
                            files: ['js/plugins/uievents/event.js?t='+randomString]
                        },
                        {
                            name: 'ui.map',
                            files: ['js/plugins/uimaps/ui-map.js?t='+randomString]
                        },
                    ]);
                }
            }
        })
        .state('miscellaneous.code_editor', {
            url: "/code_editor",
            templateUrl: "views/code_editor.html?t="+randomString,
            data: { pageTitle: 'Code Editor' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: ['css/plugins/codemirror/codemirror.css?t='+randomString,'css/plugins/codemirror/ambiance.css?t='+randomString,'js/plugins/codemirror/codemirror.js?t='+randomString,'js/plugins/codemirror/mode/javascript/javascript.js?t='+randomString]
                        },
                        {
                            name: 'ui.codemirror',
                            files: ['js/plugins/ui-codemirror/ui-codemirror.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.modal_window', {
            url: "/modal_window",
            templateUrl: "views/modal_window.html?t="+randomString,
            data: { pageTitle: 'Modal window' }
        })
        .state('miscellaneous.chat_view', {
            url: "/chat_view",
            templateUrl: "views/chat_view.html?t="+randomString,
            data: { pageTitle: 'Chat view' }
        })
        .state('miscellaneous.nestable_list', {
            url: "/nestable_list",
            templateUrl: "views/nestable_list.html?t="+randomString,
            data: { pageTitle: 'Nestable List' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.tree',
                            files: ['css/plugins/uiTree/angular-ui-tree.min.css?t='+randomString,'js/plugins/uiTree/angular-ui-tree.min.js?t='+randomString]
                        },
                    ]);
                }
            }
        })
        .state('miscellaneous.notify', {
            url: "/notify",
            templateUrl: "views/notify.html?t="+randomString,
            data: { pageTitle: 'Notifications for angularjs?t='+randomString },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.timeline_2', {
            url: "/timeline_2",
            templateUrl: "views/timeline_2.html?t="+randomString,
            data: { pageTitle: 'Timeline version 2' }
        })
        .state('miscellaneous.forum_view', {
            url: "/forum_view",
            templateUrl: "views/forum_view.html?t="+randomString,
            data: { pageTitle: 'Forum - general view' }
        })
        .state('miscellaneous.forum_post_view', {
            url: "/forum_post_view",
            templateUrl: "views/forum_post_view.html?t="+randomString,
            data: { pageTitle: 'Forum - post view' }
        })
        .state('miscellaneous.diff', {
            url: "/diff",
            templateUrl: "views/diff.html?t="+randomString,
            data: { pageTitle: 'Text Diff' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/diff_match_patch/javascript/diff_match_patch.js?t='+randomString]
                        },
                        {
                            name: 'diff-match-patch',
                            files: ['js/plugins/angular-diff-match-patch/angular-diff-match-patch.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.sweet_alert', {
            url: "/sweet_alert",
            templateUrl: "views/sweet_alert.html?t="+randomString,
            data: { pageTitle: 'Sweet alert' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js?t='+randomString, 'css/plugins/sweetalert/sweetalert.css?t='+randomString]
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.idle_timer', {
            url: "/idle_timer",
            templateUrl: "views/idle_timer.html?t="+randomString,
            data: { pageTitle: 'Idle timer' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'cgNotify',
                            files: ['css/plugins/angular-notify/angular-notify.min.css','js/plugins/angular-notify/angular-notify.min.js']
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.live_favicon', {
            url: "/live_favicon",
            templateUrl: "views/live_favicon.html?t="+randomString,
            data: { pageTitle: 'Live favicon' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/tinycon/tinycon.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.spinners', {
            url: "/spinners",
            templateUrl: "views/spinners.html?t="+randomString,
            data: { pageTitle: 'Spinners' }
        })
        .state('miscellaneous.validation', {
            url: "/validation",
            templateUrl: "views/validation.html?t="+randomString,
            data: { pageTitle: 'Validation' }
        })
        .state('miscellaneous.agile_board', {
            url: "/agile_board",
            templateUrl: "views/agile_board.html?t="+randomString,
            data: { pageTitle: 'Agile board' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ui.sortable',
                            files: ['js/plugins/ui-sortable/sortable.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.masonry', {
            url: "/masonry",
            templateUrl: "views/masonry.html?t="+randomString,
            data: { pageTitle: 'Masonry' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/masonry/masonry.pkgd.min.js?t='+randomString]
                        },
                        {
                            name: 'wu.masonry',
                            files: ['js/plugins/masonry/angular-masonry.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.toastr', {
            url: "/toastr",
            templateUrl: "views/toastr.html?t="+randomString,
            data: { pageTitle: 'Toastr' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js?t='+randomString, 'css/plugins/toastr/toastr.min.css?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.i18support', {
            url: "/i18support",
            templateUrl: "views/i18support.html?t="+randomString,
            data: { pageTitle: 'i18support' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            insertBefore: '#loadBefore',
                            name: 'toaster',
                            files: ['js/plugins/toastr/toastr.min.js?t='+randomString, 'css/plugins/toastr/toastr.min.css?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.truncate', {
            url: "/truncate",
            templateUrl: "views/truncate.html?t="+randomString,
            data: { pageTitle: 'Truncate' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/dotdotdot/jquery.dotdotdot.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.clipboard', {
            url: "/clipboard",
            templateUrl: "views/clipboard.html?t="+randomString,
            data: { pageTitle: 'Clipboard' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/ngclipboard/clipboard.min.js?t='+randomString]
                        },
                        {
                            name: 'ngclipboard',
                            files: ['js/plugins/ngclipboard/ngclipboard.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.loading_buttons', {
            url: "/loading_buttons",
            templateUrl: "views/loading_buttons.html?t="+randomString,
            data: { pageTitle: 'Loading buttons' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            name: 'angular-ladda',
                            files: ['js/plugins/ladda/spin.min.js?t='+randomString, 'js/plugins/ladda/ladda.min.js?t='+randomString, 'css/plugins/ladda/ladda-themeless.min.css?t='+randomString,'js/plugins/ladda/angular-ladda.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.tour', {
            url: "/tour",
            templateUrl: "views/tour.html?t="+randomString,
            data: { pageTitle: 'Tour' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            insertBefore: '#loadBefore',
                            files: ['js/plugins/bootstrap-tour/bootstrap-tour.min.js?t='+randomString, 'css/plugins/bootstrap-tour/bootstrap-tour.min.css?t='+randomString]
                        },
                        {
                            name: 'bm.bsTour',
                            files: ['js/plugins/angular-bootstrap-tour/angular-bootstrap-tour.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('miscellaneous.tree_view', {
            url: "/tree_view",
            templateUrl: "views/tree_view.html?t="+randomString,
            data: { pageTitle: 'Tree view' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/jsTree/style.min.css?t='+randomString,'js/plugins/jsTree/jstree.min.js?t='+randomString]
                        },
                        {
                            name: 'ngJsTree',
                            files: ['js/plugins/jsTree/ngJsTree.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('tables', {
            abstract: true,
            url: "/tables",
            templateUrl: "views/common/content.html?t="+randomString
        })
        .state('tables.static_table', {
            url: "/static_table",
            templateUrl: "views/table_basic.html?t="+randomString,
            data: { pageTitle: 'Static table' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'angular-peity',
                            files: ['js/plugins/peity/jquery.peity.min.js?t='+randomString, 'js/plugins/peity/angular-peity.js?t='+randomString]
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css?t='+randomString,'js/plugins/iCheck/icheck.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('tables.data_tables', {
            url: "/data_tables",
            templateUrl: "views/table_data_tables.html?t="+randomString,
            data: { pageTitle: 'Data Tables' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js?t='+randomString,'css/plugins/dataTables/datatables.min.css?t='+randomString]
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js?t='+randomString]
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('tables.foo_table', {
            url: "/foo_table",
            templateUrl: "views/foo_table.html?t="+randomString,
            data: { pageTitle: 'Foo Table' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js?t='+randomString, 'css/plugins/footable/footable.core.css?t='+randomString]
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('tables.nggrid', {
            url: "/nggrid",
            templateUrl: "views/nggrid.html?t="+randomString,
            data: { pageTitle: 'ng Grid' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'ngGrid',
                            files: ['js/plugins/nggrid/ng-grid-2.0.3.min.js?t='+randomString]
                        },
                        {
                            insertBefore: '#loadBefore',
                            files: ['js/plugins/nggrid/ng-grid.css?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('commerce', {
            abstract: true,
            url: "/commerce",
            templateUrl: "views/common/content.html?t="+randomString,
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/footable/footable.all.min.js?t='+randomString, 'css/plugins/footable/footable.core.css?t='+randomString]
                        },
                        {
                            name: 'ui.footable',
                            files: ['js/plugins/footable/angular-footable.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('commerce.products_grid', {
            url: "/products_grid",
            templateUrl: "views/ecommerce_products_grid.html?t="+randomString,
            data: { pageTitle: 'E-commerce grid' }
        })
        .state('commerce.product_list', {
            url: "/product_list",
            templateUrl: "views/ecommerce_product_list.html?t="+randomString,
            data: { pageTitle: 'E-commerce product list' }
        })
        .state('commerce.orders', {
            url: "/orders",
            templateUrl: "views/ecommerce_orders.html?t="+randomString,
            data: { pageTitle: 'E-commerce orders' }
        })
        .state('commerce.product', {
            url: "/product",
            templateUrl: "views/ecommerce_product.html?t="+randomString,
            data: { pageTitle: 'Product edit' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/summernote/summernote.css?t='+randomString,'css/plugins/summernote/summernote-bs3.css?t='+randomString,'js/plugins/summernote/summernote.min.js?t='+randomString]
                        },
                        {
                            name: 'summernote',
                            files: ['css/plugins/summernote/summernote.css?t='+randomString,'css/plugins/summernote/summernote-bs3.css?t='+randomString,'js/plugins/summernote/summernote.min.js?t='+randomString,'js/plugins/summernote/angular-summernote.min.js?t='+randomString]
                        }
                    ]);
                }
            }

        })
        .state('commerce.product_details', {
            url: "/product_details",
            templateUrl: "views/ecommerce_product_details.html?t="+randomString,
            data: { pageTitle: 'E-commerce Product detail' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/slick/slick.css?t='+randomString,'css/plugins/slick/slick-theme.css?t='+randomString,'js/plugins/slick/slick.min.js?t='+randomString]
                        },
                        {
                            name: 'slick',
                            files: ['js/plugins/slick/angular-slick.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('commerce.payments', {
            url: "/payments",
            templateUrl: "views/ecommerce_payments.html?t="+randomString,
            data: { pageTitle: 'E-commerce payments' }
        })
        .state('commerce.cart', {
            url: "/cart",
            templateUrl: "views/ecommerce_cart.html?t="+randomString,
            data: { pageTitle: 'Shopping cart' }
        })
        .state('gallery', {
            abstract: true,
            url: "/gallery",
            templateUrl: "views/common/content.html?t="+randomString
        })
        .state('gallery.basic_gallery', {
            url: "/basic_gallery",
            templateUrl: "views/basic_gallery.html?t="+randomString,
            data: { pageTitle: 'Lightbox Gallery' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['js/plugins/blueimp/jquery.blueimp-gallery.min.js?t='+randomString,'css/plugins/blueimp/css/blueimp-gallery.min.css?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('gallery.bootstrap_carousel', {
            url: "/bootstrap_carousel",
            templateUrl: "views/carousel.html?t="+randomString,
            data: { pageTitle: 'Bootstrap carousel' }
        })
        .state('gallery.slick_gallery', {
            url: "/slick_gallery",
            templateUrl: "views/slick.html?t="+randomString,
            data: { pageTitle: 'Slick carousel' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['css/plugins/slick/slick.css?t='+randomString,'css/plugins/slick/slick-theme.css?t='+randomString,'js/plugins/slick/slick.min.js?t='+randomString]
                        },
                        {
                            name: 'slick',
                            files: ['js/plugins/slick/angular-slick.min.js?t='+randomString]
                        }
                    ]);
                }
            }
        })
        .state('css_animations', {
            url: "/css_animations",
            templateUrl: "views/css_animation.html?t="+randomString,
            data: { pageTitle: 'CSS Animations' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            reconfig: true,
                            serie: true,
                            files: ['js/plugins/rickshaw/vendor/d3.v3.js?t='+randomString,'js/plugins/rickshaw/rickshaw.min.js?t='+randomString]
                        },
                        {
                            reconfig: true,
                            name: 'angular-rickshaw',
                            files: ['js/plugins/rickshaw/angular-rickshaw.js?t='+randomString]
                        }
                    ]);
                }
            }

        })
        .state('landing', {
            url: "/landing",
            templateUrl: "views/landing.html?t="+randomString,
            data: { pageTitle: 'Landing page', specialClass: 'landing-page' }
        })
        .state('outlook', {
            url: "/outlook",
            templateUrl: "views/outlook.html?t="+randomString,
            data: { pageTitle: 'Outlook view', specialClass: 'fixed-sidebar' }
        })
        .state('off_canvas', {
            url: "/off_canvas",
            templateUrl: "views/off_canvas.html?t="+randomString,
            data: { pageTitle: 'Off canvas menu', specialClass: 'canvas-menu' }
        });*/

}
inspinia
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
