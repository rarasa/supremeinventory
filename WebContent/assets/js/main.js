
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);

login = function() {
	
	console.log("login action called")
	var obj={};
	obj.action="login";
	obj.email=$("#login-form").find("#email-input").val();
	obj.password=$("#login-form").find("#password-input").val();
	
	console.log("Object: ", obj);
    $('#login-button').html("<i class='fa fa-spinner fa-spin'></i>");
    $.ajax({
    	url:"app/user",
    	type:"post",
    	dataType:"json",
    	data:obj,
    	success:function(data){
    		console.log(data);

    		if (data.success==true){
    			console.log("data success is true");
    			console.log("Taking to app/",hashTag);
            	window.location.href="app/"+hashTag;
            } else {
            	alert(data.error);
            	$('#login-response').slideUp(100);
            	$("#login-form").slideDown(100);
                /*$('#login-response').html("<div class='content-message'><i class='fa fa-exclamation-circle fa-2x'></i> <h2>"+data.error+"</h2> <p><a href='#login' class='show-side-panel-link color-red'>Please try again.</a>.</p></div>");
                setTimeout(function(){
                }, 5000);*/
            }
    	}
    });
};