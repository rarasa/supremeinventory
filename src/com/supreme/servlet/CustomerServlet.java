package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.CustomerController;

public class CustomerServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		CustomerController ctrl = new CustomerController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{

			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("addSingleNewCustomer")){

				System.out.println("updateSingleCustomer action called");

				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("phone") || request.getParameter("phone").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer phone not specified.");
				}
				else if(!request.getParameterMap().containsKey("country") || request.getParameter("country").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer country not specified.");
				}
				else if(!request.getParameterMap().containsKey("email") || request.getParameter("email").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer email not specified.");
				}
				else if(!request.getParameterMap().containsKey("emirate") || request.getParameter("emirate").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer emirate not specified.");
				}
				else if(!request.getParameterMap().containsKey("VATRegistration") || request.getParameter("VATRegistration").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer VATRegistration not specified.");
				}
				
				else
				{
					Long orderId = 0L;
					String trnNumber = "", email = ""; 
					Date VATRegistrationDate = null;
					String name = request.getParameter("name");
					String emirate = request.getParameter("emirate");
					String phone = request.getParameter("phone");
					String country = request.getParameter("country");
					boolean VATRegistration = request.getParameter("VATRegistration").equals("1") ? true : false;
					
					if(request.getParameterMap().containsKey("email") && !request.getParameter("email").equals(""))
					{
						email = request.getParameter("email");
					}
					if(request.getParameterMap().containsKey("orderId") && !request.getParameter("orderId").equals(""))
					{
						orderId = Long.parseLong(request.getParameter("orderId"));
					}
					
					if(VATRegistration) {
						if(!request.getParameterMap().containsKey("trnNumber") || request.getParameter("trnNumber").equals(""))
						{
							res.put("success", false);
							res.put("error", "Customer TRN Number not specified.");
						}
						else {
							trnNumber = request.getParameter("trnNumber");
						}
						if(!request.getParameterMap().containsKey("VATRegistrationDate") || request.getParameter("VATRegistrationDate").equals(""))
						{
							res.put("success", false);
							res.put("error", "Customer TRN Number not specified.");
						}
						else {
							VATRegistrationDate = sdf.parse(request.getParameter("VATRegistrationDate"));
						}
						
					}
					Map<String, Object> result = null;

					result = ctrl.addSingleNewCustomer(name, emirate, phone, country, email, VATRegistration, VATRegistrationDate, trnNumber, orderId);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
					res.put("data", result.get("data"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("editSingleCustomer")){

				System.out.println("editSingleCustomer action called");

				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("customerId") || request.getParameter("customerId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("attachToOrder") || request.getParameter("attachToOrder").equals(""))
				{
					res.put("success", false);
					res.put("error", "AttachToOrder not specified.");
				}
				else if(!request.getParameterMap().containsKey("orderId") || request.getParameter("orderId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Order Id not specified.");
				}
				else
				{
					long id = Long.parseLong(request.getParameter("customerId"));
					long orderId = Long.parseLong(request.getParameter("orderId"));
					String name = request.getParameter("name");
					boolean attachToOrder = request.getParameter("attachToOrder").equals("true") ? true : false;

					Map<String, Object> result = null;

					result = ctrl.editSingleCustomer(name, id, attachToOrder, orderId);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("deleteCustomer")){
				
				System.out.println("updateSingleCustomer action called");
				
				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Customer Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("id") || request.getParameter("id").equals(""))
				{
					res.put("success", false);
					res.put("error", "Id not specified.");
				}
				else
				{
					long id = Long.parseLong(request.getParameter("id"));
					String name = request.getParameter("name");
					
					Map<String, Object> result = ctrl.deleteCustomer(name, id);
					
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		CustomerController ctrl = new CustomerController();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllCustomerList"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllCustomerList();

				out.write(result.get("data").toString());

				res.put("success", true);
				res.put("data", result.get("data"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}

}
