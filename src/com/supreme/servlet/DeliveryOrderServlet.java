package com.supreme.servlet;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;  

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.supreme.Config;
import com.supreme.controller.CuttingController;
import com.supreme.controller.DeliveryOrderController;
import com.supreme.controller.OrderController;
import com.supreme.dao.Cart;
import com.supreme.dao.Orders;
import com.supreme.pojo.OrderSet;
import com.supreme.util.DOStatementPdf;
import com.supreme.util.HibernateBridge;
import com.supreme.util.PageEventToAddRectangle;

public class DeliveryOrderServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-11-30
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="rashirathi@outlook.com";

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		ServletOutputStream os = response.getOutputStream();
		HttpSession session = request.getSession();
		OrderController ctrl = new OrderController();
		SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
		DeliveryOrderController DOCtrl = new DeliveryOrderController();
		
		try
		{
			if(session.getAttribute("id") == null)
			{
				throw new ValidationException("You are not logged in.");
			}
				
				try{
					if(!request.getParameterMap().containsKey("vehicleNumber") && request.getParameter("vehicleNumber") == null ){
						res.put("success", false);
						res.put("error", "Vehicle Number not specified.");
					}
					else if(!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null ){
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					}
					else if(!request.getParameterMap().containsKey("address") && request.getParameter("address") == null ){
						res.put("success", false);
						res.put("error", "Address not specified.");
					}
					else if(request.getParameterMap().containsKey("LPONumber") && request.getParameter("LPONumber") != null 
							&& (!request.getParameterMap().containsKey("LPODate") && request.getParameter("LPODate") == null)){
						res.put("success", false);
						res.put("error", "LPO Date not specified.");
					}
					else if(request.getParameterMap().containsKey("LPODate") && request.getParameter("LPODate") != null 
							&& (!request.getParameterMap().containsKey("LPONumber") && request.getParameter("LPONumber") == null)){
						res.put("success", false);
						res.put("error", "LPO Number not specified.");
					}
					else{
						Date LPODate = null;
						String LPONumber = null;
						long orderId = Long.parseLong(request.getParameter("orderId"));
						String vehicleNumber = request.getParameter("orderId");
						String address = request.getParameter("address");
						
						if(request.getParameterMap().containsKey("LPONumber") && request.getParameter("LPONumber") != null) {
							LPODate = sdf.parse(request.getParameter("LPODate"));
						}
						if(request.getParameterMap().containsKey("LPONumber") && request.getParameter("LPODate") != null){ 
							LPONumber = request.getParameter("LPONumber");
						}
						else{
							LPONumber = "";
						}
						
						Map<String,Object> result = ctrl.saveDOAdditionalData(orderId, vehicleNumber, address, LPODate, LPONumber);
						
						Map<String,Object> DOSaveResult = new HashMap<String, Object>();
						/**
						 * If additional details got saved, Print DO
						 */
						if((boolean) result.get("success")){
							System.out.println("printing DO");
							Orders order = (Orders)result.get("order");
							DOSaveResult = DOCtrl.printDO(order);
							
							if((boolean) DOSaveResult.get("success")){
								
								String path = "";
								try{
									Config config=new Config();
									if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
										path=config.get(Config.PDF_DEV_PATH);
										System.out.println("Dev path: "+path);
									}else{
										path=config.get(Config.PDF_PROD_PATH);
										System.out.println("Prod path: "+path);
									}
								}catch(IOException e){
									path = "/opt/supremeinventory/pdf/";
								}
								
								
								ByteArrayOutputStream baos=new ByteArrayOutputStream();
								
								//Document document = new Document(path);
								
								//PdfWriter.getInstance(document, baos);
								
								
								response.setContentType("application/pdf");
								response.addHeader("Content-Disposition", "attachment; filename="+"dreamladder.pdf");			
								response.setContentLength((int)baos.size());

								OutputStream os1 = response.getOutputStream();
								
								baos.writeTo(os1);
								System.out.println("os is"+os1);
								os1.flush();
								os1.close();
								
								/*String fileName = (String) DOSaveResult.get("fileName");
								if(fileName == null || fileName.equals("")){
									throw new ServletException("File Name can't be null or empty");
								}
								File file = new File(path+File.separator+fileName);
								if(!file.exists()){
									throw new ServletException("File doesn't exists on server.");
								}
								System.out.println("File location on server::"+file.getAbsolutePath());
								ServletContext ctx = getServletContext();
								InputStream fis = new FileInputStream(file);
								
								
								response.setContentType("application/octet-stream");
								response.setContentLength((int) file.length());
								response.setHeader("Content-Disposition", "attachment; filename=\"" + "AAAA"+fileName+ "\"");
								
								os1 = response.getOutputStream();
								byte[] bufferData = new byte[1024];
								int read=0;
								while((read = fis.read(bufferData))!= -1){
									os1.write(bufferData, 0, read);
								}
								os1.flush();
								os1.close();
								fis.close();
								System.out.println("File downloaded at client successfully");*/
								
								/*response.setContentType("application/octet-stream");
								response.setHeader("Content-Disposition", "filename=\"hoge.txt\"");
							    File srcFile = new File(path+File.separator+DOSaveResult.get("fileName"));
							    FileUtils.copyFile(srcFile, response.getOutputStream());*/
							}
						}
						
						res = new JSONObject();
						res.put("success", DOSaveResult.get("success"));
						res.put("fileName", DOSaveResult.get("fileName"));
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			os.write(res.toString().getBytes());
		}
	
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JSONObject res=new JSONObject();
		Session hSession=HibernateBridge.getSessionFactory().openSession();
		response.addHeader("Access-Control-Allow-Origin", "*");
		PrintWriter out=null;
		try{
			if(!request.getParameterMap().containsKey("action")){
				out=response.getWriter();
				throw new ValidationException("Action not passed.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("downloadDO"))
			{
				System.out.println("downloadDO entered");
				if(!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null ){
					res.put("success", false);
					res.put("error", "Order Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("customerName") && request.getParameter("customerName") == null ){
					res.put("success", false);
					res.put("error", "Customer Name not specified.");
				}
				else{
					System.out.println("downloadDO else entered");
					long orderId = Long.parseLong(request.getParameter("orderId"));
					String customerName = request.getParameter("customerName");
					HttpSession session = request.getSession();
					long userId=0;
					String emailId=null;
					Date todayDate= new Date();
					SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy");
					String todate=sdf2.format(todayDate);
					
					Document document = new Document();
					OutputStream os = null;
					ByteArrayOutputStream baos=null;
					try
					{
		                document.setPageSize(PageSize.A4.rotate());
		                baos = new ByteArrayOutputStream();
						PdfWriter writer = PdfWriter.getInstance(document, baos);
						PageEventToAddRectangle pageEventToAddRectangle=new PageEventToAddRectangle();
						writer.setPageEvent(pageEventToAddRectangle);
						document.open();
		 			    document.setPageSize(PageSize.A4.rotate());
					     
						DOStatementPdf accountStatementPdf=new DOStatementPdf();
						accountStatementPdf.createPdf(orderId, document);
								
						 
						document.close();
					    response.setContentType("application/pdf");
						response.addHeader("Content-Disposition", "attachment; filename="+customerName+".pdf");			
						response.setContentLength((int)baos.size());
						os = response.getOutputStream();
						
						baos.writeTo(os);
						System.out.println("os is"+os);
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
						throw e;
					}
					finally
					{
						os.flush();
						os.close();
						baos.close();
					}
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			hSession.close();
		}
	}
}