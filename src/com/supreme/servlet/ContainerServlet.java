package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.ContainerController;
import com.supreme.controller.CustomerController;
import com.supreme.controller.CuttingController;
import com.supreme.controller.OrderController;
import com.supreme.dao.Cart;
import com.supreme.dao.ContainerMaterialList;
import com.supreme.dao.ContainerStatusDetails;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingOrderTask;
import com.supreme.dao.CuttingOrderTaskTypeDetails;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;

public class ContainerServlet extends HttpServlet {

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL = "test@fundexpert.in";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		ContainerController ctrl = new ContainerController();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} 
			else if (request.getParameter("action").equalsIgnoreCase("deleteContainerWithId")) {
				System.out.println("addContainer Entered");
				
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}
				
				try {
					if (!request.getParameterMap().containsKey("containerId") && request.getParameter("containerId") == null) {
						res.put("success", false);
						res.put("error", "Container not specified.");
					}
					else {
						
						Long containerId = Long.parseLong(request.getParameter("containerId"));
						
						Map<String, Object> result = new HashMap<>();
						
						result = ctrl.deleteContainerWithId(containerId);
						
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
						res.put("error", result.get("error"));
						
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				} finally {
					//out.write(res.toString());
				}
			}

			else if (request.getParameter("action").equalsIgnoreCase("deleteMaterialFormContainerWithId")) {
				System.out.println("addContainer Entered");
				
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}
				
				try {
					if (!request.getParameterMap().containsKey("materialId") && request.getParameter("materialId") == null) {
						res.put("success", false);
						res.put("error", "Material not specified.");
					}
					else if (!request.getParameterMap().containsKey("containerId") && request.getParameter("containerId") == null) {
						res.put("success", false);
						res.put("error", "Container not specified.");
					}
					else {
						
						Long containerId = Long.parseLong(request.getParameter("containerId"));
						Long materialId = Long.parseLong(request.getParameter("materialId"));
						
						Map<String, Object> result = new HashMap<>();
						
						result = ctrl.deleteMaterialFormContainerWithId(containerId, materialId);
						
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
						res.put("error", result.get("error"));
						
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				} finally {
					//out.write(res.toString());
				}
			}
			else if (request.getParameter("action").equalsIgnoreCase("addContainer")) {
				System.out.println("addContainer Entered");
				
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("materialList[0][materialId]") && request.getParameter("materialList[0][materialId]") == null) {
						res.put("success", false);
						res.put("error", "Please enter atleast one material.");
					} 
					else if (!request.getParameterMap().containsKey("supplierId") && request.getParameter("supplierId") == null) {
						res.put("success", false);
						res.put("error", "Supplier Id not specified.");
					}
					else if (!request.getParameterMap().containsKey("containerNumber") && request.getParameter("containerNumber") == null) {
						res.put("success", false);
						res.put("error", "Container No not specified.");
					}
					else if (!request.getParameterMap().containsKey("containerStatus") && request.getParameter("containerStatus") == null) {
						res.put("success", false);
						res.put("error", "Container Status not specified.");
					}
					else {
						
						System.out.println("Else entered");
						long paymentStatus, DOStatus, BLStatus, containerStatus, bankId, supplierId;
						String invoiceNumber, containerNumber, description;
						Date invoiceDate, paymentDate;
						double amount;
						
						if (request.getParameterMap().containsKey("paymentStatus") 
								&& request.getParameter("paymentStatus") != null
								&& !request.getParameter("paymentStatus").equals("")) {
							paymentStatus = Long.parseLong(request.getParameter("paymentStatus"));
						}
						else {
							paymentStatus = 0L;
						}
						
						if (request.getParameterMap().containsKey("DOStatus") 
								&& request.getParameter("DOStatus") != null
								&& !request.getParameter("DOStatus").equals("")) {
							DOStatus = Long.parseLong(request.getParameter("DOStatus"));
						}
						else {
							DOStatus = 0L;
						}
						
						if (request.getParameterMap().containsKey("BLStatus") 
								&& request.getParameter("BLStatus") != null
								&& !request.getParameter("BLStatus").equals("")) {
							BLStatus = Long.parseLong(request.getParameter("BLStatus"));
						}
						else {
							BLStatus = 0L;
						}
						
						if (request.getParameterMap().containsKey("containerStatus") 
								&& request.getParameter("containerStatus") != null
								&& !request.getParameter("containerStatus").equals("")) {
							containerStatus = Long.parseLong(request.getParameter("containerStatus"));
						}
						else {
							containerStatus = 0L;
						}
						
						if (request.getParameterMap().containsKey("bankId") 
								&& request.getParameter("bankId") != null
								&& !request.getParameter("bankId").equals("")) {
							bankId = Long.parseLong(request.getParameter("bankId"));
						}
						else {
							bankId = 0L;
						}
						
						if (request.getParameterMap().containsKey("supplierId") 
								&& request.getParameter("supplierId") != null
								&& !request.getParameter("supplierId").equals("")) {
							supplierId = Long.parseLong(request.getParameter("supplierId"));
						}
						else {
							supplierId = 0L;
						}
						
						if (request.getParameterMap().containsKey("invoiceNumber") 
								&& request.getParameter("invoiceNumber") != null
								&& !request.getParameter("invoiceNumber").equals("")) {
							invoiceNumber = request.getParameter("invoiceNumber");
						}
						else {
							invoiceNumber = "";
						}
						
						if (request.getParameterMap().containsKey("containerNumber") 
								&& request.getParameter("containerNumber") != null
								&& !request.getParameter("containerNumber").equals("")) {
							containerNumber = request.getParameter("containerNumber");
						}
						else {
							containerNumber = "";
						}
						
						if (request.getParameterMap().containsKey("description") 
								&& request.getParameter("description") != null
								&& !request.getParameter("description").equals("")) {
							description = request.getParameter("description");
						}
						else {
							description = "";
						}
						
						if (request.getParameterMap().containsKey("invoiceDate") 
								&& request.getParameter("invoiceDate") != null
								&& !request.getParameter("invoiceDate").equals("")) {
							invoiceDate = sdf.parse(request.getParameter("invoiceDate"));
						}
						else {
							invoiceDate = null;
						}
						
						if (request.getParameterMap().containsKey("paymentDate") 
								&& request.getParameter("paymentDate") != null
								&& !request.getParameter("paymentDate").equals("")) {
							paymentDate = sdf.parse(request.getParameter("paymentDate"));
						}
						else {
							paymentDate = null;
						}
						
						if (request.getParameterMap().containsKey("amount") 
								&& request.getParameter("amount") != null
								&& !request.getParameter("amount").equals("")) {
							amount = Double.parseDouble(request.getParameter("amount"));
						}
						else {
							amount = 0;
						}
						
						int i = 0;
						Set<ContainerMaterialList> cmlSet = new HashSet<ContainerMaterialList>();
						
						while(request.getParameterMap().containsKey("materialList["+i+"][materialId]")) {
							
							long materialId = 0, noOfPieces = 0;
							int thickness = 0;
							double quantity = 0, billRate = 0, realRate = 0;
							
							if(request.getParameterMap().containsKey("materialList["+i+"][materialId]")) {
								materialId = Long.parseLong(request.getParameter("materialList["+i+"][materialId]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter material in row "+(i+1)+".");
							}
							
							if(request.getParameterMap().containsKey("materialList["+i+"][noOfPieces]")) {
								noOfPieces = Long.parseLong(request.getParameter("materialList["+i+"][noOfPieces]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter no of pieces in row "+(i+1)+".");
							}
							
							if(request.getParameterMap().containsKey("materialList["+i+"][thickness]")) {
								thickness = Integer.parseInt(request.getParameter("materialList["+i+"][thickness]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter thickness in row "+(i+1)+".");
							}
							
							if(request.getParameterMap().containsKey("materialList["+i+"][quantity]")) {
								quantity = Double.parseDouble(request.getParameter("materialList["+i+"][quantity]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter quantity in row "+(i+1)+".");
							}
							
							if(request.getParameterMap().containsKey("materialList["+i+"][billRate]")) {
								billRate = Double.parseDouble(request.getParameter("materialList["+i+"][billRate]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter billRate in row "+(i+1)+".");
							}
							
							if(request.getParameterMap().containsKey("materialList["+i+"][realRate]")) {
								realRate = Double.parseDouble(request.getParameter("materialList["+i+"][realRate]"));
							}
							else {
								res.put("success", false);
								res.put("error", "Please enter realRate in row "+(i+1)+".");
							}
							
							ContainerMaterialList cml = new ContainerMaterialList(materialId, noOfPieces, thickness, quantity, billRate, realRate);
							cmlSet.add(cml);
							
							i++;
						}
						
						long containerId = 0L;
						
						/*
						 * ContainerStatusDetails csdObj = new ContainerStatusDetails(BLStatus,
						 * DOStatus, containerStatus, paymentStatus, amount,
						 * containerNumber, invoiceNumber, description, paymentDate, invoiceDate);
						 */
						
						
						System.out.println("DOStatus: "+ DOStatus);
						System.out.println("containerNumber: "+ containerNumber);
						System.out.println("BLStatus: "+ BLStatus);
						System.out.println("paymentStatus: "+ paymentStatus);
						System.out.println("containerStatus: "+ containerStatus);
						System.out.println("bankId: "+ bankId);
						System.out.println("invoiceNumber: "+ invoiceNumber);
						System.out.println("invoiceDate: "+ invoiceDate);
						System.out.println("description: "+ description);
						System.out.println("paymentDate: "+ paymentDate);
						System.out.println("amount: "+ amount);
						System.out.println("supplierId: "+ supplierId);
						System.out.println("containerId: "+ containerId);
						
						
						Map<String, Object> result = new HashMap<>();
						
						result = ctrl.addOrEditContainerDetails(containerId, supplierId, containerNumber, DOStatus, BLStatus, containerStatus, bankId, invoiceNumber, description, paymentDate, amount, paymentStatus, invoiceDate, cmlSet);
						
						
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
						res.put("error", result.get("error"));
						
						
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					//out.write(res.toString());
				}
			}
		else if (request.getParameter("action").equalsIgnoreCase("editContainer")) {
			System.out.println("editContainer Entered");
			
			if (session.getAttribute("id") == null) {
				throw new ValidationException("You are not logged in.");
			}
			
			try {
				if (!request.getParameterMap().containsKey("materialList[0][materialId]") && request.getParameter("materialList[0][materialId]") == null) {
					res.put("success", false);
					res.put("error", "Please enter atleast one material.");
				} 
				else if (!request.getParameterMap().containsKey("containerId") && request.getParameter("containerId") == null) {
					res.put("success", false);
					res.put("error", "Container Id not specified.");
				}
				else if (!request.getParameterMap().containsKey("supplierId") && request.getParameter("supplierId") == null) {
					res.put("success", false);
					res.put("error", "Supplier Id not specified.");
				}
				else if (!request.getParameterMap().containsKey("containerNumber") && request.getParameter("containerNumber") == null) {
					res.put("success", false);
					res.put("error", "Container No not specified.");
				}
				else if (!request.getParameterMap().containsKey("containerStatus") && request.getParameter("containerStatus") == null) {
					res.put("success", false);
					res.put("error", "Container Status not specified.");
				}
				else {
					
					System.out.println("Else entered");
					long paymentStatus, DOStatus, BLStatus, containerStatus, bankId, supplierId;
					String invoiceNumber, containerNumber, description;
					Date invoiceDate, paymentDate;
					double amount;
					
					if (request.getParameterMap().containsKey("paymentStatus") 
							&& request.getParameter("paymentStatus") != null
							&& !request.getParameter("paymentStatus").equals("")) {
						paymentStatus = Long.parseLong(request.getParameter("paymentStatus"));
					}
					else {
						paymentStatus = 0L;
					}
					
					if (request.getParameterMap().containsKey("DOStatus") 
							&& request.getParameter("DOStatus") != null
							&& !request.getParameter("DOStatus").equals("")) {
						DOStatus = Long.parseLong(request.getParameter("DOStatus"));
					}
					else {
						DOStatus = 0L;
					}
					
					if (request.getParameterMap().containsKey("BLStatus") 
							&& request.getParameter("BLStatus") != null
							&& !request.getParameter("BLStatus").equals("")) {
						BLStatus = Long.parseLong(request.getParameter("BLStatus"));
					}
					else {
						BLStatus = 0L;
					}
					
					if (request.getParameterMap().containsKey("containerStatus") 
							&& request.getParameter("containerStatus") != null
							&& !request.getParameter("containerStatus").equals("")) {
						containerStatus = Long.parseLong(request.getParameter("containerStatus"));
					}
					else {
						containerStatus = 0L;
					}
					
					if (request.getParameterMap().containsKey("bankId") 
							&& request.getParameter("bankId") != null
							&& !request.getParameter("bankId").equals("")) {
						bankId = Long.parseLong(request.getParameter("bankId"));
					}
					else {
						bankId = 0L;
					}
					
					if (request.getParameterMap().containsKey("supplierId") 
							&& request.getParameter("supplierId") != null
							&& !request.getParameter("supplierId").equals("")) {
						supplierId = Long.parseLong(request.getParameter("supplierId"));
					}
					else {
						supplierId = 0L;
					}
					
					if (request.getParameterMap().containsKey("invoiceNumber") 
							&& request.getParameter("invoiceNumber") != null
							&& !request.getParameter("invoiceNumber").equals("")) {
						invoiceNumber = request.getParameter("invoiceNumber");
					}
					else {
						invoiceNumber = "";
					}
					
					if (request.getParameterMap().containsKey("containerNumber") 
							&& request.getParameter("containerNumber") != null
							&& !request.getParameter("containerNumber").equals("")) {
						containerNumber = request.getParameter("containerNumber");
					}
					else {
						containerNumber = "";
					}
					
					if (request.getParameterMap().containsKey("description") 
							&& request.getParameter("description") != null
							&& !request.getParameter("description").equals("")) {
						description = request.getParameter("description");
					}
					else {
						description = "";
					}
					
					if (request.getParameterMap().containsKey("invoiceDate") 
							&& request.getParameter("invoiceDate") != null
							&& !request.getParameter("invoiceDate").equals("")) {
						invoiceDate = sdf.parse(request.getParameter("invoiceDate"));
					}
					else {
						invoiceDate = null;
					}
					
					if (request.getParameterMap().containsKey("paymentDate") 
							&& request.getParameter("paymentDate") != null
							&& !request.getParameter("paymentDate").equals("")) {
						paymentDate = sdf.parse(request.getParameter("paymentDate"));
					}
					else {
						paymentDate = null;
					}
					
					if (request.getParameterMap().containsKey("amount") 
							&& request.getParameter("amount") != null
							&& !request.getParameter("amount").equals("")) {
						amount = Double.parseDouble(request.getParameter("amount"));
					}
					else {
						amount = 0;
					}
					
					int i = 0;
					Set<ContainerMaterialList> cmlSet = new HashSet<ContainerMaterialList>();
					
					while(request.getParameterMap().containsKey("materialList["+i+"][materialId]")) {
						
						long materialId = 0, noOfPieces = 0;
						int thickness = 0;
						double quantity = 0, billRate = 0, realRate = 0;
						
						if(request.getParameterMap().containsKey("materialList["+i+"][materialId]")) {
							materialId = Long.parseLong(request.getParameter("materialList["+i+"][materialId]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter material in row "+(i+1)+".");
						}
						
						if(request.getParameterMap().containsKey("materialList["+i+"][noOfPieces]")) {
							noOfPieces = Long.parseLong(request.getParameter("materialList["+i+"][noOfPieces]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter no of pieces in row "+(i+1)+".");
						}
						
						if(request.getParameterMap().containsKey("materialList["+i+"][thickness]")) {
							thickness = Integer.parseInt(request.getParameter("materialList["+i+"][thickness]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter thickness in row "+(i+1)+".");
						}
						
						if(request.getParameterMap().containsKey("materialList["+i+"][quantity]")) {
							quantity = Double.parseDouble(request.getParameter("materialList["+i+"][quantity]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter quantity in row "+(i+1)+".");
						}
						
						if(request.getParameterMap().containsKey("materialList["+i+"][billRate]")) {
							billRate = Double.parseDouble(request.getParameter("materialList["+i+"][billRate]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter billRate in row "+(i+1)+".");
						}
						
						if(request.getParameterMap().containsKey("materialList["+i+"][realRate]")) {
							realRate = Double.parseDouble(request.getParameter("materialList["+i+"][realRate]"));
						}
						else {
							res.put("success", false);
							res.put("error", "Please enter realRate in row "+(i+1)+".");
						}
						
						ContainerMaterialList cml = new ContainerMaterialList(materialId, noOfPieces, thickness, quantity, billRate, realRate);
						cmlSet.add(cml);
						
						i++;
					}
					
					long containerId = Long.parseLong(request.getParameter("containerId"));
					
					System.out.println("DOStatus: "+ DOStatus);
					System.out.println("containerNumber: "+ containerNumber);
					System.out.println("BLStatus: "+ BLStatus);
					System.out.println("paymentStatus: "+ paymentStatus);
					System.out.println("containerStatus: "+ containerStatus);
					System.out.println("bankId: "+ bankId);
					System.out.println("invoiceNumber: "+ invoiceNumber);
					System.out.println("invoiceDate: "+ invoiceDate);
					System.out.println("description: "+ description);
					System.out.println("paymentDate: "+ paymentDate);
					System.out.println("amount: "+ amount);
					System.out.println("supplierId: "+ supplierId);
					System.out.println("containerId: "+ containerId);
					
					
					Map<String, Object> result = new HashMap<>();
					
					result = ctrl.addOrEditContainerDetails(containerId, supplierId, containerNumber, DOStatus, BLStatus, containerStatus, bankId, invoiceNumber, description, paymentDate, amount, paymentStatus, invoiceDate, cmlSet);
					
					
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
					res.put("error", result.get("error"));
					
					
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				//out.write(res.toString());
			}
		} 
		}
			catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		CuttingController ctrl = new CuttingController();

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} else if (request.getParameter("action").equalsIgnoreCase("getAllCuttingList")) {
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllCuttingList();

				res.put("success", true);
				res.put("data", result.get("data"));
			} 
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}
	}

}
