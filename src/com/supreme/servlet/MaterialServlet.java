package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.MaterialController;

public class MaterialServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		MaterialController ctrl = new MaterialController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{

			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("updateSingleMaterial")){

				System.out.println("updateSingleMaterial action called");

				if(!request.getParameterMap().containsKey("materialName") || request.getParameter("materialName").equals(""))
				{
					res.put("success", false);
					res.put("error", "Material Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("originCountry") || request.getParameter("originCountry").equals(""))
				{
					res.put("success", false);
					res.put("error", "Origin Country not specified.");
				}
				else if(!request.getParameterMap().containsKey("lowerPriceRange") || request.getParameter("lowerPriceRange").equals(""))
				{
					res.put("success", false);
					res.put("error", "Lower Price Range not specified.");
				}
				else if(!request.getParameterMap().containsKey("higherPriceRange") || request.getParameter("higherPriceRange").equals(""))
				{
					res.put("success", false);
					res.put("error", "Higher Price Range not specified.");
				}
				else
				{
					String materialName = request.getParameter("materialName");
					String originCountry = request.getParameter("originCountry");
					double lowerPriceRange = Double.parseDouble(request.getParameter("lowerPriceRange"));
					double higherPriceRange = Double.parseDouble(request.getParameter("higherPriceRange"));

					Map<String, Object> result = null;

					result = ctrl.addSingleNewMaterial(materialName, originCountry, lowerPriceRange, higherPriceRange);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("editSingleMaterial")){
				
				System.out.println("updateSingleMaterial action called");
				
				if(!request.getParameterMap().containsKey("materialName") || request.getParameter("materialName").equals(""))
				{
					res.put("success", false);
					res.put("error", "Material Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("materialId") || request.getParameter("materialId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Material Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("originCountry") || request.getParameter("originCountry").equals(""))
				{
					res.put("success", false);
					res.put("error", "Origin Country not specified.");
				}
				else if(!request.getParameterMap().containsKey("lowerPriceRange") || request.getParameter("lowerPriceRange").equals(""))
				{
					res.put("success", false);
					res.put("error", "Lower Price Range not specified.");
				}
				else if(!request.getParameterMap().containsKey("higherPriceRange") || request.getParameter("higherPriceRange").equals(""))
				{
					res.put("success", false);
					res.put("error", "Higher Price Range not specified.");
				}
				else
				{
					long id = Long.parseLong(request.getParameter("materialId"));
					String materialName = request.getParameter("materialName");
					String originCountry = request.getParameter("originCountry");
					double lowerPriceRange = Double.parseDouble(request.getParameter("lowerPriceRange"));
					double higherPriceRange = Double.parseDouble(request.getParameter("higherPriceRange"));
					
					Map<String, Object> result = null;
					
					result = ctrl.editSingleMaterial(materialName, originCountry, lowerPriceRange, higherPriceRange, id);
					
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		MaterialController ctrl = new MaterialController();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllMaterialList"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllMaterialList();

				out.write(result.get("data").toString());

				res.put("success", true);
				res.put("data", result.get("data"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}

}
