package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.StatusController;
import com.supreme.controller.SupplierController;

public class StatusServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		StatusController ctrl = new StatusController();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllStatusesAndTypes"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllStatusList();

				out.write(result.get("data").toString());

				res.put("success", true);
				res.put("data", result.get("data"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}

}
