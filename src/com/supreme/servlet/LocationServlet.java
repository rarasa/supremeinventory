package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.LocationController;

public class LocationServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		LocationController ctrl = new LocationController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{

			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("updateSingleLocation")){

				System.out.println("updateSingleLocations action called");

				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Locations Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("address") || request.getParameter("address").equals(""))
				{
					res.put("success", false);
					res.put("error", "Address not specified.");
				}
				else if(!request.getParameterMap().containsKey("code") || request.getParameter("code").equals(""))
				{
					res.put("success", false);
					res.put("error", "Code not specified.");
				}
				else if(!request.getParameterMap().containsKey("phone") || request.getParameter("phone").equals(""))
				{
					res.put("success", false);
					res.put("error", "Phone not specified.");
				}
				else
				{
					String name = request.getParameter("name");
					String address = request.getParameter("address");
					String code = request.getParameter("code");
					String phone = request.getParameter("phone");

					Map<String, Object> result = null;

					result = ctrl.addSingleNewLocations(name, address, phone, code);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("editSingleLocation")){

				System.out.println("updateSingleLocations action called");

				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Locations Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("address") || request.getParameter("address").equals(""))
				{
					res.put("success", false);
					res.put("error", "Address not specified.");
				}
				else if(!request.getParameterMap().containsKey("id") || request.getParameter("id").equals(""))
				{
					res.put("success", false);
					res.put("error", "Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("code") || request.getParameter("code").equals(""))
				{
					res.put("success", false);
					res.put("error", "Code not specified.");
				}
				else if(!request.getParameterMap().containsKey("phone") || request.getParameter("phone").equals(""))
				{
					res.put("success", false);
					res.put("error", "Phone not specified.");
				}
				else if(!request.getParameterMap().containsKey("deleted") || request.getParameter("deleted").equals(""))
				{
					res.put("success", false);
					res.put("error", "Deleted not specified.");
				}
				else if(!request.getParameterMap().containsKey("active") || request.getParameter("active").equals(""))
				{
					res.put("success", false);
					res.put("error", "Active not specified.");
				}
				else
				{
					long id = Long.parseLong(request.getParameter("id"));
					String name = request.getParameter("name");
					String address = request.getParameter("address");
					String code = request.getParameter("code");
					String phone = request.getParameter("phone");
					boolean deleted = request.getParameter("deleted").equals("true") ? true : false;
					boolean active = request.getParameter("active").equals("true") ? true : false;

					Map<String, Object> result = null;

					result = ctrl.editSingleLocation(name, address, phone, code, active, deleted, id);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("deleteLocation")){
				
				System.out.println("updateSingleLocations action called");
				
				if(!request.getParameterMap().containsKey("name") || request.getParameter("name").equals(""))
				{
					res.put("success", false);
					res.put("error", "Locations Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("id") || request.getParameter("id").equals(""))
				{
					res.put("success", false);
					res.put("error", "Id not specified.");
				}
				else
				{
					long id = Long.parseLong(request.getParameter("id"));
					String name = request.getParameter("name");
					
					Map<String, Object> result = null;
					
					result = ctrl.deleteLocation(name, id);
					
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}


	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		LocationController ctrl = new LocationController();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllLocationsList"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllLocationsList();

				out.write(result.get("data").toString());

				res.put("success", true);
				res.put("data", result.get("data"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}

}
