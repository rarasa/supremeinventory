/**
 * 
 */
package com.supreme.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.Config;
import com.supreme.controller.LocationController;

/**
 * @author Rashi
 *
 */
public class LocationListUploadServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		JSONObject responseJson = new JSONObject();
		PrintWriter writer = response.getWriter();
		LocationController ctrl = new LocationController();

		Config config = new Config();
		String path = "";
		if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV))
		{
			path = config.get(Config.EXCEL_DEV_PATH);
			System.out.println("dev path="+path);
		}
		else
		{
			path = config.get(Config.EXCEL_PROD_PATH);
			System.out.println("prod path="+path);
		}
		
		try
		{
				/**
				 * All data available in query string.
				 * Picking values now
				 */
				
				
				System.out.println("entered bulk supplier upload");
				
				// TODO Auto-generated method stub

				String fileName = null;
				boolean isMultiPart = false;
				isMultiPart = ServletFileUpload.isMultipartContent(request);
				// userId contains the current loggedIn user on Germinate
				if(!new File(path).exists())
				{
					System.out.println("file path exists");
					File f = new File(path);
					f.mkdir();
					System.out.println("Absolute Path of the file : " + f.getAbsolutePath());
				}
				else{
					System.out.println("file path exists exists");
				}
				if(isMultiPart == true)
				{	
					DiskFileItemFactory diskFactory = new DiskFileItemFactory();
					ServletFileUpload sfu = new ServletFileUpload(diskFactory);
					List list = sfu.parseRequest(request);
					Iterator iterator = list.iterator();
					File packingListExcel;
					
					while (iterator.hasNext())
					{
						System.out.print("In");
						FileItem fileItem = (FileItem) iterator.next();
						if(!fileItem.isFormField())
						{
							System.out.println(path);
							System.out.println(fileItem.getName());
							path=path+"/"+new Date().getTime()+fileItem.getName();
							final String finalFileName = new Date().getTime()+fileItem.getName();
							
							try
							{
								packingListExcel = new File(path);
								fileItem.write(packingListExcel);
								System.out.println("path=======================");
								
								System.out.println(finalFileName);
								
								ctrl.bulkLocationsListUpload(finalFileName);
								
								responseJson.put("success", true);
								responseJson.put("message","File uploaded successfully.");
								
							}
							catch (Exception e)
							{
								responseJson.put("success",false);
								responseJson.put("error","Not able to save file");
							}
			
						}
					}			
				}
				else
				{
					responseJson.put("success",false);
					responseJson.put("error","No File Selected");
				}
	
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				responseJson.put("success", false);
				responseJson.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				responseJson.put("success", false);
				responseJson.put("error", e.getMessage());
				writer.write(responseJson.toString(3));
			}
			catch (JSONException ex)
			{
				ex.printStackTrace();
			}
		}

	
		
	}
}
