package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.SupplierController;

public class SupplierServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		SupplierController ctrl = new SupplierController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{

			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("updateSingleSupplier")){

				System.out.println("updateSingleSupplier action called");

				if(!request.getParameterMap().containsKey("supplierName") || request.getParameter("supplierName").equals(""))
				{
					res.put("success", false);
					res.put("error", "Supplier Name not specified.");
				}
				
				else if(!request.getParameterMap().containsKey("address") || request.getParameter("address").equals("")) { 
					res.put("success", false);
					res.put("error", "Address not specified."); 
				} else if(!request.getParameterMap().containsKey("phone") || request.getParameter("phone").equals("")) { 
					res.put("success", false);
					res.put("error", "Phone not specified."); 
				} else if(!request.getParameterMap().containsKey("email") || request.getParameter("email").equals("")) { 
					res.put("success", false);
					res.put("error", "Email not specified."); 
				}
				else
				{
					String supplierName = request.getParameter("supplierName");
					
					long id = 0L;
					if(request.getParameterMap().containsKey("id") && !request.getParameter("id").equals("")) { 
						id = Long.parseLong(request.getParameter("id"));
					}
					
					String address = null, phone = null, email = null;
					
					if(request.getParameterMap().containsKey("address") && !request.getParameter("address").equals(""))
					{
						address = request.getParameter("address");
					}
					
					if(request.getParameterMap().containsKey("phone") && !request.getParameter("phone").equals(""))
					{
						phone = request.getParameter("phone");
					}
					
					if(request.getParameterMap().containsKey("email") && !request.getParameter("email").equals(""))
					{
						email = request.getParameter("email");
					}
					
					
					Map<String, Object> result = null;

					result = ctrl.addSingleNewSupplier(supplierName, address, phone, email, id);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("editSingleSupplier")){
				
				System.out.println("updateSingleSupplier action called");
				
				if(!request.getParameterMap().containsKey("supplierName") || request.getParameter("supplierName").equals(""))
				{
					res.put("success", false);
					res.put("error", "Supplier Name not specified.");
				}
				
				else if(!request.getParameterMap().containsKey("address") || request.getParameter("address").equals("")) { 
					res.put("success", false);
					res.put("error", "Address not specified."); 
				} else if(!request.getParameterMap().containsKey("phone") || request.getParameter("phone").equals("")) { 
					res.put("success", false);
					res.put("error", "Phone not specified."); 
				} else if(!request.getParameterMap().containsKey("email") || request.getParameter("email").equals("")) { 
					res.put("success", false);
					res.put("error", "Email not specified."); 
				}
				else if(!request.getParameterMap().containsKey("supplierId") || request.getParameter("supplierId").equals("")) { 
					res.put("success", false);
					res.put("error", "Id not specified."); 
				}
				else
				{
					String supplierName = request.getParameter("supplierName");
					
					Long id = Long.parseLong(request.getParameter("supplierId"));
					
					String address = null, phone = null, email = null;
					
					if(request.getParameterMap().containsKey("address") && !request.getParameter("address").equals(""))
					{
						address = request.getParameter("address");
					}
					
					if(request.getParameterMap().containsKey("phone") && !request.getParameter("phone").equals(""))
					{
						phone = request.getParameter("phone");
					}
					
					if(request.getParameterMap().containsKey("email") && !request.getParameter("email").equals(""))
					{
						email = request.getParameter("email");
					}
					
					
					Map<String, Object> result = null;
					
					result = ctrl.editSingleNewSupplier(supplierName, address, phone, email, id);
					
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
				
			}
			else if(request.getParameter("action").equalsIgnoreCase("deleteSupplier")){

				System.out.println("deleteSupplier action called");

				if(!request.getParameterMap().containsKey("supplierName") || request.getParameter("supplierName").equals(""))
				{
					res.put("success", false);
					res.put("error", "Supplier Name not specified.");
				}
				else if(!request.getParameterMap().containsKey("id") || request.getParameter("id").equals(""))
				{
					res.put("success", false);
					res.put("error", "Supplier ID not specified.");
				}
				else {
					String supplierName = request.getParameter("supplierName");
					
					Long id = Long.parseLong(request.getParameter("id"));
					
					Map<String, Object> result = null;

					result = ctrl.deleteSupplier(supplierName, id);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllSupplierList"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}

				SupplierController ctrl = new SupplierController();
				Map<String, Object> result = null;

				result = ctrl.getAllSupplierList();

				out.write(result.get("data").toString());

				res.put("success", true);
				res.put("data", result.get("data"));
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}

}
