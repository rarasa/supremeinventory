package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.RestockController;
import com.supreme.controller.SupplierController;

public class RestockServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		RestockController ctrl = new RestockController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{

			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("restockInventoryId")){

				if(!request.getParameterMap().containsKey("inventoryId") || request.getParameter("inventoryId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Inventory Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("orderId") || request.getParameter("orderId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Order Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("cartId") || request.getParameter("cartId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Cart Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("statusTypeId") || request.getParameter("statusTypeId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Status Id not specified.");
				}
				else {
					
					Long inventoryId = Long.parseLong(request.getParameter("inventoryId"));
					Long orderId = Long.parseLong(request.getParameter("orderId"));
					Long statusTypeId = Long.parseLong(request.getParameter("statusTypeId"));
					Long cartId = Long.parseLong(request.getParameter("cartId"));
					
					String remarks = "";
					boolean tile = false;
					int tileCount = 0;
					if(request.getParameterMap().containsKey("remarks") && !request.getParameter("remarks").equals("")) {
						remarks = request.getParameter("remarks");
					}
					
					if(request.getParameterMap().containsKey("tile") && !request.getParameter("tile").equals("")) {
						tile = request.getParameter("tile").equals("true") ? true : false;
						
						if(tile) {
							if(!request.getParameterMap().containsKey("tileCount") || request.getParameter("tileCount").equals(""))
							{
								res.put("success", false);
								res.put("error", "Tile count not specified.");
							}
							else {
								tileCount = Integer.parseInt(request.getParameter("tileCount"));
							}
						}
					}
					
					Map<String, Object> result = null;

					result = ctrl.restockCartId(inventoryId, orderId, cartId, statusTypeId, remarks, tile, tileCount);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
			}
			else if(request.getParameter("action").equalsIgnoreCase("addSlabToOrder")){
				
				if(!request.getParameterMap().containsKey("barcodeString") || request.getParameter("barcodeString").equals(""))
				{
					res.put("success", false);
					res.put("error", "Barcode String not specified.");
				}
				else if(!request.getParameterMap().containsKey("orderId") || request.getParameter("orderId").equals(""))
				{
					res.put("success", false);
					res.put("error", "Order Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("markSlabAsSold") || request.getParameter("markSlabAsSold").equals(""))
				{
					res.put("success", false);
					res.put("error", "Mark Slab As Sold checkbox not specified.");
				}
				else {
					
					Long orderId = Long.parseLong(request.getParameter("orderId"));
					String barcodeString = request.getParameter("barcodeString");
					boolean markSlabAsSold = request.getParameter("markSlabAsSold").equals("1") ? true : false;
					
					Map<String, Object> result = null;
					
					result = ctrl.addSlabToOrder(orderId, barcodeString, markSlabAsSold);
					
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();

		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("getOrderDetailsForBarcodeString"))
			{
				System.out.println("action getOrderDetailsForBarcodeString called");
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}
				
				if(!request.getParameterMap().containsKey("barcodeString") || request.getParameter("barcodeString").equals(""))
				{
					res.put("success", false);
					res.put("error", "Barcode String not specified.");
				}
				else {
					
					String barcodeString = request.getParameter("barcodeString");
					RestockController ctrl = new RestockController();
					Map<String, Object> result = null;
					
					result = ctrl.getOrderDetailsForBarcodeString(barcodeString);
					
					res.put("success", result.get("success"));
					res.put("data", result.get("data"));
					res.put("message", result.get("message") != null ? result.get("message") : "");
					res.put("error", result.get("error") != null ? result.get("error") : "");
					 
				}

			}
			else if(request.getParameter("action").equalsIgnoreCase("getOrderDetailsForBarcodeStringToAddToOrder"))
			{
				System.out.println("action getOrderDetailsForBarcodeString called");
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}
				
				if(!request.getParameterMap().containsKey("barcodeString") || request.getParameter("barcodeString").equals(""))
				{
					res.put("success", false);
					res.put("error", "Barcode String not specified.");
				}
				else {
					
					String barcodeString = request.getParameter("barcodeString");
					RestockController ctrl = new RestockController();
					Map<String, Object> result = null;
					
					result = ctrl.getOrderDetailsForBarcodeStringToAddToOrder(barcodeString);
					
					res.put("success", result.get("success"));
					res.put("data", result.get("data"));
					res.put("message", result.get("message") != null ? result.get("message") : "");
					res.put("error", result.get("error") != null ? result.get("error") : "");
					
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			out.write(res.toString());
		}
	}

}
