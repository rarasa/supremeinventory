package com.supreme.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.Config;
import com.supreme.controller.CuttingController;
import com.supreme.controller.DeliveryOrderController;
import com.supreme.controller.OrderController;
import com.supreme.dao.Cart;
import com.supreme.dao.Orders;
import com.supreme.pojo.OrderSet;

public class OrderServlet extends HttpServlet {

	/**
	 * Author : Rashi Rathi Date : 2020-11-30
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL = "rashirathi@outlook.com";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		OrderController ctrl = new OrderController();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		DeliveryOrderController DOCtrl = new DeliveryOrderController();

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} else if (request.getParameter("action").equalsIgnoreCase("addPurchaseOrder")) {
				System.out.println("addPurchaseOrderEntered");
				System.out.println(request.getQueryString());
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("customerName")
							&& request.getParameter("customerName") == null) {
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					}
					else if (!request.getParameterMap().containsKey("customerId")
							&& request.getParameter("customerId") == null) {
						res.put("success", false);
						res.put("error", "Customer Id Name not specified.");
					}
					else if (!request.getParameterMap().containsKey("cuttingOrderNumber")
							&& request.getParameter("cuttingOrderNumber") == null) {
						res.put("success", false);
						res.put("error", "Cutting Order Number Name not specified.");
					}
					else if (!request.getParameterMap().containsKey("count") && request.getParameter("count") == null) {
						res.put("success", false);
						res.put("error", "Count not specified.");
					}
					else if (!request.getParameterMap().containsKey("totalMtSquare")
							&& request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} 
					else if ((!request.getParameterMap().containsKey("purchaseOrder[0][id]"))
							|| request.getParameter("purchaseOrder[0][id]") == null) {
						res.put("success", false);
						res.put("error", "Purchase Order id not specified.");
					}
					else {

						long userId = 0;
						if (session.getAttribute("id") == null) {
							throw new ValidationException("You are not logged in.");
						}
						else {
							userId = (long) session.getAttribute("id");
						}
						
						long customerId = Long.parseLong(request.getParameter("customerId"));
						String customerName = request.getParameter("customerName");
						String cuttingOrderNumber = request.getParameter("cuttingOrderNumber");
						double count = Double.parseDouble(request.getParameter("count"));
						double totalMtSquare = Double.parseDouble(request.getParameter("totalMtSquare"));
						System.out.println("customerName: " + request.getParameter("customerName"));

						Set<Cart> cartOrders = new HashSet<Cart>();

						// TODO create baskets
						int i = 0;

						while (request.getParameterMap().containsKey("purchaseOrder[" + i + "][id]")) {
							long inventoryId = Long.parseLong(request.getParameter("purchaseOrder[" + i + "][id]"));
							String type = "";
							int tileCount = 0;

							System.out.println("request.getParameter(\"purchaseOrder[" + i + "][type]\")" + request.getParameter("purchaseOrder[" + i + "][type]"));
							
							if (request.getParameterMap().containsKey("purchaseOrder[" + i + "][type]")
									&& request.getParameter("purchaseOrder[" + i + "][type]") != null
									&& !request.getParameter("purchaseOrder[" + i + "][type]").equals("")
									&& request.getParameter("purchaseOrder[" + i + "][type]").equals("TILE")) {

								type = request.getParameter("purchaseOrder[" + i + "][type]");
								tileCount = Integer.parseInt(request.getParameter("purchaseOrder[" + i + "][tileCount]"));

								System.out.println("Entered tile and adding tilecount");
								Cart cartTileSet = new Cart(inventoryId, true, tileCount);
								cartOrders.add(cartTileSet);

							} else {
								System.out.println("Entered else so not adding tile and tileCount");
								Cart cartSet = new Cart(inventoryId, false, 0);
								cartOrders.add(cartSet);
							}


							i++;
						}
						
						boolean hasCuttingSlabs = false;
						if(request.getParameterMap().containsKey("hasCuttingSlabs")) {
							hasCuttingSlabs = true;
						}

						Map<String, Object> result = ctrl.addOrder(customerName, cartOrders, totalMtSquare, count, customerId, userId, cuttingOrderNumber, hasCuttingSlabs);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			} else if (request.getParameter("action").equalsIgnoreCase("printDO")) {
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("vehicleNumber")
							&& request.getParameter("vehicleNumber") == null) {
						res.put("success", false);
						res.put("error", "Vehicle Number not specified.");
					} else if (!request.getParameterMap().containsKey("orderId")
							&& request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					} else if (!request.getParameterMap().containsKey("emirate")
							&& request.getParameter("emirate") == null) {
						res.put("success", false);
						res.put("error", "emirate not specified.");
					} else if (request.getParameterMap().containsKey("LPONumber")
							&& request.getParameter("LPONumber") != null
							&& (!request.getParameterMap().containsKey("LPODate")
									&& request.getParameter("LPODate") == null)) {
						res.put("success", false);
						res.put("error", "LPO Date not specified.");
					} else if (request.getParameterMap().containsKey("LPODate")
							&& request.getParameter("LPODate") != null
							&& (!request.getParameterMap().containsKey("LPONumber")
									&& request.getParameter("LPONumber") == null)) {
						res.put("success", false);
						res.put("error", "LPO Number not specified.");
					} else {
						Date LPODate = null;
						String LPONumber = null;
						long orderId = Long.parseLong(request.getParameter("orderId"));
						String vehicleNumber = request.getParameter("orderId");
						String emirate = request.getParameter("emirate");

						if (request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPONumber") != null) {
							LPODate = sdf.parse(request.getParameter("LPODate"));
						}
						if (request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPODate") != null) {
							LPONumber = request.getParameter("LPONumber");
						} else {
							LPONumber = "";
						}

						Map<String, Object> result = ctrl.saveDOAdditionalData(orderId, vehicleNumber, emirate, LPODate,
								LPONumber);

						Map<String, Object> DOSaveResult = new HashMap<String, Object>();
						/**
						 * If additional details got saved, Print DO
						 */
						if ((boolean) result.get("success")) {
							System.out.println("printing DO");
							Orders order = (Orders) result.get("order");
							DOSaveResult = DOCtrl.printDO(order);

							if ((boolean) DOSaveResult.get("success")) {

								String path = "";
								try {
									Config config = new Config();
									if (config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)) {
										path = config.get(Config.PDF_DEV_PATH);
										System.out.println("Dev path: " + path);
									} else {
										path = config.get(Config.PDF_PROD_PATH);
										System.out.println("Prod path: " + path);
									}
								} catch (IOException e) {
									path = "/opt/supremeinventory/pdf/";
								}

								String fileName = (String) DOSaveResult.get("fileName");
								if (fileName == null || fileName.equals("")) {
									throw new ServletException("File Name can't be null or empty");
								}
								File file = new File(path + File.separator + fileName);
								if (!file.exists()) {
									throw new ServletException("File doesn't exists on server.");
								}
								System.out.println("File location on server::" + file.getAbsolutePath());
								ServletContext ctx = getServletContext();
								InputStream fis = new FileInputStream(file);
								String mimeType = ctx.getMimeType(file.getAbsolutePath());
								response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
								response.setContentLength((int) file.length());
								response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

								ServletOutputStream os = response.getOutputStream();
								byte[] bufferData = new byte[1024];
								int read = 0;
								while ((read = fis.read(bufferData)) != -1) {
									os.write(bufferData, 0, read);
								}
								os.flush();
								os.close();
								fis.close();
								System.out.println("File downloaded at client successfully");

								/*
								 * response.setContentType("application/octet-stream");
								 * response.setHeader("Content-Disposition", "filename=\"hoge.txt\""); File
								 * srcFile = new File(path+File.separator+DOSaveResult.get("fileName"));
								 * FileUtils.copyFile(srcFile, response.getOutputStream());
								 */
							}
						}

						res = new JSONObject();
						res.put("success", DOSaveResult.get("success"));
						res.put("fileName", DOSaveResult.get("fileName"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (request.getParameter("action").equalsIgnoreCase("editExistingPurchaseOrder")) {
				System.out.println("editExistingPurchaseOrder entered");
				
				long userId = (long) session.getAttribute("id");
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}
				else {
					userId = Long.parseLong(session.getAttribute("id").toString());
				}

				try {
					if (request.getParameterMap().containsKey("LPO Number") && request.getParameter("LPONumber") != null) {
						if (!request.getParameterMap().containsKey("LPODate")
								&& request.getParameter("LPODate") == null) {
							res.put("success", false);
							res.put("error", "LPO Date not specified.");
						}
					}
					if (request.getParameterMap().containsKey("LPODate") && request.getParameter("LPODate") != null) {
						if (!request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPONumber") == null) {
							res.put("success", false);
							res.put("error", "LPO Number not specified.");
						}
					}
					if (!request.getParameterMap().containsKey("customerName") && request.getParameter("customerName") == null) {
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					} else if (!request.getParameterMap().containsKey("id") && request.getParameter("id") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					} else if (!request.getParameterMap().containsKey("slabCount") && request.getParameter("slabCount") == null) {
						res.put("success", false);
						res.put("error", "Count not specified.");
					} else if (!request.getParameterMap().containsKey("totalMtSquare") && request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} 
					else if (!request.getParameterMap().containsKey("approveOrder") && request.getParameter("approveOrder") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} 
					else if (!request.getParameterMap().containsKey("emirate") && request.getParameter("emirate") == null) {
						res.put("success", false);
						res.put("error", "emirate not specified.");
					} 
					else if (!request.getParameterMap().containsKey("totalMtSquare") && request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Meter Square not specified.");
					} 
					 
					else if (!request.getParameterMap().containsKey("phone") && request.getParameter("phone") == null) {
						res.put("success", false);
						res.put("error", "Phone not specified.");
					} 
					else if (!request.getParameterMap().containsKey("VATRegistration") && request.getParameter("VATRegistration") == null) {
						res.put("success", false);
						res.put("error", "VATRegistration not specified.");
					} 
					else if (!request.getParameterMap().containsKey("hasCuttingOrders") && request.getParameter("hasCuttingOrders") == null) {
						res.put("success", false);
						res.put("error", "Has Cutting Orders not specified.");
					} 
					else if ((!request.getParameterMap().containsKey("cart[0][id]")) || request.getParameter("cart[0][id]") == null) {
						res.put("success", false);
						res.put("error", "Cart Details not specified.");
					} 
					else if ((!request.getParameterMap().containsKey("cart[0][type]")) || request.getParameter("cart[0][type]") == null) {
						res.put("success", false);
						res.put("error", "Slab Type not specified.");
					} 
					else if ((request.getParameterMap().containsKey("cart[0][type]") && request.getParameter("cart[0][type]").equals("TILE"))
							&& (!request.getParameterMap().containsKey("cart[0][tileCount]") || request.getParameter("cart[0][tileCount]") == null)) {
						res.put("success", false);
						res.put("error", "Tile Count not specified.");
					} 
					else {

						int i = 0;
						while (request.getParameterMap().containsKey("cart[" + i + "][id]")) {
							int tileCount = 0;
							String type = "";
							
							System.out.println("while i: " + i);
							long cartId = Long.parseLong(request.getParameter("cart[" + i + "][id]"));
							System.out.println("servlet cartId: " + cartId);
							long inventoryId = Long.parseLong(request.getParameter("cart[" + i + "][inventoryId]"));
							double actualLength = Double
									.parseDouble(request.getParameter("cart[" + i + "][actualLength]"));
							double actualHeight = Double
									.parseDouble(request.getParameter("cart[" + i + "][actualHeight]"));
							double actualSqMetre = Double
									.parseDouble(request.getParameter("cart[" + i + "][actualSqMetre]"));
							long thickness = Long.parseLong(request.getParameter("cart[" + i + "][thickness]"));
							type = request.getParameter("cart[" + i + "][type]");
							
							
							/*
							 * if(type.equals("TILE")) { tileCount =
							 * Integer.parseInt(request.getParameter("cart[" + i + "][tileCount]")); }
							 */
							
							ctrl.updateOrderCartRow(cartId, inventoryId, actualHeight, actualLength, thickness,
									actualSqMetre, type);

							i++;
						}
						
						
						
						String phone = "", vehicleNumber = "";
						long orderId = Long.parseLong(request.getParameter("id"));
						String customerName = request.getParameter("customerName");
						double slabCount = Double.parseDouble(request.getParameter("slabCount"));
						double totalMtSquare = Double.parseDouble(request.getParameter("totalMtSquare"));
						boolean approveOrder = (request.getParameter("approveOrder").equals("true")) ? true : false;
						String emirate = request.getParameter("emirate");
						
						String LPONumber = "", narration="";
						Date LPODate = null;
						boolean companyPickupBoolean = request.getParameter("companyPickup").equals("0") ? false:true;
						boolean hasCuttingOrders = request.getParameter("hasCuttingOrders").equals("0") ? false:true;
						phone = request.getParameter("phone");
						
						Date VATRegistrationDate = null;
						String trnNumber = null;
						boolean VATRegistrationBoolean = request.getParameter("VATRegistration").equals("0") ? false:true;
						if(VATRegistrationBoolean) {
							
							if (request.getParameterMap().containsKey("trnNumber")
									&& request.getParameter("trnNumber") != null) {
								trnNumber = request.getParameter("trnNumber");
							}
							else {
								res.put("success", false);
								res.put("error", "TRN Number not specified.");
							}
							if (request.getParameterMap().containsKey("VATRegistrationDate")
									&& request.getParameter("VATRegistrationDate") != null) {
								VATRegistrationDate = sdf.parse(request.getParameter("VATRegistrationDate"));
							}
							else {
								res.put("success", false);
								res.put("error", "VAT Registration Date Type not specified.");
							}
							
						}

						if(!companyPickupBoolean) {
							
							if (!request.getParameterMap().containsKey("vehicleNumber") && request.getParameter("vehicleNumber") == null) {
								System.out.println("request.getParameter(vehicle No "+ request.getParameter("vehicleNumber"));
								res.put("success", false);
								res.put("error", "Vehicle Number not specified.");
							}
							else {
								vehicleNumber = request.getParameter("vehicleNumber");
							}
						}
						if (request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPONumber") != null) {
							LPONumber = request.getParameter("LPONumber");
						}
						if (request.getParameterMap().containsKey("LPODate")
								&& request.getParameter("LPODate") != null) {
							LPODate = sdf.parse(request.getParameter("LPODate"));
							System.out.println("LPODate: "+LPODate);
						}
						if (request.getParameterMap().containsKey("narration")
								&& request.getParameter("narration") != null) {
							narration = request.getParameter("narration");
						}

						System.out.println("customerName: " + request.getParameter("customerName"));

						Map<String, Object> result = ctrl.updateOrderRow(orderId, customerName, slabCount,
								totalMtSquare, vehicleNumber, emirate, LPONumber, LPODate, approveOrder, userId,
								phone, companyPickupBoolean, narration, hasCuttingOrders,
								VATRegistrationBoolean, VATRegistrationDate, trnNumber);

						// TODO create baskets
						long cuttingOrderId = 0L;
						if(hasCuttingOrders) {
							
							System.out.println("Cutting order is there");
							if (request.getParameterMap().containsKey("addToCuttingOrderArray[0]") && request.getParameter("addToCuttingOrderArray[0]") != null) {
								System.out.println();
								System.out.println("addToCuttingOrderArray[0] exists");
								int j = 0;
								ArrayList<Long> addToCOArrayList = new ArrayList<>();
								while (request.getParameterMap().containsKey("addToCuttingOrderArray[" + j + "]")) {
									System.out.println("addToCuttingOrderArray[" + j + "]");
									addToCOArrayList.add(Long.parseLong(request.getParameter("addToCuttingOrderArray[" + j + "]")));
									j++;
								}
								
								double inputTotalMeterSquare = 0;
								if (request.getParameterMap().containsKey("inputTotalMeterSquare") && request.getParameter("inputTotalMeterSquare") != null) {
									inputTotalMeterSquare = Double.parseDouble(request.getParameter("inputTotalMeterSquare"));
								}
								
								/*System.out.println("calling createCtngOrder function");
								cuttingOrderId = ctrl.createCuttingOrder(addToCOArrayList, (long) result.get("orderId"), inputTotalMeterSquare);*/
							}
						}
						else {
							System.out.println("cutting order is not there");
						}
						

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
						
						if(cuttingOrderId!= 0) {
							res.put("cuttingOrderId", cuttingOrderId);
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			} else if (request.getParameter("action").equalsIgnoreCase("moveOrderToInvoice")) {
				System.out.println("moveOrderToInvoiceEntered");
				System.out.println(request.getQueryString());
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
						throw new ValidationException("Order Id not specified.");
					} else {

						long orderId = Long.parseLong(request.getParameter("orderId"));

						// TODO create baskets

						Map<String, Object> result = ctrl.moveOrderToInvoice(orderId);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			} else if (request.getParameter("action").equalsIgnoreCase("deleteOrder")) {
				System.out.println("delete Order Entered");
				System.out.println(request.getQueryString());

				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
						throw new ValidationException("Order Id not specified.");
					} else {

						long orderId = Long.parseLong(request.getParameter("orderId"));

						// TODO create baskets

						Map<String, Object> result = ctrl.deleteOrder(orderId);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			} else if (request.getParameter("action").equalsIgnoreCase("deleteCartRow")) {
				System.out.println("deleteCartRow Entered");

				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("cartId") && request.getParameter("cartId") == null) {
						res.put("success", false);
						res.put("error", "Cart Id not specified.");
						throw new ValidationException("Order Id not specified.");
					} else {

						long cartId = Long.parseLong(request.getParameter("cartId"));

						// TODO create baskets

						Map<String, Object> result = ctrl.deleteCartRow(cartId);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			}

			else if (request.getParameter("action").equalsIgnoreCase("mergeOrders")) {
				System.out.println("mergeOrders Entered");

				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				System.out.println(
						"request.getParameter(mergeOrderList[0]): " + request.getParameter("mergeOrderList[0]"));
				System.out.println(
						"request.getParameter(mergeOrderList[1]): " + request.getParameter("mergeOrderList[1]"));

				try {
					if (!request.getParameterMap().containsKey("mergeOrderList[0]")
							&& request.getParameter("mergeOrderList[0]") == null) {
						res.put("success", false);
						res.put("error", "Main Order id not specified.");
						throw new ValidationException("Main Order id not specified.");
					}
					if (!request.getParameterMap().containsKey("mergeOrderList[1]")
							&& request.getParameter("mergeOrderList[1]") == null) {
						res.put("success", false);
						res.put("error", "Secondary Order id not specified.");
						throw new ValidationException("Secondary Order id not specified.");
					} else {

						int i = 0;
						long mainOrderId = 0;
						long secondaryOrderId = 0;
						Map<String, Object> result = null;

						System.out.println(request.getParameter("mergeOrderList[" + i + "]"));
						while (request.getParameterMap().containsKey("mergeOrderList[" + i + "]")) {
							System.out.println(request.getParameter("mergeOrderList[" + i + "]"));
							if (i == 0) {
								mainOrderId = Long.parseLong(request.getParameter("mergeOrderList[" + i + "]"));
							} else {
								secondaryOrderId = Long.parseLong(request.getParameter("mergeOrderList[" + i + "]"));
							}
							long inventoryId = Long.parseLong(request.getParameter("mergeOrderList[" + i + "]"));

							result = ctrl.mergeOrder(mainOrderId, secondaryOrderId);
							i++;
						}

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
						res.put("id", result.get("id"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			}
			
			else if (request.getParameter("action").equalsIgnoreCase("addCuttingOrder")) {
				System.out.println("addCuttingOrder Entered");
				System.out.println(request.getQueryString());
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("customerName")
							&& request.getParameter("customerName") == null) {
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					}
					else if (!request.getParameterMap().containsKey("count") && request.getParameter("count") == null) {
						res.put("success", false);
						res.put("error", "Count not specified.");
					}
					else if (!request.getParameterMap().containsKey("totalMtSquare")
							&& request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} 
					else if ((!request.getParameterMap().containsKey("cuttingOrder[0][id]"))
							|| request.getParameter("cuttingOrder[0][id]") == null) {
						res.put("success", false);
						res.put("error", "Cutting Order id not specified.");
					}
					else {

						String customerName = request.getParameter("customerName");
						double count = Double.parseDouble(request.getParameter("count"));
						double totalMtSquare = Double.parseDouble(request.getParameter("totalMtSquare"));
						System.out.println("customerName: " + request.getParameter("customerName"));

						Set<Cart> cartOrders = new HashSet<Cart>();

						// TODO create baskets
						int i = 0;

						while (request.getParameterMap().containsKey("cuttingOrder[" + i + "][id]")) {
							long inventoryId = Long.parseLong(request.getParameter("cuttingOrder[" + i + "][id]"));
							String type = "";
							int tileCount = 0;

							System.out.println("request.getParameter(\"cuttingOrder[" + i + "][type]\")" + request.getParameter("cuttingOrder[" + i + "][type]"));
							
							if (request.getParameterMap().containsKey("cuttingOrder[" + i + "][type]")
									&& request.getParameter("cuttingOrder[" + i + "][type]") != null
									&& !request.getParameter("cuttingOrder[" + i + "][type]").equals("")
									&& request.getParameter("cuttingOrder[" + i + "][type]").equals("TILE")) {

								type = request.getParameter("cuttingOrder[" + i + "][type]");
								tileCount = Integer.parseInt(request.getParameter("cuttingOrder[" + i + "][tileCount]"));

								System.out.println("Entered tile and adding tilecount");
								Cart cartTileSet = new Cart(inventoryId, true, tileCount);
								cartOrders.add(cartTileSet);

							} else {
								System.out.println("Entered else so not adding tile and tileCount");
								Cart cartSet = new Cart(inventoryId, false, 0);
								cartOrders.add(cartSet);
							}


							i++;
						}

						Map<String, Object> result = ctrl.addCuttingOrder(customerName, cartOrders, totalMtSquare, count);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		OrderController ctrl = new OrderController();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} else if (request.getParameter("action").equalsIgnoreCase("printDOPage")) {
				if (!request.getParameterMap().containsKey("fileName") && request.getParameter("fileName") == null) {
					res.put("success", false);
					res.put("error", "File Name not specified.");
					throw new ValidationException("fileName not specified.");
				} else {
					String fileName = request.getParameter("fileName");
					String fileUrl = "file:///C:/opt/pdfDocs/" + fileName;
					String outputPath = "C:\\Users\\User\\Desktop\\BlankDO1610005207405.pdf";
					FileUtils.copyURLToFile(new URL(fileUrl), new File(outputPath));
				}

			} else if (request.getParameter("action").equalsIgnoreCase("fetchAllOrders")) {
				Map<String, Object> result = ctrl.fetchAllOrders();

				res = new JSONObject();
				res.put("success", result.get("success"));
				res.put("data", result.get("data"));
				res.put("message", result.get("message"));
			} else if (request.getParameter("action").equalsIgnoreCase("fetchApprovedOrdersFilterByDate")) {
				
				System.out.println("Entered fetchApprovedOrders");
				if (!request.getParameterMap().containsKey("createdOn") 
						|| request.getParameter("createdOn") == null
						|| request.getParameter("createdOn").equals("")) {
					throw new ValidationException("Date not specified.");
				} else {
					
					long customerId = 0L;
					if(request.getParameterMap().containsKey("customerId") && request.getParameter("customerId") != null && !request.getParameter("customerId").equals("")) {
						customerId = Long.parseLong(request.getParameter("customerId"));
					}
					
					long materialId = 0L;
					if(request.getParameterMap().containsKey("materialId") && request.getParameter("materialId") != null && !request.getParameter("materialId").equals("")) {
						materialId = Long.parseLong(request.getParameter("materialId"));
					}
					
					boolean allOrders = false;
					boolean placed = true;
					Date createdOn = null;
					
					System.out.println("request.getParameter(\"createdOn\"): " + request.getParameter("createdOn"));
					if(request.getParameter("createdOn").toLowerCase().equals("all")) {
						allOrders = true;
					}
					else {
						createdOn = sdf.parse(request.getParameter("createdOn"));
					}
					
					System.out.println("materialId: "+ materialId);
					Map<String, Object> result = ctrl.fetchFilteredApprovedOrders(customerId, materialId, createdOn, allOrders, placed);
					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("data", result.get("data") != null ? result.get("data") : "");
					res.put("message", result.get("message") != null ? result.get("message") : "");
					res.put("error", result.get("error") != null ? result.get("error") : "");
				}

			}

			else if (request.getParameter("action").equalsIgnoreCase("getOrderDetails")) {

				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					} else {

						long orderId = Long.parseLong(request.getParameter("orderId"));

						Map<String, Object> result = ctrl.getOrderDetails(orderId);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("data", result.get("data"));
						res.put("message", result.get("message"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}
	}

}
