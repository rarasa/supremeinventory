package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.CustomerController;
import com.supreme.controller.CuttingController;
import com.supreme.controller.OrderController;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingOrderTask;
import com.supreme.dao.CuttingOrderTaskTypeDetails;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;

public class CuttingServlet extends HttpServlet {

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL = "test@fundexpert.in";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		CuttingController ctrl = new CuttingController();
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} else if (request.getParameter("action").equalsIgnoreCase("addCuttingOrders")) {
				System.out.println("update Cutting Orders Entered");
				System.out.println(request.getQueryString());
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("customerId")
							&& request.getParameter("customerId") == null) {
						res.put("success", false);
						res.put("error", "Customer Id not specified.");
					} else if (!request.getParameterMap().containsKey("customerName")
							&& request.getParameter("customerName") == null) {
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					} else if ((!request.getParameterMap().containsKey("date"))
							|| request.getParameter("date") == null) {
						res.put("success", false);
						res.put("error", "Order Date not specified.");
					} else if ((!request.getParameterMap().containsKey("deliveryDate"))
							|| request.getParameter("deliveryDate") == null) {
						res.put("success", false);
						res.put("error", "Order Delivery Date not specified.");
					} else if ((!request.getParameterMap().containsKey("materialCount"))
							|| request.getParameter("materialCount") == null) {
						res.put("success", false);
						res.put("error", "Count not specified.");
					} else if ((!request.getParameterMap().containsKey("cuttingList[0][materialId]"))
							|| request.getParameter("cuttingList[0][materialId]") == null) {
						res.put("success", false);
						res.put("error", "MaterialId not specified.");
					} else if ((!request.getParameterMap().containsKey("cuttingList[0][materialThickness]"))
							|| request.getParameter("cuttingList[0][materialThickness]") == null) {
						res.put("success", false);
						res.put("error", "materialThickness not specified.");
					} else if ((!request.getParameterMap()
							.containsKey("cuttingList[0][tasks][0][taskDetails][0][unit]"))
							|| request.getParameter("cuttingList[0][tasks][0][taskDetails][0][unit]") == null) {
						res.put("success", false);
						res.put("error", "Details not specified.");
					} else if ((!request.getParameterMap().containsKey("cuttingList[0][tasks][0][taskList][0]"))
							|| request.getParameter("cuttingList[0][tasks][0][taskList][0]") == null) {
						res.put("success", false);
						res.put("error", "Cutting Type not specified.");
					} else {

						Map<String, Object> result = new HashMap<>();
						OrderController oCtrl = new OrderController();

						long customerId = Long.parseLong(request.getParameter("customerId"));
						String customerName = request.getParameter("customerName");
						Date orderDate = sdf.parse(request.getParameter("date"));
						Date deliveryDate = sdf.parse(request.getParameter("deliveryDate"));
						System.out.println("orderDate: " + orderDate);
						int count = Integer.parseInt(request.getParameter("materialCount"));

						Double amount = (double) 0;
						if ((request.getParameterMap().containsKey("amount"))
								&& request.getParameter("amount") != null) {
							amount = Double.parseDouble(request.getParameter("amount"));
						}

						Double totalInputMtSquare = (double) 0;
						if ((request.getParameterMap().containsKey("totalInputMtSquare"))
								&& request.getParameter("totalInputMtSquare") != null) {
							totalInputMtSquare = Double.parseDouble(request.getParameter("totalInputMtSquare"));
						}

						Double totalOutputMtSquare = (double) 0;
						if ((request.getParameterMap().containsKey("totalOutputMtSquare"))
								&& request.getParameter("totalOutputMtSquare") != null) {
							totalOutputMtSquare = Double.parseDouble(request.getParameter("totalOutputMtSquare"));
						}

						Double totalOutputLinearMeter = (double) 0;
						if ((request.getParameterMap().containsKey("totalOutputLinearMeter"))
								&& request.getParameter("totalOutputLinearMeter") != null) {
							totalOutputLinearMeter = Double.parseDouble(request.getParameter("totalOutputLinearMeter"));
						}

						long orderId = (long) 0;
						if ((request.getParameterMap().containsKey("orderId"))
								&& request.getParameter("orderId") != null) {
							orderId = Long.parseLong(request.getParameter("orderId"));
						}

						Session hSession = HibernateBridge.getSessionFactory().openSession();

						long newSavedCustomerId = 0L;
						Customer customer = null;
						if (customerId != 0) {
							customer = (Customer) hSession.get(Customer.class, customerId);
						} else {
							System.out.println("Add new customer and return id");
							Map<String, Object> customerSaveRes = new HashMap<>();
							CustomerController custCtrl = new CustomerController();
							customerSaveRes = custCtrl.addNewCustomer(customerName);
							newSavedCustomerId = (long) customerSaveRes.get("customerId");

							if (newSavedCustomerId == 0) {
								throw new RuntimeException("Customer could not be saved");
							} else {
								customerId = newSavedCustomerId;
							}
						}

						if (orderId == 0) {
							//Add New Cutting Order with given customer
							orderId = ctrl.addNewEmptyOrderRow(customerId, deliveryDate, orderDate);

							if (orderId == 0) {
								throw new RuntimeException("Order could not be saved");
							}
						}

						long userId = (long) session.getAttribute("id");

						CuttingOrder cOrder = null;
						int i = 0;
						while (request.getParameterMap().containsKey("cuttingList[" + i + "][materialId]")) {
							
							System.out.println("New cutting list obj: /////////////////////////");
							
							long materialId = Long.parseLong(request.getParameter("cuttingList["+i+"][materialId]"));
							int thickness = Integer.parseInt(request.getParameter("cuttingList["+i+"][materialThickness]"));
							double totalOutputLinearMeterMaterial = Double.parseDouble(request.getParameter("cuttingList["+i+"][totalOutputLinearMeter]"));
							double totalOutputMtSquareMaterial = Double.parseDouble(request.getParameter("cuttingList["+i+"][totalOutputMtSquare]"));
							
							Material material = (Material)hSession.get(Material.class, materialId);
							
							int j = 0;
							
							System.out.println("materialId: "+ materialId);
							System.out.println("thickness: "+ thickness);
							System.out.println("totalOutputLinearMeterMaterial: "+ totalOutputLinearMeterMaterial);
							System.out.println("totalOutputMtSquareMaterial: "+ totalOutputMtSquareMaterial);
							
							Set<CuttingOrderTaskTypeDetails> cotSet = new HashSet<CuttingOrderTaskTypeDetails>();
							Set<CuttingOrderCart> coCartSet = new HashSet<CuttingOrderCart>();
							Set<CuttingOrderTask> cuttingOrderTaskSet = new HashSet<CuttingOrderTask>();
														
							
							while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][number]")){
								
								cotSet = new HashSet<CuttingOrderTaskTypeDetails>();
								coCartSet = new HashSet<CuttingOrderCart>();
								
								
								System.out.println("Cutting List obj: "+ i + "\t TaskArrayObj: "+j+"\t number: "+ request.getParameter("cuttingList["+i+"][tasks]["+j+"][number]"));
								
								int k=0;
								
								while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskList]["+k+"]")){
									
									System.out.println("k: "+ k);
									long cuttingTypeId = Long.parseLong(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskList]["+k+"]"));
									CuttingType cuttingType = (CuttingType) hSession.get(CuttingType.class, cuttingTypeId);
									
									double taskAmount = 0;
									if(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskList]["+k+"][taskAmount]") && request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskList]["+k+"][taskAmount]") != null) {
										taskAmount = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskList]["+k+"][taskAmount]"));
									}
									
									CuttingOrderTaskTypeDetails cot = new CuttingOrderTaskTypeDetails(cuttingTypeId, taskAmount, cuttingType);
									cotSet.add(cot);
									
									k++;
								}
								
								int l=0;
								while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][unit]")){
									
									/**
									 * "unit": "1",
						              "length": "202",
						              "height": "02",
						              "nos": "20",
						              "quantity": 40.4,
						              "cutToSize": 0
									 */
									
									System.out.println("l: "+ l);
									
									long unitId = Long.parseLong(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][unit]"));
									double length = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][length]"));
									double height = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][height]"));
									double nos = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][nos]"));
									double quantity = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][quantity]"));
									boolean cutToSize = request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][cutToSize]").equals("1") ? true : false;
									
									double rate = 0;
									double miscValue = 0;
									if(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][rate]") && request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][rate]") != null) {
										rate = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][rate]"));
									}
									if(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][miscValue]") && request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][miscValue]") != null) {
										miscValue = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+l+"][miscValue]"));
									}
									
									CuttingOrderCart coCart = new CuttingOrderCart(materialId, customerId, thickness, orderDate, unitId, length, 
											height, nos, quantity, rate, amount, cutToSize, miscValue);
									coCartSet.add(coCart);
									
									l++;
								}
								
								CuttingOrderTask coTask = new CuttingOrderTask(amount, materialId, material, cotSet, coCartSet);
								cuttingOrderTaskSet.add(coTask);
								
								j++;
							}
							
							
							cOrder = new CuttingOrder(orderId, materialId, 0L, totalInputMtSquare, totalOutputMtSquareMaterial, totalOutputLinearMeterMaterial, 
									0, new Date(), new Date(), cuttingOrderTaskSet);
								
							result = ctrl.addNewCuttingOrder(orderId, orderDate, cOrder, customerId, customerName, userId, deliveryDate);
							System.out.println("While: orderId: after: "+ result.get("orderId"));
							
							orderId = (long)result.get("orderId");
							
							i++;
							
							/*
							
							
							System.out.println("cuttingOrderCartSet size: "+ cuttingOrderCartSet.size());
							System.out.println("cuttingOrderTaskSet size: "+ cuttingOrderTaskSet.size());
							
							System.out.println("Material Id before cuttingorder submit: "+ materialId);
							
							System.out.println("While: orderId: before: "+ result.get("orderId"));
							cOrder = new CuttingOrder(orderId, materialId, 0L, totalInputMtSquare, totalOutputMtSquareMaterial, totalOutputLinearMeterMaterial, 
								0, new Date(), new Date(), cuttingOrderTaskSet);
							
							result = ctrl.addNewCuttingOrder(orderId, orderDate, cOrder, customerId, customerName, userId);
							System.out.println("While: orderId: after: "+ result.get("orderId"));
							
							orderId = (long)result.get("orderId");
							
							i++;
							*/
							
						}

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("orderId", result.get("orderId"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					//out.write(res.toString());
				}
			} else if (request.getParameter("action").equalsIgnoreCase("addCuttingOrdersOld")) {
				/*
				System.out.println("update Cutting Orders Entered");
				System.out.println(request.getQueryString());
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}
				
				try{
					if(!request.getParameterMap().containsKey("customerId") && request.getParameter("customerId") == null ){
						res.put("success", false);
						res.put("error", "Customer Id not specified.");
					}
					else if(!request.getParameterMap().containsKey("customerName") && request.getParameter("customerName") == null ){
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					}
					else if((!request.getParameterMap().containsKey("date")) || request.getParameter("date") == null)
					{
						res.put("success", false);
						res.put("error", "Order Date not specified.");
					}
					else if((!request.getParameterMap().containsKey("materialCount")) || request.getParameter("materialCount") == null)
					{
						res.put("success", false);
						res.put("error", "Count not specified.");
					}
					else if((!request.getParameterMap().containsKey("cuttingList[0][materialId]")) || request.getParameter("cuttingList[0][materialId]") == null)
					{
						res.put("success", false);
						res.put("error", "MaterialId not specified.");
					}
					else if((!request.getParameterMap().containsKey("cuttingList[0][materialThickness]")) || request.getParameter("cuttingList[0][materialThickness]") == null)
					{
						res.put("success", false);
						res.put("error", "materialThickness not specified.");
					}
					else if((!request.getParameterMap().containsKey("cuttingList[0][tasks][0][taskDetails][0][unit]")) || request.getParameter("cuttingList[0][tasks][0][taskDetails][0][unit]") == null)
					{
						res.put("success", false);
						res.put("error", "Details not specified.");
					}
					else if((!request.getParameterMap().containsKey("cuttingList[0][tasks][0][taskList][0]")) || request.getParameter("cuttingList[0][tasks][0][taskList][0]") == null)
					{
						res.put("success", false);
						res.put("error", "Cutting Type not specified.");
					}
					else{
						
						Map<String, Object> result = new HashMap<>();
						OrderController oCtrl = new OrderController();
						
						long customerId = Long.parseLong(request.getParameter("customerId"));
						String customerName = request.getParameter("customerName");
						Date orderDate = sdf.parse(request.getParameter("date"));
						System.out.println("orderDate: "+ orderDate);
						int count = Integer.parseInt(request.getParameter("materialCount"));
						
						Double amount = (double) 0;
						if((request.getParameterMap().containsKey("amount")) && request.getParameter("amount") != null)
						{
							amount = Double.parseDouble(request.getParameter("amount"));
						}
						Double totalInputMtSquare = (double) 0;
						if((request.getParameterMap().containsKey("totalInputMtSquare")) && request.getParameter("totalInputMtSquare") != null)
						{
							totalInputMtSquare = Double.parseDouble(request.getParameter("totalInputMtSquare"));
						}
						Double totalOutputMtSquare = (double) 0;
						if((request.getParameterMap().containsKey("totalOutputMtSquare")) && request.getParameter("totalOutputMtSquare") != null)
						{
							totalOutputMtSquare = Double.parseDouble(request.getParameter("totalOutputMtSquare"));
						}
						Double totalOutputLinearMeter = (double) 0;
						if((request.getParameterMap().containsKey("totalOutputLinearMeter")) && request.getParameter("totalOutputLinearMeter") != null)
						{
							totalOutputLinearMeter = Double.parseDouble(request.getParameter("totalOutputLinearMeter"));
						}
						long orderId = (long) 0;
						if((request.getParameterMap().containsKey("orderId")) && request.getParameter("orderId") != null)
						{
							orderId = Long.parseLong(request.getParameter("orderId"));
						}
						
						Session hSession = HibernateBridge.getSessionFactory().openSession();
						
						long newSavedCustomerId = 0L;
						Customer customer = null;
						if(customerId != 0) {
							customer = (Customer)hSession.get(Customer.class, customerId);
						}
						else {
							System.out.println("Add new customer and return id");
							Map<String, Object> customerSaveRes = new HashMap<>();
							CustomerController custCtrl = new CustomerController();
							customerSaveRes = custCtrl.addNewCustomer(customerName);
							newSavedCustomerId = (long)customerSaveRes.get("customerId");
							
							if(newSavedCustomerId == 0) {
								throw new RuntimeException("Customer could noy be saved");
							}
							else {
								customerId = newSavedCustomerId;
							}
						}
						
						long userId = (long)session.getAttribute("id");
						
						CuttingOrder cOrder = null;
						int i= 0;
						while (request.getParameterMap().containsKey("cuttingList["+i+"][materialId]")) {
							System.out.println("New Cutting List OBJ------------");
							System.out.println("i = "+ i);
							
							long materialId = Long.parseLong(request.getParameter("cuttingList["+i+"][materialId]"));
							int thickness = Integer.parseInt(request.getParameter("cuttingList["+i+"][materialThickness]"));
							
							double totalOutputLinearMeterMaterial = Double.parseDouble(request.getParameter("cuttingList["+i+"][totalOutputLinearMeter]"));
							double totalOutputMtSquareMaterial = Double.parseDouble(request.getParameter("cuttingList["+i+"][totalOutputMtSquare]"));
							
							int j = 0;
							Set<CuttingOrderCart> cuttingOrderCartSet = new HashSet<CuttingOrderCart>();
							Set<CuttingOrderTask> cuttingOrderTaskSet = new HashSet<CuttingOrderTask>();
							while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][number]")){
				//								System.out.println("jjjjj = "+ j);
				//								
								System.out.println("cuttingList["+i+"][tasks]["+j+"][number]: "+ request.getParameter("cuttingList["+i+"][tasks]["+j+"][number]"));
								
								
								int k=0;
								while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][unit]")){
									long unit = 0L;
									double height, length, nos, qty;
									
									boolean cutToSize = false;
									if(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][cutToSize]")) {
										cutToSize = request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][cutToSize]").equals("1") ? true : false;
									}
									
									unit = Long.parseLong(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][unit]"));
									nos = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][nos]"));
									qty = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][quantity]"));
									length = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][length]"));
									height = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskDetails]["+k+"][height]"));
									
				//									System.out.println("BEFORE cSET-----------------");
				//									System.out.println("unit: "+ unit+ "\t nos: "+ nos+"\t qty: "+ qty+" \t length"+ length+"\t height: "+ height+ "\t cutToSize: "+ cutToSize);
									
									double miscValue = 0;
									
									CuttingOrderCart cSet = new CuttingOrderCart(materialId, customerId, thickness, new Date(), unit, length, height, nos, qty, 0,0, cutToSize, miscValue);
									cuttingOrderCartSet.add(cSet);
									k++;
								}
								
								int x=0;
								while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskList]["+x+"]")){
									System.out.println("jjjjj = "+ j);
									System.out.println("tasks exists");
									while(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskList]["+x+"]")){
										long cuttingTypeId = 0L;
										double taskAmount = 0;
										
										if(request.getParameterMap().containsKey("cuttingList["+i+"][tasks]["+j+"][taskList]["+x+"]")) {
											taskAmount = Double.parseDouble(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskList]["+x+"]"));
										}
										
										cuttingTypeId = Long.parseLong(request.getParameter("cuttingList["+i+"][tasks]["+j+"][taskList]["+x+"]"));
										
										System.out.println("BEFORE cSET-----------------");
										System.out.println("cuttingTypeId: "+ cuttingTypeId);
										System.out.println("taskAmount: "+ taskAmount);
										
										Material material = (Material) hSession.get(Material.class, materialId);
				//										CuttingOrderTask cSet = new CuttingOrderTask(amount, materialId, material);
				//										cuttingOrderTaskSet.add(cSet);
										x++;
									}
									
								}
								
								System.out.println("ONE ITERATION OF J DONE////////////////////////////");
								j++;
							}
							
							
							System.out.println("cuttingOrderCartSet size: "+ cuttingOrderCartSet.size());
							System.out.println("cuttingOrderTaskSet size: "+ cuttingOrderTaskSet.size());
							
							System.out.println("While: orderId: before: "+ result.get("orderId"));
							cOrder = new CuttingOrder(orderId, materialId, 0L, totalInputMtSquare, totalOutputMtSquareMaterial, totalOutputLinearMeterMaterial, 
									0, new Date(), new Date(), cuttingOrderCartSet, cuttingOrderTaskSet);
							
							result = ctrl.addNewCuttingOrder(orderId, orderDate, cOrder, customerId, customerName, userId);
							System.out.println("While: orderId: after: "+ result.get("orderId"));
							
							orderId = (long)result.get("orderId");
							
							i++;
						}
						//Create cutting order
						
						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("orderId", result.get("orderId"));
						
					}
				}
				catch(Exception e){
					e.printStackTrace();	
				}
				finally{
					//out.write(res.toString());
				}
				*/} else if (request.getParameter("action").equalsIgnoreCase("addScannedSlabsToCuttingOrder")) {
				System.out.println("update Cutting Orders Entered");
				System.out.println(request.getQueryString());
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					if (!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					} else if (!request.getParameterMap().containsKey("slabCount")
							&& request.getParameter("slabCount") == null) {
						res.put("success", false);
						res.put("error", "Slab Count not specified.");
					} else if ((!request.getParameterMap().containsKey("totalMtSquare"))
							|| request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Meter Sq. not specified.");
					} else if ((!request.getParameterMap().containsKey("cart[0][inventory][id]"))
							|| request.getParameter("cart[0][inventory][id]") == null) {
						res.put("success", false);
						res.put("error", "Slabs not scanned not specified.");
					} else if ((!request.getParameterMap().containsKey("cart[0][inventory][type]"))
							|| request.getParameter("cart[0][inventory][type]") == null) {
						res.put("success", false);
						res.put("error", "Slab Type not specified.");
					} else if ((request.getParameterMap().containsKey("cart[0][inventory][type]"))
							&& request.getParameter("cart[0][inventory][type]") != null
							&& request.getParameter("cart[0][inventory][type]").equals("TILE")
							&& (!request.getParameterMap().containsKey("cart[0][inventory][tileCount]")
									&& request.getParameter("cart[0][inventory][tileCount]") == null)) {
						res.put("success", false);
						res.put("error", "Tile Count not specified.");
					} else {
						Map<String, Object> result = null;

						long orderId = Long.parseLong(request.getParameter("orderId"));
						int slabCount = Integer.parseInt(request.getParameter("slabCount"));
						double totalMtSquare = Double.parseDouble(request.getParameter("totalMtSquare"));

						Set<Cart> cartSet = new HashSet<Cart>();

						int i = 0;
						while (request.getParameterMap().containsKey("cart[" + i + "][id]")) {

							long cartId = Long.parseLong(request.getParameter("cart[" + i + "][inventory][id]"));
							long inventoryId = Long.parseLong(request.getParameter("cart[" + i + "][inventory][id]"));
							String type = request.getParameter("cart[" + i + "][inventory][type]");
							int tileCount = 0;
							boolean tile = false;
							if (type.equals("TILE")) {
								tile = true;
								tileCount = Integer.parseInt(request.getParameter("cart[" + i + "][inventory][tileCount]"));
							}

							Cart cart = new Cart(inventoryId, tile, tileCount);
							cartSet.add(cart);

							i++;
						}

						result = ctrl.addScannedSlabsToCuttingOrder(orderId, cartSet, totalMtSquare, slabCount);

						res.put("success", result.get("success"));
						res.put("message", result.get("message") != null ? result.get("message") : "");
						res.put("data", result.get("data") != null ? result.get("data") : "");

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					//out.write(res.toString());
				}
			}

			else if (request.getParameter("action").equalsIgnoreCase("updateSingleCuttingType")) {

				System.out.println("updateSingleCuttingType action called");

				if (!request.getParameterMap().containsKey("name") || request.getParameter("name").equals("")) {
					res.put("success", false);
					res.put("error", "Customer Name not specified.");
				} else if (!request.getParameterMap().containsKey("cuttingMode")
						|| request.getParameter("cuttingMode").equals("")) {
					res.put("success", false);
					res.put("error", "Cutting Mode not specified.");
				} else {
					String name = request.getParameter("name");
					String cuttingMode = request.getParameter("cuttingMode");

					Map<String, Object> result = null;

					result = ctrl.addSingleCuttingType(name, cuttingMode);

					res = new JSONObject();
					res.put("success", result.get("success"));
					res.put("message", result.get("message"));
				}

			} else if (request.getParameter("action").equalsIgnoreCase("editExistingCuttingOrder")) {
				System.out.println("editExistingPurchaseOrder entered");

				long userId = (long) session.getAttribute("id");
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				} else {
					userId = Long.parseLong(session.getAttribute("id").toString());
				}

				try {
					if (request.getParameterMap().containsKey("LPO Number")
							&& request.getParameter("LPONumber") != null) {
						if (!request.getParameterMap().containsKey("LPODate")
								&& request.getParameter("LPODate") == null) {
							res.put("success", false);
							res.put("error", "LPO Date not specified.");
						}
					}
					if (request.getParameterMap().containsKey("LPODate") && request.getParameter("LPODate") != null) {
						if (!request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPONumber") == null) {
							res.put("success", false);
							res.put("error", "LPO Number not specified.");
						}
					}
					if (!request.getParameterMap().containsKey("customerName")
							&& request.getParameter("customerName") == null) {
						res.put("success", false);
						res.put("error", "Customer Name not specified.");
					} else if (!request.getParameterMap().containsKey("id") && request.getParameter("id") == null) {
						res.put("success", false);
						res.put("error", "Order Id not specified.");
					} else if (!request.getParameterMap().containsKey("slabCount")
							&& request.getParameter("slabCount") == null) {
						res.put("success", false);
						res.put("error", "Count not specified.");
					} else if (!request.getParameterMap().containsKey("totalMtSquare")
							&& request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} else if (!request.getParameterMap().containsKey("approveOrder")
							&& request.getParameter("approveOrder") == null) {
						res.put("success", false);
						res.put("error", "Total Meter Square not specified.");
					} else if (!request.getParameterMap().containsKey("emirate")
							&& request.getParameter("emirate") == null) {
						res.put("success", false);
						res.put("error", "emirate not specified.");
					} else if (!request.getParameterMap().containsKey("totalMtSquare")
							&& request.getParameter("totalMtSquare") == null) {
						res.put("success", false);
						res.put("error", "Meter Square not specified.");
					}

					else if (!request.getParameterMap().containsKey("phone") && request.getParameter("phone") == null) {
						res.put("success", false);
						res.put("error", "Phone not specified.");
					} else if (!request.getParameterMap().containsKey("VATRegistration")
							&& request.getParameter("VATRegistration") == null) {
						res.put("success", false);
						res.put("error", "VATRegistration not specified.");
					} else if (!request.getParameterMap().containsKey("hasCuttingSlabs")
							&& request.getParameter("hasCuttingSlabs") == null) {
						res.put("success", false);
						res.put("error", "Has Cutting Orders not specified.");
					} 
					
					
					 else {
						
						boolean skipScan = false, outsideMaterial = false;
						
						if ((request.getParameterMap().containsKey("skipScan") && request.getParameter("skipScan") != null && !request.getParameter("skipScan").equals(""))) {
							skipScan = request.getParameter("skipScan").equals("1") ? true : false;
						}
						
						if((request.getParameterMap().containsKey("outsideMaterial") && request.getParameter("outsideMaterial") != null && !request.getParameter("outsideMaterial").equals(""))) {
							outsideMaterial = request.getParameter("outsideMaterial").equals("1") ? true : false;
						}
						
						
						if(!skipScan && !outsideMaterial) {
							
							if ((!request.getParameterMap().containsKey("cart[0][id]")) || request.getParameter("cart[0][id]") == null) {
								res.put("success", false);
								res.put("error", "Please Scan Slabs to order OR check Skip Scan checkbox.");
							} 
							
							else if ((!request.getParameterMap().containsKey("cart[0][inventory][id]")) || request.getParameter("cart[0][inventory][id]") == null) {
								res.put("success", false);
								res.put("error", "Cart Inventory Id not specified.");
							}
							else if ((!request.getParameterMap().containsKey("cart[0][inventory][type]")) || request.getParameter("cart[0][inventory][type]") == null) {
								res.put("success", false);
								res.put("error", "Slab Type not specified.");
							} else if ((request.getParameterMap().containsKey("cart[0][inventory][type]") 
									&& request.getParameter("cart[0][inventory][type]").equals("TILE"))
									&& (!request.getParameterMap().containsKey("cart[0][inventory][tileCount]")
											|| request.getParameter("cart[0][inventory][tileCount]") == null)) {
								
								res.put("success", false);
								res.put("error", "Tile Count not specified.");
							
							}
							
							int i = 0;
							while (request.getParameterMap().containsKey("cart[" + i + "][id]")) {
								int tileCount = 0;
								String type = "";
								
								System.out.println("while i: " + i);
								long cartId = Long.parseLong(request.getParameter("cart[" + i + "][id]"));
								System.out.println("servlet cartId: " + cartId);
								long inventoryId = Long.parseLong(request.getParameter("cart[" + i + "][inventory][id]"));
								double actualLength = Double
										.parseDouble(request.getParameter("cart[" + i + "][inventory][actualLength]"));
								double actualHeight = Double
										.parseDouble(request.getParameter("cart[" + i + "][inventory][actualHeight]"));
								double actualSqMetre = Double
										.parseDouble(request.getParameter("cart[" + i + "][inventory][actualSqMetre]"));
								long thickness = Long.parseLong(request.getParameter("cart[" + i + "][inventory][thickness]"));
								type = request.getParameter("cart[" + i + "][inventory][type]");
								
								/*
								 * if(type.equals("TILE")) { tileCount =
								 * Integer.parseInt(request.getParameter("cart[" + i + "][tileCount]")); }
								 */
								
								ctrl.updateOrderCartRow(cartId, inventoryId, actualHeight, actualLength, thickness,
										actualSqMetre, type);
								
								i++;
							}
						}


						String phone = "", vehicleNumber = "";
						long orderId = Long.parseLong(request.getParameter("id"));
						String customerName = request.getParameter("customerName");
						double slabCount = Double.parseDouble(request.getParameter("slabCount"));
						double totalMtSquare = Double.parseDouble(request.getParameter("totalMtSquare"));
						boolean approveOrder = (request.getParameter("approveOrder").equals("true")) ? true : false;
						String emirate = request.getParameter("emirate");

						String LPONumber = "", narration = "";
						Date LPODate = null;
						boolean companyPickupBoolean = request.getParameter("companyPickup").equals("0") ? false : true;
						boolean hasCuttingSlabs = request.getParameter("hasCuttingSlabs").equals("0") ? false : true;
						phone = request.getParameter("phone");

						Date VATRegistrationDate = null;
						String trnNumber = null;
						boolean VATRegistrationBoolean = request.getParameter("VATRegistration").equals("0") ? false
								: true;
						if (VATRegistrationBoolean) {

							if (request.getParameterMap().containsKey("trnNumber")
									&& request.getParameter("trnNumber") != null) {
								trnNumber = request.getParameter("trnNumber");
							} else {
								res.put("success", false);
								res.put("error", "TRN Number not specified.");
							}
							if (request.getParameterMap().containsKey("VATRegistrationDate")
									&& request.getParameter("VATRegistrationDate") != null) {
								VATRegistrationDate = sdf.parse(request.getParameter("VATRegistrationDate"));
							} else {
								res.put("success", false);
								res.put("error", "VAT Registration Date Type not specified.");
							}

						}

						if (!companyPickupBoolean) {

							if (!request.getParameterMap().containsKey("vehicleNumber")
									&& request.getParameter("vehicleNumber") == null) {
								System.out.println(
										"request.getParameter(vehicle No " + request.getParameter("vehicleNumber"));
								res.put("success", false);
								res.put("error", "Vehicle Number not specified.");
							} else {
								vehicleNumber = request.getParameter("vehicleNumber");
							}
						}
						if (request.getParameterMap().containsKey("LPONumber")
								&& request.getParameter("LPONumber") != null) {
							LPONumber = request.getParameter("LPONumber");
						}
						if (request.getParameterMap().containsKey("LPODate")
								&& request.getParameter("LPODate") != null) {
							LPODate = sdf.parse(request.getParameter("LPODate"));
							System.out.println("LPODate: " + LPODate);
						}
						if (request.getParameterMap().containsKey("narration")
								&& request.getParameter("narration") != null) {
							narration = request.getParameter("narration");
						}

						System.out.println("customerName: " + request.getParameter("customerName"));

						Map<String, Object> result = ctrl.updateOrderRow(orderId, customerName, slabCount,
								totalMtSquare, vehicleNumber, emirate, LPONumber, LPODate, approveOrder, userId, phone,
								companyPickupBoolean, narration, hasCuttingSlabs, VATRegistrationBoolean,
								VATRegistrationDate, trnNumber, outsideMaterial, skipScan);

						res = new JSONObject();
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));

					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					// out.write(res.toString());
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		CuttingController ctrl = new CuttingController();

		try {
			if (!request.getParameterMap().containsKey("action")) {
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			} else if (request.getParameter("action").equalsIgnoreCase("getAllCuttingList")) {
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllCuttingList();

				res.put("success", true);
				res.put("data", result.get("data"));
			} else if (request.getParameter("action").equalsIgnoreCase("getAllCuttingTypeList")) {
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				Map<String, Object> result = null;

				result = ctrl.getAllCuttingTypeList();

				res.put("success", true);
				res.put("data", result.get("data"));
			} else if (request.getParameter("action").equalsIgnoreCase("getCuttingOrderDetails")) {
				System.out.println("getCuttingOrderDetails called");

				try {
					if (!request.getParameterMap().containsKey("cuttingOrderId")
							&& request.getParameter("cuttingOrderId") == null) {
						res.put("success", false);
						res.put("error", "Cutting Order Id not specified.");
					} else {

						System.out.println(request.getParameter("cuttingOrderId"));

						long cuttingOrderId = Long.parseLong(request.getParameter("cuttingOrderId"));
						Map<String, Object> result = null;

						result = ctrl.fetchCuttingOrder(cuttingOrderId);

						res.put("success", result.get("success"));
						res.put("data", result.get("data"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {

				}

			} else if (request.getParameter("action").equalsIgnoreCase("fetchCuttingOrderWithOrderId")) {
				System.out.println("fetchCuttingOrderWithOrderId called");

				try {
					if (!request.getParameterMap().containsKey("orderId") && request.getParameter("orderId") == null) {
						res.put("success", false);
						res.put("error", "Cutting Order Id not specified.");
					} else {

						System.out.println(request.getParameter("cuttingOrderId"));

						long orderId = Long.parseLong(request.getParameter("orderId"));
						Map<String, Object> result = null;

						result = ctrl.fetchCuttingOrderWithOrderId(orderId);

						res.put("success", result.get("success"));
						res.put("data", result.get("data"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {

				}

			} else if (request.getParameter("action").equalsIgnoreCase("fetchAllCuttingOrders")) {
				System.out.println("fetchAllCuttingOrders called");
				if (session.getAttribute("id") == null) {
					throw new ValidationException("You are not logged in.");
				}

				try {
					Map<String, Object> result = null;

					result = ctrl.fetchAllCuttingOrders();

					res.put("success", result.get("success"));
					res.put("data", result.get("data"));
				} catch (Exception e) {
					e.printStackTrace();
				} finally {

				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			out.write(res.toString());
		}
	}

}
