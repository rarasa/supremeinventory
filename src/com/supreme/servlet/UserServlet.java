package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;  
 
/*import com.Germinate.connect.sms.IsipMandateSMS;*/








import com.supreme.Config;
import com.supreme.controller.UserController;
import com.supreme.dao.User;
import com.supreme.util.JSON;
import com.supreme.util.Verify;

public class UserServlet extends HttpServlet
{

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-06-20
	 */
	private static final long serialVersionUID = 1L;
	private static final String TEST_EMAIL="test@fundexpert.in";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		UserController ctrl = new UserController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			
			if(!request.getParameterMap().containsKey("action"))
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
			}
			else if(request.getParameter("action").equalsIgnoreCase("login")){

				System.out.println("Login action called");
				
				if((!request.getParameterMap().containsKey("email")) || request.getParameter("password") == null)
				{
					res.put("success", false);
					res.put("error", "Email not specified.");
				}
				else if(request.getParameterMap().containsKey("password") && !Verify.passwordValidate(request.getParameter("password")))
				{
					res.put("success", false);
					res.put("error", "Your password must be minimum 8 characters long.");
				}
				else
				{
					String email = request.getParameterMap().containsKey("email") ? request.getParameter("email") : "";
					String password = request.getParameter("password");
					
					User user ;

					if(!email.equals("") && !Verify.emailValidate(email))
					{
						throw new ValidationException("Please enter a valid email address.");
					}	
					else
					{
						user = ctrl.login(email, password);
					
						// Check if User is kyc registered or not
						user = ctrl.get(user.getId());
						
						System.out.println("User ID: "+ user.getId());
						
						session.setAttribute("user", user);
						session.setAttribute("id", user.getId());
						session.setAttribute("role", user.getRole());
	
						// If login is coming from risk profile page
	
						try
						{
							res = JSON.toJSONObject(user);
						}
						catch (Exception e)
						{
							e.printStackTrace();
						}
						res.put("success", true);
					}
				}
			
			}
			else if(request.getParameter("action").equalsIgnoreCase("deleteUser")){
				if((!request.getParameterMap().containsKey("name")) || request.getParameter("name") == null)
				{
					res.put("success", false);
					res.put("error", "Name not specified.");
				}
				else if((!request.getParameterMap().containsKey("id")) || request.getParameter("id") == null)
				{
					res.put("success", false);
					res.put("error", "id not specified.");
				}
				else {
					String name = request.getParameter("name");
					long id = Long.parseLong(request.getParameter("id"));
					
					Map<String, Object> result = ctrl.deleteUser(name, id);
					res.put("success", result.get("success"));
					res.put("success", result.get("message"));
					
				}
				
			}
			else if(request.getParameter("action").equalsIgnoreCase("addOrEditNewUser")){
				
				System.out.println("addOrEditNewUser action called");
				
				if((!request.getParameterMap().containsKey("name")) || request.getParameter("name") == null)
				{
					res.put("success", false);
					res.put("error", "Name not specified.");
				}
				else if((!request.getParameterMap().containsKey("email")) || request.getParameter("email") == null)
				{
					res.put("success", false);
					res.put("error", "Email not specified.");
				}
				else if((!request.getParameterMap().containsKey("password")) || request.getParameter("password") == null)
				{
					res.put("success", false);
					res.put("error", "Password not specified.");
				}
				else if(request.getParameterMap().containsKey("password") && !Verify.passwordValidate(request.getParameter("password")))
				{
					res.put("success", false);
					res.put("error", "Your password must be minimum 8 characters long.");
				}
				else if((!request.getParameterMap().containsKey("gender")) || request.getParameter("gender") == null)
				{
					res.put("success", false);
					res.put("error", "gender not specified.");
				}
				else if((!request.getParameterMap().containsKey("phone")) || request.getParameter("phone") == null)
				{
					res.put("success", false);
					res.put("error", "mobileNumber not specified.");
				}
				else if((!request.getParameterMap().containsKey("addressLine1")) || request.getParameter("addressLine1") == null)
				{
					res.put("success", false);
					res.put("error", "addressLine1 not specified.");
				}
				else if((!request.getParameterMap().containsKey("addressLine2")) || request.getParameter("addressLine2") == null)
				{
					res.put("success", false);
					res.put("error", "addressLine2 not specified.");
				}
				else if((!request.getParameterMap().containsKey("emiratesId")) || request.getParameter("emiratesId") == null)
				{
					res.put("success", false);
					res.put("error", "emiratesId not specified.");
				}
				else if((!request.getParameterMap().containsKey("city")) || request.getParameter("city") == null)
				{
					res.put("success", false);
					res.put("error", "City not specified.");
				}
				else if((!request.getParameterMap().containsKey("role")) || request.getParameter("role") == null)
				{
					res.put("success", false);
					res.put("error", "Role not specified.");
				}
				else if((!request.getParameterMap().containsKey("editUser")) || request.getParameter("editUser") == null)
				{
					res.put("success", false);
					res.put("error", "editUser not specified.");
				}
				else
				{
					
					String email = request.getParameter("email");
					if(!email.equals("") && !Verify.emailValidate(email))
					{
						throw new ValidationException("Please enter a valid email address.");
					}	
					else
					{
						String name = request.getParameter("name");
						String password = request.getParameter("password");
						String mobileNumber = request.getParameter("phone");
						String addressLine1 = request.getParameter("addressLine1");
						String addressLine2 = !request.getParameter("addressLine2").equals("") ? request.getParameter("addressLine2") : "";
						String emiratesId = !request.getParameter("emiratesId").equals("") ? request.getParameter("emiratesId") : "";
						String city = request.getParameter("city");
						int gender = Integer.parseInt(request.getParameter("gender"));
						long role = Long.parseLong(request.getParameter("role"));
						
						boolean editUser = false;
						if(request.getParameterMap().containsKey("editUser") && !request.getParameter("editUser").equals(""))
						{
							editUser = request.getParameter("editUser").equals("true") ? true : false;
						}
						
						System.out.println("editUser: "+ editUser); 
						
						Date dob = null;

						Map<String, Object> result = ctrl.addOrEditNewUser(name, email, password, addressLine1, addressLine2, gender, emiratesId, dob, mobileNumber, city, role, editUser);
						
						res.put("success", result.get("success"));
						res.put("message", result.get("message"));
					}
				}
				
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (ValidationException e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		finally
		{
			out.write(res.toString());
		}
	}
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		JSONObject res = new JSONObject();
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();
		UserController ctrl = new UserController();
		
		try
		{
			if(!request.getParameterMap().containsKey("action")) 
			{
				res.put("success", false);
				res.put("error", "Action not specified.");
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("logout"))
			{
				User user = (User) session.getAttribute("user");
				session.invalidate();
				response.setStatus(response.SC_MOVED_TEMPORARILY);
				try
				{
					response.setHeader("Location", "../");
				}
				catch (Exception e)
				{
					response.setHeader("Location", "../");
				}
				
			}
			else if(request.getParameter("action").equalsIgnoreCase("getAllUserList"))
			{
				Map<String, Object> result = ctrl.getAllUserList();
				
				res.put("success", result.get("success"));
				res.put("data", result.get("data"));
				
				out.write(res.toString());
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{

		}
	}
	
}
