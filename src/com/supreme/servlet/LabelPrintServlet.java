/**
 * 
 */
package com.supreme.servlet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.supreme.controller.InventoryController;
import com.supreme.dao.Inventory;
import com.supreme.util.DOStatementPdf;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;
import com.supreme.util.LabelPrintPdf;
import com.supreme.util.PageEventToAddRectangle;
import com.supreme.util.PageEventToAddRectangleinLabel;

/**
 * @author Rashi
 *
 */
public class LabelPrintServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		InventoryController inv = new InventoryController();
		HttpSession session=request.getSession();
		JSONObject res=new JSONObject();
		PrintWriter writer = null;
		/*PrintWriter out = response.getWriter();*/

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				throw new ValidationException("Action not passed");
			}
			
			else if(request.getParameter("action").trim().equalsIgnoreCase("printLablesInContainer"))
			{
				
				if(!request.getParameterMap().containsKey("containerId") && request.getParameter("containerId") == null ){
					res.put("success", false);
					res.put("error", "Container Id not specified.");
				}
				else if(!request.getParameterMap().containsKey("containerCode") && request.getParameter("containerCode") == null ){
					res.put("success", false);
					res.put("error", "Container Code not specified.");
				}
				else{
					long containerId = Long.parseLong(request.getParameter("containerId"));
					String containerCode = request.getParameter("containerCode");
					System.out.println("downloadDO else entered");
					Rectangle Label = new Rectangle(282, 36);
					HttpSession session1 = request.getSession();
					long userId=0;
					String emailId=null;
					Date todayDate= new Date();
					SimpleDateFormat sdf2=new SimpleDateFormat("dd/MM/yyyy");
					String todate=sdf2.format(todayDate);
					
					Document document = new Document();
					OutputStream os = null;
					ByteArrayOutputStream baos=null;
					try
					{
		                document.setPageSize(Label);
		                baos = new ByteArrayOutputStream();
						PdfWriter writer1 = PdfWriter.getInstance(document, baos);
						PageEventToAddRectangleinLabel pageEventToAddRectangle=new PageEventToAddRectangleinLabel();
						writer1.setPageEvent(pageEventToAddRectangle);
						document.open();
					     
		 			    LabelPrintPdf accountStatementPdf=new LabelPrintPdf();
						accountStatementPdf.createPdf(containerId, document);
								
						 
						document.close();
					    response.setContentType("application/pdf");
						response.addHeader("Content-Disposition", "attachment; filename="+containerCode+".pdf");		
						response.setContentLength((int)baos.size());
						os = response.getOutputStream();
						
						baos.writeTo(os);
						System.out.println("os is"+os);
						
					}
					catch(Exception e)
					{
						e.printStackTrace();
						throw e;
					}
					finally
					{
						os.flush();
						os.close();
						baos.close();
					}
				}
				
			}
			
		}
		catch (ValidationException e)
		{
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());

			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		
	}
	
}
