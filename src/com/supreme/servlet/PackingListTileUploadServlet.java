/**
 * 
 */
package com.supreme.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.supreme.Config;
import com.supreme.controller.InventoryController;
import com.supreme.controller.PackingListUploadController;
import com.supreme.controller.UserController;
import com.supreme.dao.User;
import com.supreme.util.JSON;
import com.supreme.util.ReadPackingList;
import com.supreme.util.Verify;

/**
 * @author Rashi
 *
 */
public class PackingListTileUploadServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		JSONObject innerJson = null;
		JSONArray jsonArray = new JSONArray();
		JSONObject responseJson = new JSONObject();
		PrintWriter writer = response.getWriter();
		PackingListUploadController ctrl = new PackingListUploadController();
		HttpSession session = request.getSession();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long userId = -1;

		Config config = new Config();
		String path = "/opt/supremeinventory/packingList";
		if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV))
		{
			path = config.get(Config.PACKINGLIST_DEV_PATH);
			System.out.println("dev path="+path);
		}
		else
		{
			path = config.get(Config.PACKINGLIST_PROD_PATH);
			System.out.println("prod path="+path);
		}
		/*path = "C:\\opt\\supremeinventory\\";*/
		File excel = null;
		String completePath = "";
		
		try
		{
			System.out.println("supplierId: "+request.getParameter("supplierId"));
			if((!request.getParameterMap().containsKey("supplierId")) || request.getParameter("supplierId") == null)
			{
				responseJson.put("success", false);
				responseJson.put("message", "Supplier Name not specified.");
			}
			else if((!request.getParameterMap().containsKey("arrivalDate")) || request.getParameter("arrivalDate") == null)
			{
				responseJson.put("success", false);
				responseJson.put("message", "Arrival Date not specified.");
			}
			
			else if((!request.getParameterMap().containsKey("yard")) || request.getParameter("yard") == null)
			{
				responseJson.put("success", false);
				responseJson.put("message", "Yard not specified.");
			}
			else if((!request.getParameterMap().containsKey("containerCode")) || request.getParameter("containerCode") == null)
			{
				responseJson.put("success", false);
				responseJson.put("message", "Container Number not specified.");
			}
			
			else if((!request.getParameterMap().containsKey("oldStock")) || request.getParameter("oldStock") == null)
			{
				responseJson.put("success", false);
				responseJson.put("message", "Old Stock not specified.");
			}
			else{
				
				/**
				 * All data available in query string.
				 * Picking values now
				 */
				String yard = request.getParameter("yard");
				String containerCode = request.getParameter("containerCode");
				long supplierId = Long.parseLong(request.getParameter("supplierId"));
				boolean oldStock = request.getParameter("oldStock").equals("0") ? false : true;
				Date arrivalDate = sdf.parse(request.getParameter("arrivalDate"));
				System.out.println("arrivalDate: "+ arrivalDate);
				
				System.out.println("entered else condition");
				
				// TODO Auto-generated method stub

				String fileName = null;
				boolean isMultiPart = false;
				isMultiPart = ServletFileUpload.isMultipartContent(request);
				// userId contains the current loggedIn user on Germinate
				if(!new File(path).exists())
				{
					System.out.println("file path exists");
					File f = new File(path);
					f.mkdir();
					System.out.println("Absolute Path of the file : " + f.getAbsolutePath());
				}
				else{
					System.out.println("file path exists exists");
				}
				if(isMultiPart == true)
				{	
					DiskFileItemFactory diskFactory = new DiskFileItemFactory();
					ServletFileUpload sfu = new ServletFileUpload(diskFactory);
					List list = sfu.parseRequest(request);
					Iterator iterator = list.iterator();
					File packingListExcel;
					
					while (iterator.hasNext())
					{
						System.out.print("In");
						FileItem fileItem = (FileItem) iterator.next();
						if(!fileItem.isFormField())
						{
							System.out.println(path);
							System.out.println(fileItem.getName());
							path=path+"/"+new Date().getTime()+fileItem.getName();
							final String finalFileName = new Date().getTime()+fileItem.getName();
							
							try
							{
								packingListExcel = new File(path);
								fileItem.write(packingListExcel);
								System.out.println("path=======================");
								
								System.out.println(finalFileName);
								System.out.println(supplierId);
								
								long containerId = ctrl.getContainerId(containerCode);
								
								/*
								 * boolean result = ctrl.ReadTilePackingList(finalFileName, supplierId,
								 * arrivalDate, yard, oldStock, containerId);
								 */
								
								/*
								 * responseJson.put("success", result);
								 * 
								 * if(result) {
								 * responseJson.put("message","Successfully updated list.");
								 * }
								 * else {
								 * responseJson.put("message","Could not update list.");
								 * }
								 */
								writer.write(responseJson.toString());
							}
							catch (Exception e)
							{
								e.printStackTrace();
								responseJson.put("success",false);
								responseJson.put("message","Not able to save file");
								writer.write(responseJson.toString());
							}
			
						}
					}			
				}
				else
				{
					responseJson.put("success",false);
					responseJson.put("message","No File Selected");
					writer.write(responseJson.toString());
				}
			}
	
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			try
			{
				responseJson.put("success", false);
				responseJson.put("message", e.getMessage());
				writer.write(responseJson.toString());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				responseJson.put("success", false);
				responseJson.put("message", e.getMessage());
				writer.write(responseJson.toString(3));
			}
			catch (JSONException ex)
			{
				ex.printStackTrace();
			}
		}

	
		
	}
}
