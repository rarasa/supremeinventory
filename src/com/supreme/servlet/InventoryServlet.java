
/**
 * 
 */
package com.supreme.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.ValidationException;

import org.hibernate.Query;
import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.controller.InventoryController;
import com.supreme.dao.Inventory;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

/**
 * @author Rashi
 *
 */
public class InventoryServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		InventoryController inv = new InventoryController();
		HttpSession session=request.getSession();
		JSONObject res=new JSONObject();
		PrintWriter writer = response.getWriter();
		PrintWriter out = response.getWriter();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				throw new ValidationException("Action not passed");
			}
			
			
			
			/*else if(request.getParameter("action").trim().equalsIgnoreCase("updateSingleInventory"))
			{
				System.out.println("updateSingleInventory entered");
					String supplierCode = "GLB_GRN", materialCode = "BLK_ABS", type = "GS", barcodeString = "00000/////sssss";
					Long slabNo = 0L, thickness = 2L;
					Date arrivalDate = sdf.parse("2020-05-05");
					double length = 100, height = 100, actualLength = 100, actualHeight = 100, stepLength = 0, riserLength = 0, squareMetre = 10000, runningMetre = 100;
					boolean polished = true, flamed = true, steps = false, risers = false;
					
					inv.singleInventoryUpdate(supplierCode, materialCode, arrivalDate, thickness, flamed, 
							polished, steps, risers, stepLength, riserLength, 
							type, squareMetre, runningMetre, slabNo, length, height, 
							actualLength, actualHeight, barcodeString);
					
					
					
			}*/
			else if(request.getParameter("action").trim().equalsIgnoreCase("getFirstTwentyInventoryList"))
			{
				
				Map<String, Object> result = null;
				result = inv.getFirstTwentyInventoryList();
				
				//out.write(result.get("data").toString());
				
				res.put("success", true);
				res.put("data", result.get("data"));
				
			}
			else if(request.getParameter("action").trim().equalsIgnoreCase("getContainerWiseInventoryList"))
			{
				
				if(!request.getParameterMap().containsKey("dateLowerRange") || request.getParameter("dateLowerRange").equals("")){
					throw new ValidationException("Enter lower date");
				}
				else if(!request.getParameterMap().containsKey("dateHigherRange") || request.getParameter("dateHigherRange").equals("")){
					throw new ValidationException("Enter higher date");
				}
				else {
					
					System.out.println("request.getParameter(\"dateLowerRange\"): "+ request.getParameter("dateLowerRange"));
					System.out.println("request.getParameter(\"dateHigherRange\"): "+ request.getParameter("dateHigherRange"));
					Date lowerDate = sdf.parse(request.getParameter("dateLowerRange"));
					Date higherDate = sdf.parse(request.getParameter("dateHigherRange"));
					
					Map<String, Object> result = null;
					result = inv.getContainerWiseInventoryList(lowerDate, higherDate);
					
					//out.write(result.get("data").toString());
					
					res.put("success", true);
					res.put("data", result.get("data"));
				}
				
				
			}
			
			else if(request.getParameter("action").trim().equalsIgnoreCase("getSingleContainerInventoryList"))
			{
				
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}
				
				if(!request.getParameterMap().containsKey("containerId") || request.getParameter("containerId").equals("")){
					throw new ValidationException("Enter Bar Code String");
				}
				else{
					long containerId = Long.parseLong(request.getParameter("containerId"));
					Map<String, Object> result = null;
					result = inv.getSingleContainerInventoryList(containerId);
					res.put("success", true);
					res.put("data", result.get("data"));
				}
			}
			
			else if(request.getParameter("action").trim().equalsIgnoreCase("readBarcode"))
			{
				if(session.getAttribute("id") == null)
				{
					throw new ValidationException("You are not logged in.");
				}
				String barcodeString;
				if(request.getParameterMap().containsKey("barcodeString") && !request.getParameter("barcodeString").equals("")){
					barcodeString = request.getParameter("barcodeString");
				}
				else{
					throw new ValidationException("Enter Bar Code String");
				}
				
				Map<String, Object> result = null;
				result = inv.getSingleSlabData(barcodeString);
				
				res.put("success", result.get("success"));
				res.put("message", result.get("message"));
				res.put("data", result.get("data") != null ? result.get("data") : "");
			}
			else if(request.getParameter("action").trim().equalsIgnoreCase("getMaxContainer"))
			{
				
				Session hSession=null;
				try
				{
					hSession=HibernateBridge.getSessionFactory().openSession();
					System.out.println("Connection Opened");
					Query list= hSession.createQuery("select max(container) from Inventory");
					System.out.println(list.list().get(0));
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					hSession.close();
				}
				
			}
			else if(request.getParameter("action").trim().equalsIgnoreCase("findUniqueMaterialinInventory"))
			{
				
				Map<String, Object> result = null;
				result = inv.findUniqueMaterialinInventory();
				
	
				res.put("success", result.get("success"));
				res.put("message", result.get("message"));
				res.put("data", result.get("data") != null ? result.get("data") : "");
			}
			
			writer.write(res.toString());
		}
		catch (ValidationException e)
		{
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
				writer.write(res.toString());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
				writer.write(res.toString());

			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		
		//doPost(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		InventoryController inv = new InventoryController();
		HttpSession session=request.getSession();
		JSONObject res=new JSONObject();
		PrintWriter writer = response.getWriter();
		PrintWriter out = response.getWriter();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try
		{
			if(!request.getParameterMap().containsKey("action"))
			{
				throw new ValidationException("Action not passed");
			}
			else if(request.getParameter("action").equalsIgnoreCase("searchStock"))
			{
				long supplierId = 0;
				long materialId = 0;
				long thickness = 0;
				String sold = "";
				int lengthEqualOrGreaterThan = 0;
				
				
				if(request.getParameterMap().containsKey("supplierId") && !request.getParameter("supplierId").equals(""))
				{
					supplierId = (request.getParameterMap().containsKey("supplierId")) ? Long.parseLong(request.getParameter("supplierId")): 0;
				}
				if(request.getParameterMap().containsKey("materialId") && !request.getParameter("materialId").equals(""))
				{
					materialId = (request.getParameterMap().containsKey("materialId")) ? Long.parseLong(request.getParameter("materialId")): 0;
				}
				if(request.getParameterMap().containsKey("thickness") && !request.getParameter("thickness").equals(""))
				{
					thickness = (request.getParameterMap().containsKey("thickness")) ? Long.parseLong(request.getParameter("thickness")): 0;
				}
				if(request.getParameterMap().containsKey("lengthEqualOrGreaterThan") && !request.getParameter("lengthEqualOrGreaterThan").equals(""))
				{
					lengthEqualOrGreaterThan = (request.getParameterMap().containsKey("lengthEqualOrGreaterThan")) ? Integer.parseInt(request.getParameter("lengthEqualOrGreaterThan")): 0;
				}
				
				
				if(request.getParameterMap().containsKey("sold") && request.getParameter("sold").equals("0"))
				{
					sold = "false";
				}
				else if(request.getParameterMap().containsKey("sold") && request.getParameter("sold").equals("1"))
				{
					sold = "true";
				}
				else
				{
					sold = "";
				}
				
				String type = request.getParameterMap().containsKey("type") ? request.getParameter("type") : "";
				String barcodeString = request.getParameterMap().containsKey("barcodeString") ? request.getParameter("barcodeString") : "";
				
				System.out.println("type: "+ type);
				Map<String, Object> inventoryList = inv.searchStock(supplierId, materialId, thickness, sold, lengthEqualOrGreaterThan, type, barcodeString);
				
				res.put("success", inventoryList.get("success"));
				res.put("data", inventoryList.get("data"));
				
				out.write(res.toString());
			}
			else if(request.getParameter("action").equalsIgnoreCase("updateStockRow"))
			{
				
				if(!request.getParameterMap().containsKey("stockId") || request.getParameter("stockId").equals(""))
				{
					throw new ValidationException("Stock Id not provided");
				}
				else if(!request.getParameterMap().containsKey("actualLength") || request.getParameter("actualLength").equals(""))
				{
					throw new ValidationException("Length not provided");
				}
				else if(!request.getParameterMap().containsKey("actualHeight") || request.getParameter("actualHeight").equals(""))
				{
					throw new ValidationException("Height not provided");
				}
				else if(!request.getParameterMap().containsKey("actualThickness") || request.getParameter("actualThickness").equals(""))
				{
					throw new ValidationException("Thickness not provided");
				}
				else {
					
					long stockId = Long.parseLong(request.getParameter("stockId"));
					
					double length = Double.parseDouble(request.getParameter("actualLength"));
					double height = Double.parseDouble(request.getParameter("actualHeight"));
					long thickness = Long.parseLong(request.getParameter("actualThickness"));
					
					Map<String, Object> inventoryList = inv.updateStockRow(stockId, length, height, thickness);

					res.put("success", inventoryList.get("success"));
					res.put("data", inventoryList.get("data"));
					
					out.write(res.toString());
				}
				
				
			}
		}
		catch (ValidationException e)
		{
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
				writer.write(res.toString());
			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			try
			{
				res.put("success", false);
				res.put("error", e.getMessage());
				writer.write(res.toString());

			}
			catch (JSONException je)
			{
				je.printStackTrace();
			}
		}
	}

}
