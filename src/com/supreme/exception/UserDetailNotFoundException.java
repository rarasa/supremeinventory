package com.supreme.exception;

public class UserDetailNotFoundException extends GerminateException {
	String message;
	
	public UserDetailNotFoundException(String message) {
		this.message=message;
	}
	
	public String getMessage() {
		return message;
	}

}
