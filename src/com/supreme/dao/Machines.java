package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class Machines implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-08-29
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long machineId;
	String cuttingType, machineName, location, machineCode;
	Date createdOn, updatedOn;
	
	public Long getMachineId() {
		return machineId;
	}
	public void setMachineId(Long machineId) {
		this.machineId = machineId;
	}
	public String getCuttingType() {
		return cuttingType;
	}
	public void setCuttingType(String cuttingType) {
		this.cuttingType = cuttingType;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the machineCode
	 */
	public String getMachineCode() {
		return machineCode;
	}
	/**
	 * @param machineCode the machineCode to set
	 */
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	
}
