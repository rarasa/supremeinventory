package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ContainerStatusDetails implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-12-15
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id, containerId, BLStatus, DOStatus, containerStatus, paymentStatus, bankDetailsId;
	double amount;
	String containerCode, invoiceNo, description;
	Date createdOn, updatedOn, paymentDate, invoiceDate;
	
	long currencyId;
	double rate;
	Date rateOnDate;
	
	public ContainerStatusDetails() {
		
	}

	public ContainerStatusDetails(long BLStatus, long DOStatus, long containerStatus, long paymentStatus,
			double amount, String containerCode, String invoiceNumber, String description,
			Date paymentDate, Date invoiceDate, long currencyId, double rate, Date rateOnDate) {
		
		this.BLStatus = BLStatus;
		this.DOStatus = DOStatus;
		this.containerStatus = containerStatus;
		this.paymentStatus = paymentStatus;
		this.amount = amount;
		this.containerCode = containerCode;
		this.invoiceNo = invoiceNumber;
		this.description = description;
		this.paymentDate = paymentDate;
		this.invoiceDate = invoiceDate;
		this.currencyId = currencyId;
		this.rate = rate;
		this.rateOnDate = rateOnDate;
	}

	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the containerId
	 */
	public Long getContainerId() {
		return containerId;
	}
	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	/**
	 * @return the bLStatus
	 */
	public Long getBLStatus() {
		return BLStatus;
	}
	/**
	 * @param bLStatus the bLStatus to set
	 */
	public void setBLStatus(Long bLStatus) {
		BLStatus = bLStatus;
	}
	/**
	 * @return the dOStatus
	 */
	public Long getDOStatus() {
		return DOStatus;
	}
	/**
	 * @param dOStatus the dOStatus to set
	 */
	public void setDOStatus(Long dOStatus) {
		DOStatus = dOStatus;
	}
	/**
	 * @return the containerStatus
	 */
	public Long getContainerStatus() {
		return containerStatus;
	}
	/**
	 * @param containerStatus the containerStatus to set
	 */
	public void setContainerStatus(Long containerStatus) {
		this.containerStatus = containerStatus;
	}
	/**
	 * @return the paymentStatus
	 */
	public Long getPaymentStatus() {
		return paymentStatus;
	}
	/**
	 * @param paymentStatus the paymentStatus to set
	 */
	public void setPaymentStatus(Long paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/**
	 * @return the containerCode
	 */
	public String getContainerCode() {
		return containerCode;
	}
	/**
	 * @param containerCode the containerCode to set
	 */
	public void setContainerCode(String containerCode) {
		this.containerCode = containerCode;
	}
	
	/**
	 * @return the invoiceNo
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * @param invoiceNo the invoiceNo to set
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the paymentDate
	 */
	public Date getPaymentDate() {
		return paymentDate;
	}
	/**
	 * @param paymentDate the paymentDate to set
	 */
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	/**
	 * @return the invoiceDate
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	/**
	 * @param invoiceDate the invoiceDate to set
	 */
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the currencyId
	 */
	public long getCurrencyId() {
		return currencyId;
	}

	/**
	 * @param currencyId the currencyId to set
	 */
	public void setCurrencyId(long currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}

	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}

	/**
	 * @return the rateOnDate
	 */
	public Date getRateOnDate() {
		return rateOnDate;
	}

	/**
	 * @param rateOnDate the rateOnDate to set
	 */
	public void setRateOnDate(Date rateOnDate) {
		this.rateOnDate = rateOnDate;
	}

	/**
	 * @return the bankDetailsId
	 */
	public Long getBankDetailsId() {
		return bankDetailsId;
	}

	/**
	 * @param bankDetailsId the bankDetailsId to set
	 */
	public void setBankDetailsId(Long bankDetailsId) {
		this.bankDetailsId = bankDetailsId;
	}
	
	
}
