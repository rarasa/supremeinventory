package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class Inventory implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-04-28
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id, slabNoSupreme, slabNoSupplier, supplierId, materialId, containerId;
	Long thickness;
	int yard;
	String type, barcodeString;
	Date arrivalDate, createdOn, updatedOn;
	Boolean steps, risers, flamed, polished, sold, oldStock, inCutting;
	double riserLength, stepLength, length, height, actualLength, actualHeight,
			squareMetre, runningMetre, actualSquareMetre, diffSquareMetre;
	
	Supplier supplier;
	Material material;
	Container container;
	
	int tileCount, originalTileCount;
	Boolean tile;
	
	public Long getContainerId() {
		return containerId;
	}
	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	public Container getContainer() {
		return container;
	}
	public void setContainer(Container container) {
		this.container = container;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	public Long getSlabNoSupreme() {
		return slabNoSupreme;
	}
	public void setSlabNoSupreme(Long slabNoSupreme) {
		this.slabNoSupreme = slabNoSupreme;
	}
	
	public Long getSlabNoSupplier() {
		return slabNoSupplier;
	}
	public void setSlabNoSupplier(Long slabNoSupplier) {
		this.slabNoSupplier = slabNoSupplier;
	}
	public Long getThickness() {
		return thickness;
	}
	public void setThickness(Long thickness) {
		this.thickness = thickness;
	}
	public Long getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	
	public Long getMaterialId() {
		return materialId;
	}
	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBarcodeString() {
		return barcodeString;
	}
	public void setBarcodeString(String barcodeString) {
		this.barcodeString = barcodeString;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Boolean getSteps() {
		return steps;
	}
	public void setSteps(Boolean steps) {
		this.steps = steps;
	}
	public Boolean getRisers() {
		return risers;
	}
	public void setRisers(Boolean risers) {
		this.risers = risers;
	}
	public Boolean getFlamed() {
		return flamed;
	}
	public void setFlamed(Boolean flamed) {
		this.flamed = flamed;
	}
	public Boolean getPolished() {
		return polished;
	}
	public void setPolished(Boolean polished) {
		this.polished = polished;
	}
	public double getRiserLength() {
		return riserLength;
	}
	public void setRiserLength(double riserLength) {
		this.riserLength = riserLength;
	}
	public double getStepLength() {
		return stepLength;
	}
	public void setStepLength(double stepLength) {
		this.stepLength = stepLength;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getActualLength() {
		return actualLength;
	}
	public void setActualLength(double actualLength) {
		this.actualLength = actualLength;
	}
	public double getActualHeight() {
		return actualHeight;
	}
	public void setActualHeight(double actualHeight) {
		this.actualHeight = actualHeight;
	}
	public double getSquareMetre() {
		return squareMetre;
	}
	public void setSquareMetre(double squareMetre) {
		this.squareMetre = squareMetre;
	}
	public double getRunningMetre() {
		return runningMetre;
	}
	public void setRunningMetre(double runningMetre) {
		this.runningMetre = runningMetre;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public double getActualSquareMetre() {
		return actualSquareMetre;
	}
	public void setActualSquareMetre(double actualSquareMetre) {
		this.actualSquareMetre = actualSquareMetre;
	}
	public double getDiffSquareMetre() {
		return diffSquareMetre;
	}
	public void setDiffSquareMetre(double diffSquareMetre) {
		this.diffSquareMetre = diffSquareMetre;
	}
	public Boolean getSold() {
		return sold;
	}
	public void setSold(Boolean sold) {
		this.sold = sold;
	}
	
	public int getYard() {
		return yard;
	}
	public void setYard(int yard) {
		this.yard = yard;
	}
	public Boolean getOldStock() {
		return oldStock;
	}
	public void setOldStock(Boolean oldStock) {
		this.oldStock = oldStock;
	}
	public Boolean getInCutting() {
		return inCutting;
	}
	public void setInCutting(Boolean inCutting) {
		this.inCutting = inCutting;
	}
	public int getTileCount() {
		return tileCount;
	}
	public void setTileCount(int tileCount) {
		this.tileCount = tileCount;
	}
	public Boolean getTile() {
		return tile;
	}
	public void setTile(Boolean tile) {
		this.tile = tile;
	}
	public int getOriginalTileCount() {
		return originalTileCount;
	}
	public void setOriginalTileCount(int originalTileCount) {
		this.originalTileCount = originalTileCount;
	}
	
}