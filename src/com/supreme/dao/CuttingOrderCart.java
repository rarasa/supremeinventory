package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class CuttingOrderCart implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-12-05
	 */
	
	private static final long serialVersionUID = 1L;
	
	long id, cuttingOrderTaskId, customerId, materialId, unitId;
	int thickness;
	boolean deleted, cutToSize;
	double length, height, quantity, nos, rate, amount, miscValue;
	Date createdOn, updatedOn, deletedOn, orderDate;
	
	public CuttingOrderCart() {
		
	}

	public CuttingOrderCart(long materialId, long customerId, int thickness, Date orderDate, long unitId, double length, 
								double height, double nos, double quantity, double rate, double amount,
								boolean cutToSize, double miscValue) {
		this.unitId  = unitId;
		this.quantity  = quantity;
		this.length  = length;
		this.height  = height;
		this.nos  = nos;
		this.rate  = rate;
		this.amount  = amount;
		this.thickness  = thickness;
		this.customerId  = customerId;
		this.materialId  = materialId;
		this.orderDate = orderDate;
		this.cutToSize = cutToSize;
		this.miscValue = miscValue;
	}
	
	/**
	 * @return the miscValue
	 */
	public double getMiscValue() {
		return miscValue;
	}

	/**
	 * @param miscValue the miscValue to set
	 */
	public void setMiscValue(double miscValue) {
		this.miscValue = miscValue;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the materialId
	 */
	public long getMaterialId() {
		return materialId;
	}
	/**
	 * @param materialId the materialId to set
	 */
	public void setMaterialId(long materialId) {
		this.materialId = materialId;
	}
	/**
	 * @return the unitId
	 */
	public long getUnitId() {
		return unitId;
	}
	/**
	 * @param unitId the unitId to set
	 */
	public void setUnitId(long unitId) {
		this.unitId = unitId;
	}
	/**
	 * @return the thickness
	 */
	public int getThickness() {
		return thickness;
	}
	/**
	 * @param thickness the thickness to set
	 */
	public void setThickness(int thickness) {
		this.thickness = thickness;
	}
	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @return the length
	 */
	public double getLength() {
		return length;
	}
	/**
	 * @param length the length to set
	 */
	public void setLength(double length) {
		this.length = length;
	}
	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		this.height = height;
	}
	/**
	 * @return the quantity
	 */
	public double getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the nos
	 */
	public double getNos() {
		return nos;
	}
	/**
	 * @param nos the nos to set
	 */
	public void setNos(double nos) {
		this.nos = nos;
	}
	/**
	 * @return the rate
	 */
	public double getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}
	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	/**
	 * @return the orderDate
	 */
	public Date getOrderDate() {
		return orderDate;
	}
	/**
	 * @param orderDate the orderDate to set
	 */
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the cutToSize
	 */
	public boolean isCutToSize() {
		return cutToSize;
	}
	/**
	 * @param cutToSize the cutToSize to set
	 */
	public void setCutToSize(boolean cutToSize) {
		this.cutToSize = cutToSize;
	}

	/**
	 * @return the cuttingOrderTaskId
	 */
	public long getCuttingOrderTaskId() {
		return cuttingOrderTaskId;
	}

	/**
	 * @param cuttingOrderTaskId the cuttingOrderTaskId to set
	 */
	public void setCuttingOrderTaskId(long cuttingOrderTaskId) {
		this.cuttingOrderTaskId = cuttingOrderTaskId;
	}
	
	
}
