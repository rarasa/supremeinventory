package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Customer implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2021-04-24
	 */
	
	private static final long serialVersionUID = 1L;
	
	long id;
	String name, phone, country, email, emirate, billingAddress, trnNumber;
	boolean VATRegistration, deleted, walkIn;
	Date createdOn, updatedOn, deletedOn, VATRegistrationDate;
	
	Set<BankDetails> bankDetails = new HashSet<BankDetails>();
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the emirate
	 */
	public String getEmirate() {
		return emirate;
	}
	/**
	 * @param emirate the emirate to set
	 */
	public void setEmirate(String emirate) {
		this.emirate = emirate;
	}
	/**
	 * @return the billingAddress
	 */
	public String getBillingAddress() {
		return billingAddress;
	}
	/**
	 * @param billingAddress the billingAddress to set
	 */
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	/**
	 * @return the trnNumber
	 */
	public String getTrnNumber() {
		return trnNumber;
	}
	/**
	 * @param trnNumber the trnNumber to set
	 */
	public void setTrnNumber(String trnNumber) {
		this.trnNumber = trnNumber;
	}
	/**
	 * @return the vATRegistration
	 */
	public boolean isVATRegistration() {
		return VATRegistration;
	}
	/**
	 * @param vATRegistration the vATRegistration to set
	 */
	public void setVATRegistration(boolean vATRegistration) {
		VATRegistration = vATRegistration;
	}
	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @return the walkIn
	 */
	public boolean isWalkIn() {
		return walkIn;
	}
	/**
	 * @param walkIn the walkIn to set
	 */
	public void setWalkIn(boolean walkIn) {
		this.walkIn = walkIn;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}
	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the vATRegistrationDate
	 */
	public Date getVATRegistrationDate() {
		return VATRegistrationDate;
	}
	/**
	 * @param vATRegistrationDate the vATRegistrationDate to set
	 */
	public void setVATRegistrationDate(Date vATRegistrationDate) {
		VATRegistrationDate = vATRegistrationDate;
	}
	/**
	 * @return the bankDetails
	 */
	public Set<BankDetails> getBankDetails() {
		return bankDetails;
	}
	/**
	 * @param bankDetails the bankDetails to set
	 */
	public void setBankDetails(Set<BankDetails> bankDetails) {
		this.bankDetails = bankDetails;
	}

	
	
	
}
