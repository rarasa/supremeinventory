package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CuttingOrderTask implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2021-06-16
	 */

	private static final long serialVersionUID = 1L;

	long id, cuttingOrderId;
	double amount;
	boolean deleted;
	Date deletedOn;

	Material material = new Material();

	Set<CuttingOrderTaskTypeDetails> cuttingOrderTaskTypeDetails = new HashSet<CuttingOrderTaskTypeDetails>();
	Set<CuttingOrderCart> cuttingOrderCart = new HashSet<CuttingOrderCart>();

	public CuttingOrderTask() {
	}

	public CuttingOrderTask(double amount, long materialId, Material material,
			Set<CuttingOrderTaskTypeDetails> cuttingOrderTaskTypeDetailsSet, Set<CuttingOrderCart> cuttingOrderCart) {
		this.amount = amount;
		/*this.materialId = materialId;*/
		this.material = material;
		this.cuttingOrderTaskTypeDetails = cuttingOrderTaskTypeDetailsSet;
		this.cuttingOrderCart = cuttingOrderCart;

	}

	/**
	 * @return the material
	 */
	public Material getMaterial() {
		return material;
	}

	/**
	 * @param material the material to set
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}

	/**
	 * @return the materialId
	 */
	/*
	public long getMaterialId() {
	return materialId;
	}
	
	*//**
		 * @param materialId the materialId to set
		 *//*
			public void setMaterialId(long materialId) {
			this.materialId = materialId;
			}*/

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the cuttingOrderId
	 */
	public long getCuttingOrderId() {
		return cuttingOrderId;
	}

	/**
	 * @param cuttingOrderId the cuttingOrderId to set
	 */
	public void setCuttingOrderId(long cuttingOrderId) {
		this.cuttingOrderId = cuttingOrderId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}

	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	/**
	 * @return the cuttingOrderTaskTypeDetails
	 */
	public Set<CuttingOrderTaskTypeDetails> getCuttingOrderTaskTypeDetails() {
		return cuttingOrderTaskTypeDetails;
	}

	/**
	 * @param cuttingOrderTaskTypeDetails the cuttingOrderTaskTypeDetails to set
	 */
	public void setCuttingOrderTaskTypeDetails(Set<CuttingOrderTaskTypeDetails> cuttingOrderTaskTypeDetails) {
		this.cuttingOrderTaskTypeDetails = cuttingOrderTaskTypeDetails;
	}

	/**
	 * @return the cuttingOrderCart
	 */
	public Set<CuttingOrderCart> getCuttingOrderCart() {
		return cuttingOrderCart;
	}

	/**
	 * @param cuttingOrderCart the cuttingOrderCart to set
	 */
	public void setCuttingOrderCart(Set<CuttingOrderCart> cuttingOrderCart) {
		this.cuttingOrderCart = cuttingOrderCart;
	}

}
