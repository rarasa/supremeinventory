package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class InvoiceCutting implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-08-29
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id;
	String invoiceNumber;
	Date createdOn, updatedOn, invoiceDate;
	
	Set<Cutting> cuttingOrders = new HashSet<Cutting>();
	
	
	public Set<Cutting> getCuttingOrders() {
		return cuttingOrders;
	}
	public void setCuttingOrders(Set<Cutting> cuttingOrders) {
		this.cuttingOrders = cuttingOrders;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

		
}
