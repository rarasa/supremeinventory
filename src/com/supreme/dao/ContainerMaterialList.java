package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class ContainerMaterialList implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-12-15
	 */
	
	long id, containerId, materialId, noOfPieces;
	int thickness;
	double quantity, billRate, realRate;
	
	boolean deleted;
	
	public ContainerMaterialList() {
		
	}

	public ContainerMaterialList(long materialId, long noOfPieces, int thickness, double quantity, double billRate, double realRate) {
		this.materialId = materialId;
		this.noOfPieces = noOfPieces;
		this.thickness = thickness;
		this.quantity = quantity;
		this.realRate = realRate;
		this.billRate = billRate;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the containerId
	 */
	public long getContainerId() {
		return containerId;
	}

	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(long containerId) {
		this.containerId = containerId;
	}

	/**
	 * @return the materialId
	 */
	public long getMaterialId() {
		return materialId;
	}

	/**
	 * @param materialId the materialId to set
	 */
	public void setMaterialId(long materialId) {
		this.materialId = materialId;
	}

	/**
	 * @return the noOfPieces
	 */
	public long getNoOfPieces() {
		return noOfPieces;
	}

	/**
	 * @param noOfPieces the noOfPieces to set
	 */
	public void setNoOfPieces(long noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	/**
	 * @return the thickness
	 */
	public int getThickness() {
		return thickness;
	}

	/**
	 * @param thickness the thickness to set
	 */
	public void setThickness(int thickness) {
		this.thickness = thickness;
	}

	/**
	 * @return the quantity
	 */
	public double getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the billRate
	 */
	public double getBillRate() {
		return billRate;
	}

	/**
	 * @param billRate the billRate to set
	 */
	public void setBillRate(double billRate) {
		this.billRate = billRate;
	}

	/**
	 * @return the realRate
	 */
	public double getRealRate() {
		return realRate;
	}

	/**
	 * @param realRate the realRate to set
	 */
	public void setRealRate(double realRate) {
		this.realRate = realRate;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	

}
