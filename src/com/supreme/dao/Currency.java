package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class Currency implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-12-15
	 */
	
	long id, updatedByUser;
	String name;
	double value;
	Date changedOn, updatedOn, createdOn;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the updatedByUser
	 */
	public long getUpdatedByUser() {
		return updatedByUser;
	}
	/**
	 * @param updatedByUser the updatedByUser to set
	 */
	public void setUpdatedByUser(long updatedByUser) {
		this.updatedByUser = updatedByUser;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(double value) {
		this.value = value;
	}
	/**
	 * @return the changedOn
	 */
	public Date getChangedOn() {
		return changedOn;
	}
	/**
	 * @param changedOn the changedOn to set
	 */
	public void setChangedOn(Date changedOn) {
		this.changedOn = changedOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
}
