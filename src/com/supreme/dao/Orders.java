package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Orders implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-12-05
	 */
	
	private static final long serialVersionUID = 1L;
	
	long id, customerDetailsId;
	String LPONumber, cuttingOrderString, narration, vehicleNumber, address;
	boolean placed, deleted, DOPrinted, hasCuttingSlabs, companyPickup;
	long orderId;
	long customerId;
	Set<Cart> cart = new HashSet<Cart>();
	Set<CuttingOrder> cuttingOrder = new HashSet<CuttingOrder>();

	double count, totalMtSquare;
	
	Date createdOn, updatedOn, LPODate, DOPrintedOn, deliveryDate;
	
	Long mergedWithId;
	boolean merged;
	Date mergedOn;
	
	long placedBy;
	Date placedOn;
	
	long scannedBy;
	
	boolean skipAddingManualData, outsideMaterial;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Boolean getPlaced() {
		return placed;
	}
	public void setPlaced(Boolean placed) {
		this.placed = placed;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public Set<Cart> getCart() {
		return cart;
	}
	public void setCart(Set<Cart> cart) {
		this.cart = cart;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public double getTotalMtSquare() {
		return totalMtSquare;
	}
	public void setTotalMtSquare(double totalMtSquare) {
		this.totalMtSquare = totalMtSquare;
	}
	public void setPlaced(boolean placed) {
		this.placed = placed;
	}
	public double getCount() {
		return count;
	}
	public void setCount(double count) {
		this.count = count;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public String getLPONumber() {
		return LPONumber;
	}
	public void setLPONumber(String lPONumber) {
		LPONumber = lPONumber;
	}
	public Date getLPODate() {
		return LPODate;
	}
	public void setLPODate(Date lPODate) {
		LPODate = lPODate;
	}
	public boolean isDOPrinted() {
		return DOPrinted;
	}
	public void setDOPrinted(boolean dOPrinted) {
		DOPrinted = dOPrinted;
	}
	public Date getDOPrintedOn() {
		return DOPrintedOn;
	}
	public void setDOPrintedOn(Date dOPrintedOn) {
		DOPrintedOn = dOPrintedOn;
	}
	
	public boolean isMerged() {
		return merged;
	}
	public void setMerged(boolean merged) {
		this.merged = merged;
	}
	public Date getMergedOn() {
		return mergedOn;
	}
	public void setMergedOn(Date mergedOn) {
		this.mergedOn = mergedOn;
	}
	public Long getMergedWithId() {
		return mergedWithId;
	}
	public void setMergedWithId(Long mergedWithId) {
		this.mergedWithId = mergedWithId;
	}
	public long getPlacedBy() {
		return placedBy;
	}
	public void setPlacedBy(long placedBy) {
		this.placedBy = placedBy;
	}
	public Date getPlacedOn() {
		return placedOn;
	}
	public void setPlacedOn(Date placedOn) {
		this.placedOn = placedOn;
	}
	/**
	 * @return the customerId
	 */
	public long getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the cuttingOrderString
	 */
	public String getCuttingOrderString() {
		return cuttingOrderString;
	}
	/**
	 * @param cuttingOrderString the cuttingOrderString to set
	 */
	public void setCuttingOrderString(String cuttingOrderString) {
		this.cuttingOrderString = cuttingOrderString;
	}
	/**
	 * @return the hasCuttingSlabs
	 */
	public boolean isHasCuttingSlabs() {
		return hasCuttingSlabs;
	}
	/**
	 * @param hasCuttingSlabs the hasCuttingSlabs to set
	 */
	public void setHasCuttingSlabs(boolean hasCuttingSlabs) {
		this.hasCuttingSlabs = hasCuttingSlabs;
	}
	
	/**
	 * @return the customerDetailsId
	 */
	public long getCustomerDetailsId() {
		return customerDetailsId;
	}
	/**
	 * @param customerDetailsId the customerDetailsId to set
	 */
	public void setCustomerDetailsId(long customerDetailsId) {
		this.customerDetailsId = customerDetailsId;
	}
	/**
	 * @return the cuttingOrder
	 */
	public Set<CuttingOrder> getCuttingOrder() {
		return cuttingOrder;
	}
	/**
	 * @param cuttingOrder the cuttingOrder to set
	 */
	public void setCuttingOrder(Set<CuttingOrder> cuttingOrder) {
		this.cuttingOrder = cuttingOrder;
	}
	/**
	 * @return the scannedBy
	 */
	public long getScannedBy() {
		return scannedBy;
	}
	/**
	 * @param scannedBy the scannedBy to set
	 */
	public void setScannedBy(long scannedBy) {
		this.scannedBy = scannedBy;
	}
	/**
	 * @return the narration
	 */
	public String getNarration() {
		return narration;
	}
	/**
	 * @param narration the narration to set
	 */
	public void setNarration(String narration) {
		this.narration = narration;
	}
	/**
	 * @return the vehicleNumber
	 */
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	/**
	 * @param vehicleNumber the vehicleNumber to set
	 */
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	/**
	 * @return the companyPickup
	 */
	public boolean isCompanyPickup() {
		return companyPickup;
	}
	/**
	 * @param companyPickup the companyPickup to set
	 */
	public void setCompanyPickup(boolean companyPickup) {
		this.companyPickup = companyPickup;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the deliveryDate
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	/**
	 * @param deliveryDate the deliveryDate to set
	 */
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	/**
	 * @return the outsideMaterial
	 */
	public boolean isOutsideMaterial() {
		return outsideMaterial;
	}
	/**
	 * @param outsideMaterial the outsideMaterial to set
	 */
	public void setOutsideMaterial(boolean outsideMaterial) {
		this.outsideMaterial = outsideMaterial;
	}
	/**
	 * @return the skipAddingManualData
	 */
	public boolean isSkipAddingManualData() {
		return skipAddingManualData;
	}
	/**
	 * @param skipAddingManualData the skipAddingManualData to set
	 */
	public void setSkipAddingManualData(boolean skipAddingManualData) {
		this.skipAddingManualData = skipAddingManualData;
	}
	
}
