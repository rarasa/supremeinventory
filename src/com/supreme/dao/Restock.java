package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Restock implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2021-06-26
	 */

	long id, statusTypeId, inventoryId, cartId, orderId;
	String remarks;
	int tileCount;
	boolean deleted, active, tile;
	Date deletedOn, createdOn, updatedOn;
	
	StatusTypes statusType = new StatusTypes();
	Inventory inventory = new Inventory();
	Cart cart = new Cart();
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the statusTypeId
	 */
	public long getStatusTypeId() {
		return statusTypeId;
	}
	/**
	 * @param statusTypeId the statusTypeId to set
	 */
	public void setStatusTypeId(long statusTypeId) {
		this.statusTypeId = statusTypeId;
	}
	/**
	 * @return the inventoryId
	 */
	public long getInventoryId() {
		return inventoryId;
	}
	/**
	 * @param inventoryId the inventoryId to set
	 */
	public void setInventoryId(long inventoryId) {
		this.inventoryId = inventoryId;
	}
	/**
	 * @return the cartId
	 */
	public long getCartId() {
		return cartId;
	}
	/**
	 * @param cartId the cartId to set
	 */
	public void setCartId(long cartId) {
		this.cartId = cartId;
	}
	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}
	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}
	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the statusType
	 */
	public StatusTypes getStatusType() {
		return statusType;
	}
	/**
	 * @param statusType the statusType to set
	 */
	public void setStatusType(StatusTypes statusType) {
		this.statusType = statusType;
	}
	/**
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return inventory;
	}
	/**
	 * @param inventory the inventory to set
	 */
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	/**
	 * @return the cart
	 */
	public Cart getCart() {
		return cart;
	}
	/**
	 * @param cart the cart to set
	 */
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	/**
	 * @return the tileCount
	 */
	public int getTileCount() {
		return tileCount;
	}
	/**
	 * @param tileCount the tileCount to set
	 */
	public void setTileCount(int tileCount) {
		this.tileCount = tileCount;
	}
	/**
	 * @return the tile
	 */
	public boolean isTile() {
		return tile;
	}
	/**
	 * @param tile the tile to set
	 */
	public void setTile(boolean tile) {
		this.tile = tile;
	}
	
}
