package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class Cutting implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-08-29
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id, cuttingOrderNumber, invoiceId;
	String particulars, unit;
	Double amount, quantity, rate;
	boolean extraProfitGiven;
	Date orderDate;
	
	public Cutting()
	{
	}
	
	public Cutting(String particulars, String unit, double quantity, double rate, double amount, boolean extraProfitGiven, long cuttingOrderNumber, Date orderDate)
	{
		this.particulars = particulars;
		this.unit = unit;
		this.quantity = quantity;
		this.rate = rate;
		this.amount = amount;
		this.extraProfitGiven = extraProfitGiven;
		this.cuttingOrderNumber = cuttingOrderNumber;
		this.orderDate = orderDate;
	}
	public Cutting(Long id, String particulars, String unit, double quantity, double rate, double amount, boolean extraProfitGiven, long cuttingOrderNumber, Date orderDate)
	{
		this.id = id;
		this.particulars = particulars;
		this.unit = unit;
		this.quantity = quantity;
		this.rate = rate;
		this.amount = amount;
		this.extraProfitGiven = extraProfitGiven;
		this.cuttingOrderNumber = cuttingOrderNumber;
		this.orderDate = orderDate;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCuttingOrderNumber() {
		return cuttingOrderNumber;
	}
	public void setCuttingOrderNumber(Long cuttingOrderNumber) {
		this.cuttingOrderNumber = cuttingOrderNumber;
	}
	public String getParticulars() {
		return particulars;
	}
	public void setParticulars(String particulars) {
		this.particulars = particulars;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	public boolean isExtraProfitGiven() {
		return extraProfitGiven;
	}
	public void setExtraProfitGiven(boolean extraProfitGiven) {
		this.extraProfitGiven = extraProfitGiven;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	
		
}
