package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class StatusTypes implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2021-06-26
	 */

	long id, statusId;
	String name;
	boolean deleted, active, finalStatus;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the statusId
	 */
	public long getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(long statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * @return the finalStatus
	 */
	public boolean isFinalStatus() {
		return finalStatus;
	}
	/**
	 * @param finalStatus the finalStatus to set
	 */
	public void setFinalStatus(boolean finalStatus) {
		this.finalStatus = finalStatus;
	}
	
	
}
