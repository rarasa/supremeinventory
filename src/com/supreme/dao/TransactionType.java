package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TransactionType implements Serializable
{

	/**
	 * Author : Rashi
	 * Date : 2021-04-30
	 */
	private static final long serialVersionUID = 1L;

	Long id;
	String transationType, transationFunction;
	
	Date createdOn, updatedOn;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	/**
	 * @return the transationType
	 */
	public String getTransationType() {
		return transationType;
	}
	/**
	 * @param transationType the transationType to set
	 */
	public void setTransationType(String transationType) {
		this.transationType = transationType;
	}
	/**
	 * @return the transationFunction
	 */
	public String getTransationFunction() {
		return transationFunction;
	}
	/**
	 * @param transationFunction the transationFunction to set
	 */
	public void setTransationFunction(String transationFunction) {
		this.transationFunction = transationFunction;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
}
