package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class CuttingType implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2021-05-02
	 */
	
	long id;
	String name;
	boolean manual, machine;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the manual
	 */
	public boolean isManual() {
		return manual;
	}
	/**
	 * @param manual the manual to set
	 */
	public void setManual(boolean manual) {
		this.manual = manual;
	}
	/**
	 * @return the machine
	 */
	public boolean isMachine() {
		return machine;
	}
	/**
	 * @param machine the machine to set
	 */
	public void setMachine(boolean machine) {
		this.machine = machine;
	}
	
	
}
