package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class Cart implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2020-12-05
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id;
	Long orderId, inventoryId, cuttingOrderId;

	double sellingPrice;
	Inventory inventory = null;
	int tileCount;
	boolean tile, sentForCutting;
	
	Date createdOn, updatedOn;
	
	boolean deleted;
	
	public Cart() {
	}
	
	public Cart(long inventoryId) {
		// TODO Auto-generated constructor stub
		this.inventoryId = inventoryId;
	}
	
	public Cart(long inventoryId, boolean tile, int tileCount) {
		// TODO Auto-generated constructor stub
		this.inventoryId = inventoryId;
		this.tileCount = tileCount;
		this.tile = tile;
	}
	public Cart(long cartId, long inventoryId, boolean tile, int tileCount) {
		// TODO Auto-generated constructor stub
		this.id = cartId;
		this.inventoryId = inventoryId;
		this.tileCount = tileCount;
		this.tile = tile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public int getTileCount() {
		return tileCount;
	}

	public void setTileCount(int tileCount) {
		this.tileCount = tileCount;
	}

	public boolean isTile() {
		return tile;
	}

	public void setTile(boolean tile) {
		this.tile = tile;
	}

	/**
	 * @return the cuttingOrderId
	 */
	public Long getCuttingOrderId() {
		return cuttingOrderId;
	}
	/**
	 * @param cuttingOrderId the cuttingOrderId to set
	 */
	public void setCuttingOrderId(Long cuttingOrderId) {
		this.cuttingOrderId = cuttingOrderId;
	}

	/**
	 * @return the sentForCutting
	 */
	public boolean isSentForCutting() {
		return sentForCutting;
	}

	/**
	 * @param sentForCutting the sentForCutting to set
	 */
	public void setSentForCutting(boolean sentForCutting) {
		this.sentForCutting = sentForCutting;
	}
	
	
}
