package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class SoldStock implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-04-28
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long id;
	
	Double sellingPrice;
	
	Supplier supplier;
	Material material;
	Inventory inventory;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public Double getSellingPrice() {
		return sellingPrice;
	}
	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	
	
}