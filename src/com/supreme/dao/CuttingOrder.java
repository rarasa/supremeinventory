package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class CuttingOrder implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-08-29
	 */
	
	private static final long serialVersionUID = 1L;
	
	long id, cuttingOrderNumber, orderId, materialId;
	double totalInputMtSquare, totalOutputMtSquare, totalOutputLinearMeter, amount;
	boolean DOPrinted, deleted, placed;
	Date createdOn, updatedOn, DOPrintedOn, deletedOn, placedOn;
	int thickness;

	Set<CuttingOrderTask> cuttingOrderTask = new HashSet<CuttingOrderTask>();
	
	Material material = new Material();

	public CuttingOrder() {
		
	}
	public CuttingOrder(long orderId, long materialId, long cuttingOrderNumber, double totalInputMtSquare, double totalOutputMtSquare, double totalOutputLinearMeter, 
						double amount, Date createdOn, Date updatedOn, Set<CuttingOrderTask> cuttingOrderTask) {
		this.orderId = orderId;
		this.materialId = materialId;
		this.cuttingOrderNumber = cuttingOrderNumber;
		this.totalInputMtSquare = totalInputMtSquare;
		this.totalOutputMtSquare = totalOutputMtSquare;
		this.totalOutputLinearMeter = totalOutputLinearMeter;
		this.amount = amount;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.cuttingOrderTask = cuttingOrderTask;
	}
	

	/**
	 * @return the material
	 */
	public Material getMaterial() {
		return material;
	}
	/**
	 * @param material the material to set
	 */
	public void setMaterial(Material material) {
		this.material = material;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the cuttingOrderNumber
	 */
	public long getCuttingOrderNumber() {
		return cuttingOrderNumber;
	}

	/**
	 * @param cuttingOrderNumber the cuttingOrderNumber to set
	 */
	public void setCuttingOrderNumber(long cuttingOrderNumber) {
		this.cuttingOrderNumber = cuttingOrderNumber;
	}

	/**
	 * @return the orderId
	 */
	public long getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the totalInputMtSquare
	 */
	public double getTotalInputMtSquare() {
		return totalInputMtSquare;
	}

	/**
	 * @param totalInputMtSquare the totalInputMtSquare to set
	 */
	public void setTotalInputMtSquare(double totalInputMtSquare) {
		this.totalInputMtSquare = totalInputMtSquare;
	}

	/**
	 * @return the totalOutputMtSquare
	 */
	public double getTotalOutputMtSquare() {
		return totalOutputMtSquare;
	}

	/**
	 * @param totalOutputMtSquare the totalOutputMtSquare to set
	 */
	public void setTotalOutputMtSquare(double totalOutputMtSquare) {
		this.totalOutputMtSquare = totalOutputMtSquare;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the dOPrinted
	 */
	public boolean isDOPrinted() {
		return DOPrinted;
	}

	/**
	 * @param dOPrinted the dOPrinted to set
	 */
	public void setDOPrinted(boolean dOPrinted) {
		DOPrinted = dOPrinted;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the placed
	 */
	public boolean isPlaced() {
		return placed;
	}

	/**
	 * @param placed the placed to set
	 */
	public void setPlaced(boolean placed) {
		this.placed = placed;
	}

	/**
	 * @return the createdOn
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the dOPrintedOn
	 */
	public Date getDOPrintedOn() {
		return DOPrintedOn;
	}

	/**
	 * @param dOPrintedOn the dOPrintedOn to set
	 */
	public void setDOPrintedOn(Date dOPrintedOn) {
		DOPrintedOn = dOPrintedOn;
	}

	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}

	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the placedOn
	 */
	public Date getPlacedOn() {
		return placedOn;
	}
	/**
	 * @param placedOn the placedOn to set
	 */
	public void setPlacedOn(Date placedOn) {
		this.placedOn = placedOn;
	}

	/**
	 * @return the totalOutputLinearMeter
	 */
	public double getTotalOutputLinearMeter() {
		return totalOutputLinearMeter;
	}

	/**
	 * @param totalOutputLinearMeter the totalOutputLinearMeter to set
	 */
	public void setTotalOutputLinearMeter(double totalOutputLinearMeter) {
		this.totalOutputLinearMeter = totalOutputLinearMeter;
	}

	/**
	 * @return the materialId
	 */
	public long getMaterialId() {
		return materialId;
	}

	/**
	 * @param materialId the materialId to set
	 */
	public void setMaterialId(long materialId) {
		this.materialId = materialId;
	}
	/**
	 * @return the cuttingOrderTask
	 */
	public Set<CuttingOrderTask> getCuttingOrderTask() {
		return cuttingOrderTask;
	}
	/**
	 * @param cuttingOrderTask the cuttingOrderTask to set
	 */
	public void setCuttingOrderTask(Set<CuttingOrderTask> cuttingOrderTask) {
		this.cuttingOrderTask = cuttingOrderTask;
	}
	/**
	 * @return the thickness
	 */
	public int getThickness() {
		return thickness;
	}
	/**
	 * @param thickness the thickness to set
	 */
	public void setThickness(int thickness) {
		this.thickness = thickness;
	}
	
}
