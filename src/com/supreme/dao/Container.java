package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Container implements Serializable {

	/**
	 * Author : Rashi
	 * Date : 2020-12-15
	 */
	
	private static final long serialVersionUID = 1L;
	
	Long containerId, supplierId, locationId;
	String containerCode;
	Date createdOn, updatedOn, arriveOn;
	
	boolean printed, detailsExist, packingListUploaded, deleted;
	
	Supplier supplier = new Supplier();
	
	public Long getContainerId() {
		return containerId;
	}
	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	public String getContainerCode() {
		return containerCode;
	}
	public void setContainerCode(String containerCode) {
		this.containerCode = containerCode;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public boolean isPrinted() {
		return printed;
	}
	public void setPrinted(boolean printed) {
		this.printed = printed;
	}
	
	/**
	 * @return the supplierId
	 */
	public Long getSupplierId() {
		return supplierId;
	}
	/**
	 * @param supplierId the supplierId to set
	 */
	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}
	/**
	 * @return the supplier
	 */
	public Supplier getSupplier() {
		return supplier;
	}
	/**
	 * @param supplier the supplier to set
	 */
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	/**
	 * @return the locationId
	 */
	public Long getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return the arriveOn
	 */
	public Date getArriveOn() {
		return arriveOn;
	}
	/**
	 * @param arriveOn the arriveOn to set
	 */
	public void setArriveOn(Date arriveOn) {
		this.arriveOn = arriveOn;
	}
	/**
	 * @return the detailsExist
	 */
	public boolean isDetailsExist() {
		return detailsExist;
	}
	/**
	 * @param detailsExist the detailsExist to set
	 */
	public void setDetailsExist(boolean detailsExist) {
		this.detailsExist = detailsExist;
	}
	/**
	 * @return the packingListUploaded
	 */
	public boolean isPackingListUploaded() {
		return packingListUploaded;
	}
	/**
	 * @param packingListUploaded the packingListUploaded to set
	 */
	public void setPackingListUploaded(boolean packingListUploaded) {
		this.packingListUploaded = packingListUploaded;
	}
	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}
	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
}
