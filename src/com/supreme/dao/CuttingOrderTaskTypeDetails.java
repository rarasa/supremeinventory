package com.supreme.dao;

import java.io.Serializable;
import java.util.Date;

public class CuttingOrderTaskTypeDetails implements Serializable {

	/**
	 * Author : Rashi Rathi
	 * Date : 2021-06-16
	 */
	
	private static final long serialVersionUID = 1L;
	
	long id, cuttingOrderTaskId, cuttingTypeId;
	boolean deleted;
	Date deletedOn;
	double amount;

	CuttingType cuttingType = new CuttingType();
	
	public CuttingOrderTaskTypeDetails(){
		
	}
	
	public CuttingOrderTaskTypeDetails(long cuttingTypeId, double amount, CuttingType cuttingType){
		this.cuttingTypeId = cuttingTypeId;
		this.amount = amount;
		this.cuttingType = cuttingType;
	}
	
	/**
	 * @return the cuttingOrderTaskId
	 */
	public long getCuttingOrderTaskId() {
		return cuttingOrderTaskId;
	}

	/**
	 * @param cuttingOrderTaskId the cuttingOrderTaskId to set
	 */
	public void setCuttingOrderTaskId(long cuttingOrderTaskId) {
		this.cuttingOrderTaskId = cuttingOrderTaskId;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the cuttingTypeId
	 */
	public long getCuttingTypeId() {
		return cuttingTypeId;
	}

	/**
	 * @param cuttingTypeId the cuttingTypeId to set
	 */
	public void setCuttingTypeId(long cuttingTypeId) {
		this.cuttingTypeId = cuttingTypeId;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return the deletedOn
	 */
	public Date getDeletedOn() {
		return deletedOn;
	}

	/**
	 * @param deletedOn the deletedOn to set
	 */
	public void setDeletedOn(Date deletedOn) {
		this.deletedOn = deletedOn;
	}

	/**
	 * @return the cuttingType
	 */
	public CuttingType getCuttingType() {
		return cuttingType;
	}

	/**
	 * @param cuttingType the cuttingType to set
	 */
	public void setCuttingType(CuttingType cuttingType) {
		this.cuttingType = cuttingType;
	} 
	
}
