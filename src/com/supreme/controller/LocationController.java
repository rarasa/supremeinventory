package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Locations;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class LocationController {

	public Map<String, Object> getAllLocationsList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			List<Locations> locationList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray locationListArray = new JSONArray();

			locationList = hSession.createQuery("from Locations where deleted=0 order by name asc").list();

			Iterator<Locations> i = locationList.iterator();
			while(i.hasNext()){
				Locations location = i.next();
				object = JSON.toJSONObject(location);
				locationListArray.put(object);
			}

			result.put("success", true);
			result.put("data", locationListArray);

			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> editSingleLocation(String name, String address, String phone, String code, boolean active, boolean delete, long id) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Locations location = null;

			location = (Locations)hSession.createQuery("from Locations where id=?").setLong(0, id).setMaxResults(1).uniqueResult();

			if(location != null){
				
				location.setName(name.toUpperCase());
				location.setAddress(address.toUpperCase());
				location.setCode(code.toUpperCase());
				location.setContact(phone);
				location.setDeleted(delete);
				location.setActive(active);
				location.setUpdatedOn(new Date());

				hSession.save(location);
				transaction.commit();

				result.put("success", true);
				result.put("message", "Locations saved.");

			}
			else{

				message = "Locations does not exist. Please check name again.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException(message);
				
			}

			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> addSingleNewLocations(String name, String address, String phone, String code) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Locations location = null;

			location = (Locations)hSession.createQuery("from Locations where name=?").setString(0, name).setMaxResults(1).uniqueResult();

			if(location != null){

				message = "Locations Already Exists. Please check name again.";
				throw new ValidationException(message);

			}
			else{

				location = new Locations();
				location.setName(name.toUpperCase());
				location.setAddress(address.toUpperCase());
				location.setContact(phone);
				location.setCode(code.toUpperCase());
				location.setActive(true);
				location.setDeleted(false);
				location.setCreatedOn(new Date());
				location.setUpdatedOn(new Date());

				hSession.save(location);
				transaction.commit();
			}

			result.put("success", true);
			result.put("message", "Locations saved.");
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> deleteLocation(String name, long id) throws ValidationException
	{
		System.out.println("deleteLocation called \tname: "+ name + "\tId: "+id);
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Locations location = (Locations)hSession.createQuery("from Locations where id=?").setLong(0, id).setMaxResults(1).uniqueResult();
			
			if(location != null) {
				
				System.out.println("location name: "+ location.getName());
				
				if(location.isActive() && !location.isDeleted()) {
					location.setActive(false);
					location.setDeleted(true);
					location.setUpdatedOn(new Date());
					hSession.saveOrUpdate(location);
					transaction.commit();
					
					result.put("success", true);
					result.put("message", "Location deleted successfully.");
				}
				else {
					result.put("success", false);
					result.put("message", "Location already deleted.");
				}
			}
			else {
				result.put("success", false);
				result.put("message", "Location could not be found.");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			hSession.close();
		}
		return result;
	}
	
	public boolean bulkLocationsListUpload(String fileName){

		Session hSession = HibernateBridge.getSessionFactory().openSession();

		try{

			// TODO Auto-generated method stub
			String path="";

			try{
				Config config=new Config();
				if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
					path=config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: "+path);
				}else{
					path=config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: "+path);
				}
			}catch(IOException e){
				path = "/opt/supremeinventory/excels/";
			}

			System.out.println(path+File.separator+fileName);
			System.out.println("Path in mf: "+path+File.separator+fileName);
			File sourceFile = new File(path);

			System.out.println("Helloooo");

			if(sourceFile.exists())
			{
				System.out.println("sourceFileExists");
				File file=new File(path+File.separator+fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				boolean flag = false;
				int i=0;

				System.out.println("filetoRead: "+ filetoRead);
				System.out.println("entering while");
				while (rowIterator.hasNext()) {

					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					Locations location = new Locations();

					if(i != 0){
						int j=0;
						System.out.println("i = "+i);
						while (cellIterator.hasNext()) {

							System.out.println("J === "+ j);
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly

							String name = "";

							if(j == 0){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									location.setName(cell.getStringCellValue().trim().toUpperCase());
									name = location.getName();

								}
							}

							if(j == 1){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									location.setCode(cell.getStringCellValue().trim().toUpperCase());
								}
							}

							if(j == 2){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									location.setAddress(cell.getStringCellValue().trim().toUpperCase());
								}
							}

							if(j == 3){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									location.setContact(cell.getStringCellValue());
								}
							}


							location.setActive(true);
							location.setDeleted(false);
							location.setCreatedOn(new Date());
							location.setUpdatedOn(new Date());

							j++;
						}

						System.out.println("FLAG::: "+ flag);

						if(!flag){
							Transaction transaction = hSession.beginTransaction();

							try{
								System.out.println("I = "+ i+" location= "+location.getName());

								hSession.saveOrUpdate(location);

								hSession.flush();

								transaction.commit();
							}
							catch(Exception e){
								e.printStackTrace();
								/*return false;*/
							}
						}
					}
					else{
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			hSession.close();
		}
	}
}
