package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.commons.collections.list.SetUniqueList;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Customer;
import com.supreme.dao.CuttingOrderTask;
import com.supreme.dao.CuttingOrderTaskTypeDetails;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Locations;
import com.supreme.dao.Orders;
import com.supreme.pojo.CustomerDetailsSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class UpdateCustomerController {

	public boolean bulkCustomerListUpload(String fileName) {

		Session hSession = HibernateBridge.getSessionFactory().openSession();

		try {

			// TODO Auto-generated method stub
			String path = "";

			try {
				Config config = new Config();
				if (config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)) {
					path = config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: " + path);
				} else {
					path = config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: " + path);
				}
			} catch (IOException e) {
				path = "/opt/supremeinventory/excels/";
			}

			System.out.println(path + File.separator + fileName);
			System.out.println("Path in mf: " + path + File.separator + fileName);
			File sourceFile = new File(path);

			System.out.println("Helloooo");

			if (sourceFile.exists()) {
				System.out.println("sourceFileExists");
				File file = new File(path + File.separator + fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				boolean flag = false;
				int i = 0;

				System.out.println("filetoRead: " + filetoRead);
				System.out.println("entering while");
				while (rowIterator.hasNext()) {

					String customerString = null;

					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					Customer existingCustomer = new Customer();
					Customer customer = new Customer();
					boolean customerExists = false;
					boolean customerMade = false;
					boolean customerDetailsExists = false;
					boolean customerDetailsMade = false;
					

					Orders order = null;
					
					String name = "";
					long orderId;
					String customerName = null, actualCustomerName, address, vehicleNumber, cuttingOrderString;
					boolean companyPickup = false;

					if (i != 0) {
						int j = 0;
						System.out.println("i = " + i);
						while (cellIterator.hasNext()) {

							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly

							
							

							if (j == 0) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {

									cell.setCellType(Cell.CELL_TYPE_STRING);
									String orderString = cell.getStringCellValue();
									orderId = Long.parseLong(orderString);
									order = (Orders) hSession.get(Orders.class, orderId);

								}
							}

							if (j == 1) {
							}

							if (j == 2) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									customerString = cell.getStringCellValue().trim().toUpperCase();
									customerName = cell.getStringCellValue().trim().toUpperCase();
								}
							}
							
							if(j==5) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									address = cell.getStringCellValue().trim().toUpperCase();
								}
								else {
									address = "";
								}
							}
							
							if(j==6) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									String companyPickupString = cell.getStringCellValue().trim().toLowerCase();
									
									if(companyPickupString.equals("yes")) {
										companyPickup = true;
									}
									else {
										companyPickup = false;
									}
								}
								else {
									companyPickup = false;
								}
							}
							
							if(j==7) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									vehicleNumber = cell.getStringCellValue().trim().toUpperCase();
								}
								else {
									vehicleNumber = "";
								}
							}
							if(j==10) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingOrderString = cell.getStringCellValue().trim().toUpperCase();
								}
								else {
									cuttingOrderString = "";
								}
								
								if(!cuttingOrderString.equals("")) {
									
								}
							}
							
							

							j++;
						}
						
						existingCustomer = (Customer) (Customer) hSession.createQuery("from Customer where name=?")
								.setString(0, customerString).setMaxResults(1).uniqueResult();
						if (existingCustomer != null) {
							System.out.println("customerExists");
							Transaction txn = hSession.beginTransaction();
							order.setCustomerId(existingCustomer.getId());
							hSession.saveOrUpdate(order);
							txn.commit();
						} 
						else {
							
							System.out.println("CUSTOMER BEING CREATED//////////////////: "+ customerMade);
							Transaction txn = hSession.beginTransaction();
							customer = new Customer();
							customer.setName(customerName.toUpperCase());
							customer.setCreatedOn(new Date());
							customer.setUpdatedOn(new Date());
							hSession.saveOrUpdate(customer);
							txn.commit();
							
							txn = hSession.beginTransaction();
							Customer cust = (Customer) hSession.createQuery("from Customer where name=?")
									.setString(0, customerString).setMaxResults(1).uniqueResult();
							order.setCustomerId(cust.getId());
							hSession.saveOrUpdate(order);
							txn.commit();
						}

						System.out.println("FLAG::: "+ flag);
						
						if(!flag){
							Transaction transaction = hSession.beginTransaction();
						
							try{
								System.out.println("I = "+ i+" customer= "+customer.getName());
						
								hSession.saveOrUpdate(customer);
						
								hSession.flush();
						
								transaction.commit();
							}
							catch(Exception e){
								e.printStackTrace();
								return false;
							}
						}
					} else {
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			hSession.close();
		}
	}
	
	public boolean bulkCustomerDetailsListUpload(String fileName) {
		
		
		System.out.println("bulkCustomerDetailsListUpload called");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try {
			
			// TODO Auto-generated method stub
			String path = "";
			
			try {
				Config config = new Config();
				if (config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)) {
					path = config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: " + path);
				} else {
					path = config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: " + path);
				}
			} catch (IOException e) {
				path = "/opt/supremeinventory/excels/";
			}
			
			System.out.println(path + File.separator + fileName);
			System.out.println("Path in mf: " + path + File.separator + fileName);
			File sourceFile = new File(path);
			
			System.out.println("Helloooo");
			
			if (sourceFile.exists()) {
				System.out.println("sourceFileExists");
				File file = new File(path + File.separator + fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);
				
				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);
				
				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();
				
				boolean flag = false;
				int i = 0;
				
				System.out.println("filetoRead: " + filetoRead);
				System.out.println("entering while");
				
				
				while (rowIterator.hasNext()) {
					
					String customerString = null;
					
					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
					String customerName = "", narration = "";
					long orderId = 0;
					
					if (i != 0) {
						int j = 0;
						while (cellIterator.hasNext()) {
							
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly
							
							if (j == 0) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									orderId = (long) cell.getNumericCellValue();
								}
								else flag = true;
							}
							
							if (j == 2) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cell.setCellType(Cell.CELL_TYPE_STRING);
									customerName = cell.getStringCellValue();
								}
							}

							if (j == 10) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cell.setCellType(Cell.CELL_TYPE_STRING);
									narration = cell.getStringCellValue();
								}
							}
							
							
							
							j++;
						}
						if(!flag) {
							
							
							Transaction transaction = hSession.beginTransaction();
							Orders order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
							
							if(order != null) {
								System.out.println("Order exists: "+ order.getId());
								Customer customer = (Customer)hSession.createQuery("from Customer where name=?").setString(0, customerName.toUpperCase()).setMaxResults(1).uniqueResult();

								if(customer != null) {
									System.out.println("DB customer exists: "+ customer.getName());
									order.setCustomerId(customer.getId());
									order.setNarration(narration);
									
									hSession.saveOrUpdate(order);
									transaction.commit();
								}
							}
							
						}
					} else {
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			hSession.close();
		}
	}
	
	public boolean updateCuttingOrderTasks(String fileName) {
		
		System.out.println("updateCuttingOrderTasks called");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try {
			
			// TODO Auto-generated method stub
			String path = "";
			
			try {
				Config config = new Config();
				if (config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)) {
					path = config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: " + path);
				} else {
					path = config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: " + path);
				}
			} catch (IOException e) {
				path = "/opt/supremeinventory/excels/";
			}
			
			System.out.println(path + File.separator + fileName);
			System.out.println("Path in mf: " + path + File.separator + fileName);
			File sourceFile = new File(path);
			
			System.out.println("Helloooo");
			
			if (sourceFile.exists()) {
				System.out.println("sourceFileExists");
				File file = new File(path + File.separator + fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);
				
				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);
				
				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();
				
				boolean flag = false;
				int i = 0;
				
				System.out.println("filetoRead: " + filetoRead);
				System.out.println("entering while");
				
				while (rowIterator.hasNext()) {
					
					Set<CuttingOrderTaskTypeDetails> cottSet = new HashSet<CuttingOrderTaskTypeDetails>();
					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
					long cuttingOrderId = 0;
					long cuttingOrderTaskId = 0;
					long cuttingTypeId = 0;
					long currentCuttingOrderId = 0;
					CuttingOrderTaskTypeDetails cott = null;
					CuttingOrderTask coTask = null;
					
					if (i != 0) {
						double amount = 0;
						CuttingType cType = null;
						int j = 0;
						while (cellIterator.hasNext()) {
							
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly
							
							if(j==0) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingOrderTaskId = (long) cell.getNumericCellValue();
								}
							}
							coTask = (CuttingOrderTask) hSession.get(CuttingOrderTask.class, cuttingOrderTaskId);
							
							
							
							if (j == 1) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingOrderId = (long) cell.getNumericCellValue();
									
									System.out.println("j is "+ j);
									
									if(currentCuttingOrderId == 0) {
										currentCuttingOrderId = cuttingOrderId;
									}
									else if(currentCuttingOrderId == cuttingOrderId) {
										//same order so delete this one and add new row in cuttingordertasktypedetails with same cuttingTaskId
										coTask.setDeleted(true);
										coTask.setDeletedOn(new Date());
									}
									else if(currentCuttingOrderId != cuttingOrderId) {
										//order changes so create new taskTypeId row and do not delete this one
										currentCuttingOrderId = cuttingOrderId;
									}
								}
								else {
									flag = true;
								}
							}
							
							if (j == 2) {
								System.out.println("j is "+ j);
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingTypeId = (long) cell.getNumericCellValue();
									
									cType = (CuttingType) hSession.get(CuttingType.class, cuttingTypeId);
								}
							}
							j++;
						}
						cott = new CuttingOrderTaskTypeDetails(cuttingTypeId, amount, cType);
						cottSet.add(cott);
						
					} else {
						System.out.println("i is 0. moving on");
					}
					
					//ADD TO COTask as Set--------------------------------------------------------- IMPORTANT
					if(!flag) {
						Transaction transaction = hSession.beginTransaction();
						
						if(cottSet != null && cottSet.size() > 0) {
							coTask.setCuttingOrderTaskTypeDetails(cottSet);
							hSession.save(coTask);
							transaction.commit();
						}
					}
					
					i++;
				}
				filetoRead.close();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			hSession.close();
		}
	
	}

}
