package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;

import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.Machines;
import com.supreme.Config;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.dao.User;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class OrderController {
	
	public static void main(String[] args) throws ValidationException {
//		addOrderCutomersToCustomerTable();
	}
	
	public static void addOrderCutomersToCustomerTable() throws ValidationException
	{
		/*
		System.out.println("addOrder controller func entered");
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		
		try
		{
			List<Orders> orderList = hSession.createQuery("from Orders").list();
			
			if(orderList != null && orderList.size() > 0) {
				System.out.println("Orderlist is not null");
				
				Iterator<Orders> orderListItr = orderList.iterator();
				
				long lastId, newId;
				Query idList = hSession.createQuery("select max(id) from Customer");
				if(idList != null && !idList.list().isEmpty()){
					if(idList.list().get(0) != null){
						lastId = (long) idList.list().get(0);
						newId = lastId + 1;
					}
					else{
						lastId = 0;
						newId = 1;
					}
				}
				else{
					lastId = 0;
					newId = 1;
				}
				
				while(orderListItr.hasNext()) {
					
					System.out.println("newId: "+ newId);
					
					Orders order = orderListItr.next();
					
					
					transaction = hSession.beginTransaction();
					
					Customer customer = (Customer) hSession.createQuery("from Customer where name like ?").setString(0, "%"+order.getCustomerName()+"%").setMaxResults(1).uniqueResult();
					
					if(customer != null) {
						//Already exists. DO Nothing
						System.out.println(order.getCustomerName()+"\t"+order.getVehicleNumber()+"\t"+order.getAddress());
						System.out.println("customerexists: do nothing");
						customer.setName(order.getCustomerName() != null ? order.getCustomerName() : "");
		//						customer.setVehicleNumber(order.getVehicleNumber() != null ? order.getVehicleNumber() : "");
						
						hSession.saveOrUpdate(customer);
						transaction.commit();
					}
					else {
						
						Customer newCustomer = new Customer();
						String customerName = order.getCustomerName() != null ? order.getCustomerName() : "";
						
						System.out.println("customerName: "+ customerName);
						
						newCustomer.setName(order.getCustomerName() != null ? order.getCustomerName() : "");
		//						newCustomer.setVehicleNumber(order.getVehicleNumber() != null ? order.getVehicleNumber() : "");
						
						newCustomer.setCreatedOn(new Date());
						newCustomer.setUpdatedOn(new Date());
						
						order.setCustomerId(newId);
						order.setCustomer(newCustomer);
						hSession.saveOrUpdate(newCustomer);
						hSession.saveOrUpdate(order);
						transaction.commit();
						
						newId++;
					}
					
				}
			}
			else {
				System.out.println("Orderlist is null");
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
		*/}
	
	public Map<String, Object> addOrder(String customerName, Set<Cart> cartSet, double totalMtSquare, double count, long customerId, long userId, 
										String cuttingOrderNumber, boolean hasCuttingSlabs) throws ValidationException
	{
		System.out.println("addOrder controller func entered");
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Cart> cartIterator = cartSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			if(customerId != 0) {
				Customer existingCustomer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, customerId).setMaxResults(1).uniqueResult();
				
				if(existingCustomer != null) {
					
					Transaction transaction = hSession.beginTransaction();
					List<Orders> orderList = null;
					Orders order = null;
					
					order = new Orders();
					order.setScannedBy(userId);
					order.setNarration(cuttingOrderNumber);
					order.setCustomerId(customerId);
					order.setCart(cartSet);
					order.setHasCuttingSlabs(hasCuttingSlabs);
					order.setCount(cartSet.size());
					order.setCount(count);
					order.setTotalMtSquare(totalMtSquare);
					order.setCreatedOn(new Date());
					order.setUpdatedOn(new Date());
					
					hSession.save(order);
					transaction.commit();
					
					
					result.put("success", true);
					
					Iterator<Cart> i = cartSet.iterator();
					while (i.hasNext()) {
						Cart cart = i.next();
						
						if(cart.isTile()) {
							
							transaction = hSession.beginTransaction();
							System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
							Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
							
							int currentTileCount = inventory.getTileCount();
							int newTileCount = currentTileCount - cart.getTileCount();
							
							if(newTileCount < 0) {
								result.put("success", false);
								result.put("message", "Final Inventory Tilecount cannot be less than 0");
								throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
							}
							else {
								inventory.setTileCount(newTileCount);
								if(newTileCount == 0) {
									inventory.setSold(true);
								}
								
								hSession.save(inventory);
								transaction.commit();
								result.put("success", true);
							}
							
						}
					}
				}
				
			}
			else {
				
				Transaction transaction = hSession.beginTransaction();
				Customer newCustomer = new Customer();
				
				long lastId, newId;
				Query idList = hSession.createQuery("select max(id) from Customer");
				if(idList != null && !idList.list().isEmpty()){
					if(idList.list().get(0) != null){
						lastId = (long) idList.list().get(0);
						newId = lastId + 1;
					}
					else{
						lastId = 0;
						newId = 1;
					}
				}
				else{
					lastId = 0;
					newId = 1;
				}
				
				
				newCustomer.setName(customerName.toUpperCase());
				newCustomer.setCreatedOn(new Date());
				newCustomer.setUpdatedOn(new Date());
				newCustomer.setDeleted(false);
				newCustomer.setWalkIn(false);

				hSession.saveOrUpdate(newCustomer);
				transaction.commit();
				
				System.out.println("saved new customer");
				
				Customer newSavedCust = (Customer)hSession.createQuery("from Customer where name=?").setString(0, customerName).setMaxResults(1).uniqueResult();
				
				List<Orders> orderList = null;
				Orders order = null;
				
				transaction = hSession.beginTransaction();
				
				order = new Orders();
				order.setScannedBy(userId);
				order.setCustomerId(newCustomer.getId());
				order.setCart(cartSet);
				order.setCount(cartSet.size());
				order.setCount(count);
				order.setTotalMtSquare(totalMtSquare);
				order.setCreatedOn(new Date());
				order.setUpdatedOn(new Date());
				
				
				
				hSession.save(order);
				transaction.commit();
				
				transaction = hSession.beginTransaction();
				result.put("success", true);
				
				Iterator<Cart> i = cartSet.iterator();
				while (i.hasNext()) {
					Cart cart = i.next();
					
					if(cart.isTile()) {
						
						transaction = hSession.beginTransaction();
						System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
						Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
						
						int currentTileCount = inventory.getTileCount();
						int newTileCount = currentTileCount - cart.getTileCount();
						
						if(newTileCount < 0) {
							result.put("success", false);
							result.put("message", "Final Inventory Tilecount cannot be less than 0");
							throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
						}
						else {
							inventory.setTileCount(newTileCount);
							if(newTileCount == 0) {
								inventory.setSold(true);
							}
							
							hSession.save(inventory);
							transaction.commit();
							result.put("success", true);
						}
						
					}
				}
				
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			if(result.get("message") == null || result.get("message").equals("")) {
				result.put("message", "Order of "+customerName+" could not be saved.");
			}
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> addOrderWithCuttingOrder(String customerName, Set<Cart> cartSet, double totalMtSquare, double count, long customerId, long userId, 
			String cuttingOrderNumber, boolean hasCuttingSlabs, Set<CuttingOrder> cuttingOrder) throws ValidationException
	{
		System.out.println("addOrder controller func entered");
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Cart> cartIterator = cartSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			if(customerId != 0) {
				Customer existingCustomer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, customerId).setMaxResults(1).uniqueResult();
				
				if(existingCustomer != null) {
					
					Transaction transaction = hSession.beginTransaction();
					List<Orders> orderList = null;
					Orders order = null;
					
					order = new Orders();
					order.setScannedBy(userId);
					order.setNarration(cuttingOrderNumber);
					order.setCustomerId(customerId);
					order.setCart(cartSet);
					order.setHasCuttingSlabs(hasCuttingSlabs);
					order.setCount(cartSet.size());
					order.setCount(count);
					order.setTotalMtSquare(totalMtSquare);
					order.setCreatedOn(new Date());
					order.setUpdatedOn(new Date());
					
					hSession.save(order);
					transaction.commit();
					
					
					result.put("success", true);
					
					Iterator<Cart> i = cartSet.iterator();
					while (i.hasNext()) {
						Cart cart = i.next();
						
						if(cart.isTile()) {
							
							transaction = hSession.beginTransaction();
							System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
							Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
							
							int currentTileCount = inventory.getTileCount();
							int newTileCount = currentTileCount - cart.getTileCount();
							
							if(newTileCount < 0) {
								result.put("success", false);
								result.put("message", "Final Inventory Tilecount cannot be less than 0");
								throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
							}
							else {
								inventory.setTileCount(newTileCount);
								if(newTileCount == 0) {
									inventory.setSold(true);
								}
								
								hSession.save(inventory);
								transaction.commit();
								result.put("success", true);
							}
							
						}
					}
				}
				
			}
			else {
				
				Transaction transaction = hSession.beginTransaction();
				Customer newCustomer = new Customer();
				
				long lastId, newId;
				Query idList = hSession.createQuery("select max(id) from Customer");
				if(idList != null && !idList.list().isEmpty()){
					if(idList.list().get(0) != null){
						lastId = (long) idList.list().get(0);
						newId = lastId + 1;
					}
					else{
						lastId = 0;
						newId = 1;
					}
				}
				else{
					lastId = 0;
					newId = 1;
				}
				
				
				newCustomer.setName(customerName.toUpperCase());
				newCustomer.setCreatedOn(new Date());
				newCustomer.setUpdatedOn(new Date());
				newCustomer.setDeleted(false);
				newCustomer.setWalkIn(false);
				
				hSession.saveOrUpdate(newCustomer);
				transaction.commit();
				
				System.out.println("saved new customer");
				
				Customer newSavedCust = (Customer)hSession.createQuery("from Customer where name=?").setString(0, customerName).setMaxResults(1).uniqueResult();
				
				List<Orders> orderList = null;
				Orders order = null;
				
				transaction = hSession.beginTransaction();
				
				order = new Orders();
				order.setScannedBy(userId);
				order.setCustomerId(newCustomer.getId());
				order.setCart(cartSet);
				order.setCount(cartSet.size());
				order.setCount(count);
				order.setTotalMtSquare(totalMtSquare);
				order.setCreatedOn(new Date());
				order.setUpdatedOn(new Date());
				
				
				
				hSession.save(order);
				transaction.commit();
				
				transaction = hSession.beginTransaction();
				result.put("success", true);
				
				Iterator<Cart> i = cartSet.iterator();
				while (i.hasNext()) {
					Cart cart = i.next();
					
					if(cart.isTile()) {
						
						transaction = hSession.beginTransaction();
						System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
						Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
						
						int currentTileCount = inventory.getTileCount();
						int newTileCount = currentTileCount - cart.getTileCount();
						
						if(newTileCount < 0) {
							result.put("success", false);
							result.put("message", "Final Inventory Tilecount cannot be less than 0");
							throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
						}
						else {
							inventory.setTileCount(newTileCount);
							if(newTileCount == 0) {
								inventory.setSold(true);
							}
							
							hSession.save(inventory);
							transaction.commit();
							result.put("success", true);
						}
						
					}
				}
				
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			if(result.get("message") == null || result.get("message").equals("")) {
				result.put("message", "Order of "+customerName+" could not be saved.");
			}
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> addCuttingOrder(String customerName, Set<Cart> cartSet, double totalMtSquare, double count) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Cart> cartIterator = cartSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<Orders> orderList = null;
			Orders order = null;
			
			order = new Orders();
//			order.setCustomerName(customerName);
			order.setCart(cartSet);
			order.setCount(cartSet.size());
			order.setCount(count);
			order.setTotalMtSquare(totalMtSquare);
			order.setCreatedOn(new Date());
			order.setUpdatedOn(new Date());

			hSession.save(order);
			transaction.commit();
			
			Iterator<Cart> i = cartSet.iterator();
			while (i.hasNext()) {
				Cart cart = i.next();
				
				if(cart.isTile()) {
					
					transaction = hSession.beginTransaction();
					System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
					Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
					
					int originalTileCount = inventory.getTileCount();
					int newTileCount = originalTileCount - cart.getTileCount();
					inventory.setTileCount(newTileCount);
					
					if(newTileCount == 0) {
						inventory.setSold(true);
					}
					
					inventory.setInCutting(true);
					
					hSession.save(inventory);
					transaction.commit();
				}
			}
			
			
			result.put("success", true);
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", "Cutting Order of "+customerName+" could not be saved.");
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	
	public Map<String, Object> fetchAllOrders() throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			JSONArray orderListArray = new JSONArray();
			org.json.JSONObject object = new org.json.JSONObject();
			
			List<Orders> orderList = null;
			Orders order = null;
			
			orderList = (List<Orders>)hSession.createQuery("from Orders where placed=0 and deleted=0").list();
		
			if(orderList.size() > 0){

				Iterator<Orders> i = orderList.iterator();
				while (i.hasNext()) {
					order = i.next();
					object = JSON.toJSONObject(order);
					orderListArray.put(object);
				}

				result.put("success", true);
				result.put("data", orderListArray);
				
			}
			else{
				
				message = "No New Orders";
				result.put("success", false);
				result.put("message", message);
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> fetchApprovedOrders() throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			JSONArray orderListArray = new JSONArray();
			org.json.JSONObject object = new org.json.JSONObject();
			
			List<Orders> orderList = null;
			Orders order = null;
			
			orderList = (List<Orders>)hSession.createQuery("from Orders where placed=true and deleted=false and merged=false").list();
			
			if(orderList.size() > 0){
				
				Iterator<Orders> i = orderList.iterator();
				while (i.hasNext()) {
					order = i.next();
					object = JSON.toJSONObject(order);
					orderListArray.put(object);
				}
				
				result.put("success", true);
				result.put("data", orderListArray);
				
			}
			else{
				
				message = "No Approved Orders";
				result.put("success", false);
				result.put("message", message);
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> getOrderDetails(long orderId) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			JSONArray orderListArray = new JSONArray();
			org.json.JSONObject object = new org.json.JSONObject();
			
			Orders order = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
		
			if(order != null){
				result.put("success", true);
				result.put("data", JSON.toJSONObject(order));
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("data", "");
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> fetchFilteredApprovedOrders(long customerId, long materialId, Date createdOn, boolean allOrders, boolean placed) {
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		org.json.JSONObject object = new org.json.JSONObject();

		try {
			String q = "from Orders where id is not null ";
			if (customerId != 0) {
				q += " and customerId=:customerId ";
			}
			if (!allOrders) {
				q += " and createdOn>=:createdOn ";
			}
			if (placed) {
				q += " and placed=:placed ";
			}

			Query query = hSession.createQuery(q);
			int i = 1;

			if (customerId != 0) {
				query.setLong("customerId", customerId);
				i++;
			}
			if (!allOrders) {
				query.setDate("createdOn", createdOn);
				i++;
			}
			if (placed) {
				query.setBoolean("placed", placed);
				i++;
			}

			JSONArray orderListArray = new JSONArray();
			System.out.println("--------------------- qury list size: " + query.list().size());
			if (query.list().size() <= 0) {
				System.out.println("No Approved Orders. Please change search criteria.");
				result.put("success", false);
				result.put("data", "");
				result.put("error", "No Approved Orders. Please change search criteria.");
			} else {

				if(query.list().size() > 0){
					
					System.out.println("materialId: "+ materialId);
					if(materialId !=0) {
						
						Orders order = null;
						Iterator<Orders> ordersItr = query.list().iterator();
						while (ordersItr.hasNext()) {
							order = ordersItr.next();
							
							Iterator<Cart> cartItr = order.getCart().iterator();
							while(cartItr.hasNext()) {
								Cart cart = cartItr.next();
								if(cart.getInventory() != null  
										&& cart.getInventory().getMaterialId() != null
										&& cart.getInventory().getMaterialId() == materialId) {
									object = JSON.toJSONObject(order);
									orderListArray.put(object);
									System.out.println("adding data to order");
								}
								else {
									System.out.println("not adding data to order");
								}
							}
						}
						result.put("success", true);
						result.put("data", orderListArray);
						
					}
					else {
						
						Orders order = null;
						Iterator<Orders> ordersItr = query.list().iterator();
						while (ordersItr.hasNext()) {
							order = ordersItr.next();
							object = JSON.toJSONObject(order);
							orderListArray.put(object);
						}
						
						result.put("success", true);
						result.put("data", orderListArray);
					}
					
					
				}
				else{
					
					String message = "No Approved Orders. Please change search criteria.";
					result.put("success", false);
					result.put("message", message);
				}
				
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}
	
	public Map<String, Object> fetchApprovedOrdersFilterByDate(Date createdOn, Long customerId, boolean allOrders) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			JSONArray orderListArray = new JSONArray();
			org.json.JSONObject object = new org.json.JSONObject();
			
			List<Orders> orderList = null;
			Orders order = null;
			
			
			System.out.println("CreatedOn: "+ createdOn);
			
			if(customerId != 0) {
				
				if(allOrders) {
					orderList = (List<Orders>)hSession.createQuery("from Orders where placed=true and deleted=false and merged=false and customerId=?").setLong(0, customerId).list();
				}
				else {
					orderList = (List<Orders>)hSession.createQuery("from Orders where placed=true and deleted=false and createdOn>? and merged=false and customerId=?").setDate(0, createdOn).setLong(1, customerId).list();
				}
			}
			else {
				if(allOrders) {
					orderList = (List<Orders>)hSession.createQuery("from Orders where placed=true and deleted=false and merged=false").list();
				}
				else {
					orderList = (List<Orders>)hSession.createQuery("from Orders where placed=true and deleted=false and createdOn>? and merged=false").setDate(0, createdOn).list();
				}
			}
			
			
			if(orderList.size() > 0){
				
				Iterator<Orders> i = orderList.iterator();
				while (i.hasNext()) {
					order = i.next();
					object = JSON.toJSONObject(order);
					orderListArray.put(object);
				}
				
				result.put("success", true);
				result.put("data", orderListArray);
				
			}
			else{
				
				message = "No Approved Orders";
				result.put("success", false);
				result.put("message", message);
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> saveDOAdditionalData(long orderId, String vehicleNumber, String address, Date LPODate, String LPONumber) throws ValidationException{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			JSONArray orderListArray = new JSONArray();
			org.json.JSONObject object = new org.json.JSONObject();
			
			Orders order = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
		
			if(order != null){
				
//				order.setAddress(address);
//				order.setVehicleNumber(vehicleNumber);
				
				/*if(LPODate != null){
					order.setLPODate(LPODate);
				}
				if(LPONumber != null){
					order.setLPONumber(LPONumber);
				}*/
				
				hSession.save(order);
				transaction.commit();
				
				order = (Orders)hSession.get(Orders.class, order.getId());
				
				message = "Additional details saved";
				result.put("success", true);
				result.put("message", message);
				result.put("order", order);
			}
			else{
				
				message = "Additional details could not be saved";
				result.put("success", false);
				result.put("message", message);
				result.put("order", order);
			}
			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			result.put("order", null);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> moveOrderToInvoice(long orderId) throws ValidationException
	{
		
		System.out.println("orderId: "+orderId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		
		try{
			
			Transaction transaction = hSession.beginTransaction();
			Orders order = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(order != null){
				order.setPlaced(true);
				order.setPlaced(true);
				order.setUpdatedOn(new Date());
			
				hSession.save(order);
				transaction.commit();
				
				
				message = "Order moved to Invoice";
				result.put("success", true);
				result.put("message", message);
	
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	
	public Map<String, Object> deleteOrder(long orderId) throws ValidationException
	{
		
		System.out.println("orderId: "+orderId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		
		try{
			
			Transaction transaction = hSession.beginTransaction();
			Orders order = null;
			Cart cart = null;
			List<Cart> cartList = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(order != null){
				
				if(order.isDeleted()){
					result.put("success", false);
					result.put("message", "Order has already been deleted.");
				}
				else{
					
					cartList = (List<Cart>)hSession.createQuery("from Cart where orderId=?").setLong(0, orderId).list();
					
					order.setDeleted(true);
					order.setUpdatedOn(new Date());
					
					hSession.save(order);
					
					Iterator<Cart> i = cartList.iterator();
					while (i.hasNext()) {
						cart = i.next();
						cart.setDeleted(true);
						
						if(cart.isTile()) {
							Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
							
							int inventorynewTileCount = inventory.getTileCount() + cart.getTileCount();
							inventory.setTileCount(inventorynewTileCount);
							inventory.setSold(false);
							
							hSession.saveOrUpdate(inventory);
						}
						
						hSession.saveOrUpdate(cart);
					}
					
					transaction.commit();
					
					message = "Order deleted successfully.";
					result.put("success", true);
					result.put("message", message);
				}
	
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> deleteCartRow(long cartId) throws ValidationException
	{
		
		System.out.println("orderId: "+cartId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		
		try{
			
			Transaction transaction = hSession.beginTransaction();
			Cart cart = null;
			Orders order = null;
			
			cart = (Cart)hSession.createQuery("from Cart where id=?").setLong(0, cartId).setMaxResults(1).uniqueResult();
			
			if(cart != null){
				System.out.println("cart id: "+ cart.getId());
				order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, cart.getOrderId()).setMaxResults(1).uniqueResult();
				if(cart.isDeleted()){
					result.put("success", false);
					result.put("message", "Order has already been deleted earlier.");
				}
				else{
					
					if(cart.isTile()) {
						Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
						int newTileCount = inventory.getTileCount() + cart.getTileCount();
						inventory.setTileCount(newTileCount);
						hSession.save(inventory);
					}

					cart.setDeleted(true);
					cart.setUpdatedOn(new Date());
					hSession.save(cart);
					
					double count = order.getCount();
					double newCount = --count;
					order.setCount(newCount);
					
					hSession.save(order);
					
					Inventory inventory = (Inventory) hSession.createQuery("from Inventory where id=?").setLong(0, cart.getInventoryId()).setMaxResults(1).uniqueResult();
					inventory.setSold(false);
					hSession.save(inventory);
					
					transaction.commit();
					
					message = "Cart order deleted successfully.";
					result.put("success", true);
					result.put("message", message);
				}
	
			}
			else{
				message = "Cart Row does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Cart Row does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> updateOrderRow(long orderId, String customerName, double slabCount, double totalMtSquare, 
			String vehicleNumber, String emirate, String LPONumber, Date LPODate, boolean approveOrder, long userId, 
			String phone, boolean companyPickup, String narration, boolean hasCuttingOrders,
			boolean VATRegistration, Date VATRegistrationDOneOn, String trnNumber) throws ValidationException{
		
		
		
		System.out.println("orderId: "+orderId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		
		try{
			
			Transaction transaction = hSession.beginTransaction();
			Cart cart = null;
			Orders order = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(order != null){
				
				Customer customer = (Customer) hSession.createQuery("from Customer where id=?").setLong(0, order.getCustomerId()).setMaxResults(1).uniqueResult();
				
				if(customer != null) {
					
					customer.setUpdatedOn(new Date());
					customer.setPhone(phone);
					
					if(VATRegistration) {
						customer.setVATRegistrationDate(VATRegistrationDOneOn);
						customer.setVATRegistration(VATRegistration);
						customer.setTrnNumber(trnNumber);
					}
					else {
						customer.setVATRegistration(VATRegistration);
					}
					
					order.setCustomerId(customer.getId());
					
					hSession.save(customer);
					hSession.save(order);
					transaction.commit();
				}
				else {
					customer = new Customer();
					
					customer.setEmirate(emirate);
					customer.setCreatedOn(new Date());
					customer.setUpdatedOn(new Date());
					customer.setPhone(phone);
					customer.setName(customerName);
					
					if(VATRegistration) {
						customer.setVATRegistrationDate(VATRegistrationDOneOn);
						customer.setVATRegistration(VATRegistration);
						customer.setTrnNumber(trnNumber);
					}
					else {
						customer.setVATRegistration(VATRegistration);
					}
					
					hSession.save(customer);
					transaction.commit();
					
				}
				
				
				transaction = hSession.beginTransaction();
				System.out.println("cart id: "+ order.getId());
				
				order.setNarration(narration);
				order.setCount(slabCount);
				order.setHasCuttingSlabs(hasCuttingOrders);
				order.setTotalMtSquare(totalMtSquare);
				
				if(companyPickup) {
					order.setCompanyPickup(companyPickup);
				}
				else {
					order.setCompanyPickup(companyPickup);
					order.setVehicleNumber(vehicleNumber);
				}
				
				if(LPODate != null){
					order.setLPODate(LPODate);
				}
				if(LPONumber != null && !LPONumber.equals("")){
					order.setLPONumber(LPONumber);
				}
				order.setUpdatedOn(new Date());
				
				if(approveOrder) {
					order.setPlaced(approveOrder);
					order.setPlacedBy(userId);
					order.setPlacedOn(new Date());
				}
				
				hSession.save(order);
				transaction.commit();
				
				Iterator<Cart> i = order.getCart().iterator();
				while (i.hasNext()) {
					
					transaction = hSession.beginTransaction();
					cart = i.next();
					
					Inventory inventory = (Inventory)hSession.createQuery("from Inventory where id=?").setLong(0, cart.getInventoryId()).setMaxResults(1).uniqueResult();
					
					if(inventory != null){
						
						if(inventory.getTileCount() == 0) {
							inventory.setSold(true);
						}
						
						hSession.saveOrUpdate(inventory);
						transaction.commit();
					}
					else{
						message = "Order does not exist.";
						result.put("success", false);
						result.put("message", message);
						throw new ValidationException(message);
					}
				}
				
				
					
				message = "Order saved successfully.";
				result.put("success", true);
				result.put("message", message);
				result.put("orderId", order.getId());
	
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	
	}
	
	public Map<String, Object> updateOrderCartRow(long cartId, long inventoryId, double actualHeight, double actualLength, 
													long thickness, double actualSqMetre, String type) throws ValidationException{
		
		System.out.println("updateOrderCartRow cartId: "+ cartId);
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		Transaction transaction = hSession.beginTransaction();
		
		try{
			
			Cart cart = null;
			Orders order = null;
			
			cart = (Cart)hSession.createQuery("from Cart where id=?").setLong(0, cartId).setMaxResults(1).uniqueResult();
			
			if(cart != null){
				System.out.println("cart id: "+ cart.getId());
				
				Inventory inventory = (Inventory)hSession.createQuery("from Inventory where id=?").setLong(0, cart.getInventoryId()).setMaxResults(1).uniqueResult();
				
				inventory.setActualHeight(actualHeight);
				inventory.setActualLength(actualLength);
				inventory.setActualSquareMetre(actualSqMetre);
				inventory.setThickness(thickness);
				
				if(type.equals("TILE")) {
					
				}

				
				hSession.save(cart);
				hSession.save(inventory);
					
				transaction.commit();
					
				message = "Order saved successfully.";
				result.put("success", true);
				result.put("message", message);
	
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	
	}
		

	public Map<String, Object> mergeOrder(long mainOrderId, long secondaryOrderId) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{

			Orders mainOrder = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, mainOrderId).setMaxResults(1).uniqueResult();
			Orders secondaryOrder = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, secondaryOrderId).setMaxResults(1).uniqueResult();
			List<Cart> cartList = (List<Cart>)hSession.createQuery("from Cart where orderId=?").setLong(0, secondaryOrderId).list();
		
			Date currentDate = new Date();
			if(cartList != null && cartList.size() > 0){

				Transaction transaction;
				Cart cart = new Cart();
				Iterator<Cart> i = cartList.iterator();
				Double totalSqMeter = 0.0;
				double slabCount = 0;
				while (i.hasNext()) {
					transaction = hSession.beginTransaction();
					cart = i.next();
					cart.setOrderId(mainOrderId);
					cart.setUpdatedOn(currentDate);
					
					
					if(cart.getInventory().getType().equals("TILE")) {
						totalSqMeter += cart.getInventory().getActualHeight() * cart.getInventory().getActualLength() / 10000 * cart.getTileCount();
						slabCount += cart.getTileCount();
					}
					else {
						totalSqMeter += cart.getInventory().getActualHeight() * cart.getInventory().getActualLength() / 10000;
						slabCount += 1;
					}
					
					hSession.saveOrUpdate(cart);
					transaction.commit();
				}
				
				
				transaction = hSession.beginTransaction();

				totalSqMeter = mainOrder.getTotalMtSquare() + secondaryOrder.getTotalMtSquare();
				slabCount = mainOrder.getCount() + secondaryOrder.getCount();
				mainOrder.setCount(mainOrder.getCount() + secondaryOrder.getCount());
				mainOrder.setTotalMtSquare(mainOrder.getTotalMtSquare() + secondaryOrder.getTotalMtSquare());
				mainOrder.setUpdatedOn(currentDate);
				
				secondaryOrder.setMerged(true);
				secondaryOrder.setMergedOn(currentDate);
				secondaryOrder.setUpdatedOn(currentDate);
				secondaryOrder.setMergedWithId(mainOrderId);

				hSession.saveOrUpdate(secondaryOrder);
				hSession.saveOrUpdate(mainOrder);
				transaction.commit();
				
				
				result.put("success", true);
				result.put("id", mainOrderId);
				
			}
			else{
				result.put("success", false);
				result.put("message", "Order does not exist.");
			}			
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", "Some Problem occured. Please contact Admin.");
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

}
