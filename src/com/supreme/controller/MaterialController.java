package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Material;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class MaterialController {

	public Map<String, Object> getAllMaterialList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			List<Material> materialList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray materialListArray = new JSONArray();

			materialList = hSession.createQuery("from Material order by materialName asc").list();

			Iterator<Material> i = materialList.iterator();
			while(i.hasNext()){
				Material material = i.next();
				object = JSON.toJSONObject(material);
				materialListArray.put(object);
			}

			result.put("success", true);
			result.put("data", materialListArray);

			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> editSingleMaterial(String materialName, String originCountry, double lowerPriceRange, double higherPriceRange, long id) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Material material = null;

			material = (Material)hSession.createQuery("from Material where materialId=?").setLong(0, id).setMaxResults(1).uniqueResult();

			if(material != null){
				
				material.setMaterialName(materialName.toUpperCase());
				material.setOriginCountry(originCountry.toUpperCase());
				material.setLowerPrice(lowerPriceRange);
				material.setUpperPrice(higherPriceRange);
				material.setUpdatedOn(new Date());

				hSession.save(material);
				transaction.commit();

				result.put("success", true);
				result.put("message", "Material saved.");

			}
			else{

				message = "Material does not exist. Please check name again.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException(message);
				
			}

			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> addSingleNewMaterial(String materialName, String originCountry, double lowerPriceRange, double higherPriceRange) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Material material = null;

			material = (Material)hSession.createQuery("from Material where materialName=?").setString(0, materialName).setMaxResults(1).uniqueResult();

			if(material != null){

				message = "Material Already Exists. Please check name again.";
				throw new ValidationException(message);

			}
			else{

				material = new Material();
				material.setMaterialName(materialName.toUpperCase());
				material.setOriginCountry(originCountry.toUpperCase());
				material.setLowerPrice(lowerPriceRange);
				material.setUpperPrice(higherPriceRange);
				material.setCreatedOn(new Date());
				material.setUpdatedOn(new Date());

				hSession.save(material);
				transaction.commit();
			}

			result.put("success", true);
			result.put("message", "Material saved.");
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", "Material could not be saved.");
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public boolean bulkMaterialListUpload(String fileName){

		Session hSession = HibernateBridge.getSessionFactory().openSession();

		try{

			// TODO Auto-generated method stub
			String path="";

			try{
				Config config=new Config();
				if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
					path=config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: "+path);
				}else{
					path=config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: "+path);
				}
			}catch(IOException e){
				path = "/opt/supremeinventory/excels/";
			}

			System.out.println(path+File.separator+fileName);
			System.out.println("Path in mf: "+path+File.separator+fileName);
			File sourceFile = new File(path);

			System.out.println("Helloooo");

			if(sourceFile.exists())
			{
				System.out.println("sourceFileExists");
				File file=new File(path+File.separator+fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				boolean flag = false;
				int i=0;

				System.out.println("filetoRead: "+ filetoRead);
				System.out.println("entering while");
				while (rowIterator.hasNext()) {

					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					Material material = new Material();

					if(i != 0){
						int j=0;
						System.out.println("i = "+i);
						while (cellIterator.hasNext()) {

							System.out.println("J === "+ j);
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly

							String materialName = "";

							if(j == 0){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									material.setMaterialName(cell.getStringCellValue().trim().toUpperCase());
									materialName = material.getMaterialName();

								}
							}

							if(j == 1){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									material.setOriginCountry(cell.getStringCellValue().trim().toUpperCase());
								}
							}

							if(j == 2){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									material.setLowerPrice(cell.getNumericCellValue());
								}
								else{
									material.setLowerPrice(0);
								}
							}

							if(j == 3){
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									material.setUpperPrice(cell.getNumericCellValue());
								}
								else{
									material.setUpperPrice(0);
								}
							}



							material.setCreatedOn(new Date());
							material.setUpdatedOn(new Date());

							j++;
						}

						System.out.println("FLAG::: "+ flag);

						if(!flag){
							Transaction transaction = hSession.beginTransaction();

							try{
								System.out.println("I = "+ i+" inventory= "+material.getMaterialName());

								hSession.saveOrUpdate(material);

								hSession.flush();

								transaction.commit();
							}
							catch(Exception e){
								e.printStackTrace();
								/*return false;*/
							}
						}
					}
					else{
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			hSession.close();
		}
	}
}
