package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.commons.collections.list.SetUniqueList;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Customer;
import com.supreme.dao.CuttingOrderTask;
import com.supreme.dao.CuttingOrderTaskTypeDetails;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Locations;
import com.supreme.dao.Orders;
import com.supreme.pojo.CustomerDetailsSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class UpdateCuttingOrderTaskTypesController {

	public boolean updateCuttingOrderTasks(String fileName) {
		
		System.out.println("updateCuttingOrderTasks called");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try {
			
			// TODO Auto-generated method stub
			String path = "";
			
			try {
				Config config = new Config();
				if (config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)) {
					path = config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: " + path);
				} else {
					path = config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: " + path);
				}
			} catch (IOException e) {
				path = "/opt/supremeinventory/excels/";
			}
			
			System.out.println(path + File.separator + fileName);
			System.out.println("Path in mf: " + path + File.separator + fileName);
			File sourceFile = new File(path);
			
			System.out.println("Helloooo");
			
			if (sourceFile.exists()) {
				System.out.println("sourceFileExists");
				File file = new File(path + File.separator + fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);
				
				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);
				
				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();
				
				boolean flag = false;
				int i = 0;
				
				System.out.println("filetoRead: " + filetoRead);
				System.out.println("entering while");
				
				CuttingOrderTask oldCOTask = null;
				CuttingOrderTaskTypeDetails cott = null;
				CuttingOrderTask coTask = null;
				
				long cuttingOrderId = 0;
				long cuttingOrderTaskId = 0;
				long cuttingTypeId = 0;
				long currentCuttingOrderId = 0;
				while (rowIterator.hasNext()) {
					
					Set<CuttingOrderTaskTypeDetails> cottSet = new HashSet<CuttingOrderTaskTypeDetails>();
					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
						double amount = 0;
						CuttingType cType = null;
						int j = 0;
						while (cellIterator.hasNext()) {
							
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly
							
							if(j==0) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingOrderTaskId = (long) cell.getNumericCellValue();
									System.out.println("cuttingOrderTaskId: "+cuttingOrderTaskId);
									coTask = (CuttingOrderTask) hSession.get(CuttingOrderTask.class, cuttingOrderTaskId);
								}
							}
							
							if(j==1) {
								if (cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
									cuttingOrderId = (long) cell.getNumericCellValue();
									System.out.println("cuttingOrderTaskId: "+cuttingOrderTaskId);
								}
							}
							
							
							j++;
						}
					
					//ADD TO COTask as Set--------------------------------------------------------- IMPORTANT
					
					
					i++;
				}
				filetoRead.close();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			hSession.close();
		}
	
	}

}
