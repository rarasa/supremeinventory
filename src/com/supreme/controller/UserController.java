package com.supreme.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.dao.User;
import com.supreme.dao.UserRole;
import com.supreme.dao.User;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class UserController {

	SimpleDateFormat normalDate = new SimpleDateFormat("dd/MM/yyyy");
	private static final String LOGIN_ERROR = "Email address or password is incorrect. If you have another account with same email, please login via User Id.";
	private static final String SIGNUP_USER_EXISTS_ERROR = "Email address is already registered. Please login.";
	private static final String CHANGE_PASSWORD_ERROR = "You have entered an incorrect old password.";
	private static final String EMAIL_VERIFICATION_ERROR = "Your email verification code is incorrect.";
	private static final String PASSWORD_RESET_ERROR = "Invalid password reset URL.";
	private static final String EMAIL_VERIFIED_ERROR = "Your email is already verified.";
	private static final String MOBILE_VERIFICATION_ERROR = "Your mobile number verification code is incorrect.";
	private static final String MOBILE_VERIFIED_ERROR = "Your mobile number is already verified.";
	private static final String USER_NOT_FOUND_ERROR = "User not found. Please login again.";
	private static final String NOMINEE_NOT_FOUND = "Nominee not found.";
	private static final String EMAIL_NOT_REGISTERED_ERROR = "This email is not registered. Please make sure you are entering a correct email address.";
	private static final String PASSWORD_INCORRECT_ERROR = "Your password is incorrect.";

	
	public static void main(String[] args) {
		getAllUserRoleList();
	}
	
	public User login(String email, String password) throws ValidationException
	{
		
		System.out.println("Email: "+ email);
		System.out.println("Password: "+ password);
		System.out.println("Password HASH: "+ password.hashCode());
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		System.out.println(hSession.toString());
		
		Transaction transaction = hSession.beginTransaction();
		
		
		try
		{
			User user = null;
			
			System.out.println("User is null, fetching values from DB");
			user = (User) hSession.createQuery("from User where email=? and password=?").setString(0, email).setString(1, password.hashCode() + "").uniqueResult();

			if(user != null)
			{
				System.out.println("User is not null");
				if(user.getActive()){
					System.out.println("User is active");
					
					if(!user.getPassword().equals(password.hashCode() + "")){
						System.out.println("password mismatch");
						throw new ValidationException(UserController.PASSWORD_INCORRECT_ERROR);
					}
					else{
						System.out.println("password correct");
					}
					
					user.setLastLogin(new Date());
					hSession.save(user);
					transaction.commit();

					return user;
				}
				else{
					throw new Exception("Your account has been temporarily disabled. Please write back to us on service@germinatewealth.com");
				}
			}
			else
			{
				System.out.println("User is null");
				transaction.rollback();
				throw new ValidationException(UserController.LOGIN_ERROR);
			}
		}
		catch (ValidationException v)
		{
			throw v;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ValidationException(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public User get(long id) throws ValidationException
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		try
		{
			User user = (User) hSession.createQuery("from User where id=?").setLong(0, id).uniqueResult();
			if(user != null)
			{
				return user;
			}
			else
			{
				throw new ValidationException(UserController.USER_NOT_FOUND_ERROR);
			}
		}
		catch (ValidationException v)
		{
			throw v;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new ValidationException(e.getMessage());
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> getAllUserList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<User> userList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray userListArray = new JSONArray();
			
			userList = hSession.createQuery("from User where deleted=0").list();
			
			Iterator<User> i = userList.iterator();
			while(i.hasNext()){
				User user = i.next();
				object = JSON.toJSONObject(user);
				userListArray.put(object);
			}
			
			result.put("success", true);
			result.put("data", userListArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	public static Map<String, Object> getAllUserRoleList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray userRoleListArray = new JSONArray();
			
			List<UserRole> userList = (List<UserRole>) hSession.createQuery("from UserRole").list();
			
			Iterator<UserRole> i = userList.iterator();
			while(i.hasNext()){
				UserRole userRole = i.next();
				object = JSON.toJSONObject(userRole);
				userRoleListArray.put(object);
			}
			
			result.put("success", true);
			result.put("data", userRoleListArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	
	public Map<String, Object> addOrEditNewUser(String name, String  email, String  password, String addressLine1, String addressLine2, 
												int sex, String emiratesId, Date dob, String mobileNumber, String city, long role, boolean editUser)
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		try
		{
			User user = (User) hSession.createQuery("from User where email=?").setString(0, email).setMaxResults(1).uniqueResult();
			
			if(user != null && editUser) {
				//User exists
				
				System.out.println("editing existing user");
				
				int hashCodePassword = password.hashCode();
				String hashCodeString = ""+hashCodePassword;
				
				Date currentDate = new Date();
				
				user.setName(name);
				user.setEmail(email);
				user.setPassword(hashCodeString);
				user.setMobileNumber(mobileNumber);
				user.setRole(role);
				user.setAddressLine1(addressLine1);
				user.setAddressLine2(addressLine2);
				user.setCity(city);
				user.setEmiratesId(emiratesId);
				user.setActive(true);
				user.setSex(null);
				user.setDob(dob);
				user.setLastLogin(currentDate);
				user.setUpdatedOn(currentDate);
				
				hSession.saveOrUpdate(user);
				transaction.commit();
				
				result.put("success", true);
				
				result.put("message", "User edited successfully");
				
			}
			else if(user != null && !editUser) {

				System.out.println("user exists, but not edit command");
				result.put("success", false);
				result.put("message", "Email "+email+" already exists.");
				throw new ValidationException("Email "+email+" already exists.");
				
			}
			else {
				
				System.out.println("create new user entered");
				user = new User();
				
				int hashCodePassword = password.hashCode();
				String hashCodeString = ""+hashCodePassword;
				
				Date currentDate = new Date();
				
				user.setName(name);
				user.setEmail(email);
				user.setPassword(hashCodeString);
				user.setMobileNumber(mobileNumber);
				user.setRole(role);
				user.setAddressLine1(addressLine1);
				user.setAddressLine2(addressLine2);
				user.setCity(city);
				user.setEmiratesId(emiratesId);
				user.setActive(true);
				user.setSex(null);
				user.setDob(dob);
				user.setLastLogin(currentDate);
				user.setCreatedOn(currentDate);
				user.setUpdatedOn(currentDate);
				
				hSession.save(user);
				transaction.commit();
				
				result.put("success", true);
				result.put("message", "New user saved successfully.");
				return result;
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.put("success", false);
			return result;
		}
		finally
		{
			hSession.close();
		}
		
		return result;
	}
	public Map<String, Object> deleteUser(String name, long id)
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		try
		{
			User user = (User) hSession.createQuery("from User where id=?").setLong(0, id).setMaxResults(1).uniqueResult();
			
			if(user != null) {
				//User exists
				
				user.setActive(false);
				user.setDeleted(true);
				user.setUpdatedOn(new Date());
				
				hSession.save(user);
				transaction.commit();
				
				result.put("success", true);
				result.put("message", "User edited successfully");
				
			}
			else {
				
				result.put("success", true);
				result.put("message", "User could not be found.");
				return result;
				
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
		
		return result;
	}


}
