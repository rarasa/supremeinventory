package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;
import org.json.JSONObject;

import com.supreme.Config;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Inventory;
import com.supreme.dao.Orders;
import com.supreme.dao.Restock;
import com.supreme.dao.StatusTypes;
import com.supreme.dao.Supplier;
import com.supreme.dao.Supplier;
import com.supreme.dao.User;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class RestockController {
	
	public Map<String, Object> getOrderDetailsForBarcodeStringToAddToOrder(String barcodeString){
		
		System.out.println("getOrderDetailsForBarcodeString entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			Inventory inventory = (Inventory) hSession.createQuery("from Inventory where barcodeString=?").setString(0, barcodeString).setMaxResults(1).uniqueResult();
			
			JSONArray ordersWithInventoryId = new JSONArray();
			
			if(inventory != null) {
				
				List<Cart> cartList = (List<Cart>) hSession.createQuery("from Cart where inventoryId=? and deleted=0 and orderId is not NULL").setLong(0, inventory.getId()).list();
				
				if(cartList != null && cartList.size() > 0
						&&(inventory.getTile() != null && !inventory.getTile())) {
					
					result.put("success", false);
					result.put("error", "Inventory already exists in Cart. Please restock before adding to order");
				}
				else if(inventory.getTile() != null && inventory.getTile() && inventory.getTileCount() == 0) {
					result.put("success", false);
					result.put("error", "Inventory Tiles already exists in Cart and hav ebeen sold completely. Please restock before adding to order");
				}
				else {
					System.out.println("Cart has data");
					
					Iterator<Cart> cartItr = cartList.iterator();
					
					int totalTilesInCrate = 0;
					int totalTilesInUse = 0;
					int remainingTilesInCrate = 0;
					while(cartItr.hasNext()) {
						
						Cart cart = cartItr.next();
						if(cart.getOrderId() != null) {
							
							System.out.println("order id is not null");
							
							Orders order = (Orders) hSession.get(Orders.class, cart.getOrderId());
							
							JSONObject cartObj = new JSONObject();
							
							cartObj.put("cartId", cart.getId());
							cartObj.put("orderId", order.getId());
							cartObj.put("orderPlaced", order.getPlaced());
							cartObj.put("orderPlacedBy", order.getPlacedBy());
							cartObj.put("customerId", order.getCustomerId());

							Customer cust = (Customer) hSession.get(Customer.class, order.getCustomerId());
							cartObj.put("customer", JSON.toJSONObject(cust));
							
							cartObj.put("inventoryId", inventory.getId());
							cartObj.put("length", inventory.getActualLength());
							cartObj.put("height", inventory.getActualHeight());
							cartObj.put("thickness", inventory.getThickness());
							cartObj.put("barcodeString", inventory.getBarcodeString());
							
							if(cart.isTile()) {
								cartObj.put("tile", cart.isTile());
								cartObj.put("tileCount", cart.getTileCount());
								
								totalTilesInUse += cart.getTileCount();
								totalTilesInCrate = inventory.getOriginalTileCount();
								cartObj.put("totalTilesInCrate", totalTilesInCrate);
								cartObj.put("totalTilesInUse", totalTilesInUse);
							}
							
							ordersWithInventoryId.put(cartObj);
							
						}
					}
					result.put("success", true);
					result.put("data", ordersWithInventoryId);
				}
				
			}
			else {
				result.put("success", false);
				result.put("error", "Barcode not found.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		finally {
			hSession.close();
		}
		
		return result;
		
	}
	public Map<String, Object> getOrderDetailsForBarcodeString(String barcodeString){
		
		System.out.println("getOrderDetailsForBarcodeString entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			Inventory inventory = (Inventory) hSession.createQuery("from Inventory where barcodeString=?").setString(0, barcodeString).setMaxResults(1).uniqueResult();
			
			JSONArray ordersWithInventoryId = new JSONArray();
			
			if(inventory != null) {
				
				List<Cart> cartList = (List<Cart>) hSession.createQuery("from Cart where inventoryId=? and deleted=0 and orderId is not NULL").setLong(0, inventory.getId()).list();
				
				if(cartList != null && cartList.size() > 0) {
					
					System.out.println("Cart has data");
					
					Iterator<Cart> cartItr = cartList.iterator();
					while(cartItr.hasNext()) {
						
						Cart cart = cartItr.next();
						if(cart.getOrderId() != null) {
							
							System.out.println("order id is not null");
							
							Orders order = (Orders) hSession.get(Orders.class, cart.getOrderId());
							
							JSONObject cartObj = new JSONObject();
							
							cartObj.put("cartId", cart.getId());
							cartObj.put("orderId", order.getId());
							cartObj.put("orderPlaced", order.getPlaced());
							cartObj.put("orderPlacedBy", order.getPlacedBy());
							cartObj.put("customerId", order.getCustomerId());
							
							Customer cust = (Customer) hSession.get(Customer.class, order.getCustomerId());
							cartObj.put("customer", JSON.toJSONObject(cust));
							
							cartObj.put("inventoryId", inventory.getId());
							cartObj.put("length", inventory.getActualLength());
							cartObj.put("height", inventory.getActualHeight());
							cartObj.put("thickness", inventory.getThickness());
							cartObj.put("barcodeString", inventory.getBarcodeString());
							
							if(cart.isTile()) {
								cartObj.put("tile", cart.isTile());
								cartObj.put("tileCount", cart.getTileCount());
							}
							
							ordersWithInventoryId.put(cartObj);
							
						}
					}
					result.put("success", true);
					result.put("data", ordersWithInventoryId);
				}
				else {
					System.out.println("inventory not in cart");
					
					result.put("success", false);
					result.put("error", "Inventory does not exists in Cart.");
				}
				
			}
			else {
				result.put("success", false);
				result.put("error", "Barcode not found.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		finally {
			hSession.close();
		}
		
		return result;
		
	}
	
	public Map<String, Object> restockCartId(long inventoryId, long orderId, long cartId, long statusTypeId, String remarks, boolean tile, int tileCount){
		
		System.out.println("getOrderDetailsForBarcodeString entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			Inventory inventory = (Inventory) hSession.createQuery("from Inventory where id=?").setLong(0, inventoryId).setMaxResults(1).uniqueResult();
			
			JSONArray ordersWithInventoryId = new JSONArray();
			
			if(inventory != null) {
				
				int addTileCountToOriginal = 0;
				Cart cart = (Cart) hSession.createQuery("from Cart where inventoryId=? and deleted=0 and orderId=? ").setLong(0, inventory.getId()).setLong(1, orderId).setMaxResults(1).uniqueResult();
				
				if(cart != null) {
					
					System.out.println("Cart has data");
					Transaction tx = hSession.beginTransaction();
					
					
					if(cart.isTile()) {
						addTileCountToOriginal += tileCount;
						if(inventory.getTile()) {
							int inventoryNewTileCount = inventory.getTileCount() + addTileCountToOriginal;
							inventory.setTileCount(inventoryNewTileCount);
							
							int remainingTiles = cart.getTileCount() - tileCount;
							cart.setTileCount(remainingTiles);
							
							if(remainingTiles == 0) {
								cart.setDeleted(true);
							}
							
							hSession.saveOrUpdate(inventory);
						}
						
					}
					else {
						cart.setDeleted(true);
					}

					hSession.saveOrUpdate(cart);
					tx.commit();
					
					result.put("success", true);
					result.put("message", "Slab restocked");
				}
				
				Transaction tx = hSession.beginTransaction();
				
				
				StatusTypes statusType = (StatusTypes) hSession.get(StatusTypes.class, statusTypeId);
				Orders order = (Orders) hSession.get(Orders.class, orderId);
				
				Restock restock = new Restock();
				restock.setStatusTypeId(statusTypeId);
				restock.setStatusType(statusType);
				restock.setCartId(cartId);
				restock.setCart(cart);
				restock.setInventoryId(inventoryId);
				restock.setInventory(inventory);
				restock.setOrderId(orderId);
				restock.setRemarks(remarks);
				
				if(tile) {
					restock.setTile(tile);
					restock.setTileCount(tileCount);
				}
				
				restock.setActive(true);
				restock.setDeleted(false);
				restock.setCreatedOn(new Date());
				restock.setUpdatedOn(new Date());
				
				hSession.saveOrUpdate(restock);
				tx.commit();
				
			}
			else {
				result.put("success", false);
				result.put("error", "Barcode not found.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		finally {
			hSession.close();
		}
		
		return result;
		
	}
	
	public Map<String, Object> addSlabToOrder(long orderId, String barcodeString, boolean markSlabAsSold){
		
		System.out.println("getOrderDetailsForBarcodeString entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			Inventory inventory = (Inventory) hSession.createQuery("from Inventory where barcodeString=?").setString(0, barcodeString).setMaxResults(1).uniqueResult();
			
			if(inventory != null) {
				
				List<Cart> cartList = (List<Cart>) hSession.createQuery("from Cart where inventoryId=? and deleted != 0 and orderId not like null").setLong(0, inventory.getId()).list();
				if(cartList != null && cartList.size() > 0) {
					result.put("success", false);
					result.put("error", "Slab already exists in other order. Please Restock the slab first.");
				}
				else {
					Orders order = (Orders) hSession.get(Orders.class, orderId);
					
					Set<Cart> cartSet = order.getCart();
					
					Cart cart = new Cart();
					cart.setCreatedOn(new Date());
					cart.setUpdatedOn(new Date());
					cart.setInventoryId(inventory.getId());
					cart.setInventory(inventory);
					cart.setDeleted(false);
					cart.setTile(inventory.getTile());
					cart.setTile(inventory.getTile());
					
				}
				
			}
			else {
				result.put("success", false);
				result.put("error", "Barcode not found.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
		}
		finally {
			hSession.close();
		}
		
		return result;
	}
	
}
