package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.supreme.Config;
import com.supreme.dao.Container;
import com.supreme.dao.Inventory;
import com.supreme.dao.Locations;
import com.supreme.dao.Material;
import com.supreme.dao.Supplier;
import com.supreme.util.HibernateBridge;

public class PackingListUploadController {
	
	public String TILE = "TILE";
	public String CUTTER = "CUTTER";
	public String GANGSAW = "GANGSAW";
	public String MINI_GANGSAW = "MINI_GANGSAW";
	public String UNDERSIZE = "UNDERSIZE";
	public String STEP = "STEP";
	public String RISER = "RISER";

	public long getContainerId(String containerCode){
		
		long containerId = 0L;
		
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try{
			Transaction transaction = hSession.beginTransaction();
			
			Container container = (Container) hSession.createQuery("from Container where containerCode=? and createdOn=?").setString(0, containerCode).setDate(1, new Date()).setMaxResults(1).uniqueResult();
			
			if(container != null){
				
				System.out.println("Container code has been added today. so return the same id");
				containerId = container.getContainerId();
			}
			else{
				System.out.println("container is null, so need to be added");
				
				container = new Container();
				container.setContainerCode(containerCode);
				container.setCreatedOn(new Date());
				container.setUpdatedOn(new Date());
	    		hSession.saveOrUpdate(container);
	    		transaction.commit();
	    		
	    		container = (Container) hSession.createQuery("from Container where containerCode=?").setString(0, containerCode).setMaxResults(1).uniqueResult();
	    		
	    		System.out.println("container.getContainerId(): "+container.getContainerId());
	    		
	    		containerId = container.getContainerId();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			hSession.close();
		}
		
		return containerId;
	}
	
	public boolean ReadPackingList(String fileName, Date arrivalDate, long locationId, boolean oldStock, long containerId){

		//reason of using this list because excel download from morning start has multiple amfiiCodes in it
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try{
			
			Date currentDate = new Date();
			Material material = null;
			String previousRowMaterialName = "";
			String previousRowMaterialType = "";
			
			Container container = (Container) hSession.get(Container.class, containerId);
			Supplier supplier = (Supplier) hSession.get(Supplier.class, container.getSupplierId());
			
			System.out.println("container: "+ container.getContainerId());
			
			
			long lastId, newId;
			Query idList = hSession.createQuery("select max(id) from Inventory");
			if(idList != null && !idList.list().isEmpty()){
				if(idList.list().get(0) != null){
					lastId = (long) idList.list().get(0);
					newId = lastId + 1;
				}
				else{
					lastId = 0;
					newId = 1;
				}
			}
			else{
				lastId = 0;
				newId = 1;
			}
			
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.PACKINGLIST_DEV_PATH);
    				System.out.println("Dev path: "+path);
    			}else{
    				path=config.get(Config.PACKINGLIST_PROD_PATH);
    				System.out.println("Prod path: "+path);
    			}
    		}catch(IOException e){
    			path = "/opt/supremeinventory/packingList/";
    		}
    		
    		
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext()) { 
	            	
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Inventory inventory = new Inventory();
	                
	                double actualLength = 0, actualHeight = 0, squareMetre = 0, actualSquareMetre = 0, diffSquareMetre = 0;

	
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    /**
		                     * Reading Slab Number Supreme
		                     */
		                    if(j == 0){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		int sNoSupreme = (int) cell.getNumericCellValue();
		                    		Long slabNoSupreme = (long) sNoSupreme;
		                    		inventory.setSlabNoSupreme(slabNoSupreme);
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Slab Number Supplier
		                     * CAN BE 0 OR BLANK SPACE 
		                     */
		                    if(j == 1){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		int sNoSupplier = (int) cell.getNumericCellValue();
		                    		Long slabNoSupplier = (long) sNoSupplier;
		                    		inventory.setSlabNoSupplier(slabNoSupplier);
		                    	}
		                    	else{
		                    		inventory.setSlabNoSupplier(0L);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Name
		                     */
		                    if(j == 2){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		
		                    		String materialName = "";
		                    		materialName = cell.getStringCellValue();
		                    		
		                    		if(previousRowMaterialName.equals(materialName)){
		                    			inventory.setMaterial(material);
	                    				inventory.setMaterialId(material.getMaterialId());
		                    		}
		                    		else{
		                    			material = (Material) hSession.createQuery("from Material where materialName=?").setString(0, (materialName).toUpperCase()).setMaxResults(1).uniqueResult();
		                    			if(material == null){
		                    				System.out.println("Could not find Material with name: "+materialName);
		                    				throw new ValidationException("Could not find Material with name: "+materialName);
		                    			}
		                    			else{
		                    				
		                    				materialName = material.getMaterialName();
		                    				previousRowMaterialName = material.getMaterialName();
                    						System.out.println("New Material: "+ material.getMaterialName());
		                    				inventory.setMaterial(material);
		                    				inventory.setMaterialId(material.getMaterialId());
		                    			}
		                    		}
		                    		
		                    		
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Thickness
		                     */
		                    if(j == 3){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setThickness((long)cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Packing Length
		                     */
		                    if(j == 4){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		actualLength = cell.getNumericCellValue();
		                    		inventory.setActualLength(actualLength);
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Packing Height
		                     */
		                    if(j == 5){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		actualHeight = cell.getNumericCellValue();
		                    		inventory.setActualHeight(actualHeight);
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Type
		                     */
		                    if(j == 6){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		
		                    		String materialType = cell.getStringCellValue().toLowerCase();
		                    		
		                    		if(previousRowMaterialType.toLowerCase().equals(materialType)){
		                    			inventory.setType(previousRowMaterialType);
		                    		}
		                    		else{
		                    			if(materialType.equals(CUTTER.toLowerCase())){
		                    				inventory.setType(CUTTER);
		                    				previousRowMaterialType = CUTTER;
		                    			}
		                    			else if(materialType.equals(GANGSAW.toLowerCase())){
		                    				inventory.setType(GANGSAW);
		                    				previousRowMaterialType = GANGSAW;
		                    			}
		                    			else if(materialType.equals(MINI_GANGSAW.toLowerCase())){
		                    				inventory.setType(MINI_GANGSAW);
		                    				previousRowMaterialType = MINI_GANGSAW;
		                    			}
		                    			else if(materialType.equals(UNDERSIZE.toLowerCase())){
		                    				inventory.setType(UNDERSIZE);
		                    				previousRowMaterialType = UNDERSIZE;
		                    			}
		                    			else if(materialType.equals(TILE.toLowerCase())){
		                    				inventory.setType(TILE);
		                    				previousRowMaterialType = TILE;
		                    			}
		                    			else if(materialType.equals(STEP.toLowerCase())){
		                    				inventory.setType(STEP);
		                    				previousRowMaterialType = STEP;
		                    			}
		                    			else if(materialType.equals(RISER.toLowerCase())){
		                    				inventory.setType(RISER);
		                    				previousRowMaterialType = RISER;
		                    			}
		                    		}
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Polished
		                     * DEFAULT: YES
		                     */
		                    if(j == 7){
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setPolished(true);
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String polished = cell.getStringCellValue();
		                    		if(polished.toLowerCase().equals("yes") || polished.toLowerCase().equals("y")){
		                    			inventory.setPolished(true);
		                    		}
		                    		else if(polished.toLowerCase().equals("no") || polished.toLowerCase().equals("n")){
		                    			inventory.setPolished(false);
		                    		}
		                    		else {
		                    			inventory.setPolished(false);
		                    		}
		                    	}
		                    	else{
		                    		inventory.setPolished(true);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Flamed
		                     * DEFAULT: NO
		                     */
		                    if(j == 8){
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setFlamed(false);
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String flamed = cell.getStringCellValue();
		                    		if(flamed.toLowerCase().equals("yes") || flamed.toLowerCase().equals("y")){
		                    			inventory.setFlamed(true);
		                    		}
		                    		else if(flamed.toLowerCase().equals("no") || flamed.toLowerCase().equals("n")){
		                    			inventory.setFlamed(false);
		                    		}
		                    		else{
		                    			inventory.setFlamed(false);
		                    		}
		                    	}
		                    	else{
		                    		inventory.setFlamed(false);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Sold
		                     * DEFAULT: NO
		                     */
		                    
		                    if(j == 9){
		                    	inventory.setSold(false);
		                    	System.out.println("entered j == 9------------------------------------------");
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setSold(false);
		                    		System.out.println("cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK");
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String sold = cell.getStringCellValue();
		                    		System.out.println("cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK: now going to check cases");
		                    		if(sold.toLowerCase().equals("yes") || sold.toLowerCase().equals("y")){
		                    			System.out.println("sold.toLowerCase().equals(yes) || sold.toLowerCase().equals(y)");
		                    			inventory.setSold(true);
		                    		}
		                    		else if(sold.toLowerCase().equals("no") || sold.toLowerCase().equals("n")){
		                    			inventory.setSold(false);
		                    			System.out.println("sold.toLowerCase().equals(no) || sold.toLowerCase().equals(n)");
		                    		}
		                    		else{
		                    			inventory.setSold(false);
		                    			System.out.println("Cell is not null, not blank but is NOT yes or no");
		                    		}
		                    	}
		                    	else{
		                    		System.out.println("entered else");
		                    		inventory.setSold(false);
		                    	}
	                        }
		                    
		                    
		                    j++;
		                } 
	                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
		                	
		                	try{
		                		
		                		actualSquareMetre = (actualLength * actualHeight) / 10000;
			                    inventory.setActualSquareMetre(actualSquareMetre);
			                    
			                    diffSquareMetre = actualSquareMetre - squareMetre;
			                    inventory.setDiffSquareMetre(diffSquareMetre);
			                    
			                    String barcode = "";
			                    
			                    Locations location = (Locations) hSession.get(Locations.class, locationId);
			                    
			                    if(location.getName().equals("MAIN OFFICE")){
			                    	inventory.setYard(0);
			                    	barcode = "M";
			                    }
			                    else{
			                    	inventory.setYard(1);
			                    	barcode = "W";
			                    }
			                    
			                    inventory.setSupplierId(container.getSupplierId());
			                    inventory.setSupplier(container.getSupplier());
			                    
			                    inventory.setLength(0);
			                    inventory.setHeight(0);
			                    inventory.setSquareMetre(0);
			                    
			                    inventory.setContainerId(containerId);
			                    inventory.setContainer(container);
			                    
			                    inventory.setOldStock(oldStock);
			                    
			                    String padded = String.format("%07d" , newId);
			                    if(oldStock){

			                    	barcode += "O"+padded;
			                    }
			                    else{
			                    	barcode += "N"+padded;
			                    }
			                    
			                    inventory.setBarcodeString(barcode);
			                    
			                    inventory.setArrivalDate(arrivalDate);
			                    //inventory.setSold(false);
			                    inventory.setCreatedOn(new Date());
			                    inventory.setUpdatedOn(new Date());
		                		System.out.println("I = "+ i+" inventory= "+inventory.getSlabNoSupreme());
		                		
		                		hSession.saveOrUpdate(inventory);
		                		
		                		
		                		container.setPackingListUploaded(true);
		                		hSession.saveOrUpdate(container);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                		
		                		return false;
		                	}
		                }		            	
		                newId++;
	                }
	                i++;
	            } 
	            filetoRead.close(); 
	            return true;
			}
    		else {
    			return false;
    		}
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
            return false;
        }
        finally{
        	hSession.close();
        }
	}
	
	public boolean ReadTilePackingList(String fileName, Date arrivalDate, long locationId, boolean oldStock, long containerId){

		//reason of using this list because excel download from morning start has multiple amfiiCodes in it
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try{
			
			Date currentDate = new Date();
			Material material = null;
			String previousRowMaterialName = "";
			String previousRowMaterialType = "";
			
			Container container = (Container) hSession.get(Container.class, containerId);
			
			System.out.println("container: "+ container.getContainerId());
			
			
			long lastId, newId;
			Query idList = hSession.createQuery("select max(id) from Inventory");
			if(idList != null && !idList.list().isEmpty()){
				if(idList.list().get(0) != null){
					lastId = (long) idList.list().get(0);
					newId = lastId + 1;
				}
				else{
					lastId = 0;
					newId = 1;
				}
			}
			else{
				lastId = 0;
				newId = 1;
			}
			
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.PACKINGLIST_DEV_PATH);
    				System.out.println("Dev path: "+path);
    			}else{
    				path=config.get(Config.PACKINGLIST_PROD_PATH);
    				System.out.println("Prod path: "+path);
    			}
    		}catch(IOException e){
    			path = "/opt/supremeinventory/packingList/";
    		}
    		
    		
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext()) { 
	            	
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Inventory inventory = new Inventory();
	                
	                double actualLength = 0, actualHeight = 0, squareMetre = 0, actualSquareMetre = 0, diffSquareMetre = 0;
	                int tileCount = 0;

	
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    /**
		                     * Reading Slab Number Supreme
		                     */
		                    if(j == 0){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		int sNoSupreme = (int) cell.getNumericCellValue();
		                    		Long slabNoSupreme = (long) sNoSupreme;
		                    		inventory.setSlabNoSupreme(slabNoSupreme);
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Slab Number Supplier
		                     * CAN BE 0 OR BLANK SPACE 
		                     */
		                    if(j == 1){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		int sNoSupplier = (int) cell.getNumericCellValue();
		                    		Long slabNoSupplier = (long) sNoSupplier;
		                    		inventory.setSlabNoSupplier(slabNoSupplier);
		                    	}
		                    	else{
		                    		inventory.setSlabNoSupplier(0L);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Name
		                     */
		                    if(j == 2){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		
		                    		String materialName = "";
		                    		materialName = cell.getStringCellValue();
		                    		
		                    		if(previousRowMaterialName.equals(materialName)){
		                    			inventory.setMaterial(material);
	                    				inventory.setMaterialId(material.getMaterialId());
		                    		}
		                    		else{
		                    			material = (Material) hSession.createQuery("from Material where materialName=?").setString(0, (materialName).toUpperCase()).setMaxResults(1).uniqueResult();
		                    			if(material == null){
		                    				System.out.println("Could not find Material with name: "+materialName);
		                    				throw new ValidationException("Could not find Material with name: "+materialName);
		                    			}
		                    			else{
		                    				
		                    				materialName = material.getMaterialName();
		                    				previousRowMaterialName = material.getMaterialName();
                    						System.out.println("New Material: "+ material.getMaterialName());
		                    				inventory.setMaterial(material);
		                    				inventory.setMaterialId(material.getMaterialId());
		                    			}
		                    		}
		                    		
		                    		
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Thickness
		                     */
		                    if(j == 3){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setThickness((long)cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Packing Length
		                     */
		                    if(j == 4){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		actualLength = cell.getNumericCellValue();
		                    		inventory.setActualLength(actualLength);
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Packing Height
		                     */
		                    if(j == 5){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		actualHeight = cell.getNumericCellValue();
		                    		inventory.setActualHeight(actualHeight);
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Type
		                     */
		                    if(j == 6){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		
		                    		String materialType = cell.getStringCellValue().toLowerCase();
		                    		
		                    		if(previousRowMaterialType.toLowerCase().equals(materialType)){
		                    			inventory.setType(previousRowMaterialType);
		                    		}
		                    		else{
		                    			if(materialType.equals(CUTTER.toLowerCase())){
		                    				inventory.setType(CUTTER);
		                    				previousRowMaterialType = CUTTER;
		                    			}
		                    			else if(materialType.equals(GANGSAW.toLowerCase())){
		                    				inventory.setType(GANGSAW);
		                    				previousRowMaterialType = GANGSAW;
		                    			}
		                    			else if(materialType.equals(MINI_GANGSAW.toLowerCase())){
		                    				inventory.setType(MINI_GANGSAW);
		                    				previousRowMaterialType = MINI_GANGSAW;
		                    			}
		                    			else if(materialType.equals(UNDERSIZE.toLowerCase())){
		                    				inventory.setType(UNDERSIZE);
		                    				previousRowMaterialType = UNDERSIZE;
		                    			}
		                    			else if(materialType.equals(TILE.toLowerCase())){
		                    				inventory.setType(TILE);
		                    				previousRowMaterialType = TILE;
		                    			}
		                    			else if(materialType.equals(STEP.toLowerCase())){
		                    				inventory.setType(STEP);
		                    				previousRowMaterialType = STEP;
		                    			}
		                    			else if(materialType.equals(RISER.toLowerCase())){
		                    				inventory.setType(RISER);
		                    				previousRowMaterialType = RISER;
		                    			}
		                    		}
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    /**
		                     * Reading Polished
		                     * DEFAULT: YES
		                     */
		                    if(j == 7){
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setPolished(true);
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String polished = cell.getStringCellValue();
		                    		if(polished.toLowerCase().equals("yes") || polished.toLowerCase().equals("y")){
		                    			inventory.setPolished(true);
		                    		}
		                    		else if(polished.toLowerCase().equals("no") || polished.toLowerCase().equals("n")){
		                    			inventory.setPolished(false);
		                    		}
		                    		else {
		                    			inventory.setPolished(false);
		                    		}
		                    	}
		                    	else{
		                    		inventory.setPolished(true);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Flamed
		                     * DEFAULT: NO
		                     */
		                    if(j == 8){
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setFlamed(false);
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String flamed = cell.getStringCellValue();
		                    		if(flamed.toLowerCase().equals("yes") || flamed.toLowerCase().equals("y")){
		                    			inventory.setFlamed(true);
		                    		}
		                    		else if(flamed.toLowerCase().equals("no") || flamed.toLowerCase().equals("n")){
		                    			inventory.setFlamed(false);
		                    		}
		                    		else{
		                    			inventory.setFlamed(false);
		                    		}
		                    	}
		                    	else{
		                    		inventory.setFlamed(false);
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Sold
		                     * DEFAULT: NO
		                     */
		                    
		                    if(j == 9){
		                    	System.out.println("entered j == 9------------------------------------------");
		                    	if(cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK){
		                    		inventory.setSold(false);
		                    		System.out.println("cell != null && cell.getCellType() == Cell.CELL_TYPE_BLANK");
		                    	}
		                    	else if(cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
		                    		String sold = cell.getStringCellValue();
		                    		System.out.println("sold: "+ sold);
		                    		System.out.println("cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK: now going to check cases");
		                    		if(sold.toLowerCase().equals("yes") || sold.toLowerCase().equals("y")){
		                    			System.out.println("sold.toLowerCase().equals(yes) || sold.toLowerCase().equals(y)");
		                    			inventory.setSold(true);
		                    		}
		                    		else if(sold.toLowerCase().equals("no") || sold.toLowerCase().equals("n")){
		                    			inventory.setSold(false);
		                    			System.out.println("sold.toLowerCase().equals(no) || sold.toLowerCase().equals(n)");
		                    		}
		                    		else{
		                    			inventory.setSold(false);
		                    			System.out.println("Cell is not null, not blank but is NOT yes or no");
		                    		}
		                    	}
		                    	else{
		                    		System.out.println("entered else");
		                    		inventory.setSold(false);
		                    	}
	                        }
		                    
		                    
		                    
		                    /**
		                     * Reading Tile Count
		                     * and setting tile boolean to true
		                     */
		                    
		                    if(j == 10){
		                    	System.out.println("j -- 10");
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		System.out.println("j -- 10 if condition");
		                    		tileCount = (int)cell.getNumericCellValue();
		                    		inventory.setTileCount(tileCount);
		                    		inventory.setOriginalTileCount(tileCount);
		                    		inventory.setTile(true);
		                    		System.out.println("tileCount: "+tileCount);
		                    	}
		                    	else{
		                    		System.out.println("j -- 10 else condition");
		                    		flag = true;
		                    	}
	                        }
		                    
		                    
		                    j++;
		                } 
	                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
		                	
		                	try{
		                		
		                		actualSquareMetre = (actualLength * actualHeight * tileCount) / 10000;
			                    inventory.setActualSquareMetre(actualSquareMetre);
			                    
			                    diffSquareMetre = actualSquareMetre - squareMetre;
			                    inventory.setDiffSquareMetre(diffSquareMetre);
			                    
			                    String barcode = "";
			                    
			                    Locations location = (Locations) hSession.get(Locations.class, locationId);
			                    if(location.getName().equals("MAIN OFFICE")){
			                    	inventory.setYard(0);
			                    	barcode = "M";
			                    }
			                    else{
			                    	inventory.setYard(1);
			                    	barcode = "W";
			                    }
			                    
			                    inventory.setLength(0);
			                    inventory.setHeight(0);
			                    inventory.setSquareMetre(0);
			                    
			                    inventory.setContainerId(containerId);
			                    inventory.setContainer(container);
			                    
			                    inventory.setOldStock(oldStock);
			                    
			                    inventory.setSupplierId(container.getSupplierId());
			                    inventory.setSupplier(container.getSupplier());
			                    
			                    
			                    String padded = String.format("%07d" , newId);
			                    if(oldStock){

			                    	barcode += "OT"+padded;
			                    }
			                    else{
			                    	barcode += "NT"+padded;
			                    }
			                    
			                    inventory.setBarcodeString(barcode);
			                    
			                    inventory.setArrivalDate(arrivalDate);
			                    //inventory.setSold(false);
			                    inventory.setCreatedOn(new Date());
			                    inventory.setUpdatedOn(new Date());
		                		System.out.println("I = "+ i+" inventory= "+inventory.getSlabNoSupreme());
		                		
		                		hSession.saveOrUpdate(inventory);
		                		
		                		container.setPackingListUploaded(true);
		                		hSession.saveOrUpdate(container);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                	}
		                }		            	
		                newId++;
	                }
	                i++;
	            } 
	            filetoRead.close(); 
			}
    		return true;
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
            return false;
        }
        finally{
        	hSession.close();
        }
    
	}
	
	
}
