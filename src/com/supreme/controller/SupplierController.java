package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.Supplier;
import com.supreme.dao.Supplier;
import com.supreme.dao.User;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class SupplierController {

	public Map<String, Object> getAllSupplierList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<Supplier> supplierList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray supplierListArray = new JSONArray();
			
			supplierList = hSession.createQuery("from Supplier where active=1 order by supplierName asc").list();
			
			Iterator<Supplier> i = supplierList.iterator();
			while(i.hasNext()){
				Supplier supplier = i.next();
				object = JSON.toJSONObject(supplier);
				supplierListArray.put(object);
			}
			
			result.put("success", true);
			result.put("data", supplierListArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> deleteSupplier(String supplierName, long id) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Supplier supplier = (Supplier)hSession.createQuery("from Supplier where supplierId=?").setLong(0, id).setMaxResults(1).uniqueResult();
			
			if(supplier != null) {
				
				System.out.println("supplier name: "+ supplier.getSupplierName());
				
				if(supplier.getActive()) {
					supplier.setActive(false);
					supplier.setUpdatedOn(new Date());
					hSession.saveOrUpdate(supplier);
					transaction.commit();
					
					result.put("success", true);
					result.put("message", "Supplier deleted successfully.");
				}
				else {
					result.put("success", false);
					result.put("message", "Supplier already deleted.");
				}
			}
			else {
				result.put("success", false);
				result.put("message", "Supplier could not be found.");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			hSession.close();
		}
		return result;
	}
	
	public Map<String, Object> addSingleNewSupplier(String supplierName, String address, String phone, String email, long id) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Supplier supplier = null;

			supplier = (Supplier)hSession.createQuery("from Supplier where supplierName=? and supplierId=?").setString(0, supplierName).setLong(1, id).setMaxResults(1).uniqueResult();

			if(supplier != null){

				message = "Supplier Already Exists. Please check name again.";
				throw new ValidationException(message);

			}
			else{

				supplier = new Supplier();
				supplier.setSupplierName(supplierName.toUpperCase());
				
				if(address != null && !address.equals("")) {
					supplier.setAddress(address);
				}
				
				if(phone != null && !phone.equals("")) {
					supplier.setPhone(phone);
				}
				
				if(email != null && !email.equals("")) {
					supplier.setEmail(email);
				}
				
				supplier.setActive(true);
				supplier.setCreatedOn(new Date());
				supplier.setUpdatedOn(new Date());

				hSession.save(supplier);
				transaction.commit();
				result.put("success", true);
				result.put("message", "Supplier saved.");
			}

			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	public Map<String, Object> editSingleNewSupplier(String supplierName, String address, String phone, String email, long id) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			Supplier supplier = null;
			
			supplier = (Supplier)hSession.createQuery("from Supplier where supplierId=?").setLong(0, id).setMaxResults(1).uniqueResult();
			
			if(supplier != null){
				
				supplier.setSupplierName(supplierName.toUpperCase());
				
				if(address != null && !address.equals("")) {
					supplier.setAddress(address);
				}
				
				if(phone != null && !phone.equals("")) {
					supplier.setPhone(phone);
				}
				
				if(email != null && !email.equals("")) {
					supplier.setEmail(email);
				}
				
				supplier.setActive(true);
				supplier.setUpdatedOn(new Date());
				
				hSession.save(supplier);
				transaction.commit();
				result.put("success", true);
				result.put("message", "Supplier saved.");
				
				
				
			}
			else{
				
				result.put("success", false);
				message = "Could not find Supplier";
				result.put("message", message);
				throw new ValidationException(message);
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean bulkSupplierListUpload(String fileName){

		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try{
			
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.EXCEL_DEV_PATH);
    				System.out.println("Dev path: "+path);
    			}else{
    				path=config.get(Config.EXCEL_PROD_PATH);
    				System.out.println("Prod path: "+path);
    			}
    		}catch(IOException e){
    			path = "/opt/supremeinventory/excels/";
    		}
    		
    		System.out.println(path+File.separator+new Date().getTime()+fileName);
    		System.out.println("Path in mf");
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
    			/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext()) { 
	            	
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Supplier supplier = new Supplier();
	                
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    /**
		                     * Reading Slab Number
		                     */
		                    if(j != 0){
		                    
			                    if(j == 1){
			                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
			                    		supplier.setSupplierName(cell.getStringCellValue().trim().toUpperCase().toUpperCase());
			                    		
			                    	}
			                    	else{
			                    		flag = true;
			                    	}
		                        }
			                    
			                    if(j == 2){
			                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
			                    		supplier.setPhone(cell.getStringCellValue().trim().toUpperCase());
			                    	}
			                    	else{
			                    		supplier.setPhone(null);
			                    	}
		                        }
			                    
			                    /**
			                     * Reading Packing List Height
			                     */
			                    if(j == 3){
			                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
			                    		supplier.setEmail(cell.getStringCellValue().trim().toUpperCase());
			                    	}
			                    	else{
			                    		supplier.setEmail(null);
			                    	}
		                        }
			                    
			                    if(j == 4){
			                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
			                    		supplier.setAddress(cell.getStringCellValue().trim().toUpperCase());
			                    	}
			                    	else{
			                    		supplier.setAddress(null);
			                    	}
		                        }
			                    
		                    
		                    }
		                    
		                    supplier.setActive(true);
		                    supplier.setCreatedOn(new Date());
		                    supplier.setUpdatedOn(new Date());
		                    
		                    j++;
		                } 
	                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
		                	
		                	try{
		                		System.out.println("I = "+ i+" inventory= "+supplier.getSupplierName());
		                		
		                		hSession.saveOrUpdate(supplier);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                	}
		                }		            	
	                }
	                i++;
	            } 
	            filetoRead.close(); 
			}
    		return true;
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
            return false;
        }
        finally{
        	hSession.close();
        }
    
	}
}
