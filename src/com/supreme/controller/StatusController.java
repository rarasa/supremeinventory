package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.Status;
import com.supreme.dao.Status;
import com.supreme.dao.User;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class StatusController {

	public Map<String, Object> getAllStatusList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<Status> statusList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray statusListArray = new JSONArray();
			
			statusList = hSession.createQuery("from Status where active=1 and deleted=0").list();
			
			Iterator<Status> i = statusList.iterator();
			while(i.hasNext()){
				Status status = i.next();
				object.put(""+status.getName(), JSON.toJSONObject(status).get(""+status.getName()));
			}
			
			result.put("success", true);
			result.put("data", object);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
}
