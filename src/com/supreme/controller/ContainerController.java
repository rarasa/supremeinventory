package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.supreme.dao.Cart;
import com.supreme.dao.Container;
import com.supreme.dao.ContainerMaterialList;
import com.supreme.dao.ContainerStatusDetails;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Machines;
import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.dao.Unit;
import com.supreme.dao.User;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class ContainerController {
	
	public Map<String, Object> deleteContainerWithId(long containerId) throws ValidationException{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			
			Container container = (Container) hSession.get(Container.class, containerId);
			
			if(container != null) {
				
				if(container.isDeleted()) {
					result.put("success", true);
					result.put("error", "Container has already been deleted.");
				}
				else {
					Transaction transaction = hSession.beginTransaction();
					container.setDeleted(true);
					hSession.save(container);
					transaction.commit();
					
					result.put("success", true);
					result.put("error", "Container has been deleted.");
				}
				
			}
			else {
				result.put("success", false);
				result.put("error", "Container does not exist.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		finally {
			hSession.close();
		}
		
		return result;
	}

	
	public Map<String, Object> deleteMaterialFormContainerWithId(long containerId, long materialId) throws ValidationException{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			
			Container container = (Container) hSession.get(Container.class, containerId);
			
			Set<ContainerMaterialList> cmlList = (Set<ContainerMaterialList>) hSession.createQuery("from ContainerMaterialList where containerId=?").setLong(0, containerId).list();
			
			if(cmlList != null && cmlList.size() > 0) {
				
				Iterator materialListItr = cmlList.iterator();
				while(materialListItr.hasNext()) {
					
					ContainerMaterialList cml = (ContainerMaterialList) materialListItr.next();
					
					if(cml.getMaterialId() == materialId) {
						cml.setDeleted(true);
					}
					
					hSession.saveOrUpdate(cml);
					transaction.commit();
					
					result.put("success", true);
					result.put("message", "Successfully deleted material.");
					
				}
			}
			else {
				result.put("success", false);
				result.put("error", "No Material List found.");
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("error", e.getMessage());
		}
		finally {
			hSession.close();
		}
		
		return result;
	}
	
	public Map<String, Object> addOrEditContainerDetails(long containerId, long supplierId, String containerNumber, long DOStatus, long BLStatus, long containerStatus, long bankId, String invoiceNumber, 
													String remarks, Date paymentDate, double amount, long paymentStatus, Date invoiceDate, Set<ContainerMaterialList> cmlSet) throws ValidationException{
		
		System.out.println("addContainerDetails entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			
			Supplier supplier = (Supplier)hSession.get(Supplier.class, supplierId);
			
			Container container =  null;
			if(containerId != 0L) {
				container = (Container)hSession.createQuery("from Container where containerId=? and deleted=0").setLong(0, containerId).setMaxResults(1).uniqueResult();
				
				if(container != null) {
					
					container.setContainerCode(containerNumber);
					container.setSupplierId(supplierId);
					container.setSupplier(supplier);
					container.setDetailsExist(true);
					container.setUpdatedOn(new Date());

					Iterator cmlSetItr = cmlSet.iterator();
					while(cmlSetItr.hasNext()) {
						
						ContainerMaterialList cmlSetObj = (ContainerMaterialList) cmlSetItr.next();
						ContainerMaterialList cmlObj = (ContainerMaterialList) hSession.createQuery("from ContainerMaterialList where containerId=? and materialId=?").setLong(0, containerId).setLong(1, cmlSetObj.getMaterialId()).setMaxResults(1).uniqueResult();
						
						if(cmlObj != null) {
							cmlObj.setDeleted(false);
							cmlObj.setMaterialId(cmlSetObj.getMaterialId());
							cmlObj.setContainerId(container.getContainerId());
							cmlObj.setBillRate(cmlSetObj.getBillRate());
							cmlObj.setNoOfPieces(cmlSetObj.getNoOfPieces());
							cmlObj.setThickness(cmlSetObj.getThickness());
							cmlObj.setQuantity(cmlSetObj.getQuantity());
							cmlObj.setRealRate(cmlSetObj.getRealRate());
							
							hSession.saveOrUpdate(cmlObj);
							transaction.commit();
						}
						else {
							ContainerMaterialList cml = new ContainerMaterialList();
							cml.setDeleted(false);
							cml.setMaterialId(cmlSetObj.getMaterialId());
							cml.setContainerId(container.getContainerId());
							cml.setBillRate(cmlSetObj.getBillRate());
							cml.setNoOfPieces(cmlSetObj.getNoOfPieces());
							cml.setThickness(cmlSetObj.getThickness());
							cml.setQuantity(cmlSetObj.getQuantity());
							cml.setRealRate(cmlSetObj.getRealRate());
							hSession.saveOrUpdate(cml);
							transaction.commit();
						}
					}
					
					hSession.save(container);
					
					transaction = hSession.beginTransaction();
					
					ContainerStatusDetails csd = (ContainerStatusDetails) hSession.createQuery("from ContainerStatusDetails where containerId=?").setLong(0, container.getContainerId()).setMaxResults(1).uniqueResult();
					
					if(csd != null) {
						csd.setAmount(amount);
						csd.setBLStatus(BLStatus);
						csd.setContainerCode(containerNumber);
						csd.setContainerId(container.getContainerId());
						csd.setContainerStatus(containerStatus);
						csd.setDescription(remarks);
						csd.setDOStatus(DOStatus);
						csd.setInvoiceDate(invoiceDate);
						csd.setInvoiceNo(invoiceNumber);
						csd.setPaymentDate(paymentDate);
						csd.setBankDetailsId(bankId);
						csd.setPaymentStatus(paymentStatus);
						csd.setUpdatedOn(new Date());
						
						hSession.saveOrUpdate(csd);
					}
					else {
						
						csd = new ContainerStatusDetails();
						
						csd.setAmount(amount);
						csd.setBLStatus(BLStatus);
						csd.setContainerCode(containerNumber);
						csd.setContainerId(container.getContainerId());
						csd.setContainerStatus(containerStatus);
						csd.setDescription(remarks);
						csd.setDOStatus(DOStatus);
						csd.setInvoiceDate(invoiceDate);
						csd.setInvoiceNo(invoiceNumber);
						csd.setPaymentDate(paymentDate);
						csd.setBankDetailsId(bankId);
						csd.setPaymentStatus(paymentStatus);
						csd.setCreatedOn(new Date());
						csd.setUpdatedOn(new Date());
						
						
						hSession.saveOrUpdate(csd);
						
					}
					
					transaction.commit();
					
					result.put("success", true);
					result.put("message", "Container edited.");
				}
				
			}
			else {
				container = new Container();
				container.setContainerCode(containerNumber);
				container.setSupplierId(supplierId);
				container.setSupplier(supplier);
				container.setDetailsExist(true);
				container.setCreatedOn(new Date());
				container.setUpdatedOn(new Date());
				
				hSession.save(container);
				
				Iterator<ContainerMaterialList> cmlItr = cmlSet.iterator();
				
				System.out.println("container.getContainerId(): "+ container.getContainerId());
				
				while(cmlItr.hasNext()) {
					ContainerMaterialList cml = cmlItr.next();
					cml.setContainerId(container.getContainerId());
					hSession.save(cml);
				}
				
				ContainerStatusDetails csd = new ContainerStatusDetails();
				
				csd.setAmount(amount);
				csd.setBLStatus(BLStatus);
				csd.setContainerCode(containerNumber);
				csd.setContainerId(container.getContainerId());
				csd.setContainerStatus(containerStatus);
				csd.setDescription(remarks);
				csd.setDOStatus(DOStatus);
				csd.setInvoiceDate(invoiceDate);
				csd.setInvoiceNo(invoiceNumber);
				csd.setPaymentDate(paymentDate);
				csd.setPaymentStatus(paymentStatus);
				csd.setCreatedOn(new Date());
				csd.setUpdatedOn(new Date());
				
				
				hSession.saveOrUpdate(csd);

				
				transaction.commit();
				
				result.put("success", true);
				result.put("message", "Container details saved");
				
			}
			
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
		return result;
		
	}
	
	public Map<String, Object> getNewContainerList() throws ValidationException{
		
		System.out.println("getNewContainerList entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			List<Container> containerList = (List<Container>) hSession.createQuery("from Container where detailsExist=1 and deleted=0").list();
			
			System.out.println("containerlist size: "+containerList.size());
			JSONArray containerListArray = new JSONArray();
			
			if(containerList.size() > 0) {
				
				Iterator<Container> containerListItr = containerList.iterator();
				
				while(containerListItr.hasNext()) {
					
					Container c = containerListItr.next();
					JSONObject obj = new JSONObject();
					
					
					obj = JSON.toJSONObject(c);
					
					containerListArray.put(obj);
					
				}
				
				result.put("success", true);
				result.put("data", containerListArray);
				result.put("message", "No new containers exist. Please add container details first");
			}
			else {
				result.put("success", false);
				result.put("error", "No new containers exist. Please add container details first");
			}
			
		}
		catch(Exception e) {
			
		}
		finally {
			hSession.close();
		}
		return result;
		
	}

	public Map<String, Object> getNewContainerListInLastMonth() throws ValidationException{
		
		System.out.println("getNewContainerList entered");
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try {
			
			
			Date today = new Date();
			Calendar cNow = Calendar.getInstance();
			cNow.setTime(today);
			
			cNow.add(Calendar.DAY_OF_MONTH, -30);
			Date today30 = cNow.getTime();
			
			List<Container> containerList = (List<Container>) hSession.createQuery("from Container where deleted=0 and createdOn < ? and createdOn > ?").setDate(0, today).setDate(1, today30).list();
			
			System.out.println("containerlist size: "+containerList.size());
			JSONArray containerListArray = new JSONArray();
			
			if(containerList.size() > 0) {
				
				Iterator<Container> containerListItr = containerList.iterator();
				
				while(containerListItr.hasNext()) {
					
					Container c = containerListItr.next();
					JSONObject obj = new JSONObject();
					obj = JSON.toJSONObject(c);
					
					containerListArray.put(obj);
					
				}
				
				result.put("success", true);
				result.put("data", containerListArray);
				result.put("message", "No new containers exist. Please add container details first");
			}
			else {
				result.put("success", false);
				result.put("error", "No new containers exist. Please add container details first");
			}
			
		}
		catch(Exception e) {
			
		}
		finally {
			hSession.close();
		}
		return result;
		
	}
	
}
