package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Machines;
import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.dao.Unit;
import com.supreme.dao.User;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class CuttingController {
	
	public Map<String, Object> addScannedSlabsToCuttingOrder(long orderId, Set<Cart> cartSet, double totalMtSquare, int slabCount) throws ValidationException{
		
		System.out.println("addOrder controller func entered");
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Transaction transaction = hSession.beginTransaction();
		
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Orders order = (Orders) hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(order != null) {
				
				/*if(order.getCart() != null) {
					
					Set<Cart> existingCart = order.getCart();
					existingCart.add(null)
					
					while 
					
					Cart existingCart = (Cart) hSession.createQuery("from Cart where orderId=? and inventoryId=?").setLon
				}
				else {
				}*/
				order.setCart(cartSet);
				
				order.setCount(slabCount);
				order.setTotalMtSquare(totalMtSquare);
				
				order.setUpdatedOn(new Date());
				
				hSession.saveOrUpdate(order);
				transaction.commit();
				
				order = (Orders) hSession.get(Orders.class, orderId);
				
				result.put("success", true);
				result.put("message", "Slabs successfully scanned to order "+orderId+".");
				result.put("data", JSON.toJSONObject(order));
				
			}else {
				result.put("success", false);
				result.put("message", "Could not find the order "+orderId+".");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
		return result;
		
	}
	public long addOrder(String customerName, Set<Cart> cartSet, double totalMtSquare, double count, long customerId, long userId, 
			String cuttingOrderNumber, boolean hasCuttingSlabs, Set<CuttingOrder> cuttingOrderSet) throws ValidationException{

		System.out.println("addOrder controller func entered");
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Cart> cartIterator = cartSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			if(customerId != 0) {
				Customer existingCustomer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, customerId).setMaxResults(1).uniqueResult();
				
				if(existingCustomer != null) {
					
					Transaction transaction = hSession.beginTransaction();
					List<Orders> orderList = null;
					Orders order = null;
					
					order = new Orders();
					order.setScannedBy(userId);
					order.setNarration(cuttingOrderNumber);
					order.setCustomerId(customerId);
					order.setCart(cartSet);
					order.setHasCuttingSlabs(hasCuttingSlabs);
					order.setCount(cartSet.size());
					order.setCount(count);
					order.setTotalMtSquare(totalMtSquare);
					order.setCreatedOn(new Date());
					order.setUpdatedOn(new Date());
					
					hSession.save(order);
					transaction.commit();
					
					result.put("success", true);
					
					Iterator<Cart> i = cartSet.iterator();
					while (i.hasNext()) {
						Cart cart = i.next();
						
						if(cart.isTile()) {
							
							transaction = hSession.beginTransaction();
							System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
							Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
							
							int currentTileCount = inventory.getTileCount();
							int newTileCount = currentTileCount - cart.getTileCount();
							
							if(newTileCount < 0) {
								result.put("success", false);
								result.put("message", "Final Inventory Tilecount cannot be less than 0");
								throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
							}
							else {
								inventory.setTileCount(newTileCount);
								if(newTileCount == 0) {
									inventory.setSold(true);
								}
								
								hSession.save(inventory);
								transaction.commit();
								result.put("success", true);
							}
							
						}
					}
					
					return order.getId();
				}
				
			}
			else {
				
				Transaction transaction = hSession.beginTransaction();
				Customer newCustomer = new Customer();
				
				long lastId, newId;
				Query idList = hSession.createQuery("select max(id) from Customer");
				if(idList != null && !idList.list().isEmpty()){
					if(idList.list().get(0) != null){
						lastId = (long) idList.list().get(0);
						newId = lastId + 1;
					}
					else{
						lastId = 0;
						newId = 1;
					}
				}
				else{
					lastId = 0;
					newId = 1;
				}
				
				
				newCustomer.setName(customerName.toUpperCase());
				newCustomer.setCreatedOn(new Date());
				newCustomer.setUpdatedOn(new Date());
				newCustomer.setDeleted(false);
				newCustomer.setWalkIn(false);

				hSession.saveOrUpdate(newCustomer);
				transaction.commit();
				
				System.out.println("saved new customer");
				
				Customer newSavedCust = (Customer)hSession.createQuery("from Customer where name=?").setString(0, customerName).setMaxResults(1).uniqueResult();
				
				List<Orders> orderList = null;
				Orders order = null;
				
				transaction = hSession.beginTransaction();
				
				order = new Orders();
				order.setScannedBy(userId);
				order.setCustomerId(newCustomer.getId());
				order.setCart(cartSet);
				order.setCount(cartSet.size());
				order.setCount(count);
				order.setTotalMtSquare(totalMtSquare);
				order.setCreatedOn(new Date());
				order.setUpdatedOn(new Date());
				
				hSession.save(order);
				transaction.commit();
				
				transaction = hSession.beginTransaction();
				result.put("success", true);
				
				Iterator<Cart> i = cartSet.iterator();
				while (i.hasNext()) {
					Cart cart = i.next();
					
					if(cart.isTile()) {
						
						transaction = hSession.beginTransaction();
						System.out.println("cart.getInventory().getId(): "+ cart.getInventoryId());
						Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventoryId());
						
						int currentTileCount = inventory.getTileCount();
						int newTileCount = currentTileCount - cart.getTileCount();
						
						if(newTileCount < 0) {
							result.put("success", false);
							result.put("message", "Final Inventory Tilecount cannot be less than 0");
							throw new ValidationException("Final Inventory Tilecount cannot be less than 0");
						}
						else {
							inventory.setTileCount(newTileCount);
							if(newTileCount == 0) {
								inventory.setSold(true);
							}
							
							hSession.save(inventory);
							transaction.commit();
							result.put("success", true);
						}
						
					}
				}
				
				return order.getId();
				
			}
			
			
		}
		catch (Exception e)
		{
			result.put("success", false);
			if(result.get("message") == null || result.get("message").equals("")) {
				result.put("message", "Order of "+customerName+" could not be saved.");
			}
			e.printStackTrace();
			return 0L;
		}
		finally
		{
			hSession.close();
		}
		return 0L;
	
	}
	
	public long addNewCustomer (String customerName) {
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		try {
			Transaction transaction = hSession.beginTransaction();
			Customer newCustomer = new Customer();
			
			long lastId, newId;
			Query idList = hSession.createQuery("select max(id) from Customer");
			if(idList != null && !idList.list().isEmpty()){
				if(idList.list().get(0) != null){
					lastId = (long) idList.list().get(0);
					newId = lastId + 1;
				}
				else{
					lastId = 0;
					newId = 1;
				}
			}
			else{
				lastId = 0;
				newId = 1;
			}
			
			
			newCustomer.setName(customerName.toUpperCase());
			newCustomer.setCreatedOn(new Date());
			newCustomer.setUpdatedOn(new Date());
			newCustomer.setDeleted(false);
			newCustomer.setWalkIn(false);

			hSession.saveOrUpdate(newCustomer);
			transaction.commit();
			
			return newCustomer.getId();
		}
		catch(Exception e) {
			e.printStackTrace();
			return 0L;
		}
		finally {
			hSession.close();
		}
		
	}
	
	public Map<String, Object> addNewCuttingOrder(long orderId, Date orderDate, CuttingOrder cOrder, long customerId, String customerName, long userId, Date deliveryDate) throws ValidationException{
		
		Map<String, Object> result = new HashMap<>();
		System.out.println("Called addNewCuttingOrder");
		
		System.out.println("orderId: "+ orderId);
		System.out.println("orderDate: "+ orderDate);
		System.out.println("customerId: "+ customerId);
		System.out.println("customerName: "+ customerName);
		
		System.out.println("COTaskSet SIze: "+ cOrder.getCuttingOrderTask().size());
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		try {
			Transaction transaction = hSession.beginTransaction();
			
			// Add to existing order
			Orders existingOrder = (Orders) hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(existingOrder.getCuttingOrder() != null && existingOrder.getCuttingOrder().size() > 0) {
				existingOrder.getCuttingOrder().add(cOrder);
			}
			else {
				
				Set<CuttingOrder> cutSet = new HashSet<>();
				cutSet.add(cOrder);
				existingOrder.setCuttingOrder(cutSet);
				
				hSession.save(existingOrder);
			}
			cOrder.setOrderId(existingOrder.getId());
			cOrder.setCreatedOn(orderDate);
			cOrder.setUpdatedOn(new Date());
			hSession.save(cOrder);
			transaction.commit();

			result.put("success", true);
			result.put("orderId", orderId);
			
		}
		catch(Exception e) {
			e.printStackTrace();
			result.put("success", false);
			result.put("orderId", 0);
		}
		finally {
			hSession.close();
		}
		
		
		return result;
	}
	/*	public void addNewCuttingOrder(long cuttingOrderNumber, long orderId, Date orderDate, double totalInputMtSquare, double totalOutputMtSquare, double amount, 
				Set<CuttingOrderCart> cuttingOrderCart, long customerId, String customerName, double totalOutputLinearMeter) throws ValidationException{
			
			System.out.println("Called addNewCuttingOrder");
			Session hSession = HibernateBridge.getSessionFactory().openSession();
			try {
				Transaction transaction = hSession.beginTransaction();
				
				
				CuttingOrder cOrder = new CuttingOrder();
				if(orderId == 0) {
					//Create new order
					Orders order = new Orders();
					Customer existingCustomer = null;
					if(customerId != 0) {
						existingCustomer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, customerId).setMaxResults(1).uniqueResult();
						
						if(existingCustomer != null) {
							order.setCustomerId(customerId);
						}
						else {
							
							long newCustomerId = addNewCustomer(customerName);
							order.setCustomerId(newCustomerId);
							
						}
						
						order.setTotalMtSquare(totalOutputMtSquare);
						order.setHasCuttingSlabs(true);
						order.setCreatedOn(new Date());
						order.setUpdatedOn(new Date());
						
						hSession.save(order);
						
						Iterator<CuttingOrderCart> coCartItr = cuttingOrderCart.iterator();
						
						while(coCartItr.hasNext()) {
							CuttingOrderCart coCartObj = coCartItr.next();
							
							System.out.println("machineTask - loop: "+ coCartObj.getMachineTaskId());
						}
						
						cOrder.setTotalOutputLinearMeter(totalOutputLinearMeter);
						cOrder.setAmount(amount);
						cOrder.setCuttingOrderCart(cuttingOrderCart);
						cOrder.setCuttingOrderNumber(cuttingOrderNumber);
						cOrder.setTotalInputMtSquare(totalInputMtSquare);
						cOrder.setTotalOutputMtSquare(totalOutputMtSquare);
						cOrder.setOrderId(order.getId());
						
						cOrder.setCreatedOn(new Date());
						cOrder.setUpdatedOn(new Date());
						
						hSession.save(cOrder);
						transaction.commit();
					}
				}
				else {
					// Add to existing order
					Orders existingOrder = (Orders) hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
					
					cOrder.setTotalOutputLinearMeter(totalOutputLinearMeter);
					cOrder.setAmount(amount);
					cOrder.setCuttingOrderCart(cuttingOrderCart);
					cOrder.setCuttingOrderNumber(cuttingOrderNumber);
					cOrder.setTotalInputMtSquare(totalInputMtSquare);
					cOrder.setTotalOutputMtSquare(totalOutputMtSquare);
					cOrder.setOrderId(existingOrder.getId());
					
					cOrder.setCreatedOn(new Date());
					cOrder.setUpdatedOn(new Date());
					
					hSession.save(cOrder);
					transaction.commit();
				}
				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			finally {
				hSession.close();
			}
		}
	*/	
	
	public String updateInvoice(String invoiceNumber, Date invoiceDate) throws ValidationException{

		System.out.println("invoiceNumber: "+ invoiceNumber);
		System.out.println("invoiceDate: "+ invoiceDate);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		try
		{
			List<InvoiceCutting> invoiceCuttingList = null;
			InvoiceCutting invCutting = new InvoiceCutting();
			
			invoiceCuttingList = (List<InvoiceCutting>)hSession.createQuery("from InvoiceCutting where invoiceNumber=?").setString(0, invoiceNumber).list();
			
			if(invoiceCuttingList.size() > 0){
				throw new ValidationException("Invoice Already Exists");
			}
			else{
				invCutting.setInvoiceNumber(invoiceNumber);
				invCutting.setInvoiceDate(invoiceDate);
				invCutting.setCreatedOn(new Date());
				invCutting.setUpdatedOn(new Date());

				hSession.saveOrUpdate(invCutting);
				
				return invoiceNumber;
			}
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			hSession.close();
		}
		
		return invoiceNumber;
	}
	/*public Map<String, Object> addCuttingOrderCartDetails(Long cuttingOrderNumber, double totalOutputMtSquare) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<Cutting> cuttingIterator = cuttingSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<InvoiceCutting> invoiceCuttingList = null;
			InvoiceCutting invCutting = new InvoiceCutting();
			
			invoiceCuttingList = (List<InvoiceCutting>)hSession.createQuery("from InvoiceCutting where invoiceNumber=?").setString(0, invoiceNumber).list();
		
			if(invoiceCuttingList.size() > 0){
				message = "Invoice Already Exists";
				throw new ValidationException("Invoice Already Exists");
			}
			else{
				invCutting.setInvoiceNumber(invoiceNumber);
				invCutting.setInvoiceDate(invoiceDate);
				invCutting.setCreatedOn(new Date());
				invCutting.setUpdatedOn(new Date());
				
				invCutting.setCuttingOrders(cuttingSet);
	
				hSession.save(invCutting);
				transaction.commit();
			
			}
			
			
			result.put("success", true);
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}*/
	
	public Map<String, Object> updateCuttingOrder(long cuttingOrderId, long cuttingOrderNUmber, double totalInputMtSquare, Set<CuttingOrderCart> cuttingSet, double totalOutputMtSquare) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator<CuttingOrderCart> cuttingIterator = cuttingSet.iterator();
		
		List<Cutting> cutting = null;
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<InvoiceCutting> invoiceCuttingList = null;
			CuttingOrder invCutting = new CuttingOrder();
			
			invCutting = (CuttingOrder)hSession.createQuery("from CuttingOrder where id=?").setLong(0, cuttingOrderId).setMaxResults(1).uniqueResult();
		
			if(invCutting != null){
				System.out.println("cuttingSet.size(): "+cuttingSet.size());
				invCutting.setCuttingOrderNumber(cuttingOrderNUmber);
				invCutting.setTotalOutputMtSquare(totalOutputMtSquare);
				invCutting.setUpdatedOn(new Date());
				
				hSession.save(invCutting);
				transaction.commit();

				result.put("success", true);
				return result;
			}
			else{
				throw new ValidationException("Invoice does not exist.");
			}
			
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> getAllCuttingList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<Supplier> cuttingList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray cuttingListArray = new JSONArray();
			
			cuttingList = hSession.createQuery("from Cutting order by cuttingId asc").list();
			
			Iterator<Supplier> i = cuttingList.iterator();
			while(i.hasNext()){
				Supplier cutting = i.next();
				object = JSON.toJSONObject(cutting);
				cuttingListArray.put(object);
			}
			
			result.put("success", true);
			result.put("data", cuttingListArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> fetchCuttingOrder(Long cuttingOrderId)
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		try
		{
			CuttingOrder c = new CuttingOrder();
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray basketArray = new JSONArray();
			
			c = (CuttingOrder) hSession.createQuery("from CuttingOrder where id=?").setLong(0, cuttingOrderId).setMaxResults(1).uniqueResult();


			
			JSONObject obj = null;
			
			obj = new JSONObject();
			obj.put("id", c.getId());
			obj.put("amount", c.getAmount());
			obj.put("cuttingOrderNumber", c.getCuttingOrderNumber());
			obj.put("orderId", c.getOrderId());
			obj.put("totalInputMtSquare", c.getTotalInputMtSquare());
			obj.put("totalOutputMtSquare", c.getTotalOutputMtSquare());
			obj.put("createdOn", c.getCreatedOn());
			obj.put("deletedOn", c.getDeletedOn());
			obj.put("DOPrintedOn", c.getDOPrintedOn());
			obj.put("placedOn", c.getPlacedOn());
			obj.put("updatedOn", c.getUpdatedOn());
			obj.put("deleted", c.isDeleted());
			obj.put("DOPrinted", c.isDOPrinted());
			obj.put("placed", c.isPlaced());

			
			if(c.getOrderId() != 0) {
				
				try {
					
					Orders order = (Orders) hSession.get(Orders.class, c.getOrderId());
					if(order.getCustomerId() != 0) {
						Customer customer = (Customer)hSession.get(Customer.class, order.getCustomerId());
						
						obj.put("customerName", customer.getName());
						obj.put("customerId", customer.getId());
						obj.put("emirate", customer.getEmirate());
						obj.put("phone", customer.getPhone());
						obj.put("trnNumber", customer.getTrnNumber());
						obj.put("VATRegistrationDate", customer.getVATRegistrationDate());
						obj.put("VATRegistration", customer.isVATRegistration() ? "1" : "0");
						obj.put("country", customer.getCountry());
						obj.put("customerCreatedOn", customer.getCreatedOn());
						obj.put("customerUpdatedOn", customer.getUpdatedOn());
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
			
			
		
			
			/*object = JSON.toJSONObject(cuttingOrder);*/
			
			result.put("success", true);
			result.put("data", obj);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.put("success", false);
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> fetchCuttingOrderWithOrderId(Long orderId)
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		try
		{
			Orders order = new Orders();
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray basketArray = new JSONArray();
			
			order = (Orders) hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();

			if(order.isHasCuttingSlabs()) {
				//find cutting orders
				
				JSONObject obj = JSON.toJSONObject(order);
				
				result.put("success", true);
				result.put("data", obj);
			}
			else {
				message = "This order does not have any cutting job attached";
				throw new ValidationException(message);
			}
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.put("success", false);
			result.put("message", message);
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	
	
	public Map<String, Object> fetchAllCuttingOrders()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		try
		{
			int index1 = 0, index2 = 0;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray dataArray = new JSONArray();
			
			List<CuttingOrder> invCuttingList = (List<CuttingOrder>) hSession.createQuery("from CuttingOrder").list();

			Iterator<CuttingOrder> invoiceIterator = invCuttingList.iterator();
	        while (invoiceIterator.hasNext()) {
	        	org.json.JSONObject invoiceObject = new org.json.JSONObject();
	        	CuttingOrder invCut = invoiceIterator.next();
	        	object = JSON.getCuttingList(invCut);
	        	dataArray.put(object);
	        }
			
			result.put("success", true);
			result.put("data", dataArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.put("success", false);
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> addSingleCuttingType(String name, String cuttingMode) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			CuttingType cuttingType = (CuttingType)hSession.createQuery("from CuttingType where name=?").setString(0, name).setMaxResults(1).uniqueResult();
			
			if(cuttingType != null){
				
				message = "CuttingType Already Exists. Please check name again.";
				throw new ValidationException(message);
				
			}
			else{
				
				System.out.println("");
				cuttingType = new CuttingType();
				cuttingType.setName(name.toUpperCase());
				
				if(cuttingMode.equals("1")) {
					cuttingType.setMachine(true);
					cuttingType.setManual(false);
				}
				else {
					cuttingType.setManual(true);
					cuttingType.setMachine(false);
				}
				
				hSession.save(cuttingType);
				transaction.commit();
				
			}
			
			result.put("success", true);
			result.put("message", "Customer saved.");
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> getAllCuttingTypeList() throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<CuttingType> cuttingTypeList = (List<CuttingType>)hSession.createQuery("from CuttingType order by name asc").list();
			
			if(cuttingTypeList != null && cuttingTypeList.size() > 0){
				
				JSONArray cuttingTypeListArray = new JSONArray();
				
				Iterator<CuttingType> ctItr = cuttingTypeList.iterator();
				
				while(ctItr.hasNext()) {
					
					JSONObject obj = new JSONObject(); 
					CuttingType cuttingType = ctItr.next();
					obj = JSON.toJSONObject(cuttingType);
					cuttingTypeListArray.put(obj);
				}
				result.put("success", true);
				result.put("data", cuttingTypeListArray);
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> getMachineCuttingTypeList() throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<CuttingType> cuttingTypeList = (List<CuttingType>)hSession.createQuery("from CuttingType where machine=1 order by name asc").list();
			
			if(cuttingTypeList != null && cuttingTypeList.size() > 0){
				
				JSONArray cuttingTypeListArray = new JSONArray();
				
				Iterator<CuttingType> ctItr = cuttingTypeList.iterator();
				
				while(ctItr.hasNext()) {
					
					JSONObject obj = new JSONObject(); 
					CuttingType cuttingType = ctItr.next();
					obj = JSON.toJSONObject(cuttingType);
					cuttingTypeListArray.put(obj);
				}
				result.put("success", true);
				result.put("data", cuttingTypeListArray);
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> getManualCuttingTypeList() throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			Transaction transaction = hSession.beginTransaction();
			List<CuttingType> cuttingTypeList = (List<CuttingType>)hSession.createQuery("from CuttingType where manual=1 order by name asc").list();
			
			if(cuttingTypeList != null && cuttingTypeList.size() > 0){
				
				JSONArray cuttingTypeListArray = new JSONArray();
				
				Iterator<CuttingType> ctItr = cuttingTypeList.iterator();
				
				while(ctItr.hasNext()) {
					
					JSONObject obj = new JSONObject(); 
					CuttingType cuttingType = ctItr.next();
					obj = JSON.toJSONObject(cuttingType);
					cuttingTypeListArray.put(obj);
				}
				result.put("success", true);
				result.put("data", cuttingTypeListArray);
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public boolean bulkCuttingListUpload(String fileName){

		Session hSession = HibernateBridge.getSessionFactory().openSession();

		try{

			// TODO Auto-generated method stub
			String path="";

			try{
				Config config=new Config();
				if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
					path=config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: "+path);
				}else{
					path=config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: "+path);
				}
			}catch(IOException e){
				path = "/opt/supremeinventory/excels/";
			}

			System.out.println(path+File.separator+fileName);
			System.out.println("Path in mf: "+path+File.separator+fileName);
			File sourceFile = new File(path);

			System.out.println("Helloooo");

			if(sourceFile.exists())
			{
				System.out.println("sourceFileExists");
				File file=new File(path+File.separator+fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				boolean flag = false;
				int i=0;

				System.out.println("filetoRead: "+ filetoRead);
				System.out.println("entering while");
				while (rowIterator.hasNext()) {

					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					CuttingType existingCuttingType = new CuttingType();
					CuttingType cuttingType = new CuttingType();
					boolean cuttingTypeExists = false;
					
					if(i != 0){
						int j=0;
						System.out.println("i = "+i);
						while (cellIterator.hasNext()) {

							System.out.println("J === "+ j);
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly

							String name = "";

							if(j == 1){
								//NAME
								
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String cuttingTypename = cell.getStringCellValue().trim().toUpperCase();
									
									System.out.println("checking for existing customer:::::::::: "+ cuttingTypename);
									existingCuttingType = (CuttingType)hSession.createQuery("from CuttingType where name = ?").setString(0, cuttingTypename).setMaxResults(1).uniqueResult();
									
									if(existingCuttingType != null) {
										cuttingTypeExists = true;
										existingCuttingType.setName(cuttingTypename);
									}
									else {
										System.out.println("customer does not exist");
										cuttingTypeExists = false;
										cuttingType = new CuttingType();
										cuttingType.setName(cuttingTypename);
									}
									
								}
								else {
									flag = true;
									break;
								}
							}
							
							if(j == 2) {
								//machine bool
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String machineBool = cell.getStringCellValue().trim().toUpperCase();
									if(cuttingTypeExists) {
										if(machineBool.equals("YES")) {
											System.out.println("machineBool: "+ machineBool);
											existingCuttingType.setMachine(true);
										}
										else {
											existingCuttingType.setMachine(false);
										}
									}
									else {
										if(machineBool.equals("YES")) {
											System.out.println("machineBool: "+ machineBool);
											cuttingType.setMachine(true);
										}
										else {
											cuttingType.setMachine(false);
										}
									}
								}
							}
							
							if(j == 3) {
								//manual bool
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String manualBool = cell.getStringCellValue().trim().toUpperCase();
									if(cuttingTypeExists) {
										if(manualBool.equals("YES")) {
											System.out.println("manualBool: "+ manualBool);
											existingCuttingType.setManual(true);
										}
										else {
											existingCuttingType.setManual(false);
										}
									}
									else {
										if(manualBool.equals("YES")) {
											System.out.println("manualBool: "+ manualBool);
											cuttingType.setManual(true);
										}
										else {
											cuttingType.setManual(false);
										}
									}
								}
							}

							j++;
						}

						if(!flag){
							Transaction transaction = hSession.beginTransaction();

							try{
								if(cuttingTypeExists) {
									hSession.saveOrUpdate(existingCuttingType);
								}
								else {
									hSession.saveOrUpdate(cuttingType);
								}
								
								hSession.flush();
								
								transaction.commit();
							}
							catch(Exception e){
								e.printStackTrace();
								return false;
							}
						}
					}
					else{
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			hSession.close();
		}
	}
	
	
	
	
	public Map<String, Object> updateOrderCartRow(long cartId, long inventoryId, double actualHeight, double actualLength, 
			long thickness, double actualSqMetre, String type) throws ValidationException{

		System.out.println("updateOrderCartRow cartId: "+ cartId);
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		Transaction transaction = hSession.beginTransaction();
		
		try{
		
			Cart cart = null;
			Orders order = null;
			cart = (Cart)hSession.createQuery("from Cart where id=?").setLong(0, cartId).setMaxResults(1).uniqueResult();

			if(cart != null){
				System.out.println("cart id: "+ cart.getId());
				Inventory inventory = (Inventory)hSession.createQuery("from Inventory where id=?").setLong(0, cart.getInventoryId()).setMaxResults(1).uniqueResult();

				inventory.setActualHeight(actualHeight);
				inventory.setActualLength(actualLength);
				inventory.setActualSquareMetre(actualSqMetre);
				inventory.setThickness(thickness);
				
				if(type.equals("TILE")) {
					
				}

				hSession.save(cart);
				hSession.save(inventory);
				
				transaction.commit();
				
				message = "Order saved successfully.";
				result.put("success", true);
				result.put("message", message);
				
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
			
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
		
	}

	public Map<String, Object> updateOrderRow(long orderId, String customerName, double slabCount, double totalMtSquare, 
			String vehicleNumber, String emirate, String LPONumber, Date LPODate, boolean approveOrder, long userId, 
			String phone, boolean companyPickup, String narration, boolean hasCuttingOrders,
			boolean VATRegistration, Date VATRegistrationDOneOn, String trnNumber, boolean outsideMaterial, boolean skipScan) throws ValidationException{
		
		
		
		System.out.println("orderId: "+orderId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";
		
		try{
			
			Transaction transaction = hSession.beginTransaction();
			Cart cart = null;
			Orders order = null;
			
			order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
			
			if(order != null){
				
				Customer customer = (Customer) hSession.createQuery("from Customer where id=?").setLong(0, order.getCustomerId()).setMaxResults(1).uniqueResult();
				
				if(customer != null) {
					
					customer.setUpdatedOn(new Date());
					customer.setPhone(phone);
					
					
					System.out.println("Controller: VATRegistration: "+ VATRegistration);
					if(VATRegistration) {
						customer.setVATRegistrationDate(VATRegistrationDOneOn);
						customer.setVATRegistration(VATRegistration);
						customer.setTrnNumber(trnNumber);
					}
					else {
						customer.setVATRegistration(VATRegistration);
					}
					
					order.setCustomerId(customer.getId());
					
					hSession.save(customer);
					hSession.save(order);
					transaction.commit();
				}
				else {
					customer = new Customer();
					
					customer.setEmirate(emirate);
					customer.setCreatedOn(new Date());
					customer.setUpdatedOn(new Date());
					customer.setPhone(phone);
					customer.setName(customerName);
					
					if(VATRegistration) {
						customer.setVATRegistrationDate(VATRegistrationDOneOn);
						customer.setVATRegistration(VATRegistration);
						customer.setTrnNumber(trnNumber);
					}
					else {
						customer.setVATRegistration(VATRegistration);
					}
					
					hSession.save(customer);
					transaction.commit();
					
				}
				
				
				transaction = hSession.beginTransaction();
				System.out.println("cart id: "+ order.getId());
				
				
				System.out.println("Controller: skipScan: "+ skipScan);
				System.out.println("Controller: outsideMaterial: "+ outsideMaterial);
				order.setSkipAddingManualData(skipScan);
				order.setOutsideMaterial(outsideMaterial);
				
				order.setNarration(narration);
				order.setCount(slabCount);
				order.setHasCuttingSlabs(hasCuttingOrders);
				order.setTotalMtSquare(totalMtSquare);
				
				if(companyPickup) {
					order.setCompanyPickup(companyPickup);
				}
				else {
					order.setCompanyPickup(companyPickup);
					order.setVehicleNumber(vehicleNumber);
				}
				
				if(LPODate != null){
					order.setLPODate(LPODate);
				}
				if(LPONumber != null && !LPONumber.equals("")){
					order.setLPONumber(LPONumber);
				}
				order.setUpdatedOn(new Date());
				
				if(approveOrder) {
					order.setPlaced(approveOrder);
					order.setPlacedBy(userId);
					order.setPlacedOn(new Date());
				}
				
				hSession.save(order);
				transaction.commit();
				
				Iterator<Cart> i = order.getCart().iterator();
				while (i.hasNext()) {
					
					transaction = hSession.beginTransaction();
					cart = i.next();
					
					Inventory inventory = (Inventory)hSession.createQuery("from Inventory where id=?").setLong(0, cart.getInventoryId()).setMaxResults(1).uniqueResult();
					
					if(inventory != null){
						
						if(inventory.getTileCount() == 0) {
							inventory.setSold(true);
						}
						
						hSession.saveOrUpdate(inventory);
						transaction.commit();
					}
					else{
						message = "Order does not exist.";
						result.put("success", false);
						result.put("message", message);
						throw new ValidationException(message);
					}
				}
				
				
					
				message = "Order saved successfully.";
				result.put("success", true);
				result.put("message", message);
				result.put("orderId", order.getId());
	
			}
			else{
				message = "Order does not exist.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException("Order does not exist.");
			}
			
			return result;
		
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	
	}
	
	public long addNewEmptyOrderRow(long customerId, Date deliveryDate, Date orderDate){
		
		Orders order = new Orders();
		long orderId = 0L;
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try{
		
			Transaction t = hSession.beginTransaction();
			order.setCustomerId(customerId);
			order.setHasCuttingSlabs(true);
			order.setTotalMtSquare(0);
			order.setCount(0);
			order.setDeliveryDate(deliveryDate);
			order.setCreatedOn(orderDate);
			order.setUpdatedOn(orderDate);
			
			hSession.save(order);
			t.commit();
			
			System.out.println("New empty cutting orderId::: "+ order.getId());
			
			orderId = order.getId();
		
		}
		catch(Exception e) {
			e.printStackTrace();
			orderId = 0L;
		}
		finally {
			hSession.close();
		}
		
		return orderId;
	}
}
