package com.supreme.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;

import com.supreme.dao.Cutting;
import com.supreme.dao.Orders;
import com.supreme.dao.Machines;
import com.supreme.Config;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.dao.User;
import com.supreme.pojo.CuttingSet;
import com.supreme.test.PDFTest;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;
import com.supreme.util.PDFWrite;

public class DeliveryOrderController {
	
	public Map<String, Object> printDO(Orders order) throws ValidationException
	{
		String path = "";
		try{
			Config config=new Config();
			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
				path=config.get(Config.PDF_DEV_PATH);
				System.out.println("Dev path: "+path);
			}else{
				path=config.get(Config.PDF_PROD_PATH);
				System.out.println("Prod path: "+path);
			}
		}catch(IOException e){
			path = "/opt/supremeinventory/pdf/";
		}
		
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		
		String URL="BlankDO.pdf";
		PDFWrite pdf=new PDFWrite();
		try{
			
			
			System.out.println(order.getId());
			
			
			File file=new File(PDFTest.class.getResource(URL).getPath());
			
			pdf.open(file);
			pdf.setFontSize(14);

			System.out.println("file: "+ file);
			
			
			/**
			 * Adding Order Number
			 */
			pdf.addString(""+order.getId(), 50, 666);
			
			
			pdf.setFontSize(10);
			
			/**
			 * Adding DO Print Date
			 */
			Calendar c=Calendar.getInstance();
			int date=c.get(Calendar.DAY_OF_MONTH),month=c.get(Calendar.MONTH),year=c.get(Calendar.YEAR);
			
			if(date<10){
				pdf.addString("0", 490, 670);
			}else{
				pdf.addString(""+((int)date/10), 490, 670);				
			}
			pdf.addString(""+((int)date%10), 495, 670);

			pdf.addString("/", 501, 670);
			
			if(month<10){
				pdf.addString("0", 505, 670);
			}else{
				pdf.addString(""+((int)month/10), 505, 670);				
			}
			
			pdf.addString(""+((int)(month%10) + 1), 510, 670);
			
			pdf.addString("/", 516, 670);
			
			pdf.addString(""+((int)year/1000), 520, 670);
			pdf.addString(""+((int)(year/100)%10), 525, 670);
			pdf.addString(""+((int)(year/10)%100), 530, 670);
			pdf.addString(""+((int)year%10), 535, 670);
			
			
			
			/**
			 * Adding Customer Name and Address
			 */
			
			Customer customer = (Customer) hSession.createQuery("from Customer where id = ?").setLong(0, order.getCustomerId()).setMaxResults(1).uniqueResult();
			
			String[] words = customer.getName().split("\\W+");
			String nameLine1 = "", nameLine2 = "";
			for (int i = 0; i < words.length; i++) {
				if(i < 4){
					nameLine1 += words[i]+" ";
				}
				else{
					nameLine2 += words[i]+" ";
				}
			}
			pdf.addString(nameLine1, 60, 623);
			pdf.addString(nameLine2, 35, 600);
			
			
			System.out.println("customerDetails.getShippingAddress(): "+ customer.getEmirate());
			pdf.addString(customer.getEmirate(), 35, 575);

			
			/**
			 * LPO Number: i=420, j=610
			 * LPO Date: i=510, j=610
			 * Vehice Number: i=440, j=580
			 */
			
			pdf.addString((order.getLPONumber() != null ? order.getLPONumber() : ""), 430, 610);
			pdf.addString((order.getLPODate() != null ? ""+order.getLPODate() : ""), 510, 610);
			
			if(order.isCompanyPickup()) {
				if(order.getVehicleNumber() != null && !order.getVehicleNumber().equals("")) {
					pdf.addString(order.getVehicleNumber(), 440, 580);
				}
				else {
					pdf.addString("Company Pickup", 440, 580);
				}
			}
			else {
				pdf.addString((order.getVehicleNumber() != null ? order.getVehicleNumber() : ""), 440, 580);
			}
			
			
			/**
			 * i:
			 * 30 for Serial No
			 * 60 for description
			 * 300 for length
			 * 370 for height
			 * 430 for Nos
			 * 510 for Qty
			 */
			
			/**
			 * j:
			 * 500 to 100 with 20 decrement for detailed lines
			 */
			
			
			/**
			 * Adding Cart details
			 */
			System.out.println(order.getCart().size());
			int serialNo = 1, y=505;
			Iterator<Cart> i = order.getCart().iterator();
			while (i.hasNext()) {
				Cart cart = i.next();
				
				pdf.addString(""+serialNo, 30, y);
				pdf.addString(cart.getInventory().getMaterial().getMaterialName()+" "+cart.getInventory().getThickness()+"cm "+cart.getInventory().getType(), 60, y);
				pdf.addString(""+cart.getInventory().getActualLength(), 300, y);
				pdf.addString(""+cart.getInventory().getActualHeight(), 370, y);
				pdf.addString("1", 430, y);
				pdf.addString(""+cart.getInventory().getActualSquareMetre()+" Sq. Mt.", 510, y);
				
				serialNo++;
				y -= 20;
			}
			
			String pdfName = customer.getName()+"-"+order.getId()+"-DO.pdf";
			
			pdf.save(path+File.separator+pdfName);
			
			System.out.println(file.getAbsolutePath());
			
			//printFile(pdfName);
			
			
			
			
			
			result.put("success", true);
			result.put("fileName", pdfName);
			
			return result;
		}catch(Exception e){
			e.printStackTrace();
			result.put("success", false);
			return result;
		}
		finally{
			hSession.close();
		}
		
	}
	
	public boolean printFile(String fileName) throws MalformedURLException, IOException {
		
		String path = "";
		try{
			Config config=new Config();
			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
				path=config.get(Config.PDF_DEV_PATH);
				System.out.println("Dev path: "+path);
			}else{
				path=config.get(Config.PDF_PROD_PATH);
				System.out.println("Prod path: "+path);
			}
		}catch(IOException e){
			path = "/opt/supremeinventory/pdf/";
		}
		
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
			in = new BufferedInputStream(new URL("file:///"+path).openStream());
			fout = new FileOutputStream(fileName);

			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
			}
			return true;
		 } 
		finally {
			if (in != null){
				in.close();
			}
			if (fout != null){
				fout.close();
			}
		 }
		
		
		/*
		
		
		String path = "";
		try{
			Config config=new Config();
			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
				path=config.get(Config.PDF_DEV_PATH);
				System.out.println("Dev path: "+path);
			}else{
				path=config.get(Config.PDF_PROD_PATH);
				System.out.println("Prod path: "+path);
			}
		}catch(IOException e){
			path = "/opt/supremeinventory/pdf/";
		}
		 try {
				String fileUrl = path+File.separator+fileName;
		        String outputPath = "C:\\Users\\User\\Desktop";
				URL url = new URL(fileUrl);
			        // create an input stream to the file
			        InputStream inputStream = url.openStream();
				// create a channel with this input stream
			        ReadableByteChannel channel = Channels.newChannel(
			                         url.openStream());
			        // create an output stream 
				FileOutputStream fos = new FileOutputStream(
			                         new File(outputPath));
			        // get output channel, read from input channel and write to it
				fos.getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
			        // close resources
				fos.close();
				channel.close();
				return true;
			     } catch(IOException e) {
			          e.printStackTrace();
			          return false;
			     }
	*/}
	
}
