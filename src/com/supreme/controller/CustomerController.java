package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.commons.collections.list.SetUniqueList;
import org.apache.pdfbox.contentstream.operator.state.SetMatrix;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Customer;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Locations;
import com.supreme.dao.Orders;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class CustomerController {

	public Map<String, Object> getAllCustomerList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			List<Customer> customerList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray customerListArray = new JSONArray();

			customerList = hSession.createQuery("from Customer where deleted=0 order by name asc").list();

			Iterator<Customer> i = customerList.iterator();
			while(i.hasNext()){
				Customer customer = i.next();
				object = JSON.toSmallJSONObject(customer);
				customerListArray.put(object);
			}

			result.put("success", true);
			result.put("data", customerListArray);

			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	public Map<String, Object> getAllSmallCustomerList()
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<Customer> customerList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray customerListArray = new JSONArray();
			
			customerList = hSession.createQuery("from Customer where deleted=0 order by name asc").list();
			
			Iterator<Customer> i = customerList.iterator();
			while(i.hasNext()){
				Customer customer = i.next();
				object = JSON.toSmallJSONObject(customer);
				customerListArray.put(object);
			}
			
			result.put("success", true);
			result.put("data", customerListArray);
			
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> editSingleCustomer(String name, long id, boolean attachToOrder, long orderId) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Customer customer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, id).setMaxResults(1).uniqueResult();

			if(customer != null){
				
				customer.setName(name.toUpperCase());
				customer.setUpdatedOn(new Date());

				hSession.save(customer);
				transaction.commit();
				
				if(attachToOrder) {
					transaction = hSession.beginTransaction();
					Orders order = (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).setMaxResults(1).uniqueResult();
					order.setCustomerId(id);
					hSession.saveOrUpdate(order);
					transaction.commit();
					
				}
				

				result.put("success", true);
				result.put("message", "Customer saved.");

			}
			else{

				message = "Customer does not exist. Please check name again.";
				result.put("success", false);
				result.put("message", message);
				throw new ValidationException(message);
				
			}

			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> addSingleNewCustomer(String name, String emirate, String phone, String country, String email, 
													boolean VATRegistration, Date VATRegistrationDate, String trnNumber, long orderId) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			
			Customer customer = (Customer)hSession.createQuery("from Customer where name=? and deleted=0").setString(0, name).setMaxResults(1).uniqueResult();
			
			if(customer != null) {
				result.put("success", false);
				result.put("message", "Customer with the same name exists. Please change the name and try again.");
			}
			else {
				customer = new Customer();
				customer.setName(name.toUpperCase());
				customer.setWalkIn(false);
				customer.setCountry(country);
				customer.setEmail(email);
				customer.setEmirate(emirate);
				customer.setPhone(phone);
				customer.setVATRegistration(VATRegistration);
				if(VATRegistration) {
					customer.setVATRegistrationDate(VATRegistrationDate);
					customer.setTrnNumber(trnNumber);
				}
				customer.setCreatedOn(new Date());
				customer.setUpdatedOn(new Date());
				
				hSession.save(customer);
				transaction.commit();
				
				long customerId = customer.getId();
				
				System.out.println("Customer SAVED?: yes, id: "+ customer.getId());
				
				
				transaction = hSession.beginTransaction();
				Orders order = (Orders)hSession.get(Orders.class, orderId);
				order.setCustomerId(customerId);
				hSession.save(order);
				transaction.commit();
				
				result.put("success", true);
				result.put("message", "Customer saved.");
				result.put("data", JSON.toJSONObject(customer));
			}
			
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", "Could not save customer. Please contact admin.");
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}
	
	public Map<String, Object> addNewCustomer(String name) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Customer customer = new Customer();

			customer = new Customer();
			customer.setName(name.toUpperCase());
			customer.setWalkIn(false);
			customer.setCreatedOn(new Date());
			customer.setUpdatedOn(new Date());

			hSession.save(customer);
			transaction.commit();
			
			result.put("success", true);
			result.put("message", "Customer saved.");
			result.put("customerId", customer.getId());
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			result.put("customerId", 0L);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}

	public Map<String, Object> deleteCustomer(String name, long id) throws ValidationException
	{
		System.out.println("deleteCustomer called \tname: "+ name + "\tId: "+id);
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try
		{
			Transaction transaction = hSession.beginTransaction();
			Customer customer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, id).setMaxResults(1).uniqueResult();
			
			if(customer != null) {
				
				System.out.println("customer name: "+ customer.getName());
				
				if(!customer.isDeleted()) {
					customer.setDeleted(true);
					customer.setUpdatedOn(new Date());
					hSession.saveOrUpdate(customer);
					transaction.commit();
					
					result.put("success", true);
					result.put("message", "Customer deleted successfully.");
				}
				else {
					result.put("success", false);
					result.put("message", "Customer already deleted.");
				}
			}
			else {
				result.put("success", false);
				result.put("message", "Location could not be found.");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			hSession.close();
		}
		return result;
	}
	
	public boolean bulkCustomerListUpload(String fileName){

		Session hSession = HibernateBridge.getSessionFactory().openSession();

		try{

			// TODO Auto-generated method stub
			String path="";

			try{
				Config config=new Config();
				if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
					path=config.get(Config.EXCEL_DEV_PATH);
					System.out.println("Dev path: "+path);
				}else{
					path=config.get(Config.EXCEL_PROD_PATH);
					System.out.println("Prod path: "+path);
				}
			}catch(IOException e){
				path = "/opt/supremeinventory/excels/";
			}

			System.out.println(path+File.separator+fileName);
			System.out.println("Path in mf: "+path+File.separator+fileName);
			File sourceFile = new File(path);

			System.out.println("Helloooo");

			if(sourceFile.exists())
			{
				System.out.println("sourceFileExists");
				File file=new File(path+File.separator+fileName);
				/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
				FileInputStream filetoRead = new FileInputStream(file);

				// Create Workbook instance holding reference to .xlsx file
				XSSFWorkbook workbook = new XSSFWorkbook(filetoRead);

				// Get first/desired sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);

				// Iterate through each rows one by one
				Iterator<Row> rowIterator = sheet.iterator();

				boolean flag = false;
				int i=0;

				System.out.println("filetoRead: "+ filetoRead);
				System.out.println("entering while");
				while (rowIterator.hasNext()) {

					System.out.println("row iterator has next");
					flag = false;
					Row row = rowIterator.next();
					// For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();

					Customer existingCustomer = new Customer();
					Customer customer = new Customer();
					boolean customerExists = false;
					boolean customerDetailsExists = false;
					
					customer.setUpdatedOn(new Date());
					customer.setCreatedOn(new Date());
					
					if(i != 0){
						int j=0;
						System.out.println("i = "+i);
						while (cellIterator.hasNext()) {

							System.out.println("J === "+ j);
							Cell cell = cellIterator.next();
							// Check the cell type and format accordingly

							String name = "";

							if(j == 1){
								//NAME
								
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String customername = cell.getStringCellValue().trim().toUpperCase();
									
									System.out.println("checking for existing customer:::::::::: "+ customername);
									existingCustomer = (Customer)hSession.createQuery("from Customer where name = ?").setString(0, customername).setMaxResults(1).uniqueResult();
									
									if(existingCustomer != null) {
										customerExists = true;
										existingCustomer.setName(customername);
									}
									else {
										System.out.println("customer does not exist");
										customerExists = false;
										customer = new Customer();
										customer.setName(customername);
										customer.setUpdatedOn(new Date());
										customer.setCreatedOn(new Date());
										
									}
									
								}
								else {
									flag = true;
									break;
								}
							}
							
							if(j == 2) {
								//COUNTRY
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String country = cell.getStringCellValue().trim().toUpperCase();
									
									if(customerExists) {
										 existingCustomer.setCountry(country);
									}
									else {
										System.out.println("customer does not exist");
										customer.setCountry(country);
									}
									
								}
							}
							
							
							if(j == 3) {
								//Emirate
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String emirate = cell.getStringCellValue().trim().toUpperCase();
									
									if(customerExists) {
										 existingCustomer.setEmirate(emirate);
									}
									else {
										System.out.println("customer does not exist");
										customer.setEmirate(emirate);
									}
									
								}
							}

							
							if(j == 4) {
								//VAT REG Type
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String VATReg = cell.getStringCellValue().trim().toUpperCase();
									
									if(customerExists) {
										
										if(VATReg.equals("REGULAR")) {
											existingCustomer.setVATRegistration(true);
										}
										else {
											existingCustomer.setVATRegistration(false);
										}
									}
									else {
										if(VATReg.equals("REGULAR")) {
											customer.setVATRegistration(true);
										}
										else {
											customer.setVATRegistration(false);
										}
									}
									
								}
								else {
									if(customerExists) {
										existingCustomer.setVATRegistration(false);
									}
									else {
										customer.setVATRegistration(false);
									}
								}
							}

							if(j == 5) {
								//VAT REG Date
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									Date registrationDate = cell.getDateCellValue();
									
									if(customerExists) {
										existingCustomer.setVATRegistrationDate(registrationDate);
									}
									else {
										customer.setVATRegistrationDate(registrationDate);
									}
								
								}
								/*else {
									flag = true;
								}*/
							}
							if(j == 6) {
								//TRN
								if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
									String TRN = cell.getStringCellValue().trim().toUpperCase();
									
									if(customerExists) {
										existingCustomer.setTrnNumber(TRN);
									}
									else {
										customer.setTrnNumber(TRN);
									}
									
								}
							}

							j++;
						}

						System.out.println("FLAG::: "+ flag);

						if(!flag){
							Transaction transaction = hSession.beginTransaction();

							try{
								System.out.println("I = "+ i+" customer= "+customer.getName());
								
								if(customerExists) {
									hSession.saveOrUpdate(existingCustomer);
								}
								else {
									hSession.saveOrUpdate(customer);
								}
								
								hSession.flush();
								
								transaction.commit();
							}
							catch(Exception e){
								e.printStackTrace();
								return false;
							}
						}
					}
					else{
						System.out.println("i is 0. moving on");
					}
					i++;
				}
				filetoRead.close();
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		finally{
			hSession.close();
		}
	}
	/*public Map<String, Object> addNewAddress(String address, long attachToCustomer, long orderId, String phone) throws ValidationException
	{
		String message = "";
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
	
		try
		{
			Transaction transaction = hSession.beginTransaction();
			Customer customer = null;
	
			customer = (Customer)hSession.createQuery("from Customer where id=?").setLong(0, attachToCustomer).setMaxResults(1).uniqueResult();
	
			if(customer != null){
	
				CustomerDetails custDetails = (CustomerDetails) hSession.createQuery("from CustomerDetails where shippingAddress=? and customerId=?").setString(0, address).setLong(1, customer.getId()).setMaxResults(1).uniqueResult();
				
				if(custDetails != null) {
					message = "Customer Detail exists already. Kindly select that.";
					throw new ValidationException(message);
				}
				else {
					custDetails = new CustomerDetails();
					custDetails.setShippingAddress(address);
					custDetails.setPhone(phone);
					custDetails.setCustomerId(customer.getId());
					custDetails.setCreatedOn(new Date());
					custDetails.setUpdatedOn(new Date());
					hSession.saveOrUpdate(custDetails);
					transaction.commit();
				}
				
				
				if(orderId != 0) {
					hSession = HibernateBridge.getSessionFactory().openSession();
					CustomerDetails savedCustomerDetails = (CustomerDetails)hSession.createQuery("from CustomerDetails where shippingAddress=? and phone=?").setString(0, address).setString(1, phone).setMaxResults(1).uniqueResult();
					Orders order = (Orders)hSession.get(Orders.class, orderId);
					if(order != null) {
						transaction = hSession.beginTransaction();
						order.setCustomerDetailsId(savedCustomerDetails.getId());
						order.setUpdatedOn(new Date());
						hSession.save(order);
						transaction.commit();
					}
					else {
						message = "Order does not exist. Please check again.";
						throw new ValidationException(message);
					}
					
				}
	
			}
			else{
				message = "Customer does not exist. Please check name again.";
				throw new ValidationException(message);
			}
	
			result.put("success", true);
			result.put("message", "Customer saved.");
			return result;
		}
		catch (Exception e)
		{
			result.put("success", false);
			result.put("message", message);
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
	}*/
	
}

