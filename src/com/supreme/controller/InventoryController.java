package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.hibernate.transform.Transformers;
import org.json.JSONArray;

import com.supreme.Config;
import com.supreme.dao.Cart;
import com.supreme.dao.Container;
import com.supreme.dao.Inventory;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class InventoryController {

	public Map<String, Object> searchStock(long supplierId, long materialId, long thickness, String sold,
			int lengthEqualOrGreaterThan, String type, String barcodeString) {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		org.json.JSONObject object = new org.json.JSONObject();
		JSONArray inventoryListArray = new JSONArray();

		try {
			System.out.println(lengthEqualOrGreaterThan);
			String q = "from Inventory where id is not null ";
			if (supplierId != 0) {
				q += " and supplierId=:supplierId ";
			}
			if (materialId != 0) {
				q += " and materialId=:materialId ";
			}
			if (thickness != 0) {
				q += " and thickness=:thickness ";
			}
			if (lengthEqualOrGreaterThan != 0) {
				q += " and actualLength>=:lengthEqualOrGreaterThan ";
			}
			if (!sold.equals("")) {
				q += " and sold=:sold ";
			}
			if (!type.equals("")) {
				q += " and type like :type ";
			}
			if (!barcodeString.equals("")) {
				q += " and barcodeString like :barcodeString ";
			}

			Query query = hSession.createQuery(q);
			int i = 1;

			if (supplierId != 0) {
				query.setLong("supplierId", supplierId);
				i++;
			}
			if (materialId != 0) {
				query.setLong("materialId", materialId);
				i++;
			}
			if (thickness != 0) {
				query.setLong("thickness", thickness);
				i++;
			}
			if (lengthEqualOrGreaterThan != 0) {
				query.setDouble("lengthEqualOrGreaterThan", lengthEqualOrGreaterThan);
				i++;
			}
			if (!sold.equals("")) {
				query.setBoolean("sold", Boolean.parseBoolean(sold));
				i++;
			}
			if (!type.equals("")) {
				query.setString("type", "%" + type.toUpperCase() + "%");
				i++;
			}
			if (!barcodeString.equals("")) {
				query.setString("barcodeString", barcodeString.toUpperCase());
				i++;
			}

			System.out.println("--------------------- qury list size" + query.list().size());
			if (query.list().size() <= 0) {
				result.put("success", false);
				result.put("data", "");
			} else {

				Iterator<Inventory> inventoryIterator = query.list().iterator();
				while (inventoryIterator.hasNext()) {
					Inventory inventory = inventoryIterator.next();
					object = JSON.toJSONObject(inventory);
					inventoryListArray.put(object);
				}

				result.put("success", true);
				result.put("data", inventoryListArray);

			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

	public Map<String, Object> getSingleContainerInventoryListForPrint(long containerId) {

		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";

		try {
			Container container = new Container();
			List<Inventory> inventoryList = null;
			List<Container> containerList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray inventoryListArray = new JSONArray();
			JSONArray containerListArray = new JSONArray();

			container = (Container) hSession.createQuery("from Container where containerId=?").setLong(0, containerId)
					.setMaxResults(1).uniqueResult();

			inventoryList = hSession.createQuery("from Inventory where containerId=?")
					.setLong(0, container.getContainerId()).list();
			object = JSON.toContainerInventoryJSONObject(container, inventoryList);

			result.put("success", true);
			result.put("data", object);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}

	}

	public Map<String, Object> getSingleContainerInventoryList(long containerId) {

		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		String message = "";

		try {
			Container container = new Container();
			List<Inventory> inventoryList = null;
			List<Container> containerList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray inventoryListArray = new JSONArray();
			JSONArray containerListArray = new JSONArray();

			container = (Container) hSession.createQuery("from Container where containerId=?").setLong(0, containerId)
					.setMaxResults(1).uniqueResult();

			inventoryList = hSession.createQuery("from Inventory where containerId=?").setLong(0, container.getContainerId()).list();
			
			
			object = JSON.toContainerInventoryJSONObject(container, inventoryList);

			result.put("success", true);
			result.put("data", object);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}

	}

	public Map<String, Object> getContainerWiseInventoryList(Date lowerDate, Date higherDate) {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			List<Inventory> inventoryList = null;
			List<Container> containerList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray inventoryListArray = new JSONArray();
			JSONArray containerListArray = new JSONArray();

			System.out.println(lowerDate);
			System.out.println(higherDate);
			
			containerList = (List<Container>)hSession.createQuery("from Container where deleted=0 and createdOn >= ? and createdOn <= ?").setDate(0, lowerDate).setDate(1, higherDate).list();

			System.out.println("date range containerList.size(): "+containerList.size());

			
			Iterator<Container> c = containerList.iterator();
			while (c.hasNext()) {
				Container container = c.next();

				object = JSON.toJSONObject(container);

				containerListArray.put(object);
			}

			result.put("success", true);
			result.put("data", containerListArray);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

	public Map<String, Object> getFirstTwentyInventoryList() {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			List<Inventory> inventoryList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray inventoryListArray = new JSONArray();

			inventoryList = hSession.createQuery("from Inventory where sold=false").setMaxResults(20).list();

			Iterator<Inventory> i = inventoryList.iterator();
			while (i.hasNext()) {
				Inventory inventory = i.next();
				object = JSON.toJSONObject(inventory);
				inventoryListArray.put(object);
			}

			result.put("success", true);
			result.put("data", inventoryListArray);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

	public List<Inventory> getMasterInventoryList() {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			List<Inventory> inventoryList = null;
			org.json.JSONObject object = new org.json.JSONObject();
			JSONArray inventoryListArray = new JSONArray();

			inventoryList = hSession.createQuery("from Inventory").list();

			return inventoryList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

	public Map<String, Object> updateStockRow(long stockId, double length, double height, long thickness) {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();
		Transaction txn = hSession.beginTransaction();

		try {

			Inventory inventory = (Inventory) hSession.createQuery("from Inventory where id=?").setLong(0, stockId)
					.setMaxResults(1).uniqueResult();

			if (inventory != null) {
				inventory.setActualLength(length);
				inventory.setActualHeight(height);
				inventory.setThickness(thickness);

				hSession.saveOrUpdate(inventory);
				txn.commit();

				inventory = (Inventory) hSession.get(Inventory.class, inventory.getId());
				result.put("success", true);
				result.put("data", JSON.toJSONObject(inventory));

			} else {
				result.put("success", false);
				result.put("message", "Slab does not exist.");
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}
	/*
	 * public boolean singleInventoryUpdate(String supplierCode, String
	 * materialCode, Date arrivalDate, Long thickness, boolean flamed, boolean
	 * polished, boolean steps, boolean risers, double stepLength, double
	 * riserLength, String type, double squareMetre, double runningMetre, Long
	 * slabNumber, double length, double height, double actualLength, double
	 * actualHeight, String barcodeString){
	 * 
	 * System.out.println("singleInventoryUpdate method in controller entered");
	 * Session hSession = HibernateBridge.getSessionFactory().openSession();
	 * Transaction txn = hSession.beginTransaction();
	 * 
	 * try {
	 * 
	 * System.out.println("supplierCode: "+ supplierCode);
	 * System.out.println("materialCode: "+ materialCode); Long supplierId; Long
	 * materialId;
	 * 
	 * Supplier supplier =(Supplier)hSession.
	 * createQuery("from Supplier where supplierCode = :supplierCode").setString(
	 * "supplierCode", supplierCode).setMaxResults(1).uniqueResult(); supplierId =
	 * supplier.getSupplierId();
	 * 
	 * Material material =(Material)hSession.
	 * createQuery("from Material where materialCode = :materialCode").setString(
	 * "materialCode", materialCode).setMaxResults(1).uniqueResult(); materialId =
	 * material.getMaterialId();
	 * 
	 * Inventory inventory
	 * =(Inventory)hSession.createQuery("select count(*) from Inventory");
	 * 
	 * if(inventory != null && !inventory.equals(0)){ in }
	 * 
	 * materialId = material.getMaterialId();
	 * 
	 * Inventory inventory = new Inventory();
	 * 
	 * System.out.println("supName: "+supplier.getSupplierName());
	 * System.out.println("supId: "+supplier.getSupplierId());
	 * System.out.println("materialName: "+material.getMaterialName());
	 * System.out.println("materialId: "+material.getMaterialId());
	 * 
	 * if(supplierId != null){ System.out.println("supp id is not null");
	 * inventory.setSupplier(supplier); inventory.setSupplierId((Long)supplierId); }
	 * else{ System.out.println("supplierId is null"); }
	 * 
	 * if(supplierId != null){ System.out.println("mate id is not null");
	 * inventory.setMaterial(material); inventory.setMaterialId((Long)materialId); }
	 * else{ System.out.println("mate Id is null"); }
	 * 
	 * 
	 * inventory.setArrivalDate(arrivalDate); inventory.setThickness(thickness);
	 * inventory.setFlamed(flamed); inventory.setPolished(polished);
	 * inventory.setSteps(steps); if(steps && stepLength != 0){
	 * inventory.setStepLength(stepLength); } else{ inventory.setStepLength(0); }
	 * 
	 * inventory.setRisers(risers); if(risers && riserLength != 0){
	 * inventory.setRiserLength(riserLength); } else{ inventory.setRiserLength(0); }
	 * 
	 * inventory.setType(type); inventory.setSquareMetre(squareMetre);
	 * inventory.setRunningMetre(runningMetre); inventory.setSlabNoSupreme(0L);
	 * inventory.setActualLength(actualLength);
	 * inventory.setActualHeight(actualHeight); inventory.setLength(length);
	 * inventory.setHeight(height); inventory.setBarcodeString(barcodeString);
	 * 
	 * inventory.setCreatedOn(new Date()); inventory.setUpdatedOn(new Date());
	 * 
	 * hSession.saveOrUpdate(inventory); txn.commit(); return true; } catch
	 * (Exception e) { e.printStackTrace(); } finally { hSession.close(); } return
	 * false; }
	 */

	public Map<String, Object> getSingleSlabData(String barcodeString) {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		try {
			Inventory slab = (Inventory) hSession.createQuery("from Inventory where barcodeString=?")
					.setString(0, barcodeString).uniqueResult();

			if (slab != null) {

				List<Cart> cartList = (List<Cart>) hSession.createQuery("from Cart where inventoryId=?")
						.setLong(0, slab.getId()).list();


				if (cartList.size() > 0) {

					Iterator<Cart> c = cartList.iterator();
					while (c.hasNext()) {
						Cart cart = c.next();

						if (cart.getInventory().getType().equals("TILE") && cart.getInventory().getTileCount() <= 0) {
							/**
							 * Barcode is of tile and all have been sold
							 */
							System.out.println("Barcode is of tile and all have been sold");
							result.put("success", false);
							result.put("message", "Crate does not have any tiles left.");
						}
						else if ((cart.isTile() && cart.getInventory().getTileCount() > 0)) {
							/**
							 * Slab is in cart but has not been deleted, sold or, sent to cutting
							 */
							System.out.println("Slab is in cart but has remaining tile count");
							result.put("success", true);
							result.put("data", JSON.toJSONObject(slab));
						}

						else if (cart.isDeleted() == true) {
							/**
							 * Slab is in cart but has not been sold or sent to cutting
							 */
							System.out.println("Slab is in cart but has not been sold or sent to cutting");
							result.put("success", true);
							result.put("data", JSON.toJSONObject(slab));
						} else if (cart.getInventory().getSold()) {
							/**
							 * Slab has been sold
							 */
							System.out.println("Slab has been sold");
							result.put("success", false);
							result.put("message", "Slab has been sold.");
						} else if (cart.getInventory().getInCutting() != null && cart.getInventory().getInCutting()) {
							/**
							 * Slab has been sold and is being cut
							 */
							System.out.println("Slab has been sold and is being cut");
							result.put("success", false);
							result.put("message", "Slab has been sold and is in cutting.");
						}
						else {
							System.out.println("Slab is in another order");
							result.put("success", false);
							result.put("message", "Slab has been added to another order. Please scan another slab.");
						}

					}

				} else if (slab.getSold() != null && slab.getSold()) {
					System.out.println("Slab has been sold");
					result.put("success", false);
					result.put("message", "Slab has been sold.");
				} else if (slab.getInCutting() != null && slab.getInCutting()) {
					result.put("success", false);
					result.put("message", "Slab is in cutting for other order.");
				} else {
					result.put("success", true);
					result.put("data", JSON.toJSONObject(slab));
				}

			} else {
				result.put("success", false);
				result.put("message", "No slab exists with this barcode.");
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

	public Map<String, Object> findUniqueMaterialinInventory() {
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Map<String, Object> result = new HashMap<String, Object>();

		JSONArray materialListArray = new JSONArray();

		try {

			Criteria crit = hSession.createCriteria(Inventory.class);
			crit.add(Restrictions.gt("materialId", crit));
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List list = crit.list();

			System.out.println(list.size());
			System.out.println(list.get(0));

			/*
			 * .setProjection(Projections.distinct(Projections.projectionList().add(
			 * Projections.property("materialId"), "materialId"))
			 * .setResultTransformer(Transformers.aliasToBean(Material.class)));
			 */

			List lst = crit.list();
			/*
			 * Query query =
			 * hSession.createQuery("select distinct(materialId) from Inventory");
			 * 
			 * long lastId = (long) query.list().get(0);
			 * 
			 * System.out.println(lastId); List results = query.list();
			 * 
			 * Iterator<Inventory> c = results.iterator();
			 * 
			 * 
			 * while(c.hasNext()) { Inventory inv = c.next();
			 * System.out.println("MateriaalId: "+ inv.getMaterialId()); }
			 */

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			hSession.close();
		}
	}

}
