package com.supreme.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.ValidationException;

import org.apache.poi.hssf.OldExcelFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.json.JSONArray;
import org.json.JSONObject;

import com.supreme.dao.BankDetails;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Machines;
import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Material;
import com.supreme.dao.Orders;
import com.supreme.dao.Supplier;
import com.supreme.dao.Unit;
import com.supreme.dao.User;
import com.supreme.pojo.CuttingSet;
import com.supreme.util.HibernateBridge;
import com.supreme.util.JSON;

public class BankController {
	
	public Map<String, Object> listBankDetailsForCustomerId(long customerId) throws ValidationException{
		
		System.out.println(customerId);
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		try
		{
			List<BankDetails> bankDetailsList = (List<BankDetails>) hSession.createQuery("from BankDetails where customerId=?").setLong(0, customerId).list();
			
			if(bankDetailsList != null) {
				
				JSONArray bankArray = new JSONArray();
				Iterator<BankDetails> bankItr = bankDetailsList.iterator();
				while(bankItr.hasNext()) {
					BankDetails bd = bankItr.next();
					JSONObject obj = new JSONObject();
					obj = JSON.toJSONObject(bd);
					bankArray.put(obj);
				}
				result.put("success", true);
				result.put("data", bankArray);
				
			}else {
				result.put("success", false);
				result.put("message", "Could not find bank details.");
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return result;
		}
		finally
		{
			hSession.close();
		}
		return result;
		
	}
	
}
