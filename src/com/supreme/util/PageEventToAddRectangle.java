package com.supreme.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;

public class PageEventToAddRectangle  extends PdfPageEventHelper{
DOStatementPdf pdf=new DOStatementPdf();
	
	private static void addEmptyLine(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	        paragraph.add(new Paragraph(" "));
	    }
	}
	
	@Override
	public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();
        //Phrase header = new Phrase("this is a header");
        Font supremeBlackSubHeading = new Font(FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
        Phrase footer = new Phrase("Received above material in good condition and as per order.", supremeBlackSubHeading);
        Phrase received = new Phrase("Receiver's Signature.", supremeBlackSubHeading);
        Phrase forSM = new Phrase("For SUPREME MARBLES & GRANITE TRDG.", supremeBlackSubHeading);
        
        
        
        ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                received,
                document.leftMargin(),
                document.bottom()+5, 
                0);
        ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                forSM,
                550,
                document.bottom()+5, 0);
        
        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                footer,
                (document.right() - document.left()) / 2 + document.leftMargin(),
                document.bottom() - 10, 0);
    }
}