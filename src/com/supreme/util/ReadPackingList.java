package com.supreme.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.dao.Material;
import com.supreme.dao.Supplier;
import com.supreme.dao.User;
import com.supreme.util.HibernateBridge;

public class ReadPackingList {
	
	public static String fileName = "";
	public static String type = "";
	public static long supplierId;
	public static long materialId;
	public static long thickness;
	public static boolean polished;
	public static boolean flamed;

	public static double ParseDouble(String strNumber) {
	   if (strNumber != null && strNumber.length() > 0) {
	       try {
	          return Double.parseDouble(strNumber);
	       } catch(Exception e) {
	          return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
	       }
	   }
	   else return 0;
	}
	
	public ReadPackingList(String fileNameString){
		fileName = fileNameString;
		readPackingList();
	}
	
	public ReadPackingList(String fileNameString, long supplierId, long materialId, long thickness, String type, boolean flamed, boolean polished){
		fileName = fileNameString;
		type = type;
		supplierId = supplierId;
		materialId = materialId;
		thickness = thickness;
		flamed = flamed;
		polished = polished;
		readPackingList();
	}
	
	public static void readPackingList()
	{
		//reason of using this list because excel download from morning start has multiple amfiiCodes in it
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try{
			
			System.out.println("supplierId: "+ supplierId);
			System.out.println("materialId: "+ materialId);
			Supplier supplier = (Supplier) hSession.createQuery("from Supplier where id=?").setLong(0, supplierId).uniqueResult();
			if(supplier == null){
				System.out.println("supplier is null");
			}
			else{
				System.out.println("Supplier: "+ supplier.getSupplierName());
			}

			Material material = (Material) hSession.createQuery("from Material where id=?").setLong(0, materialId).uniqueResult();
			if(material == null){
				System.out.println("material is null");
			}
			else{
				System.out.println("Material: "+ material.getMaterialName());
			}
			
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.EXCEL_DEV_PATH);
    				System.out.println("Dev path: "+path);
    			}else{
    				path=config.get(Config.EXCEL_PROD_PATH);
    				System.out.println("Prod path: "+path);
    			}
    		}catch(IOException e){
    			path = "/opt/excels";
    		}
    		
    		System.out.println(path+File.separator+fileName);
    		System.out.println("Path in mf");
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
    			/*File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");*/
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext() && i < 5) { 
	            	
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Inventory inventory = new Inventory();

	
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    /**
		                     * Reading Slab Number
		                     */
		                    if(j == 0){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		int sNo = (int) cell.getNumericCellValue();
		                    		Long slabNo = (long) sNo;
		                    		inventory.setSlabNoSupreme(slabNo);
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 1){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setLength(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 2){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setHeight(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    /**
		                     * Reading Packing List Height
		                     */
		                    if(j == 3){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualLength(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 4){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualHeight(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 5){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setSquareMetre(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 6){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    if(j == 7){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(cell.getNumericCellValue());
		                    	}
		                    	else{
		                    		flag = true;
		                    	}
	                        }
		                    
		                    inventory.setSupplier(supplier);
		                    inventory.setSupplierId(supplierId);
		                    
		                    inventory.setMaterial(material);
		                    inventory.setMaterialId(materialId);
		                    
		                    inventory.setThickness(thickness);
		                    inventory.setType(type);
		                    inventory.setPolished(polished);
		                    inventory.setFlamed(flamed);
		                    inventory.setArrivalDate(new Date());
		                    inventory.setCreatedOn(new Date());
		                    inventory.setUpdatedOn(new Date());
		                    
		                    j++;
		                } 
	                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
		                	
		                	try{
		                		System.out.println("I = "+ i+" inventory= "+inventory.getSlabNoSupreme());
		                		
		                		hSession.saveOrUpdate(inventory);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                	}
		                }		            	
	                }
	                i++;
	            } 
	            filetoRead.close(); 
			}
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        }
        finally{
        	hSession.close();
        }
    }
}



	/*
	 * SELECT nav.amfiicode, mutualfund.name, mutualfund.id FROM nav INNER JOIN mutualfund ON nav.amfiicode = mutualfund.amfiicode WHERE nav.navDate = "2019-05-20" group by nav.amfiicode;*/
