package com.supreme.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.xml.bind.ValidationException;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jfree.ui.Align;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.supreme.dao.Cart;
import com.supreme.dao.Customer;
import com.supreme.dao.Orders;
import com.supreme.test.PDFTest;

public class DOStatementPdf extends PdfPageEventHelper
{
	
	public static void main(String args[]) throws Exception {  
		PdfReader pdfReader=null;
		try
		{
			Document document = new Document();
			
			String FILE = "C:/NEW/pdfNEW14FE.pdf";  
			PdfWriter writer =PdfWriter.getInstance(document, new FileOutputStream(FILE));
			PageEventToAddRectangle pageEventToAddRectangle=new PageEventToAddRectangle();
			writer.setPageEvent(pageEventToAddRectangle);
			
			System.out.println(document.getPageSize());
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
		     
			DOStatementPdf accountStatementPdf=new DOStatementPdf();
			accountStatementPdf.createPdf(16L, document);  //1720
			  
			document.close();
			  
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(pdfReader!=null)
			pdfReader.close();
		}
		  
		System.out.println("PDF Created");    
	} 
	
	
	public void createPdf(long orderId, Document document) throws ValidationException
	{
		document.setPageSize(PageSize.LETTER);
		document.open();
		System.out.println(document.getPageSize());
		Session hSession = null;
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		/*BaseColor supremeBlue = new BaseColor(20,67,149);
		BaseColor supremeRed = new BaseColor(163,71,79);*/
		
		BaseColor black = BaseColor.BLACK;
		/**
		 * Red Color: a3474f
		 * R: 163, G: 71, B: 79
		 */
		
		/**
		 * Blue Color: 144395
		 * R: 20, G: 67, B: 149
		 */
		
		try {
			
			Font supremeBlackHeading = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
			
			Font supremeBlackNormal = new Font(FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLACK);
			Font supremeBlackSmall = new Font(FontFamily.HELVETICA, 6, Font.BOLD, BaseColor.BLACK);
			
			hSession = HibernateBridge.getSessionFactory().openSession();
			Orders order= (Orders)hSession.createQuery("from Orders where id=?").setLong(0, orderId).uniqueResult();
			
			List<Cart> cartList = (List<Cart>)hSession.createQuery("from Cart where orderId=? and deleted=false order by id asc").setLong(0, orderId).list();
			
			Customer customer = (Customer) hSession.createQuery("from Customer where id=?").setLong(0, order.getCustomerId()).setMaxResults(1).uniqueResult();
			
			document.addTitle(customer.getName());
	        document.addSubject("Using iText");
	        document.addKeywords("Java, PDF, iText");
	        document.addAuthor("Supreme Marbles");
	        document.addCreator("Supreme Marbles");
	      
	        document.setPageSize(PageSize.A4);
	        Rectangle originalPageRect=document.getPageSize(); 
	        document.add(originalPageRect);
	       
	        Date todayDate= new Date();
		    String stringTodayDate =dateFormat.format(todayDate);
		    Date formatTodayDate=dateFormat.parse(stringTodayDate);
	
		    float pageLeft=originalPageRect.getLeft();
			float pageRight=originalPageRect.getRight();
			float pageBottom=originalPageRect.getBottom();
			float pageTop=originalPageRect.getTop();
			
	        PdfPTable table1 = new PdfPTable(2);
	        table1.setTotalWidth(500);
	        table1.setWidthPercentage(100);
	        table1.setWidths(new int[]{100,400});
	        
	        /*Rectangle pageRect =getRectangle(pageLeft+30, pageBottom+30,pageRight-30,pageTop-30, document);
	        pageRect.setBorder(0);*/
	        /*document.add(pageRect);*/
	        
	        String URL="Supreme-Letter-Head-New.png";
	        File file=new File(DOStatementPdf.class.getResource(URL).getPath());
	        
	        BufferedImage headerImg = ImageIO.read(file);
	        //File file=new File(DOStatementPdf.class.getResource("Supreme-Letter-head-image.png").getPath());
	        //String logoUrl = "https://inventory.suprememarbles.com/assets/images/Supreme-Letter-head-image.png";
	        Image img = Image.getInstance(headerImg, null);
	        img.setAlignment(Element.ALIGN_CENTER);
	        document.add(img);
	        Paragraph  emptyParagraph = new Paragraph();
	        addEmptyLine(emptyParagraph, 1);
	        document.add(emptyParagraph);
	        
	        PdfPTable tableTemp = new PdfPTable(1);
	        tableTemp.setTotalWidth(500);
	        tableTemp.setWidthPercentage(100);
	        tableTemp.setWidths(new int[]{100});
	        
	        PdfPCell cell2 = new PdfPCell();
	        cell2.setBorder(0);
	        Chunk deliveryOrderHeading = new Chunk("DELIVERY ORDER", supremeBlackHeading);
	        Paragraph  pdfHeadingParagraph = new Paragraph(deliveryOrderHeading);
	        pdfHeadingParagraph.setAlignment(Element.ALIGN_CENTER);
	        document.add(pdfHeadingParagraph);
	        addEmptyLine(pdfHeadingParagraph, 3);
	        
	        /**
	         * Order Number and Date in Left and Right Alignment
	         */
	        Chunk orderNoHeading = new Chunk("Number.: ", supremeBlackHeading);
	        Chunk orderNo = new Chunk(""+order.getId(), supremeBlackHeading);
	        
	        DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");  
	        String tillDate = dateFormat2.format(new Date());
	        Chunk dateHeading = new Chunk("Date: ", supremeBlackHeading);
	        Chunk date = new Chunk(tillDate, supremeBlackHeading);
	        
	        Chunk glue = new Chunk(new VerticalPositionMark());
	        Phrase p = new Phrase();
	        p.add(orderNoHeading);
	        p.add(orderNo);
	        p.add(glue);
	        p.add(dateHeading);
	        p.add(date);
	        
	        document.add(p);
	        
	        emptyParagraph = new Paragraph();
	        addEmptyLine(emptyParagraph, 1);
	        document.add(emptyParagraph);
	        
	        
	        
	        /**
	         * Name and LPO Details in left and right alignment
	         */
	        Chunk customerNameHeading = new Chunk("M.s.: ", supremeBlackHeading);
	        Chunk customerName = new Chunk(""+customer.getName(), supremeBlackHeading);
	        Chunk LPONumberHeading = new Chunk("LPO No: ", supremeBlackHeading);
	        Chunk LPONumber = new Chunk(order.getLPONumber() != null ? order.getLPONumber() : "         ", supremeBlackHeading);
	        Chunk LPODateHeading = new Chunk(" Dt.: ", supremeBlackHeading);
	        
	        Chunk LPODate = null;
	        if(order.getLPODate() != null){
	        	LPODate = new Chunk(dateFormat2.format(order.getLPODate()).toString(), supremeBlackHeading);
	        }
	        else{
	        	LPODate = new Chunk("             ");
	        }
	        
	        Phrase NameLPODetailsPhrase = new Phrase();
	        NameLPODetailsPhrase.add(customerNameHeading);
	        NameLPODetailsPhrase.add(customerName);
	        NameLPODetailsPhrase.add(glue);
	        NameLPODetailsPhrase.add(LPONumberHeading);
	        NameLPODetailsPhrase.add(LPONumber);
	        NameLPODetailsPhrase.add(LPODateHeading);
	        NameLPODetailsPhrase.add(LPODate);
	        
	        document.add(NameLPODetailsPhrase);
	        
	        document.add(emptyParagraph);
	        
	        /**
	         * Address and Vehicle Number to be added
	         */
	        Chunk addressHeading, address;
	        if(order.getCustomerId() != 0) {
	        	addressHeading = new Chunk("Address: ", supremeBlackHeading);
		        address = new Chunk(""+customer.getEmirate(), supremeBlackHeading);
	        }
	        else {
	        	addressHeading = new Chunk("Address: ", supremeBlackHeading);
	        	address = new Chunk("UAE", supremeBlackHeading);
	        }
	        
	        Chunk vehicleNumberHeading = new Chunk("Vehicle Number: ", supremeBlackHeading);
	        /*Chunk vehicleNumber = new Chunk("     "+order.getVehicleNumber() != null ? order.getVehicleNumber() : "         ", supremeRedHeading);*/
	        Chunk vehicleNumber = new Chunk((order.getVehicleNumber() != null ? ""+order.getVehicleNumber() : "         "), supremeBlackHeading);
	        
	        
	        
	        Phrase AddressVehicleNumber = new Phrase();
	        AddressVehicleNumber.add(addressHeading);
	        AddressVehicleNumber.add(address);
	        AddressVehicleNumber.add(glue);
	        AddressVehicleNumber.add(vehicleNumberHeading);
	        AddressVehicleNumber.add(vehicleNumber);
	        
	        document.add(AddressVehicleNumber);
	        
	        document.add(emptyParagraph);
	        
	        
	        double sqMeter = 0.0;
	        double currentSqMeter = 0.0;
	        long oldMaterialId = 0;
	        long currentMaterialId = 0;
	        long newMaterialId = 0;
	        
	        PdfPTable table = new PdfPTable(6);
	        table.setWidthPercentage(100);
	        table.setWidths(new int[]{10,40,10,5,10,10});
	        PdfPCell cellSNo = new PdfPCell();
	        PdfPCell cellDescription = new PdfPCell();
	        PdfPCell cellSize = new PdfPCell();
	        PdfPCell cellNos = new PdfPCell();
	        PdfPCell cellQty = new PdfPCell();
	        PdfPCell cellSqMt = new PdfPCell();
	        
	        cellSNo.setBorder(1);
	        cellDescription.setBorder(1);
	        cellSize.setBorder(1);
	        cellNos.setBorder(1);
	        cellQty.setBorder(1);
	        cellSqMt.setBorder(1);
	        cellSNo.setBorderColorLeft(black);
	        cellDescription.setBorderColorLeft(black);
	        cellSize.setBorderColorLeft(black);
	        cellSNo.setBorderColorLeft(black);
	        cellSNo.setBorderColorLeft(black);
	        
	        cellSNo.addElement(new Phrase("S. No.", supremeBlackHeading));
	        table.addCell(cellSNo);
	
	        cellDescription.addElement(new Phrase("Description", supremeBlackHeading));
	        table.addCell(cellDescription);
	        
	        cellSize.addElement(new Phrase("Size (cm)", supremeBlackHeading));
	        table.addCell(cellSize);
	        
	        cellNos.addElement(new Phrase("Nos.", supremeBlackHeading));
	        table.addCell(cellNos);
	        
	        cellQty.addElement(new Phrase("Qty", supremeBlackHeading));
	        table.addCell(cellQty);
	        
	        cellQty.addElement(new Phrase("Sq. Mt.", supremeBlackHeading));
	        table.addCell(cellSqMt);
	        
	        int slabCount = 0;
	        
	        double totalSqMeterAddition = 0.0;
	        int i = 1;
			if(cartList != null) 
			{
				Iterator<Cart> c = cartList.iterator();
				Iterator<Cart> cNext = cartList.iterator();
				Cart newCart = new Cart();
				newCart = cNext.next();
				while(c.hasNext()){
					Cart cart = c.next();
					
					if(cNext.hasNext()) {
						newCart = cNext.next();
					}
					else {
						newCart = null;
					}
					
					currentMaterialId = cart.getInventory().getMaterial().getMaterialId();
					if(i == 1) {
						oldMaterialId = currentMaterialId;
					}
					
					cellSNo = new PdfPCell();
			        cellDescription = new PdfPCell();
			        cellSize = new PdfPCell();
			        cellNos = new PdfPCell();
			        cellQty = new PdfPCell();
			        cellSqMt = new PdfPCell();
			        
			        
					cellSNo.addElement(new Phrase(""+i, supremeBlackNormal));
					table.addCell(cellSNo);
					
					cellDescription.addElement(new Phrase(""+cart.getInventory().getMaterial().getMaterialName() + "("+ cart.getInventory().getType()+")("+cart.getInventory().getThickness()+" cm)", supremeBlackNormal));
					cellDescription.addElement(new Phrase());
					cellDescription.addElement(new Phrase(cart.getInventory().getBarcodeString(), supremeBlackSmall));
					table.addCell(cellDescription);
					
					
					cellSize.addElement(new Phrase(""+(int)cart.getInventory().getActualLength()+" x "+ (int)cart.getInventory().getActualHeight(), supremeBlackNormal));
					table.addCell(cellSize);

					double sqMeterRound = 0.0;
					
					System.out.println("cart.getTileCount(): "+ cart.getTileCount());
					if(cart.getInventory().getType() != null && cart.getInventory().getType().equals("TILE")) {
						cellNos.addElement(new Phrase(""+cart.getTileCount(), supremeBlackNormal));
						slabCount += cart.getTileCount();
						double sqMt = cart.getInventory().getActualLength() * cart.getInventory().getActualHeight() * cart.getTileCount() / 10000;
						sqMeterRound += Math.round(sqMt * 100.0) / 100.0;
						totalSqMeterAddition += sqMeterRound;
						currentSqMeter += sqMeterRound;
						table.addCell(cellNos);
					}
					else {
						cellNos.addElement(new Phrase("1", supremeBlackNormal));
						table.addCell(cellNos);
						
						double sqMt = cart.getInventory().getActualLength() * cart.getInventory().getActualHeight() / 10000;
						sqMeterRound += Math.round(sqMt * 100.0) / 100.0;
						totalSqMeterAddition += sqMeterRound;
						currentSqMeter += sqMeterRound;
						slabCount++;
					}
					
					cellQty.addElement(new Phrase(""+sqMeterRound, supremeBlackNormal));
					table.addCell(cellQty);
					
					
					if(newCart != null) { 
						if(cart.getInventory().getMaterial().getMaterialId() != newCart.getInventory().getMaterial().getMaterialId()) {
							cellSqMt.addElement(new Phrase("" + currentSqMeter, supremeBlackNormal));
							table.addCell(cellSqMt);
							currentSqMeter = 0.0;
						}
						else {
							cellSqMt.addElement(new Phrase("", supremeBlackNormal));
							table.addCell(cellSqMt);
						}
					}
					else {
						cellSqMt.addElement(new Phrase("" + currentSqMeter, supremeBlackNormal));
						table.addCell(cellSqMt);
						currentSqMeter = 0.0;
					}
					
					i++;
				}
			}
			
			if(totalSqMeterAddition > 0.0){
				cellSNo = new PdfPCell();
		        cellDescription = new PdfPCell();
		        cellSize = new PdfPCell();
		        cellNos = new PdfPCell();
		        cellQty = new PdfPCell();
		        cellSqMt = new PdfPCell();
				
		        PdfPCell totalText = new PdfPCell();
				cellDescription.addElement(new Phrase("Total"));
				cellDescription.setColspan(2);
				cellDescription.setHorizontalAlignment(Align.RIGHT);
				cellSize.addElement(new Phrase());
				cellNos.addElement(new Phrase(""+slabCount));
				
				totalSqMeterAddition = Math.round(totalSqMeterAddition * 100.0) / 100.0;
				cellQty.addElement(new Phrase(""+totalSqMeterAddition));
				
				table.addCell(cellDescription);
				table.addCell(cellSize);
				table.addCell(cellNos);
				table.addCell(cellQty);
				table.addCell(cellQty);
				
				
				
				
			}
			
			document.add(table);
			
			/*addEmptyLine(emptyParagraph, 3);
			document.add(emptyParagraph);
			
			Phrase Sign = new Phrase();
	        
	        Chunk ReceiverSign = new Chunk("Receiver's Signature");
	        Chunk SMSign = new Chunk("For SUPREME MARBLES & GRANITE TRDNG.");
	        glue = new Chunk(new VerticalPositionMark());
	        Sign.add(ReceiverSign);
	        Sign.add(glue);
	        Sign.add(SMSign);
	        
	        document.add(Sign);*/
	        
	        
	        Transaction transaction = hSession.beginTransaction();
        	
        	try{
        		order.setDOPrinted(true);
        		order.setDOPrintedOn(new Date());
        		hSession.saveOrUpdate(order);
        		
        		hSession.flush();
        		
        		transaction.commit();
        	}
        	catch(Exception e){
        		e.printStackTrace();
        		/*return false;*/
        	}
	        
	        
		}
		catch(Exception e){
			e.printStackTrace();	
			throw new ValidationException(e);	
		}
		finally {
			hSession.close();
		}
	}
	private static void addEmptyLine(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	        paragraph.add(new Paragraph(" "));
	    }
	}


}