package com.supreme.util;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.supreme.Config;

import javax.imageio.ImageIO;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class QRCodeReader {
	
	private static String QR_CODE_IMAGE_PATH = "";

    private static String decodeQRCode(File qrCodeimage) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            System.out.println("There is no QR code in the image");
            return null;
        }
    }

    public static void main(String[] args) {
        try {
        	
        	try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				QR_CODE_IMAGE_PATH = config.get(Config.QR_DEV_PATH) + File.separator + "MyQRCode.png";
    				System.out.println("Dev path: "+QR_CODE_IMAGE_PATH);
    			}else{
    				QR_CODE_IMAGE_PATH = config.get(Config.QR_PROD_PATH) + File.separator + "MyQRCode.png";
    				System.out.println("Prod path: "+QR_CODE_IMAGE_PATH);
    			}
    		}catch(IOException e){
    			QR_CODE_IMAGE_PATH = "/opt/supremeinventory/qr/";
    		}
        	
            File file = new File(QR_CODE_IMAGE_PATH);
            System.out.println(QR_CODE_IMAGE_PATH);
            boolean exists = file.exists();
            System.out.println("exists: "+ exists);
            String decodedText = decodeQRCode(file);
            if(decodedText == null) {
                System.out.println("No QR Code found in the image");
            } else {
                System.out.println("Decoded text = " + decodedText);
            }
        } catch (IOException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage());
        }
    }
}