package com.supreme.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.pdfbox.multipdf.Overlay;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class PDFWrite {
	PDDocument doc=null, newdoc=null;
	PDPage page=null, newpage=null;
	PDPageContentStream content=null;
	int fontSize=12;
	public void open(File file) throws IOException{
		newdoc=new PDDocument();		
		doc=PDDocument.load(file);
/*		for(int i=0;i<doc.getNumberOfPages();i++){
			newpage=new PDPage();
			newdoc.addPage(newpage);
		}
*/		
		newpage=new PDPage();
		newdoc.addPage(newpage);

		System.out.println("Count : "+newdoc.getNumberOfPages());
		System.out.println("Count 1 : "+doc.getNumberOfPages());
		newpage=newdoc.getPage(0);
		page=doc.getPage(0);
		content=new PDPageContentStream(newdoc, newpage);
	}
	
	public void setFontSize(int fontSize){
		this.fontSize=fontSize;
	}
	
	public void selectPage(int pageIndex) throws IOException{
		if(doc==null){
			throw new IOException("File not opened yet.");
		}
		newpage=newdoc.getPage(pageIndex);
		content.close();
		content=new PDPageContentStream(newdoc, newpage);
	}
	
	@SuppressWarnings("deprecation")
	public void addString(String text, float x, float y) throws IOException{
		if(doc==null){
			throw new IOException("File not opened yet.");
		}	
		try{
			PDFont font=PDType1Font.HELVETICA;			
			content.beginText();
			content.setFont(font, fontSize);
			content.moveTextPositionByAmount(x, y);
			content.drawString(text.replaceAll("\r\n", " ").replaceAll("\r", " ").replaceAll("\n", " "));
			content.endText();
		}catch(Exception e){
			System.out.println("Error at text : "+text);
			e.printStackTrace();
		}
	}
	
	public void save(String filename) throws IOException{
		if(doc==null){
			throw new IOException("File not opened yet.");
		}
		content.close();
		Overlay overlay = new Overlay();
		overlay.setFirstPageOverlayPDF(newdoc);
		overlay.setOverlayPosition(Overlay.Position.FOREGROUND);
		overlay.setInputPDF(doc);
		Map<Integer, String> ovmap = new HashMap<Integer, String>();
		doc=overlay.overlay(ovmap);		
		doc.save(filename);
		doc.close();
	}
	
	public void save(File file) throws IOException{
		if(doc==null){
			throw new IOException("File not opened yet.");
		}
		content.close();
		Overlay overlay = new Overlay();
		overlay.setFirstPageOverlayPDF(newdoc);
		overlay.setOverlayPosition(Overlay.Position.FOREGROUND);
		overlay.setInputPDF(doc);
		Map<Integer, String> ovmap = new HashMap<Integer, String>();
		doc=overlay.overlay(ovmap);		
		doc.save(file);
		// doc.close();
	}
	
	public ByteArrayOutputStream getByteArray() throws IOException{
		if(doc==null){
			throw new IOException("File not opened yet.");
		}
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();		
		content.close();
		Overlay overlay = new Overlay();
		overlay.setFirstPageOverlayPDF(newdoc);
		overlay.setOverlayPosition(Overlay.Position.FOREGROUND);
		overlay.setInputPDF(doc);
		Map<Integer, String> ovmap = new HashMap<Integer, String>();
		
		doc=overlay.overlay(ovmap);		
		doc.save(output);
		doc.close();
		return output;
	}	
}
