package com.supreme.util;

import java.util.Date;

public class Verify
{

	// private static final String EMAIL_REGEX="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-.]+(\\.)(?:[A-Za-z]{2,}|com|org)*$";

	private static final String EMAIL_REGEX = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String MOBILE_REGEX = "^([0-9]){10}$";
	private static final String PAN_REGEX = "^([A-Za-z]){5}([0-9]){4}([a-zA-Z]){1}?$";
	private static final String AADHAAR_REGEX = "^([0-9]){12}$";
	private static final String DATE_REGEX = "^([0-9]){4}-([0-9]){2}-([0-9]){2}$";
	private static final String LONG_REGEX = "^([0-9])+$";
	private static final String DOUBLE_REGEX = "^([0-9.])+$";
	private static final String IFSC_REGEX = "^[A-Za-z]{4}(0)[A-Z0-9]{6}$";
	private static final String FOLIO_NUMBER_REGEX = "^(([0-9]+(/)([0-9]{1,2}))|([0-9]+))$";
	private static final String PIN_REGEX = "^([0-9]){6}$";

	public static String getMobileVerificationCode()
	{
		int min = 1000, max = 9999;
		int rand = min + (int) (java.lang.Math.random() * (max - min));
		return "" + rand;
	}

	public static String getPasswordGenerated()
	{
		int min = 10000000, max = 999999999;
		int rand = min + (int) (java.lang.Math.random() * (max - min));
		return "" + rand;
	}
	
	public static String getEmailVerificationCode()
	{
		return (new Date()).hashCode() + "";
	}

	public static boolean folioNumberValidate(String folioNumber)
	{
		return folioNumber.matches(Verify.FOLIO_NUMBER_REGEX);
	}

	public static boolean emailValidate(String email)
	{
		return email.matches(Verify.EMAIL_REGEX);
	}

	public static boolean mobileValidate(String mobile)
	{
		return mobile.matches(Verify.MOBILE_REGEX);
	}

	public static boolean panValidate(String pan)
	{
		return pan.matches(Verify.PAN_REGEX);
	}

	public static boolean aadhaarValidate(String aadhaar)
	{
		return aadhaar.matches(Verify.AADHAAR_REGEX);
	}

	public static boolean passwordValidate(String password)
	{
		return password.length() > 7;
	}

	public static boolean dateValidate(String date)
	{
		return date.matches(Verify.DATE_REGEX);
	}

	public static boolean longValidate(String l)
	{
		return l.matches(Verify.LONG_REGEX);
	}

	public static boolean doubleValidate(String l)
	{
		return l.matches(Verify.DOUBLE_REGEX);
	}

	public static boolean ifscValidate(String l)
	{
		return l.matches(Verify.IFSC_REGEX);
	}

	public static boolean addressValidate(String l)
	{
		return l.length() < 40;
	}

	public static boolean pinValidate(String pin)
	{
		return pin.matches(Verify.PIN_REGEX);
	}

	public static int PANVerify(String name, String panname)
	{
		/*
		 * Return values :
		 * 0 - Almost same, except words jumbled
		 * 1 - Exactly same
		 * -1 - Not matching
		 */
		name = name.replaceAll(" +", " ").trim().toLowerCase();
		panname = panname.replaceAll(" +", " ").trim().toLowerCase();
		if(name.equals(panname))
		{
			return 1;
		}

		String[] arr1 = name.trim().split(" ");
		String[] arr2 = panname.trim().split(" ");
		if(arr1.length != arr2.length)
		{
			return -1;
		}

		for (int i = 0; i < arr1.length; i++)
		{
			boolean found = false;
			for (int j = 0; j < arr2.length; j++)
			{
				if(arr2[j] != null && arr1[i].equals(arr2[j]))
				{
					found = true;
					arr2[j] = null;
				}
			}
			if(!found)
			{
				return -1;
			}
		}
		return 0;
	}

	public static String getStateName(String stateCode) {
		// TODO Auto-generated method stub
		if(stateCode.equalsIgnoreCase("AN"))
			return "Andaman & Nicobar";
		else if(stateCode.equalsIgnoreCase("AR"))
			return "Arunachal Pradesh";
		else if(stateCode.equalsIgnoreCase("AP"))
			return "Andhra Pradesh";
		else if(stateCode.equalsIgnoreCase("AS"))
			return "Assam";
		else if(stateCode.equalsIgnoreCase("BH"))
			return "Bihar";
		else if(stateCode.equalsIgnoreCase("CH"))
			return "Chandigarh";
		else if(stateCode.equalsIgnoreCase("CG"))
			return "Chhattisgarh";
		else if(stateCode.equalsIgnoreCase("DL"))
			return "Delhi";
		else if(stateCode.equalsIgnoreCase("GO"))
			return "GOA";
		else if(stateCode.equalsIgnoreCase("GU"))
			return "Gujarat";
		else if(stateCode.equalsIgnoreCase("HA"))
			return "Haryana";
		else if(stateCode.equalsIgnoreCase("HP"))
			return "Himachal Pradesh";
		else if(stateCode.equalsIgnoreCase("JM"))
			return "Jammu & Kashmir";
		else if(stateCode.equalsIgnoreCase("JK") || stateCode.equalsIgnoreCase("JH"))
			return "Jharkhand";
		else if(stateCode.equalsIgnoreCase("KA"))
			return "Karnataka";
		else if(stateCode.equalsIgnoreCase("KE"))
			return "Kerala";
		else if(stateCode.equalsIgnoreCase("MP"))
			return "Madhya Pradesh";
		else if(stateCode.equalsIgnoreCase("MA"))
			return "Maharashtra";
		else if(stateCode.equalsIgnoreCase("MN"))
			return "Manipur";
		else if(stateCode.equalsIgnoreCase("ME"))
			return "Meghalaya";
		else if(stateCode.equalsIgnoreCase("MI"))
			return "Mizoram";
		else if(stateCode.equalsIgnoreCase("NA"))
			return "Nagaland";
		else if(stateCode.equalsIgnoreCase("ND"))
			return "New Delhi";
		else if(stateCode.equalsIgnoreCase("OR"))
			return "Orissa";
		else if(stateCode.equalsIgnoreCase("PO"))
			return "Pondicherry";
		else if(stateCode.equalsIgnoreCase("PU"))
			return "Punjab";
		else if(stateCode.equalsIgnoreCase("RA"))
			return "Rajasthan";
		else if(stateCode.equalsIgnoreCase("SI"))
			return "Sikkim";
		else if(stateCode.equalsIgnoreCase("TG"))
			return "Telengana";
		else if(stateCode.equalsIgnoreCase("TN"))
			return "Tamil Nadu";
		else if(stateCode.equalsIgnoreCase("TR"))
			return "Tripura";
		else if(stateCode.equalsIgnoreCase("UP"))
			return "Uttar Pradesh";
		else if(stateCode.equalsIgnoreCase("UC"))
			return "Uttaranchal";
		else if(stateCode.equalsIgnoreCase("WB"))
			return "West Bengal";
		else if(stateCode.equalsIgnoreCase("DN"))
			return "Dadra and Nagar Haveli";
		else if(stateCode.equalsIgnoreCase("DD"))
			return "Daman and Diu";
		else if(stateCode.equalsIgnoreCase("LA"))
			return "Lakshadweep";
		else if(stateCode.equalsIgnoreCase("OH"))
			return "Others";
		else
			return "Maharashtra";
	}

/*
 * <select name="occupation" id="occupation-input" class="form-control ng-pristine ng-valid ng-touched" ng-model="local.occupation">
	    					<option value="">--OCCUPATION--</option>
							<option value="01">BUSINESS</option>
							<option value="02">SERVICE</option>
							<option value="03">PROFESSIONAL</option>
							<option value="04">AGRICULTURIST</option>
							<option value="05">RETIRED</option>
							<option value="06">HOUSEWIFE</option>
							<option value="07">STUDENT</option>
							<option value="08">OTHERS</option>
							<option value="09">NOT SPECIFIED</option>
							<option value="41">PRIVATE SECTOR SERVICE</option>
							<option value="42">PUBLIC SECTOR / GOVERNMENT SERVICE</option>
							<option value="43">FOREX DEALER</option>
	    				</select>
 */
	public static String getOccupationName(String occupationCode) {
		if(occupationCode.equalsIgnoreCase("01"))
			return "Business";
		else if(occupationCode.equalsIgnoreCase("02"))
			return "Services";
		else if(occupationCode.equalsIgnoreCase("03"))
			return "Professional";
		else if(occupationCode.equalsIgnoreCase("04"))
			return "Agriculture";
		else if(occupationCode.equalsIgnoreCase("05"))
			return "Retired";
		else if(occupationCode.equalsIgnoreCase("06"))
			return "Housewife";
		else if(occupationCode.equalsIgnoreCase("07"))
			return "Student";
		else if(occupationCode.equalsIgnoreCase("08"))
			return "Others";
		else if(occupationCode.equalsIgnoreCase("09"))
			return "Not Specified";
		else if(occupationCode.equalsIgnoreCase("41"))
			return "Private Sector Service";
		else if(occupationCode.equalsIgnoreCase("42"))
			return "Public Sector / Government Service";
		else if(occupationCode.equalsIgnoreCase("43"))
			return "Forex Dealer";
		else
			return "";
	}	
	
	public static String getForeignCountryName(String countryCode) {
		if(countryCode.equalsIgnoreCase("001")){
			return "Afghanistan";
		}
		else if(countryCode.equalsIgnoreCase("002")){
			return "Aland Islands";
		}
		else if(countryCode.equalsIgnoreCase("003")){
			return "Albania";
		}
		else if(countryCode.equalsIgnoreCase("004")){
			return "Algeria";
		}
		else if(countryCode.equalsIgnoreCase("005")){
			return "American Samoa";
		}
		else if(countryCode.equalsIgnoreCase("006")){
			return "Andorra";
		}
		else if(countryCode.equalsIgnoreCase("007")){
			return "Angola";
		}
		else if(countryCode.equalsIgnoreCase("008")){
			return "Anguilla";
		}
		else if(countryCode.equalsIgnoreCase("009")){
			return "Antarctica";
		}
		else if(countryCode.equalsIgnoreCase("010")){
			return "Antigua And Barbuda";
		}
		else if(countryCode.equalsIgnoreCase("011")){
			return "Argentina";
		}
		else if(countryCode.equalsIgnoreCase("012")){
			return "Armenia";
		}
		else if(countryCode.equalsIgnoreCase("013")){
			return "Aruba";
		}
		else if(countryCode.equalsIgnoreCase("014")){
			return "Australia";
		}
		else if(countryCode.equalsIgnoreCase("015")){
			return "Austria";
		}
		else if(countryCode.equalsIgnoreCase("016")){
			return "Azerbaijan";
		}
		else if(countryCode.equalsIgnoreCase("017")){
			return "Bahamas";
		}
		else if(countryCode.equalsIgnoreCase("018")){
			return "Bahrain";
		}
		else if(countryCode.equalsIgnoreCase("019")){
			return "Bangladesh";
		}
		else if(countryCode.equalsIgnoreCase("020")){
			return "Barbados";
		}
		else if(countryCode.equalsIgnoreCase("021")){
			return "Belarus";
		}
		else if(countryCode.equalsIgnoreCase("022")){
			return "Belgium";
		}
		else if(countryCode.equalsIgnoreCase("023")){
			return "Belize";
		}
		else if(countryCode.equalsIgnoreCase("024")){
			return "Benin";
		}
		else if(countryCode.equalsIgnoreCase("025")){
			return "Bermuda";
		}
		else if(countryCode.equalsIgnoreCase("026")){
			return "Bhutan";
		}
		else if(countryCode.equalsIgnoreCase("027")){
			return "Bolivia";
		}
		else if(countryCode.equalsIgnoreCase("028")){
			return "Bosnia And Herzegovina";
		}
		else if(countryCode.equalsIgnoreCase("029")){
			return "Botswana";
		}
		else if(countryCode.equalsIgnoreCase("030")){
			return "Bouvet Island";
		}
		else if(countryCode.equalsIgnoreCase("031")){
			return "Brazil";
		}
		else if(countryCode.equalsIgnoreCase("032")){
			return "British Indian Ocean Territory";
		}
		else if(countryCode.equalsIgnoreCase("033")){
			return "Brunei Darussalam";
		}
		else if(countryCode.equalsIgnoreCase("034")){
			return "Bulgaria";
		}
		else if(countryCode.equalsIgnoreCase("035")){
			return "Burkina Faso";
		}
		else if(countryCode.equalsIgnoreCase("036")){
			return "Burundi";
		}
		else if(countryCode.equalsIgnoreCase("037")){
			return "Cambodia";
		}
		else if(countryCode.equalsIgnoreCase("038")){
			return "Cameroon";
		}
		else if(countryCode.equalsIgnoreCase("039")){
			return "Canada";
		}
		else if(countryCode.equalsIgnoreCase("040")){
			return "Cape Verde";
		}
		else if(countryCode.equalsIgnoreCase("041")){
			return "Cayman Islands";
		}
		else if(countryCode.equalsIgnoreCase("042")){
			return "Central African Republic";
		}
		else if(countryCode.equalsIgnoreCase("043")){
			return "Chad";
		}
		else if(countryCode.equalsIgnoreCase("044")){
			return "Chile";
		}
		else if(countryCode.equalsIgnoreCase("045")){
			return "China";
		}
		else if(countryCode.equalsIgnoreCase("046")){
			return "Christmas Island";
		}
		else if(countryCode.equalsIgnoreCase("047")){
			return "Cocos (Keeling) Islands";
		}
		else if(countryCode.equalsIgnoreCase("048")){
			return "Colombia";
		}
		else if(countryCode.equalsIgnoreCase("049")){
			return "Comoros";
		}
		else if(countryCode.equalsIgnoreCase("050")){
			return "Congo";
		}
		else if(countryCode.equalsIgnoreCase("051")){
			return "Congo";
		}
		else if(countryCode.equalsIgnoreCase("052")){
			return "Cook Islands";
		}
		else if(countryCode.equalsIgnoreCase("053")){
			return "Costa Rica";
		}
		else if(countryCode.equalsIgnoreCase("054")){
			return "Cote DIvoire";
		}
		else if(countryCode.equalsIgnoreCase("055")){
			return "Croatia";
		}
		else if(countryCode.equalsIgnoreCase("056")){
			return "Cuba";
		}
		else if(countryCode.equalsIgnoreCase("057")){
			return "Cyprus";
		}
		else if(countryCode.equalsIgnoreCase("058")){
			return "Czech Republic";
		}
		else if(countryCode.equalsIgnoreCase("059")){
			return "Denmark";
		}
		else if(countryCode.equalsIgnoreCase("060")){
			return "Djibouti";
		}
		else if(countryCode.equalsIgnoreCase("061")){
			return "Dominica";
		}
		else if(countryCode.equalsIgnoreCase("062")){
			return "Dominican Republic";
		}
		else if(countryCode.equalsIgnoreCase("063")){
			return "Dominican Republic";
		}
		else if(countryCode.equalsIgnoreCase("064")){
			return "Egypt";
		}
		else if(countryCode.equalsIgnoreCase("065")){
			return "El Salvador";
		}
		else if(countryCode.equalsIgnoreCase("066")){
			return "Equatorial Guinea";
		}
		else if(countryCode.equalsIgnoreCase("067")){
			return "Eritrea";
		}
		else if(countryCode.equalsIgnoreCase("068")){
			return "Estonia";
		}
		else if(countryCode.equalsIgnoreCase("069")){
			return "Ethiopia";
		}
		else if(countryCode.equalsIgnoreCase("070")){
			return "Falkland Islands (Malvinas)";
		}
		else if(countryCode.equalsIgnoreCase("071")){
			return "Faroe Islands";
		}
		else if(countryCode.equalsIgnoreCase("072")){
			return "Fiji";
		}
		else if(countryCode.equalsIgnoreCase("073")){
			return "Finland";
		}
		else if(countryCode.equalsIgnoreCase("074")){
			return "France";
		}
		else if(countryCode.equalsIgnoreCase("075")){
			return "French Guiana";
		}
		else if(countryCode.equalsIgnoreCase("076")){
			return "French Polynesia";
		}
		else if(countryCode.equalsIgnoreCase("077")){
			return "French Southern Territories";
		}
		else if(countryCode.equalsIgnoreCase("078")){
			return "Gabon";
		}
		else if(countryCode.equalsIgnoreCase("079")){
			return "Gambia";
		}
		else if(countryCode.equalsIgnoreCase("080")){
			return "Georgia";
		}
		else if(countryCode.equalsIgnoreCase("081")){
			return "Germany";
		}
		else if(countryCode.equalsIgnoreCase("082")){
			return "Ghana";
		}
		else if(countryCode.equalsIgnoreCase("083")){
			return "Gibraltar";
		}
		else if(countryCode.equalsIgnoreCase("084")){
			return "Greece";
		}
		else if(countryCode.equalsIgnoreCase("085")){
			return "Greenland";
		}
		else if(countryCode.equalsIgnoreCase("086")){
			return "Grenada";
		}
		else if(countryCode.equalsIgnoreCase("087")){
			return "Guadeloupe";
		}
		else if(countryCode.equalsIgnoreCase("088")){
			return "Guam";
		}
		else if(countryCode.equalsIgnoreCase("089")){
			return "Guatemala";
		}
		else if(countryCode.equalsIgnoreCase("090")){
			return "Guernsey";
		}
		else if(countryCode.equalsIgnoreCase("091")){
			return "Guinea";
		}
		else if(countryCode.equalsIgnoreCase("092")){
			return "Guinea-Bissau";
		}
		else if(countryCode.equalsIgnoreCase("093")){
			return "Guyana";
		}
		else if(countryCode.equalsIgnoreCase("094")){
			return "Haiti";
		}
		else if(countryCode.equalsIgnoreCase("095")){
			return "Heard Island And Mcdonald Islands";
		}
		else if(countryCode.equalsIgnoreCase("096")){
			return "Holy See (Vatican City State)";
		}
		else if(countryCode.equalsIgnoreCase("097")){
			return "Honduras";
		}
		else if(countryCode.equalsIgnoreCase("098")){
			return "Hong Kong";
		}
		else if(countryCode.equalsIgnoreCase("099")){
			return "Hungary";
		}
		else if(countryCode.equalsIgnoreCase("100")){
			return "Iceland";
		}
		else if(countryCode.equalsIgnoreCase("101")){
			return "India";
		}
		else if(countryCode.equalsIgnoreCase("102")){
			return "Indonesia";
		}
		else if(countryCode.equalsIgnoreCase("103")){
			return "Iran";
		}
		else if(countryCode.equalsIgnoreCase("104")){
			return "Iraq";
		}
		else if(countryCode.equalsIgnoreCase("105")){
			return "Ireland";
		}
		else if(countryCode.equalsIgnoreCase("106")){
			return "Isle Of Man";
		}
		else if(countryCode.equalsIgnoreCase("107")){
			return "Israel";
		}
		else if(countryCode.equalsIgnoreCase("108")){
			return "Italy";
		}
		else if(countryCode.equalsIgnoreCase("109")){
			return "Jamaica";
		}
		else if(countryCode.equalsIgnoreCase("110")){
			return "Japan";
		}
		else if(countryCode.equalsIgnoreCase("111")){
			return "Jersey";
		}
		else if(countryCode.equalsIgnoreCase("112")){
			return "Jordan";
		}
		else if(countryCode.equalsIgnoreCase("113")){
			return "Kazakhstan";
		}
		else if(countryCode.equalsIgnoreCase("114")){
			return "Kenya";
		}
		else if(countryCode.equalsIgnoreCase("115")){
			return "Kiribati";
		}
		else if(countryCode.equalsIgnoreCase("116")){
			return "Korea, Democratic Peoples Republic";
		}
		else if(countryCode.equalsIgnoreCase("117")){
			return "Korea";
		}
		else if(countryCode.equalsIgnoreCase("118")){
			return "Kuwait";
		}
		else if(countryCode.equalsIgnoreCase("119")){
			return "Kyrgyzstan";
		}
		else if(countryCode.equalsIgnoreCase("120")){
			return "Lao Peoples Democratic Republic";
		}
		else if(countryCode.equalsIgnoreCase("121")){
			return "Latvia";
		}
		else if(countryCode.equalsIgnoreCase("122")){
			return "Lebanon";
		}
		else if(countryCode.equalsIgnoreCase("123")){
			return "Lesotho";
		}
		else if(countryCode.equalsIgnoreCase("124")){
			return "Liberia";
		}
		else if(countryCode.equalsIgnoreCase("125")){
			return "Libyan Arab Jamahiriya";
		}
		else if(countryCode.equalsIgnoreCase("126")){
			return "Liechtenstein";
		}
		else if(countryCode.equalsIgnoreCase("127")){
			return "Lithuania";
		}
		else if(countryCode.equalsIgnoreCase("128")){
			return "Luxembourg";
		}
		else if(countryCode.equalsIgnoreCase("129")){
			return "Macao";
		}
		else if(countryCode.equalsIgnoreCase("130")){
			return "Macedonia";
		}
		else if(countryCode.equalsIgnoreCase("131")){
			return "Madagascar";
		}
		else if(countryCode.equalsIgnoreCase("132")){
			return "Malawi";
		}
		else if(countryCode.equalsIgnoreCase("133")){
			return "Malaysia";
		}
		else if(countryCode.equalsIgnoreCase("134")){
			return "Maldives";
		}
		else if(countryCode.equalsIgnoreCase("135")){
			return "Mali";
		}
		else if(countryCode.equalsIgnoreCase("136")){
			return "Malta";
		}
		else if(countryCode.equalsIgnoreCase("137")){
			return "Marshall Islands";
		}
		else if(countryCode.equalsIgnoreCase("138")){
			return "Martinique";
		}
		else if(countryCode.equalsIgnoreCase("139")){
			return "Mauritania";
		}
		else if(countryCode.equalsIgnoreCase("140")){
			return "Mauritius";
		}
		else if(countryCode.equalsIgnoreCase("141")){
			return "Mayotte";
		}
		else if(countryCode.equalsIgnoreCase("142")){
			return "Mexico";
		}
		else if(countryCode.equalsIgnoreCase("143")){
			return "Micronesia";
		}
		else if(countryCode.equalsIgnoreCase("144")){
			return "Moldova";
		}
		else if(countryCode.equalsIgnoreCase("145")){
			return "Monaco";
		}
		else if(countryCode.equalsIgnoreCase("146")){
			return "Mongolia";
		}
		else if(countryCode.equalsIgnoreCase("147")){
			return "Montserrat";
		}
		else if(countryCode.equalsIgnoreCase("148")){
			return "Morocco";
		}
		else if(countryCode.equalsIgnoreCase("149")){
			return "Mozambique";
		}
		else if(countryCode.equalsIgnoreCase("150")){
			return "Myanmar";
		}
		else if(countryCode.equalsIgnoreCase("151")){
			return "Namibia";
		}
		else if(countryCode.equalsIgnoreCase("152")){
			return "Nauru";
		}
		else if(countryCode.equalsIgnoreCase("153")){
			return "Nepal";
		}
		else if(countryCode.equalsIgnoreCase("154")){
			return "Netherlands";
		}
		else if(countryCode.equalsIgnoreCase("155")){
			return "Netherlands Antilles";
		}
		else if(countryCode.equalsIgnoreCase("156")){
			return "New Caledonia";
		}
		else if(countryCode.equalsIgnoreCase("157")){
			return "New Zealand";
		}
		else if(countryCode.equalsIgnoreCase("158")){
			return "Nicaragua";
		}
		else if(countryCode.equalsIgnoreCase("159")){
			return "Niger";
		}
		else if(countryCode.equalsIgnoreCase("160")){
			return "Nigeria";
		}
		else if(countryCode.equalsIgnoreCase("161")){
			return "Niue";
		}
		else if(countryCode.equalsIgnoreCase("162")){
			return "Norfolk Island";
		}
		else if(countryCode.equalsIgnoreCase("163")){
			return "Northern Mariana Islands";
		}
		else if(countryCode.equalsIgnoreCase("164")){
			return "Norway";
		}
		else if(countryCode.equalsIgnoreCase("165")){
			return "Oman";
		}
		else if(countryCode.equalsIgnoreCase("166")){
			return "Pakistan";
		}
		else if(countryCode.equalsIgnoreCase("167")){
			return "Palau";
		}
		else if(countryCode.equalsIgnoreCase("168")){
			return "Palestinian Territory, Occupied";
		}
		else if(countryCode.equalsIgnoreCase("169")){
			return "Panama";
		}
		else if(countryCode.equalsIgnoreCase("170")){
			return "Papua New Guinea";
		}
		else if(countryCode.equalsIgnoreCase("171")){
			return "Paraguay";
		}
		else if(countryCode.equalsIgnoreCase("172")){
			return "Peru";
		}
		else if(countryCode.equalsIgnoreCase("173")){
			return "Philippines";
		}
		else if(countryCode.equalsIgnoreCase("174")){
			return "Pitcairn";
		}
		else if(countryCode.equalsIgnoreCase("175")){
			return "Poland";
		}
		else if(countryCode.equalsIgnoreCase("176")){
			return "Portugal";
		}
		else if(countryCode.equalsIgnoreCase("177")){
			return "Puerto Rico";
		}
		else if(countryCode.equalsIgnoreCase("178")){
			return "Qatar";
		}
		else if(countryCode.equalsIgnoreCase("179")){
			return "Reunion";
		}
		else if(countryCode.equalsIgnoreCase("180")){
			return "Romania";
		}
		else if(countryCode.equalsIgnoreCase("181")){
			return "Russian Federation";
		}
		else if(countryCode.equalsIgnoreCase("182")){
			return "Rwanda";
		}
		else if(countryCode.equalsIgnoreCase("183")){
			return "Saint Helena";
		}
		else if(countryCode.equalsIgnoreCase("184")){
			return "Saint Kitts And Nevis";
		}
		else if(countryCode.equalsIgnoreCase("185")){
			return "Saint Lucia";
		}
		else if(countryCode.equalsIgnoreCase("186")){
			return "Saint Pierre And Miquelon";
		}
		else if(countryCode.equalsIgnoreCase("187")){
			return "Saint Vincent And The Grenadines";
		}
		else if(countryCode.equalsIgnoreCase("188")){
			return "Samoa";
		}
		else if(countryCode.equalsIgnoreCase("189")){
			return "San Marino";
		}
		else if(countryCode.equalsIgnoreCase("190")){
			return "Sao Tome And Principe";
		}
		else if(countryCode.equalsIgnoreCase("191")){
			return "Saudi Arabia";
		}
		else if(countryCode.equalsIgnoreCase("192")){
			return "Senegal";
		}
		else if(countryCode.equalsIgnoreCase("193")){
			return "Serbia And Montenegro";
		}
		else if(countryCode.equalsIgnoreCase("194")){
			return "Seychelles";
		}
		else if(countryCode.equalsIgnoreCase("195")){
			return "Sierra Leone";
		}
		else if(countryCode.equalsIgnoreCase("196")){
			return "Singapore";
		}
		else if(countryCode.equalsIgnoreCase("197")){
			return "Slovakia";
		}
		else if(countryCode.equalsIgnoreCase("198")){
			return "Slovenia";
		}
		else if(countryCode.equalsIgnoreCase("199")){
			return "Solomon Islands";
		}
		else if(countryCode.equalsIgnoreCase("200")){
			return "Somalia";
		}
		else if(countryCode.equalsIgnoreCase("201")){
			return "South Africa";
		}
		else if(countryCode.equalsIgnoreCase("202")){
			return "South Georgia And The South Sandwich Islands";
		}
		else if(countryCode.equalsIgnoreCase("203")){
			return "Spain";
		}
		else if(countryCode.equalsIgnoreCase("204")){
			return "Sri Lanka";
		}
		else if(countryCode.equalsIgnoreCase("205")){
			return "Sudan";
		}
		else if(countryCode.equalsIgnoreCase("206")){
			return "Suriname";
		}
		else if(countryCode.equalsIgnoreCase("207")){
			return "Svalbard And Jan Mayen";
		}
		else if(countryCode.equalsIgnoreCase("208")){
			return "Swaziland";
		}
		else if(countryCode.equalsIgnoreCase("209")){
			return "Sweden";
		}
		else if(countryCode.equalsIgnoreCase("210")){
			return "Switzerland";
		}
		else if(countryCode.equalsIgnoreCase("211")){
			return "Syrian Arab Republic";
		}
		else if(countryCode.equalsIgnoreCase("212")){
			return "Taiwan, Province Of China";
		}
		else if(countryCode.equalsIgnoreCase("213")){
			return "Tajikistan";
		}
		else if(countryCode.equalsIgnoreCase("214")){
			return "Tanzania,";
		}
		else if(countryCode.equalsIgnoreCase("215")){
			return "Thailand";
		}
		else if(countryCode.equalsIgnoreCase("216")){
			return "Timor-Leste";
		}
		else if(countryCode.equalsIgnoreCase("217")){
			return "Togo";
		}
		else if(countryCode.equalsIgnoreCase("218")){
			return "Tokelau";
		}
		else if(countryCode.equalsIgnoreCase("219")){
			return "Tonga";
		}
		else if(countryCode.equalsIgnoreCase("220")){
			return "Trinidad And Tobago";
		}
		else if(countryCode.equalsIgnoreCase("221")){
			return "Tunisia";
		}
		else if(countryCode.equalsIgnoreCase("222")){
			return "Turkey";
		}
		else if(countryCode.equalsIgnoreCase("223")){
			return "Turkmenistan";
		}
		else if(countryCode.equalsIgnoreCase("224")){
			return "Turks And Caicos Islands";
		}
		else if(countryCode.equalsIgnoreCase("225")){
			return "Tuvalu";
		}
		else if(countryCode.equalsIgnoreCase("226")){
			return "Uganda";
		}
		else if(countryCode.equalsIgnoreCase("227")){
			return "Ukraine";
		}
		else if(countryCode.equalsIgnoreCase("228")){
			return "United Arab Emirates";
		}
		else if(countryCode.equalsIgnoreCase("229")){
			return "United Kingdom";
		}
		else if(countryCode.equalsIgnoreCase("230")){
			return "United States of America";
		}
		else if(countryCode.equalsIgnoreCase("231")){
			return "United States Minor Outlying Islands";
		}
		else if(countryCode.equalsIgnoreCase("232")){
			return "Uruguay";
		}
		else if(countryCode.equalsIgnoreCase("233")){
			return "Uzbekistan";
		}
		else if(countryCode.equalsIgnoreCase("234")){
			return "Vanuatu";
		}
		else if(countryCode.equalsIgnoreCase("235")){
			return "Venezuela";
		}
		else if(countryCode.equalsIgnoreCase("236")){
			return "Viet Nam";
		}
		else if(countryCode.equalsIgnoreCase("237")){
			return "Virgin Islands, British";
		}
		else if(countryCode.equalsIgnoreCase("238")){
			return "Virgin Islands, U.S.";
		}
		else if(countryCode.equalsIgnoreCase("239")){
			return "Wallis And Futuna";
		}
		else if(countryCode.equalsIgnoreCase("240")){
			return "Western Sahara";
		}
		else if(countryCode.equalsIgnoreCase("241")){
			return "Yemen";
		}
		else if(countryCode.equalsIgnoreCase("242")){
			return "Zambia";
		}
		else if(countryCode.equalsIgnoreCase("243")){
			return "Zimbabwe";
		}
		else
			return null;
	}
	
	public static String getEmailVerificationOTP()
	{
		int min = 1000, max = 9999;
		int rand = min + (int) (java.lang.Math.random() * (max - min));
		return "" + rand;
	}
}