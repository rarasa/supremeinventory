package com.supreme.util;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.supreme.dao.Container;
import com.supreme.dao.Inventory;
import com.supreme.util.HibernateBridge;

public class SetLocationId {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("Connection Opened");
			
			Query query = hSession.createQuery("select count(*) from Container");
			Long count = (Long)query.uniqueResult();
			
			char oldYard = 'M', newYard = 'W';
			
			int i = 1;
			try {
				while(i <= count) {
					
					System.out.println("i: "+ i);
					
					Container container = (Container) hSession.createQuery("from Container where containerId=?").setLong(0, i).setMaxResults(1).uniqueResult();
					Inventory in = (Inventory)hSession.createQuery("from Inventory where containerId=?").setLong(0, i).setMaxResults(1).uniqueResult();
					
					if(in != null) {
						System.out.println("InventoryId: "+ in.getId());
						if(in.getBarcodeString() != null 
								&& !in.getBarcodeString().equals("") 
								&& in.getBarcodeString().charAt(0) == oldYard) {
							container.setLocationId(2L);
						}
						else {
							container.setLocationId(1L);
						}
						
						System.out.println("in.getSupplierId(): "+in.getSupplierId());
						
						container.setSupplierId(in.getSupplierId());
						container.setSupplier(in.getSupplier());
						
						hSession.save(container);
						Transaction t = hSession.beginTransaction();
						t.commit();
					}
					else {
						System.out.println("IN is null");
					}
					i++;
				}
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

}
