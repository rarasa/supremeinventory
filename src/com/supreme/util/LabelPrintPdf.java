package com.supreme.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.bind.ValidationException;

import org.apache.commons.io.FileUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.supreme.Config;
import com.supreme.dao.Cart;
import com.supreme.dao.Container;
import com.supreme.dao.Inventory;
import com.supreme.dao.Orders;
import com.supreme.test.MyQR;
import com.supreme.test.PDFTest;

public class LabelPrintPdf extends PdfPageEventHelper
{
	
	public static void main(String args[]) throws Exception {  
		PdfReader pdfReader=null;
		try
		{
			Document document = new Document();
			
			LabelPrintPdf accountStatementPdf=new LabelPrintPdf();
			accountStatementPdf.createPdf(16L, document);  //1720
			  
			document.close();
			  
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(pdfReader!=null)
			pdfReader.close();
		}
		  
		System.out.println("PDF Created");    
	} 
	
	
	public void createPdf(long containerId, Document document) throws ValidationException
	{
		
		String path="";
		
		try{
			Config config=new Config();
			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
				path=config.get(Config.DATA_MATRIX_DEV_PATH);
				System.out.println("Dev path: "+path);
			}else{
				path=config.get(Config.DATA_MATRIX_PROD_PATH);
				System.out.println("Prod path: "+path);
			}
		}catch(IOException e){
			path = "/opt/supremeinventory/data-matrix/";
		}
		
		System.out.println("createPDF entered");
		Rectangle Label = new Rectangle(282, 40);
        document.setPageSize(Label);
        document.setMargins(4,4,4,4);
		document.open();
		Session hSession = null;
		
		try {
			hSession = HibernateBridge.getSessionFactory().openSession();
			Container container = (Container)hSession.createQuery("from Container where id=?").setLong(0, containerId).setMaxResults(1).uniqueResult();
			List<Inventory> inventoryList = (List<Inventory>)hSession.createQuery("from Inventory where containerId=?").setLong(0, containerId).list();
			document.addTitle("Print Labels");
	        document.addSubject("Using iText");
	        document.addKeywords("Java, PDF, iText");
	        document.addAuthor("Supreme Marbles");
	        document.addCreator("Supreme Marbles");
	      
	        
	        Rectangle originalPageRect=document.getPageSize(); 
	        document.add(originalPageRect);
	       
	
		    float pageLeft=originalPageRect.getLeft();
			float pageRight=originalPageRect.getRight();
			float pageBottom=originalPageRect.getBottom();
			float pageTop=originalPageRect.getTop();
			
			System.out.println("pageLeft: "+ pageLeft);
			System.out.println("pageRight: "+ pageRight);
			System.out.println("pageBottom: "+ pageBottom);
			System.out.println("pageTop: "+ pageTop);
			
			if(inventoryList.size() > 0){
				int i = 1;
				Iterator<Inventory> inventoryListIterator = inventoryList.iterator();
				while(inventoryListIterator.hasNext()){
					document.setPageSize(Label);
					Inventory inventory = inventoryListIterator.next();

					Font supremeBlackNormal = new Font(FontFamily.COURIER, 10, Font.BOLD, BaseColor.BLACK);
					Font supremeBlackSmall = new Font(FontFamily.COURIER, 8, Font.BOLD, BaseColor.BLACK);
					supremeBlackNormal.setStyle(Font.BOLD);
					
					/**
					 * Working Code for Slab Name and Size
					 */
					Phrase p = new Phrase();
					System.out.println("inventory.getMaterial().getMaterialName(): "+ inventory.getMaterial().getMaterialName());
					Chunk slabName = new Chunk(inventory.getMaterial().getMaterialName(), supremeBlackNormal);
					slabName.setFont(supremeBlackNormal);
					if(i == 1){
						p.add("\n");
					}
					p.add(slabName);
					p.add("\n");
					
					Chunk slabNoSupreme = new Chunk(""+inventory.getSlabNoSupreme(), supremeBlackNormal);
					Chunk forwardSlash = new Chunk(" / ", supremeBlackNormal);
					Chunk slabSize = new Chunk(""+(int)inventory.getActualLength()+" x "+(int)inventory.getActualHeight()+" x "+inventory.getThickness()+" cm", supremeBlackNormal);
					slabNoSupreme.setFont(supremeBlackNormal);
					slabSize.setFont(supremeBlackNormal);
					p.add(slabNoSupreme);
					p.add(forwardSlash);
					p.add(slabSize);
					p.add(forwardSlash);
			        document.add(p);
			        Chunk barcodeString = new Chunk(inventory.getBarcodeString(), supremeBlackSmall);
			        document.add(barcodeString);
					i++;
					
					/**
					 * Adding Code for Data Matrix
					 */
					
					String dataMatrixTextString = inventory.getBarcodeString();
					int size = 25;
					String fileType = "png";
					File qrFile = new File(path+File.separator+dataMatrixTextString+".png");
					MyQR.createQRImage(qrFile, dataMatrixTextString, size, fileType);
					Image dataMatrixImage = MyQR.getDataMatrixImage(qrFile);
					dataMatrixImage.scaleToFit(25, 25);
					dataMatrixImage.setAbsolutePosition(pageLeft+225, pageTop-38);
			        document.add(dataMatrixImage);
					
			        if(qrFile.delete()){
			        	System.out.println("qrFile File deleted from Project root directory");
			        }else System.out.println("File qrFile doesn't exist in the directory");
					
			        
			        document.newPage();
					document.setMargins(2,2,2,2);
				}
			}
			
			
			Transaction transaction = hSession.beginTransaction();
        	
        	try{
        		container.setPrinted(true);
        		container.setUpdatedOn(new Date());
        		hSession.saveOrUpdate(container);
        		
        		hSession.flush();
        		
        		transaction.commit();
        	}
        	catch(Exception e){
        		e.printStackTrace();
        		/*return false;*/
        	}
			
	        
		}
		catch(Exception e){
			e.printStackTrace();	
			throw new ValidationException(e);	
		}
		finally {
			hSession.close();
		}
	}
	private static void addEmptyLine(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	        paragraph.add(new Paragraph(" "));
	    }
	}


}