package com.supreme.util;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.supreme.Config;

public class HibernateBridge
{
	private static volatile SessionFactory sessionFactory;

	private HibernateBridge()
	{
	}



	public static SessionFactory getSessionFactory()
	{
		if(sessionFactory == null)
		{
			synchronized (HibernateBridge.class)
			{
				if(sessionFactory == null)
				{
					try
					{
						Config config = new Config();
						if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV))
						{
							System.out.println("Supreme Marbles Development Environment");
							sessionFactory = (new Configuration().configure(config.get(Config.HIBERNATE_DEV_CONFIG))).buildSessionFactory();
						}
						else if(config.get(Config.ENVIRONMENT).equals(Config.ENV_TEST_DEV))
						{
							System.out.println("SupremeMarbles Test Environment");
							sessionFactory = (new Configuration().configure(config.get(Config.HIBERNATE_TEST_CONFIG))).buildSessionFactory();
						}
						else
						{
							System.out.println("Production Environment");
							sessionFactory = (new Configuration().configure(config.get(Config.HIBERNATE_PROD_CONFIG))).buildSessionFactory();
						}
					}
					catch (FileNotFoundException e)
					{
						System.out.println("Nothing2");
						// TODO Auto-generated catch block
						e.printStackTrace();
						sessionFactory = (new Configuration().configure("hibernate.cfg.xml")).buildSessionFactory();
					}
					catch (IOException e) 
					{
						System.out.println("Nothing3");
						// TODO Auto-generated catch block
						e.printStackTrace();
						sessionFactory = (new Configuration().configure("hibernate.cfg.xml")).buildSessionFactory();
					}
				}
			}
		}
		return sessionFactory;
	}

	public static void buildSessionFactory()
	{
		sessionFactory.close();
		sessionFactory = null;
		sessionFactory = (new Configuration().configure()).buildSessionFactory();
	}
}
