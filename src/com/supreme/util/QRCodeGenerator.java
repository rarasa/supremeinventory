package com.supreme.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import com.supreme.Config;

public class QRCodeGenerator {
	
    private static String QR_CODE_IMAGE_PATH = "";

    private static void generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {
    	
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
    }

    public static void main(String[] args) {
        try {
        	try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				QR_CODE_IMAGE_PATH = config.get(Config.QR_DEV_PATH) + File.separator + "MyQRCode.png";
    				System.out.println("Dev path: "+QR_CODE_IMAGE_PATH);
    			}else{
    				QR_CODE_IMAGE_PATH = config.get(Config.QR_PROD_PATH) + File.separator + "MyQRCode.png";
    				System.out.println("Prod path: "+QR_CODE_IMAGE_PATH);
    			}
    		}catch(IOException e){
    			QR_CODE_IMAGE_PATH = "/opt/supremeinventory/qr/";
    		}
            generateQRCodeImage("My Name is Rashi Rathi", 350, 350, QR_CODE_IMAGE_PATH);
        } catch (WriterException e) {
            System.out.println("Could not generate QR Code, WriterException :: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("Could not generate QR Code, IOException :: " + e.getMessage());
        }
    }
}