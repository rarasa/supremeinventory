package com.supreme.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.swing.plaf.basic.BasicScrollPaneUI.HSBChangeListener;
import javax.xml.bind.ValidationException;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.supreme.Config;
import com.supreme.dao.Container;
import com.supreme.dao.ContainerMaterialList;
import com.supreme.dao.ContainerStatusDetails;
import com.supreme.dao.Currency;
import com.supreme.dao.Customer;
import com.supreme.dao.Cutting;
import com.supreme.dao.CuttingOrder;
import com.supreme.dao.CuttingOrderCart;
import com.supreme.dao.CuttingOrderTask;
import com.supreme.dao.CuttingOrderTaskTypeDetails;
import com.supreme.dao.CuttingType;
import com.supreme.dao.Orders;
import com.supreme.dao.BankDetails;
import com.supreme.dao.Cart;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;
import com.supreme.dao.Locations;
import com.supreme.dao.Material;
import com.supreme.dao.Status;
import com.supreme.dao.StatusTypes;
import com.supreme.dao.Supplier;
import com.supreme.dao.Unit;
import com.supreme.dao.User;
import com.supreme.dao.UserRole;
import com.supreme.util.JSON;

public class JSON {
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat displayDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	static final String User = "User";
	static final String Supplier = "Supplier";
	static final String Material = "Material";
	static final String Inventory = "Inventory";
	static final String InvoiceCutting = "InvoiceCutting";
	static final String Cutting = "Cutting";
	static final String Cart = "Cart";
	static final String Orders = "Orders";
	static final String Container = "Container";
	static final String Locations = "Locations";
	static final String Customer = "Customer";
	static final String CuttingOrderCart = "CuttingOrderCart";
	static final String CuttingOrderTask = "CuttingOrderTask";
	static final String Status = "Status";
	static final String StatusTypes = "StatusTypes";
	static final String BankDetails = "BankDetails";
	static final String ContainerMaterialList = "ContainerMaterialList";
	static final String ContainerStatusDetails = "ContainerStatusDetails";
	
	public static JSONObject toJSONObject(Object next) throws JSONException {
		// TODO Auto-generated method stub
		if (next.getClass().toString().toLowerCase()
				.contains(JSON.Inventory.toLowerCase())) {
			return JSON.toJSONObject((Inventory) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.User.toLowerCase())) {
			return JSON.toJSONObject((com.supreme.dao.User) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Supplier.toLowerCase())) {
			return JSON.toJSONObject((Supplier) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Material.toLowerCase())) {
			return JSON.toJSONObject((Material) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.InvoiceCutting.toLowerCase())) {
			return JSON.toJSONObject((InvoiceCutting) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Cutting.toLowerCase())) {
			return JSON.toJSONObject((Cutting) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Locations.toLowerCase())) {
			return JSON.toJSONObject((Locations) next);
		} 
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Orders.toLowerCase())) {
			return JSON.toJSONObject((Orders) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Cart.toLowerCase())) {
			return JSON.toJSONObject((Cart) next);
		} else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Container.toLowerCase())) {
			return JSON.toJSONObject((Container) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Customer.toLowerCase())) {
			return JSON.toJSONObject((Customer) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.CuttingOrderCart.toLowerCase())) {
			return JSON.toJSONObject((CuttingOrderCart) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.CuttingOrderTask.toLowerCase())) {
			return JSON.toJSONObject((CuttingOrderTask) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.Status.toLowerCase())) {
			return JSON.toJSONObject((Status) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.StatusTypes.toLowerCase())) {
			return JSON.toJSONObject((StatusTypes) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.BankDetails.toLowerCase())) {
			return JSON.toJSONObject((BankDetails) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.ContainerMaterialList.toLowerCase())) {
			return JSON.toJSONObject((ContainerMaterialList) next);
		}
		else if (next.getClass().toString().toLowerCase()
				.contains(JSON.ContainerStatusDetails.toLowerCase())) {
			return JSON.toJSONObject((ContainerStatusDetails) next);
		}
		else{
			return null;
		}
	}

	public static JSONArray toJSONArray(Set<?> s) throws JSONException {
		JSONArray arr = new JSONArray();
		Iterator<?> i = s.iterator();
		while (i.hasNext()) {
			arr.put(JSON.toJSONObject(i.next()));

		}
		return arr;
	}
	
	public static JSONArray toJSONArray(List<?> s) throws JSONException, ValidationException
	{
		JSONArray arr = new JSONArray();
		Iterator<?> i = s.iterator();
		while (i.hasNext())
		{
			arr.put(JSON.toJSONObject(i.next()));
		}
		return arr;
	}

	public static JSONArray getAllStateNamesJSONArray(List<String> stateList)
			throws JSONException {
		JSONArray arr = new JSONArray();
		Iterator<String> i = stateList.iterator();
		JSONObject obj = null;
		int x = 0;
		while (i.hasNext()) {
			String s = i.next();
			obj = new JSONObject();
			obj.put("id", x);
			obj.put("stateName",s);
			arr.put(obj);
			x++;
		}
		return arr;
	}
	
	public static JSONArray getAllBankNamesJSONArray(List<String> stateList)
			throws JSONException {
		JSONArray arr = new JSONArray();
		Iterator<String> i = stateList.iterator();
		JSONObject obj = null;
		int x = 0;
		while (i.hasNext()) {
			String s = i.next();
			obj = new JSONObject();
			obj.put("id", x);
			obj.put("bankName",s);
			arr.put(obj);
			x++;
		}
		return arr;
	}
	
	public static JSONArray getAllCityNamesJSONArray(List<String> cityList)
			throws JSONException {
		JSONArray arr = new JSONArray();
		Iterator<String> i = cityList.iterator();
		JSONObject obj = null;
		int x = 0;
		while (i.hasNext()) {
			String c = i.next();
			obj = new JSONObject();
			obj.put("id", x);
			obj.put("cityName",c);
			arr.put(obj);
			x++;
		}
		return arr;
	}
	
	public static JSONArray getAllBranchNamesJSONArray(List<String> branchList)
			throws JSONException {
		JSONArray arr = new JSONArray();
		Iterator<String> i = branchList.iterator();
		JSONObject obj = null;
		int x = 0;
		while (i.hasNext()) {
			String c = i.next();
			obj = new JSONObject();
			obj.put("id", x);
			obj.put("branchName",c);
			arr.put(obj);
			x++;
		}
		return arr;
	}
	
	public static JSONObject toJSONObject(User user) throws JSONException {
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		JSONObject obj = new JSONObject();
		obj.put("id", user.getId());

		obj.put("name", user.getName());
		obj.put("sex", user.getSex());
		
		UserRole role = (UserRole)hSession.get(UserRole.class, user.getRole());
		obj.put("role", role.getId());
		obj.put("roleString", role.getRole());
		
		obj.put("emiratesId", user.getEmiratesId());
		obj.put("password", user.getPassword());

		obj.put("dob", user.getDob() != null ? JSON.sdf.format(user.getDob()) : "");
		obj.put("email", user.getEmail());
		obj.put("phone", user.getMobileNumber() != null ? user.getMobileNumber() : "");
		obj.put("addressLine1", user.getAddressLine1() != null ? user.getAddressLine1() : "");
		obj.put("addressLine2", user.getAddressLine2() != null ? user.getAddressLine2() : "");
		obj.put("city", user.getCity() != null ? user.getCity() : "");
		obj.put("country", "UAE");
		obj.put("active", user.getActive());
		obj.put("createdOn", user.getCreatedOn());
		obj.put("updatedOn", user.getUpdatedOn());
		obj.put("lastLogin", user.getLastLogin());

		return obj;
	}
	
	public static JSONObject toJSONObject(Supplier supplier) throws JSONException {
		
		JSONObject obj = new JSONObject();
		
		obj.put("supplierId", supplier.getSupplierId());
		obj.put("supplierName", supplier.getSupplierName());
		obj.put("address", supplier.getAddress());
		obj.put("phone", supplier.getPhone());
		obj.put("email", supplier.getEmail());
		
		return obj;
		
	}
	
	public static JSONObject toJSONObject(UserRole userRole) throws JSONException {
		
		JSONObject obj = new JSONObject();
		
		obj.put("role", userRole.getId());
		obj.put("roleString", userRole.getRole());
		obj.put("createdOn", userRole.getCreatedOn());
		obj.put("updatedOn", userRole.getUpdatedOn());
		
		return obj;
		
	}
	
	public static JSONObject toJSONObject(Material material) throws JSONException {
		
		JSONObject obj = new JSONObject();
		
		obj.put("materialId", material.getMaterialId());
		obj.put("materialName", material.getMaterialName());
		obj.put("materialCode", material.getMaterialCode());
		obj.put("originCountry", material.getOriginCountry());
		
		return obj;
		
	}
	
	public static JSONObject toJSONObject(Container container) throws JSONException {
		
		JSONObject obj = new JSONObject();
		obj.put("containerId", container.getContainerId());
		obj.put("containerCode", container.getContainerCode());
		obj.put("containerNumber", container.getContainerCode());
		obj.put("packingListUploaded", container.isPackingListUploaded());
		
		if(container.getSupplier() != null) {
			obj.put("supplier", toJSONObject(container.getSupplier()));
		}
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		if(container.isDetailsExist()) {
			
			try {
				
				List<ContainerMaterialList> cmlList = (List<ContainerMaterialList>) hSession.createQuery("from ContainerMaterialList where containerId=?").setLong(0, container.getContainerId()).list();
				
				if(cmlList != null && cmlList.size() > 0) {
					JSONArray materialList = new JSONArray();
					Iterator<ContainerMaterialList> i = cmlList.iterator();
					while(i.hasNext()) {
						ContainerMaterialList c = i.next();
						
						if(!c.isDeleted()) {
							JSONObject cmlObj = new JSONObject();
							cmlObj = JSON.toJSONObject(c);
							materialList.put(cmlObj);
						}
					}
					obj.put("materialList", materialList);
				}
				
				ContainerStatusDetails csd = (ContainerStatusDetails) hSession.createQuery("from ContainerStatusDetails where containerId=?").setLong(0, container.getContainerId()).setMaxResults(1).uniqueResult();
				if(csd != null) {
					obj.put("statusDetails", JSON.toJSONObject(csd));
				}
				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		
		obj.put("createdOn", container.getCreatedOn());
		obj.put("updatedOn", container.getUpdatedOn());
		
		
		return obj;
	}
	
	public static JSONObject toContainerInventoryJSONObject(Container container, List<Inventory> inventoryList) throws JSONException {
		
		org.json.JSONObject object = new org.json.JSONObject();
		JSONArray inventoryListArray = new JSONArray();
		
		JSONObject obj = new JSONObject();
		obj.put("containerId", container.getContainerId());
		obj.put("containerCode", container.getContainerCode());
		
		Iterator<Inventory> i = inventoryList.iterator();
		while(i.hasNext()){
			Inventory inventory = i.next();
			object = JSON.toInventoryContainerJSONObject(inventory);
			inventoryListArray.put(object);
		}
		
		obj.put("inventoryList", inventoryListArray);
		
		return obj;
	}
	
	public static JSONObject toInventoryContainerJSONObject(Inventory inventory) throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("id", inventory.getId());
		obj.put("thickness", inventory.getThickness());
		obj.put("slabNoSupreme", inventory.getSlabNoSupreme());
		obj.put("slabNoSupreme", inventory.getSlabNoSupplier());
		obj.put("type", inventory.getType());
		obj.put("arrivalDate", inventory.getArrivalDate() != null ? JSON.sdf.format(inventory.getArrivalDate()) : "");
		obj.put("actualLength", inventory.getActualLength());
		obj.put("actualHeight", inventory.getActualHeight());
		obj.put("actualSqMetre", inventory.getActualSquareMetre());
		obj.put("diffSqMetre", inventory.getDiffSquareMetre());
		
		obj.put("sold", inventory.getSold());
		obj.put("inCutting", inventory.getInCutting());
		
		obj.put("slabNoSupreme", inventory.getSlabNoSupreme());
		obj.put("slabNoSupplier", inventory.getSlabNoSupplier());
		
		obj.put("barcodeString", inventory.getBarcodeString());
		
		obj.put("yard", inventory.getYard() == 0 ? "Main" : "Warehouse" );
		obj.put("oldStock", inventory.getOldStock());
		
		obj.put("tile", inventory.getTile());
		obj.put("tileCount", inventory.getTileCount());
		
		
		if(inventory.getSupplier() != null){
			
			JSONObject supplierObj = new JSONObject();
			supplierObj.put("id", inventory.getSupplier().getSupplierId());
			supplierObj.put("name", inventory.getSupplier().getSupplierName());
			
			obj.put("supplier", supplierObj);
		}
	
		if(inventory.getMaterial() != null) {
			JSONObject materialObj = new JSONObject();
			materialObj.put("id", inventory.getMaterial().getMaterialId());
			materialObj.put("name", inventory.getMaterial().getMaterialName());
			
			obj.put("material", materialObj);
		}
		return obj;
	}
	
	public static JSONObject toJSONObject(Inventory inventory) throws JSONException {
		
		JSONObject obj = new JSONObject();
		
		obj.put("id", inventory.getId());
		obj.put("thickness", inventory.getThickness());
		obj.put("slabNoSupreme", inventory.getSlabNoSupreme());
		obj.put("slabNoSupplier", inventory.getSlabNoSupplier());
		obj.put("type", inventory.getType());
		obj.put("arrivalDate", inventory.getArrivalDate() != null ? JSON.sdf.format(inventory.getArrivalDate()) : "");
		obj.put("length", inventory.getLength());
		obj.put("height", inventory.getHeight());
		obj.put("actualLength", inventory.getActualLength());
		obj.put("actualHeight", inventory.getActualHeight());
		obj.put("sqMetre", inventory.getSquareMetre());
		obj.put("actualSqMetre", inventory.getActualSquareMetre());
		obj.put("diffSqMetre", inventory.getDiffSquareMetre());
		
		obj.put("sold", inventory.getSold());
		obj.put("inCutting", inventory.getInCutting());
		
		obj.put("barcodeString", inventory.getBarcodeString());
		
		obj.put("yard", inventory.getYard() == 0 ? "Main" : "Warehouse" );
		obj.put("container", toJSONObject(inventory.getContainer()));
		obj.put("oldStock", inventory.getOldStock());
		
		obj.put("tile", inventory.getTile());
		obj.put("tileCount", inventory.getTileCount());
		
		
		if(inventory.getSupplier() != null){
			
			JSONObject supplierObj = new JSONObject();
			supplierObj.put("id", inventory.getSupplier().getSupplierId());
			supplierObj.put("name", inventory.getSupplier().getSupplierName());
			supplierObj.put("phone", inventory.getSupplier().getPhone());
			
			obj.put("supplier", supplierObj);
		}
	
		if(inventory.getMaterial() != null) {
			JSONObject materialObj = new JSONObject();
			materialObj.put("id", inventory.getMaterial().getMaterialId());
			materialObj.put("name", inventory.getMaterial().getMaterialName());
			
			obj.put("material", materialObj);
		}
		
		return obj;
		
	}
	
	public static JSONArray getCuttingOrderListArray(List<Cutting> cuttingList) throws JSONException
	{
		JSONArray arr = new JSONArray();
		Iterator<Cutting> i = cuttingList.iterator();
		JSONObject obj = null;
		
		while(i.hasNext()){
			
			Cutting cut = i.next();
			obj = new JSONObject();
			obj.put("id", cut.getId());
			obj.put("cutingOrderNumber", cut.getCuttingOrderNumber());
			obj.put("cutingOrderDate", cut.getOrderDate());
			obj.put("particulars", cut.getParticulars());
			obj.put("quantity", cut.getQuantity());
			obj.put("rate", cut.getRate());
			obj.put("unit", cut.getUnit());
			obj.put("amount", cut.getAmount());
			arr.put(obj);
			
		}

		return arr;
	}
	
	public static JSONArray getCuttingOrderSet(Set<Cutting> cuttingSet) throws JSONException
	{
		JSONArray arr = new JSONArray();
		Iterator<Cutting> i = cuttingSet.iterator();
		JSONObject obj = null;
		
		while(i.hasNext()){
			
			Cutting cut = i.next();
			obj = new JSONObject();
			obj.put("id", cut.getId());
			obj.put("cuttingOrderNumber", cut.getCuttingOrderNumber());
			obj.put("cuttingOrderDate", cut.getOrderDate());
			obj.put("particular", cut.getParticulars());
			obj.put("quantity", cut.getQuantity());
			obj.put("rate", cut.getRate());
			obj.put("unit", cut.getUnit());
			obj.put("amount", cut.getAmount());
			obj.put("amount", cut.getAmount());
			arr.put(obj);
			
		}

		return arr;
	}
	
	public static JSONObject getInvoiceCutting(InvoiceCutting invCutting) throws JSONException {
		JSONArray arr = new JSONArray();
		JSONObject obj = null;
			
		obj = new JSONObject();
		obj.put("id", invCutting.getId());
		obj.put("invoiceDate", invCutting.getInvoiceDate());
		obj.put("invoiceNumber", invCutting.getInvoiceNumber());
		obj.put("cuttingOrders", JSON.getCuttingOrderSet(invCutting.getCuttingOrders()));
		arr.put(obj);

		return obj;
	}
	
	@SuppressWarnings("null")
	public static JSONObject getCuttingList(CuttingOrder co) throws JSONException {
		JSONArray arr = new JSONArray();
		JSONObject obj = null;
		JSONObject invoiceObj = null;
			
		obj = new JSONObject();
		obj.put("id", co.getId());
		obj.put("cuttingOrderNumber", co.getCuttingOrderNumber());
		obj.put("totalInputMtSquare", co.getTotalInputMtSquare());
		obj.put("totalOutputMtSquare", co.getTotalOutputMtSquare());
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		Orders order = (Orders) hSession.createQuery("from Orders where id=?").setLong(0, co.getOrderId()).setMaxResults(1).uniqueResult();
		
		if(order!=null) {
			
			Customer customer = (Customer) hSession.createQuery("from Customer where id=?").setLong(0, order.getCustomerId()).setMaxResults(1).uniqueResult();
			if(customer != null) {
				
				obj.put("customerName", customer.getName());
				obj.put("customerId", customer.getId());
				obj.put("emirate", customer.getEmirate());
				obj.put("phone", customer.getPhone());
				obj.put("trnNumber", customer.getTrnNumber());
				obj.put("VATRegistrationDate", customer.getVATRegistrationDate());
				obj.put("VATRegistration", customer.isVATRegistration() ? 1 : 0);
				obj.put("country", customer.getCountry());
				obj.put("customerCreatedOn", customer.getCreatedOn());
				obj.put("customerUpdatedOn", customer.getUpdatedOn());
			}
		}
		
		List<CuttingOrderTask> coTaskList = (List<CuttingOrderTask>)hSession.createQuery("from CuttingOrderTask where cuttingOrderId=? and deleted=0").setLong(0, co.getId()).list();
		if(coTaskList != null && coTaskList.size() > 0) {
			System.out.println("coTaskList.size: "+ coTaskList.size());
			JSONArray cuttingOrderTaskArray = new JSONArray();
			Iterator<CuttingOrderTask> i = coTaskList.iterator();
			while(i.hasNext()){
				System.out.println("COCOCOCCO");
				CuttingOrderTask cd = i.next();
				JSONObject cdObj = toJSONObject(cd);
				cuttingOrderTaskArray.put(cdObj);
			}
			obj.put("cuttingOrderTask", cuttingOrderTaskArray);
		}

		return obj;
	}
	
	public static JSONObject toJSONObject(Orders order) throws JSONException {
		JSONArray arr = new JSONArray();
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", order.getId());
		obj.put("placed", order.getPlaced());
		obj.put("LPODate", order.getLPODate());
		obj.put("LPONumber", order.getLPONumber());
		obj.put("slabCount", order.getCount());
		obj.put("totalMtSquare", order.getTotalMtSquare());
		obj.put("DOPrinted", order.isDOPrinted());
		obj.put("companyPickup", order.isCompanyPickup() ? 1 : 0);
		obj.put("createdOn", order.getCreatedOn());
		obj.put("updatedOn", order.getUpdatedOn());
		obj.put("DOPrintedOn", order.getDOPrintedOn());
		obj.put("vehicleNumber", order.getVehicleNumber());
		obj.put("narration", order.getNarration());
		obj.put("hasCuttingSlabs", order.isHasCuttingSlabs());

		obj.put("skipScan", order.isSkipAddingManualData() ? 1 : 0);
		obj.put("outsideMaterial", order.isOutsideMaterial() ? 1 : 0);
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		if(order.isHasCuttingSlabs()) {
			
			JSONArray cuttingOrderArray = new JSONArray();
			List<CuttingOrder> coList = (List<CuttingOrder>) hSession.createQuery("from CuttingOrder where orderId=?").setLong(0, order.getId()).list();
			Iterator<CuttingOrder> coItr = coList.iterator();
			while(coItr.hasNext()) {
				CuttingOrder co = coItr.next();
				JSONObject cdObj = toJSONObject(co);
				cuttingOrderArray.put(cdObj);
			}
			obj.put("cuttingOrderList", cuttingOrderArray);
		}
		
		obj.put("deleted", order.isDeleted());
		obj.put("merged", order.isMerged());
		
		obj.put("placedOn", order.getPlacedOn() != null ? order.getPlacedOn() : "");
		obj.put("placedBy", order.getPlacedBy());
		
		try {
						
			if(order.getCustomerId() != 0) {
				
				Customer customer = (Customer)hSession.get(Customer.class, order.getCustomerId());
				obj.put("customer", toJSONObject(customer));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		

		obj.put("cart", getCartOrderSet(order.getCart(), order.getId()));
		
		arr.put(obj);

		return obj;
	}
	
	public static JSONArray getCartOrderSet(Set<Cart> cartSet, long orderId) throws JSONException
	{
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		JSONArray arr = new JSONArray();
		try {
		
		List<Cart> cartList = hSession.createQuery("from Cart where orderId=? order by inventoryId asc").setLong(0, orderId).list();
		Iterator<Cart> i = cartList.iterator();
		JSONObject obj = null;
		
		while(i.hasNext()){
			
			Cart cart = i.next();
			obj = new JSONObject();
			
			if(cart.isDeleted()){
				/**
				 * Ignore this Cart Row
				 */
			}
			else{
			
				obj.put("id", cart.getId());
				
				obj.put("inventoryId", cart.getInventory().getId());
				Inventory inventory = (Inventory)hSession.get(Inventory.class, cart.getInventory().getId());
				
				
				obj.put("inventory", toJSONObject(inventory));
				
				obj.put("orderId", cart.getOrderId());
				obj.put("deleted", cart.isDeleted());
				obj.put("addToCuttingOrder", cart.isSentForCutting());
				obj.put("tile", inventory.getTile());
				
				obj.put("tileCount", cart.getTileCount());
				obj.put("originalTileCount", inventory.getTileCount() <= 0 ? cart.getTileCount() : (inventory.getTileCount() + cart.getTileCount()));
				
				if(inventory.getSupplier() != null){
					JSONObject supplierObj = new JSONObject();
					supplierObj.put("id", inventory.getSupplier().getSupplierId());
					supplierObj.put("name", inventory.getSupplier().getSupplierName());
					supplierObj.put("phone", inventory.getSupplier().getPhone());
					obj.put("supplier", supplierObj);
				}
			
				if(inventory.getMaterial() != null) {
					JSONObject materialObj = new JSONObject();
					materialObj.put("id", inventory.getMaterial().getMaterialId());
					materialObj.put("name", inventory.getMaterial().getMaterialName());
					
					obj.put("material", materialObj);
				}
				obj.put("createdOn", cart.getCreatedOn());
				obj.put("updatedOn", cart.getUpdatedOn());
				arr.put(obj);
			}
			}
		
			
		return arr;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static JSONObject toJSONObject(Locations location) throws JSONException {
		JSONArray arr = new JSONArray();
		JSONObject obj = null;
			
		obj = new JSONObject();
		obj.put("id", location.getId());
		obj.put("name", location.getName());
		obj.put("code", location.getCode());
		obj.put("address", location.getAddress());
		obj.put("phone", location.getContact());
		obj.put("active", location.isActive());
		obj.put("deleted", location.isDeleted());

		arr.put(obj);

		return obj;
	}
	
	public static JSONObject toSmallJSONObject(Customer c) throws JSONException {
		
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("name", c.getName());
		obj.put("deleted", c.isDeleted());

		return obj;
		
	}

	public static JSONObject toJSONObject(CuttingOrder c) throws JSONException {
		
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("amount", c.getAmount());
		obj.put("cuttingOrderNumber", c.getCuttingOrderNumber());
		obj.put("orderId", c.getOrderId());
		obj.put("totalInputMtSquare", c.getTotalInputMtSquare());
		obj.put("totalOutputMtSquare", c.getTotalOutputMtSquare());
		obj.put("totalOutputLinearMeter", c.getTotalOutputLinearMeter());
		obj.put("createdOn", c.getCreatedOn());
		obj.put("deletedOn", c.getDeletedOn());
		obj.put("DOPrintedOn", c.getDOPrintedOn());
		obj.put("placedOn", c.getPlacedOn());
		obj.put("updatedOn", c.getUpdatedOn());
		obj.put("deleted", c.isDeleted());
		obj.put("DOPrinted", c.isDOPrinted());
		obj.put("placed", c.isPlaced());
		obj.put("materialId", c.getMaterialId());
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		if(c.getMaterialId() != 0) {
			Material material = (Material) hSession.get(Material.class, c.getMaterialId());
			obj.put("material", toJSONObject(material));
		}

		
		List<CuttingOrderTask> cuttingOrderTaskList = (List<CuttingOrderTask>)hSession.createQuery("from CuttingOrderTask where cuttingOrderId=? and deleted=0").setLong(0, c.getId()).list();
		if(cuttingOrderTaskList != null && cuttingOrderTaskList.size() > 0) {
			JSONObject cutObj = new JSONObject();
			int thickness = 0;
			JSONArray cuttingOrderTaskArray = new JSONArray();
			Iterator<CuttingOrderTask> i = cuttingOrderTaskList.iterator();
			while(i.hasNext()){
				CuttingOrderTask cd = i.next();
				JSONObject cdObj = toJSONObject(cd);
				cuttingOrderTaskArray.put(cdObj);
				
				
				if(cdObj.has("thickness")) {
					thickness = Integer.parseInt(cdObj.get("thickness").toString());
					obj.put("thickness", thickness);
				}
			}
			obj.put("cuttingOrderTasks", cuttingOrderTaskArray);
		}
		
		return obj;
		
	}
	
	public static JSONObject toJSONObject(CuttingOrderCart c) throws JSONException {
		
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("amount", c.getAmount());
		obj.put("customerId", c.getCustomerId());
		obj.put("height", c.getHeight());
		obj.put("length", c.getLength());
		obj.put("nos", c.getNos());
		obj.put("orderDate", c.getOrderDate());
		obj.put("quantity", c.getQuantity());
		obj.put("rate", c.getRate());
		obj.put("thickness", c.getThickness());
		obj.put("createdOn", c.getCreatedOn());
		obj.put("deletedOn", c.getDeletedOn());
		obj.put("updatedOn", c.getUpdatedOn());
		obj.put("cutToSize", c.isCutToSize());
		obj.put("deleted", c.isDeleted());
		

		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		try {
			
			if(c.getMaterialId() != 0) {
				Material material = (Material) hSession.get(Material.class, c.getMaterialId());
				obj.put("material", toJSONObject(material));
			}
			if(c.getUnitId() != 0) {
				Unit unit = (Unit) hSession.get(Unit.class, c.getUnitId());
				obj.put("unit", toJSONObject(unit));
			}
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return obj;
	}

	public static JSONObject toJSONObject(Unit unit) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", unit.getId());
		obj.put("name", unit.getMeasureUnit());
		return obj;
	}
	
	public static JSONObject toJSONObject(CuttingType c) throws JSONException {
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("name", c.getName());
		obj.put("machine", c.isMachine());
		obj.put("manual", c.isManual());
		
		return obj;
	}
	public static JSONObject toJSONObject(CuttingOrderTask c) throws JSONException {
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("cuttingOrderId", c.getCuttingOrderId());
		obj.put("deleted", c.isDeleted());
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		
		List<CuttingOrderTaskTypeDetails> cottList = (List<CuttingOrderTaskTypeDetails>) hSession.createQuery("from CuttingOrderTaskTypeDetails where cuttingOrderTaskId=?").setLong(0, c.getId()).list();
		
		if(cottList != null && cottList.size() > 0) {
			JSONArray cuttingOrderTaskTypeArray = new JSONArray();
			Iterator<CuttingOrderTaskTypeDetails> i = cottList.iterator();
			while(i.hasNext()){
				CuttingOrderTaskTypeDetails ctt = i.next();
				JSONObject cdObj = toJSONObject(ctt);
				cuttingOrderTaskTypeArray.put(cdObj);
			}
			obj.put("cuttingOrderTaskType", cuttingOrderTaskTypeArray);
			
		}

		List<CuttingOrderCart> coCartList = (List<CuttingOrderCart>) hSession.createQuery("from CuttingOrderCart where cuttingOrderTaskId=?").setLong(0, c.getId()).list();
		if(coCartList != null & coCartList.size() > 0) {
			JSONArray cuttingOrderCartArray = new JSONArray();
			Iterator<CuttingOrderCart> j = coCartList.iterator();
			int thickness = 0;
			while(j.hasNext()){
				CuttingOrderCart coc = j.next();
				JSONObject cdObj = toJSONObject(coc);
				cuttingOrderCartArray.put(cdObj);
				
				if(cdObj.has("thickness") && cdObj.get("thickness")!= null) {
					thickness = (int)cdObj.get("thickness");
				}
			}
			obj.put("cuttingOrderCart", cuttingOrderCartArray);
			obj.put("thickness", thickness);
		}
		
		return obj;
	}
	public static JSONObject toJSONObject(CuttingOrderTaskTypeDetails ctt) throws JSONException {
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("id", ctt.getId());
		obj.put("amount", ctt.getAmount());
		obj.put("cuttingOrderTaskId", ctt.getCuttingOrderTaskId());
		obj.put("cuttingType", toJSONObject(ctt.getCuttingType()));
		obj.put("deletedOn", ctt.getDeletedOn());
		obj.put("deleted", ctt.isDeleted());
		
		return obj;
	}
	
	public static JSONObject toJSONObject(Customer customer) throws JSONException {
		JSONObject obj = null;
		
		obj = new JSONObject();
		obj.put("customerName", customer.getName());
		obj.put("customerId", customer.getId());
		obj.put("emirate", customer.getEmirate());
		obj.put("phone", customer.getPhone());
		obj.put("trnNumber", customer.getTrnNumber());
		obj.put("VATRegistrationDate", customer.getVATRegistrationDate());
		obj.put("VATRegistration", customer.isVATRegistration() ? 1 : 0);
		obj.put("country", customer.getCountry());
		obj.put("customerCreatedOn", customer.getCreatedOn());
		obj.put("customerUpdatedOn", customer.getUpdatedOn());
		
		return obj;
	}

	public static JSONObject toJSONObject(Status s) throws JSONException {
		JSONObject obj = null;
		JSONObject mainObj = new JSONObject();
		
		
		obj = new JSONObject();
		
		obj.put("id", s.getId());
		obj.put("name", s.getName());
		obj.put("createdOn", s.getCreatedOn());
		obj.put("updatedOn", s.getUpdatedOn());
		obj.put("deletedOn", s.getDeletedOn());
		obj.put("active", s.isActive());
		obj.put("deleted", s.isDeleted());
		JSONArray statusTypesArray = new JSONArray();
		Iterator<StatusTypes> sTypesItr = s.getStatusTypes().iterator();
		while(sTypesItr.hasNext()) {
			StatusTypes st = sTypesItr.next();
			JSONObject sObj = toJSONObject(st);
			statusTypesArray.put(sObj);
		}
		obj.put("statusTypes", statusTypesArray);
		
		mainObj.put(s.getName(), obj);
		
		return mainObj;
	}

	public static JSONObject toJSONObject(StatusTypes s) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", s.getId());
		obj.put("name", s.getName());
		obj.put("active", s.isActive());
		obj.put("deleted", s.isDeleted());
		obj.put("finalStatus", s.isFinalStatus());
		return obj;
	}

	public static JSONObject toJSONObject(BankDetails s) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", s.getId());
		
		obj.put("name", s.getBankName() != null ? s.getBankName() : "");
		obj.put("customerId", s.getCustomerId());
		obj.put("accountNumber", s.getAccountNumber() != null ? s.getAccountNumber() : "");
		obj.put("branchAddress", s.getBranchAddress() != null ? s.getBranchAddress() : "");
		obj.put("iban", s.getIban() != null ? s.getIban() : "");
		obj.put("ifsc", s.getIfscCode() != null ? s.getIfscCode() : "");
		obj.put("swiftCode", s.getSwiftCode() != null ? s.getSwiftCode() : "");

		obj.put("active", s.isActive());
		obj.put("deleted", s.isDeleted());
		return obj;
	}

	public static JSONObject toJSONObject(Currency c) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("name", c.getName());

		obj.put("value", c.getValue());
		obj.put("changedOn", c.getChangedOn());
		obj.put("createdOn", c.getCreatedOn());
		obj.put("updatedByUser", c.getUpdatedByUser());
		obj.put("updatedOn", c.getUpdatedOn());
		
		return obj;
	}

	public static JSONObject toJSONObject(ContainerMaterialList c) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("containerId", c.getContainerId());
		
		obj.put("materialId", c.getMaterialId());
		
		Session hSession = HibernateBridge.getSessionFactory().openSession();
		try {
			Material material = (Material)hSession.get(Material.class, c.getMaterialId());
			obj.put("material", toJSONObject(material));
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		obj.put("noOfPieces", c.getNoOfPieces());
		obj.put("quantity", c.getQuantity());
		obj.put("realRate", c.getRealRate());
		obj.put("billRate", c.getBillRate());
		obj.put("thickness", c.getThickness());
		
		return obj;
	}

	public static JSONObject toJSONObject(ContainerStatusDetails c) throws JSONException {
		JSONObject obj = null;
		obj = new JSONObject();
		obj.put("id", c.getId());
		obj.put("containerId", c.getContainerId());
		
		Session jsonHSession = HibernateBridge.getSessionFactory().openSession();
		
		try {
			if(c.getContainerStatus() != 0) {
				StatusTypes containerStatusType = (StatusTypes)jsonHSession.get(StatusTypes.class, c.getContainerStatus());
				obj.put("containerStatusObj", toJSONObject(containerStatusType));
				obj.put("containerStatus", c.getContainerStatus());
			}
			else {
				obj.put("containerStatus", c.getContainerStatus());
			}
			if(c.getBLStatus() != 0) {
				StatusTypes blStatus = (StatusTypes)jsonHSession.get(StatusTypes.class, c.getBLStatus());
				obj.put("BLStatusObj", toJSONObject(blStatus));
				obj.put("BLStatus", c.getBLStatus());
			}
			else {
				obj.put("BLStatus", c.getBLStatus());
			}
			if(c.getDOStatus() != 0) {
				StatusTypes DOStatus = (StatusTypes)jsonHSession.get(StatusTypes.class, c.getDOStatus());
				obj.put("DOStatusObj", toJSONObject(DOStatus));
				obj.put("DOStatus", c.getDOStatus());
			}
			else {
				obj.put("DOStatus", c.getDOStatus());
			}
			if(c.getPaymentStatus() != 0) {
				StatusTypes PaymentStatus = (StatusTypes)jsonHSession.get(StatusTypes.class, c.getPaymentStatus());
				obj.put("paymentStatusObj", toJSONObject(PaymentStatus));
				obj.put("paymentStatus", c.getPaymentStatus());
			}
			else {
				obj.put("paymentStatus", c.getPaymentStatus() != null ? c.getPaymentStatus() : 0);
			}
			if(c.getBankDetailsId() != null && c.getBankDetailsId() != 0) {
				BankDetails bankdetails = (BankDetails)jsonHSession.get(BankDetails.class, c.getBankDetailsId());
				obj.put("bankDetailsObj", toJSONObject(bankdetails));
				obj.put("bankId", c.getBankDetailsId());
			}
			else {
				obj.put("bankId", c.getBankDetailsId());
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			jsonHSession.close();
		}
		
		obj.put("amount", c.getAmount());
		obj.put("currencyId", c.getCurrencyId());
		
		obj.put("containerNumber", c.getContainerCode());
		obj.put("description", c.getDescription());
		obj.put("invoiceDate", c.getInvoiceDate());
		obj.put("invoiceNumber", c.getInvoiceNo());
		obj.put("paymentDate", c.getPaymentDate());
		obj.put("rateOnDate", c.getRateOnDate());
		obj.put("createdOn", JSON.displayDateFormat.format(c.getCreatedOn()));
		obj.put("updatedOn", JSON.displayDateFormat.format(c.getUpdatedOn()));
		
		return obj;
	}
	
}
