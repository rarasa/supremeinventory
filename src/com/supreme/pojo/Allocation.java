package com.supreme.pojo;

import java.util.ArrayList;
import java.util.List;

public class Allocation
{
	String company;
	Double marketValue,purchaseCost,absoluteReturn,cagr;
	String type,optionType;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOptionType() {
		return optionType;
	}
	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Double getMarketValue() {
		return marketValue;
	}
	public void setMarketValue(Double marketValue) {
		this.marketValue = marketValue;
	}
	public Double getPurchaseCost() {
		return purchaseCost;
	}
	public void setPurchaseCost(Double purchaseCost) {
		this.purchaseCost = purchaseCost;
	}
	public Double getAbsoluteReturn() {
		return absoluteReturn;
	}
	public void setAbsoluteReturn(Double absoluteReturn) {
		this.absoluteReturn = absoluteReturn;
	}
	public Double getCagr() {
		return cagr;
	}
	public void setCagr(Double cagr) {
		this.cagr = cagr;
	}
	
	

	
}
