package com.supreme.pojo;

import java.util.Date;

import com.supreme.dao.Cutting;
import com.supreme.dao.InvoiceCutting;

public class CuttingSet
{
	public String particulars;
	public String unit;
	public double quantity;
	public double rate;
	public double amount;
	public boolean extraProfitGiven = false;

	public CuttingSet(String particulars, String unit, double quantity, double rate, double amount, boolean extraProfitGiven)
	{
		this.particulars = particulars;
		this.unit = unit;
		this.quantity = quantity;
		this.rate = rate;
		this.amount = amount;
		this.extraProfitGiven = extraProfitGiven;
	}
}