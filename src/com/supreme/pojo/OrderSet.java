package com.supreme.pojo;

import java.util.Date;

import com.supreme.dao.Cutting;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;

public class OrderSet
{
	public Inventory inventory;
	public double sellingPrice;

	public OrderSet(Inventory inventory, double sellingPrice)
	{
		this.inventory = inventory;
		this.sellingPrice = sellingPrice;
	}
}