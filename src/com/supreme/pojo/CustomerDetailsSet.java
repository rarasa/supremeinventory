package com.supreme.pojo;

import java.util.Date;

import com.supreme.dao.Cutting;
import com.supreme.dao.Inventory;
import com.supreme.dao.InvoiceCutting;

public class CustomerDetailsSet
{
	public long id, customerId;
	public 	String address, phone;
	public boolean deleted, walkIn;
	
	public Date createdOn, updatedOn;

	public CustomerDetailsSet(long customerId, String address, String phone)
	{
		this.customerId = customerId;
		this.address = address;
		this.phone = phone;
	}
}