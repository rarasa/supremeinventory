package com.supreme;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;


public class Config
{
	final String filename = "config.prop";
	
	
	public static final long DEFAULT_ACCOUNT=1L;
	
	public static final String ENV_DEV = "development", ENV_PRODUCTION = "production", 
			ENV_TEST_PROD = "test", ENV_TEST_DEV = "test", 
			SYS_ENV_DEV = "development", SYS_ENV_PRODUCTION = "production", 
			TRUE = "1", FALSE = "0";
	
	public static final String ENVIRONMENT = "environment", SYSTEM_ENVIRONMENT = "system_environment", TEST_ENVIRONMENT = "test_environment", SEND_SMS = "send_sms", SEND_EMAILS = "send_emails", SEND_SUPPORT_EMAILS = "send_support_emails", CLIENTID_SUFFIX = "clientid_suffix", SEND_TEST_SMS = "send_test_sms", TEST_SMS_NUMBER = "test_sms_number",
			SEND_TEST_EMAILS = "send_test_emails", TEST_EMAIL = "test_email", CASHFREE_TEST_KEY = "cashfree_test_key", CASHFREE_TEST_SECRET = "cashfree_test_secret", CASHFREE_PROD_KEY = "cashfree_prod_key", CASHFREE_PROD_SECRET = "cashfree_prod_secret",
			CASHFREE_DEV_RETURN_URL = "cashfree_dev_return_url", CASHFREE_PROD_RETURN_URL = "cashfree_prod_return_url", CASHFREE_DEV_NOTIFY_URL = "cashfree_dev_notify_url", CASHFREE_PROD_NOTIFY_URL = "cashfree_prod_return_url", CASHFREE_TEST_URL = "cashfree_test_url",
			CASHFREE_PROD_URL = "cashfree_prod_url", DEV_BASE_URL = "dev_base_url", PROD_BASE_URL = "prod_base_url", AOF_PROD_PATH = "aof_prod_path", AOF_DEV_PATH = "aof_dev_path",
			HIBERNATE_DEV_CONFIG = "hibernate_dev_config", HIBERNATE_PROD_CONFIG = "hibernate_prod_config", HIBERNATE_TEST_CONFIG = "hibernate_test_config", 
			EXCEL_PROD_PATH="excels_prod_path", EXCEL_DEV_PATH="excels_dev_path", 
			PACKINGLIST_PROD_PATH="packingList_prod_path", PACKINGLIST_DEV_PATH="packingList_dev_path",
			DATA_MATRIX_PROD_PATH="data_matrix_prod_path", DATA_MATRIX_DEV_PATH="data_matrix_dev_path",
			PDF_PROD_PATH="pdf_prod_path", PDF_DEV_PATH="pdf_dev_path",
			QR_PROD_PATH="qr_prod_path", QR_DEV_PATH="qr_dev_path",
			EKYC_DEV_URL="ekyc_development_url",
			EKYC_PROD_URL="ekyc_production_url",
			EKYC_DEV_API_KEY="ekyc_development_api_key",
			EKYC_PROD_API_KEY="ekyc_production_api_key",
			ALERT_DEV_API_URL="alert_dev_api_url",
			ALERT_PROD_API_URL="alert_prod_api_url",
			ARN_PROD="arn_prod",
			ARN_DEV="arn_dev",
			BSE_MEMBER_ID_PROD="bse_member_id_prod",
			BSE_USER_ID_PROD="bse_user_id_prod",
			BSE_PASSWORD_PROD="bse_password_prod",
			BSE_MEMBER_ID_DEV="bse_member_id_dev",
			BSE_USER_ID_DEV="bse_user_id_dev",
			BSE_PASSWORD_DEV="bse_password_dev",
			PRODUCT_NAME="product_name",
			PRODUCT_URL="product_url",
			PRODUCT_EMAIL="product_email",
			NRI_FLAG="nriFlagBSE",
			COMPANY_FLAG="companyFlagBSE",
			UAT_FLAG="uat_environment";

	public static boolean API_CONFIG=true; //this value gets initialized in every 24 hours 
	
	public static String redirectUrl = "https://germinatewealth.com";
	
	
	Properties properties;

	public Config() throws FileNotFoundException, IOException
	{
		properties = new Properties();

		System.out.println(Config.class.getResource("config.prop").getPath());
		FileInputStream fis = new FileInputStream(Config.class.getResource("config.prop").getPath());
		properties.load(fis);
	}

	public String get(String key)
	{
		return properties.getProperty(key);
	}
	
	public void set(String key, String value) throws IOException {
		System.out.println(">> Inside setter function: key: "+key+" Value: "+value);
		properties.setProperty(key, value);
		File f = new File(Config.class.getResource("config.prop").getPath());
        OutputStream out = new FileOutputStream(f);
        properties.store(out, "This is an optional header comment string");
	}
	
	
}
