package com.supreme.maps;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class States {
	public static Map<String, String> STATES=new HashMap<String, String>();
	static{
		Map<String, String> s=new HashMap<String, String>();
		s.put("AN", "Andaman & Nicobar");
		s.put("AP", "Andhra Pradesh");
		s.put("AR", "Arunachal Pradesh");
		s.put("AS", "Assam");
		s.put("BR", "Bihar");
		s.put("CH", "Chandigarh");
		s.put("CG", "Chattisgarh");
		s.put("DN", "Dadra and Nagar Haveli");
		s.put("DD", "Daman & Diu");
		s.put("DL", "Delhi");
		s.put("GA", "Goa");
		s.put("GJ", "Gujrat");
		s.put("HR", "Haryana");
		s.put("HP", "Himachal Pradesh");
		s.put("JK", "Jammu & Kashmir");
		s.put("JH", "Jharkhand");
		s.put("KA", "Karnataka");
		s.put("KL", "Kerala");
		s.put("LP", "Lakhadweep");
		s.put("MP", "Madhya Pradesh");
		s.put("MH", "Maharasthra");
		s.put("MN", "Manipur");
		s.put("ML", "Meghalaya");
		s.put("MZ", "Mizoram");
		s.put("NL", "Nagaland");
		s.put("OR", "Orissa");
		s.put("PY", "Pondicherry");
		s.put("PB", "Punjab");
		s.put("RJ", "Rajasthan");
		s.put("RA", "Rajasthan");
		s.put("SK", "Sikkim");
		s.put("TG", "Telangana");
		s.put("TN", "Tamil Nadu");
		s.put("TR", "Tripura");
		s.put("UP", "Uttarpradesh");
		s.put("UA", "Uttrakhand");
		s.put("WB", "West Bengal");
		s.put("XX", "Other");
		STATES = Collections.unmodifiableMap(s);
	}
}
