package com.supreme.maps;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Occupation {
	public static Map<String, String> OCCUPATION=new HashMap<String, String>();
	static{
		Map<String, String> s=new HashMap<String, String>();
		s.put("01", "Business");
		s.put("02", "Service");
		s.put("03", "Professional");
		s.put("04", "Agriculturist");
		s.put("05", "Retired");
		s.put("06", "Housewife");
		s.put("07", "Student");
		s.put("08", "Others");
		s.put("09", "Doctor");
		s.put("41", "Private Sector Service");
		s.put("42", "Public Sector Service");
		s.put("43", "Forex Dealer");
		s.put("44", "Government Service");
		s.put("99", "Unknown / Not Applicable");
		OCCUPATION = Collections.unmodifiableMap(s);
	}
}
