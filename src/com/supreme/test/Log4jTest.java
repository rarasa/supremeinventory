package com.supreme.test;

import org.apache.log4j.Logger;

public class Log4jTest {

	
	public static void main(String[] args) {
		try {
			Logger logger=Logger.getLogger(Log4jTest.class.getName());
			logger.debug("Testing log4j debug");
			logger.error("Testing log4j error");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
