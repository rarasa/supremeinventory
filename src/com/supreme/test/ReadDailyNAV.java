package com.supreme.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.supreme.Config;
import com.supreme.dao.Inventory;
import com.supreme.util.HibernateBridge;

public class ReadDailyNAV {/*
	
	public static String fileName = "";

	public static double ParseDouble(String strNumber) {
	   if (strNumber != null && strNumber.length() > 0) {
	       try {
	          return Double.parseDouble(strNumber);
	       } catch(Exception e) {
	          return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
	       }
	   }
	   else return 0;
	}
	
	public ReadDailyNAV(String fileNameString){
		fileName = fileNameString;
		readNAVFile();
	}
	
	public static void main(String[] args) 
    { 
		//reason of using this list because excel download from morning start has multiple amfiiCodes in it
		List<String> listOfAmfiiCodes=new ArrayList<String>();
		Session hSession = HibernateBridge.getSessionFactory().openSession();
        try { 
        	
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.EXCEL_DEV_PATH);
    			}else{
    				path=config.get(Config.EXCEL_PROD_PATH);				
    			}
    		}catch(IOException e){
    			path = "/opt/excels";
    		}
    		
    		System.out.println("Path in mf");
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
    			File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext()) { 
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Inventory inventory = new Inventory();

	                Date date;
	                
	                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    *//**
		                     * Reading Slab Number
		                     *//*
		                    if(j == 0){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK) && cell.getStringCellValue().matches("[0-9]+")){
		                    		inventory.setSlabNo(Long.parseLong(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 1){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK) && cell.getStringCellValue().matches("[0-9]+")){
		                    		inventory.setLength(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    *//**
		                     * Reading Packing List Length
		                     *//*
		                    if(j == 2){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setHeight(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    *//**
		                     * Reading Packing List Height
		                     *//*
		                    if(j == 3){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualLength(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 4){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualHeight(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 5){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setSquareMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 6){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 7){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    inventory.setCreatedOn(new Date());
		                    inventory.setUpdatedOn(new Date());
		                    
		                    j++;
		                } 
		                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
	                		
		                	try{
		                		System.out.println("I = "+ i+" inventory= "+inventory.getSlabNo());
		                		hSession.saveOrUpdate(inventory);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                	}
	                	}		            	
	                
		                System.out.println("");
	                }
	                i++;
	            } 
	            filetoRead.close(); 
			}
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        }
        finally{
        	hSession.close();
        }
    }
	
	
	public static void readNAVFile()
	{//reason of using this list because excel download from morning start has multiple amfiiCodes in it
		List<String> listOfAmfiiCodes=new ArrayList<String>();
		Session hSession = HibernateBridge.getSessionFactory().openSession();
        try { 
        	
        	// TODO Auto-generated method stub
    		String path="";
    		
    		try{
    			Config config=new Config();
    			if(config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV)){
    				path=config.get(Config.EXCEL_DEV_PATH);
    			}else{
    				path=config.get(Config.EXCEL_PROD_PATH);				
    			}
    		}catch(IOException e){
    			path = "/opt/excels";
    		}
    		
    		System.out.println("Path in mf");
    		File sourceFile = new File(path);
    		
    		if(sourceFile.exists())
			{
        	
    			File file=new File(path+File.separator+fileName);
    			File file=new File("C:\\opt\\aof\\shlc\\New_Daily_NAV_120190924.xlsx");
	            FileInputStream filetoRead = new FileInputStream(file); 
	  
	            // Create Workbook instance holding reference to .xlsx file 
	            XSSFWorkbook workbook = new XSSFWorkbook(filetoRead); 
	  
	            // Get first/desired sheet from the workbook 
	            XSSFSheet sheet = workbook.getSheetAt(0); 
	  
	            // Iterate through each rows one by one 
	            Iterator<Row> rowIterator = sheet.iterator(); 
	            
	            boolean flag = false;
	            int i=0;
	            while (rowIterator.hasNext()) { 
	            	flag = false;
	                Row row = rowIterator.next(); 
	                // For each row, iterate through all the columns 
	                Iterator<Cell> cellIterator = row.cellIterator(); 
	                
	                Inventory inventory = new Inventory();

	                Date date;
	                
	                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	                if(i != 0){
		                int j=0;
		                System.out.println("i = "+i);
		                while (cellIterator.hasNext()) {
		                	
		                	//System.out.println("J === "+ j);
		                    Cell cell = cellIterator.next(); 
		                    // Check the cell type and format accordingly 
		                    
		                    
		                    *//**
		                     * Reading Slab Number
		                     *//*
		                    if(j == 0){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK) && cell.getStringCellValue().matches("[0-9]+")){
		                    		inventory.setSlabNo(Long.parseLong(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 1){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK) && cell.getStringCellValue().matches("[0-9]+")){
		                    		inventory.setLength(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    *//**
		                     * Reading Packing List Length
		                     *//*
		                    if(j == 2){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setHeight(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    *//**
		                     * Reading Packing List Height
		                     *//*
		                    if(j == 3){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualLength(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 4){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setActualHeight(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 5){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setSquareMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 6){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    if(j == 7){
		                    	if(cell != null && !(cell.getCellType() == Cell.CELL_TYPE_BLANK)){
		                    		inventory.setRunningMetre(Double.parseDouble(cell.getStringCellValue()+""));
		                    	}
		                    	else{
		                    		flag = true;
		                    		break;
		                    	}
	                        }
		                    
		                    inventory.setCreatedOn(new Date());
		                    inventory.setUpdatedOn(new Date());
		                    
		                    j++;
		                } 
		                
		                if(!flag){
		                	Transaction transaction = hSession.beginTransaction();
	                		
		                	try{
		                		System.out.println("I = "+ i+" inventory= "+inventory.getSlabNo());
		                		hSession.saveOrUpdate(inventory);
		                		
		                		hSession.flush();
		                		
		                		transaction.commit();
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                	}
	                	}		            	
	                
		                System.out.println("");
	                }
	                i++;
	            } 
	            filetoRead.close(); 
			}
        } 
        catch (Exception e) { 
            e.printStackTrace(); 
        }
        finally{
        	hSession.close();
        }
    }
*/}



/*
 * SELECT nav.amfiicode, mutualfund.name, mutualfund.id FROM nav INNER JOIN mutualfund ON nav.amfiicode = mutualfund.amfiicode WHERE nav.navDate = "2019-05-20" group by nav.amfiicode;*/
