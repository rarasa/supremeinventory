package com.supreme.test;

// Java code to generate QR code

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.DataMatrixWriter;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;

public class MyQR {public static void main(String[] args) throws WriterException, IOException {
	String qrCodeText = "MN0000001";
	int size = 1000;
	String fileType = "png";
	String filePath = "C:\\opt\\QR\\zxingdemo.png";
	File qrFile = new File(filePath);
	createQRImage(qrFile, qrCodeText, size, fileType);
	System.out.println("DONE");
}

public static void createDataMatrixImage(String qrCodeText, int size, String fileType, File qrFile) throws IOException, BadElementException {
	// Create the ByteMatrix for the Data Matrix that encodes the given String
	Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<>();
	hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	DataMatrixWriter dmWriter = new DataMatrixWriter();
	BitMatrix byteMatrix = dmWriter.encode(qrCodeText, BarcodeFormat.DATA_MATRIX, size, size, hintMap);
	// Make the BufferedImage that are to hold the QRCode
	int matrixWidth = byteMatrix.getWidth();
	BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
	image.createGraphics();

	Graphics2D graphics = (Graphics2D) image.getGraphics();
	graphics.setColor(Color.WHITE);
	graphics.fillRect(0, 0, matrixWidth, matrixWidth);
	// Paint and save the image using the ByteMatrix
	graphics.setColor(Color.BLACK);

	for (int i = 0; i < matrixWidth; i++) {
		for (int j = 0; j < matrixWidth; j++) {
			if (byteMatrix.get(i, j)) {
				graphics.fillRect(i, j, 1, 1);
			}
		}
	}
	
	
	
	ImageIO.write(image, fileType, qrFile);
	
	
	
	/*BufferedImage  outDataMatrix = ImageIO.read(qrFile);
	Image DataMatrixFileImage = Image.getInstance(outDataMatrix,null);
	//return  chartimg1;
	return DataMatrixFileImage;*/
	
	
	
}

public static Image getDataMatrixImage(File qrFile) throws IOException, BadElementException {
	// Create the ByteMatrix for the Data Matrix that encodes the given String
	
	BufferedImage  outDataMatrix = ImageIO.read(qrFile);
	Image DataMatrixFileImage = Image.getInstance(outDataMatrix,null);
	//return  chartimg1;
	return DataMatrixFileImage;
	
	
	
}

public static void createQRImage(File qrFile, String qrCodeText, int size, String fileType)
		throws WriterException, IOException {
	// Create the ByteMatrix for the Data Matrix that encodes the given String
	Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<>();
	hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	DataMatrixWriter dmWriter = new DataMatrixWriter();
	BitMatrix byteMatrix = dmWriter.encode(qrCodeText, BarcodeFormat.DATA_MATRIX, size, size, hintMap);
	// Make the BufferedImage that are to hold the QRCode
	int matrixWidth = byteMatrix.getWidth();
	BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_RGB);
	image.createGraphics();

	Graphics2D graphics = (Graphics2D) image.getGraphics();
	graphics.setColor(Color.WHITE);
	graphics.fillRect(0, 0, matrixWidth, matrixWidth);
	// Paint and save the image using the ByteMatrix
	graphics.setColor(Color.BLACK);

	for (int i = 0; i < matrixWidth; i++) {
		for (int j = 0; j < matrixWidth; j++) {
			if (byteMatrix.get(i, j)) {
				graphics.fillRect(i, j, 1, 1);
			}
		}
	}
	
	
	
	ImageIO.write(image, fileType, qrFile);
	
	}

}
