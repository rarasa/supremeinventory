package com.supreme.test;

import java.io.File;
import java.util.Calendar;

import javax.print.PrintService;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import com.itextpdf.text.pdf.PdfImage;
import com.supreme.util.PDFWrite;

public class PDFTest {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String URL="BlankDO.pdf";
		PDFWrite pdf=new PDFWrite();
		try{
			File file=new File(PDFTest.class.getResource(URL).getPath());
			
			pdf.open(file);
			pdf.setFontSize(10);

			System.out.println("file: "+ file);
			
			pdf.addString("Order Number", 50, 670);
			
			Calendar c=Calendar.getInstance();
			int date=c.get(Calendar.DAY_OF_MONTH),month=c.get(Calendar.MONTH),year=c.get(Calendar.YEAR);
			
			/*if(date<10){
				pdf.addString("0", 490, 670);
			}else{
				pdf.addString(""+((int)date/10), 490, 670);				
			}
			pdf.addString(""+((int)date%10), 495, 670);
			pdf.addString("/", 505, 670);
			if(month<10){
				pdf.addString(""+((int)month/10), 512, 670);				
			}
			pdf.addString(""+((int)month%10), 525, 670);*/

			if(date<10){
				pdf.addString("0", 490, 670);
			}else{
				pdf.addString(""+((int)date/10), 490, 670);				
			}
			pdf.addString(""+((int)date%10), 495, 670);

			pdf.addString("/", 501, 670);
			
			if(month<10){
				pdf.addString("0", 505, 670);
			}else{
				pdf.addString(""+((int)month/10), 505, 670);				
			}
			
			pdf.addString(""+((int)(month%10) + 1), 510, 670);
			
			pdf.addString("/", 516, 670);
			
			pdf.addString(""+((int)year/1000), 520, 670);
			pdf.addString(""+((int)(year/100)%10), 525, 670);
			pdf.addString(""+((int)(year/10)%100), 530, 670);
			pdf.addString(""+((int)year%10), 535, 670);
			
			
			/**
			 * i:
			 * 30 for Serial No
			 * 60 for description
			 * 300 for length
			 * 370 for height
			 * 430 for Nos
			 * 510 for Qty
			 */
			
			/**
			 * j:
			 * 500 to 100 with 20 decrement for detailed lines
			 */
			/*i - 60 to 500 with 20 increment*/
			/*j - 100 to 500 with 20 increment*/
			
			int i = 0, j = 0;
			
			/*for(i=0; i < 1000; i++){
				for(j = 0; j < 700; j++){
					
					if(i%20 == 0 && i < 575 && j%20 == 0){
						
						pdf.addString("i: "+i, i, j);
					}
				}
			}*/
			
			pdf.addString("MS Line 1", 70, 623);
			pdf.addString("MS Line 2", 35, 600);
			pdf.addString("MS Line 3", 35, 575);
			
			pdf.addString("MS Line 1", 250, 623);
			
			
			pdf.save("C:\\opt\\pdfDocs\\BlankDO"+System.currentTimeMillis()+".pdf");
			System.out.println(file.getAbsolutePath());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
