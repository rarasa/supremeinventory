package com.supreme.test;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import com.supreme.util.HibernateBridge;

public class HibernateBridgeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session hSession=null;
		try
		{
			hSession=HibernateBridge.getSessionFactory().openSession();
			System.out.println("Connection Opened");
			List<Object[]> list= hSession.createQuery("from Inventory ").list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			hSession.close();
		}
	}

}
