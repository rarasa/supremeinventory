package com.supreme.listener;

import java.io.IOException;
import java.util.Calendar;
import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.supreme.Config;

public class ConnectionListener implements ServletContextListener
{

	@Override
	public void contextInitialized(ServletContextEvent arg0)
	{
		// At what time message should be send to User for their SIP Alert
		Calendar cal=Calendar.getInstance();
		cal.set(Calendar.HOUR, 17);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND,0);
		
		long period=1000*60*60*24;
		
		Calendar passwordCal=Calendar.getInstance();
		passwordCal.set(Calendar.DAY_OF_WEEK, 6);
		passwordCal.set(Calendar.HOUR_OF_DAY, 18);
		passwordCal.set(Calendar.MINUTE, 1);
		
		long passwordPeriod= 1000 * 60 * 60 * 24 * 7;
		
		try{  
			
			Config config=new Config();
			if(!config.get(Config.ENVIRONMENT).equals(Config.ENV_DEV) && !config.get(Config.SYSTEM_ENVIRONMENT).equals(Config.SYS_ENV_DEV))
			{}
			else{
				
				/*PendingOrderToOrder poto=new PendingOrderToOrder();
				Thread th=new Thread(poto);
				th.start();*/
				//new Thread(new UpdateBseInConfig()).start();
				System.out.println("Will not execute password change or timer task because system envt is development.");
				System.out.println("Config.ENVIRONMENT: "+Config.ENV_DEV);
				System.out.println("Config.SYSTEM_ENVIRONMENT: "+Config.SYS_ENV_DEV);
			}
			
		}
		catch(IOException e)		
		{
			System.out.println("IOException from Listener");
			e.printStackTrace();
		}
		catch(IllegalStateException ise)
		{
			System.out.println("IllegalStateException from Listener");
			ise.printStackTrace();
		}
		catch(Exception e)
		{
			System.out.println("Exception from Listener");
			e.printStackTrace();
			
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0)
	{
		// TODO Auto-generated method stub
		try
		{
			// eeat.interrupt();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}